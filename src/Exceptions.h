#ifndef RCUTILS_EXCEPTIONS_H
#define RCUTILS_EXCEPTIONS_H

#include "ers/ers.h"

namespace daq {
ERS_DECLARE_ISSUE( rc, ISReadFailure, reason, ((const char*)reason))

ERS_DECLARE_ISSUE( rc, BadControllerState, "Controller is in ERROR state", ((const char*)reason))

ERS_DECLARE_ISSUE( rc, Timeout, reason, ((const char*)reason))

ERS_DECLARE_ISSUE(rc,
                  EnvironmentMissing,
                  "Variable \"" << variable << "\" is not set",
                  ((const char*) variable))

}
#endif //RCUTILS_EXCEPTIONS_H
