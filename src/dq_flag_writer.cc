// Author: G. Lehmann Miotto, June 2009
// This utility writes the Data Quality flag from a RC point of view:
// DQ is considered good if Recording = enabled, Run Type = physics, project tag != data_test, run duration > 10 sec

#include <sstream>
#include <stdlib.h> // for getenv
#include <signal.h>
#include <cctype>    // toupper

#include <boost/program_options.hpp>
#include <CoralBase/Attribute.h>
#include <CoolKernel/Exception.h>
#include <CoolKernel/ValidityKey.h>
#include <CoolKernel/IDatabaseSvc.h>
#include <CoolKernel/IDatabase.h>
#include <CoolKernel/IFolder.h>
#include <CoolKernel/IRecordSpecification.h>
#include <CoolKernel/Record.h>
#include <CoolApplication/DatabaseSvcFactory.h>

#include "ers/ers.h"
#include "owl/time.h"

#include "ipc/core.h"
#include "ipc/partition.h"

#include "pmg/pmg_initSync.h"

#include "rc/RunParamsNamed.h"
#include "rc/RunParams.h"
#include "rc/RCDataQuality.h"
#include "rc/RCDataQualityNamed.h"
#include "is/info.h"
#include "is/infodictionary.h"
#include "is/infodocument.h"
#include "is/inforeceiver.h"
#include "RunControl/Common/Exceptions.h"
#include "owl/semaphore.h"

#include "Exceptions.h"

OWLSemaphore sem;

#define RUNCLT_CHANNEL 460
#define RCOPS_CHANNEL 461

static     cool::IDatabasePtr db;

std::string Folder_Name="/GLOBAL/DETSTATUS/SHIFTONL";

bool s_OngoingCallback = false;

namespace daq {
ERS_DECLARE_ISSUE(	rc,
			BadCoolDB,
			"Archiving to COOL failed because " << explanation,
                        ((const char*) explanation)
		)

ERS_DECLARE_ISSUE(	rc,
			NotExistingFolder,
			"The indicated folder: " << name << " does not exist",
                        ((const char*) name)
		)
}

static ISInfoReceiver * infoRec = 0;
extern "C" void signalhandler(int)
{
   signal(SIGINT,  SIG_DFL);
   signal(SIGTERM, SIG_DFL);
   sem.post();
   ERS_INFO("Terminating rc_dq_flag_writer...");
}


std::string uppercase(const std::string & word) {
        std::string uppercase;
        uppercase.reserve(word.length());
        for (std::string::size_type i = 0; i < word.length(); ++i) {
                uppercase += static_cast<char> (std::toupper(word[i]));
        }
        return uppercase;
}

void DQcb(ISCallbackInfo *isc) {
  s_OngoingCallback = true;

  // Read info from IS
  RCDataQuality eor;
  if (isc->reason() != is::Deleted)
    isc->value(eor);
  else {
    s_OngoingCallback = false;
    return;
  }

  int channel;
  if(eor.Owner == RCDataQuality::shifter) {
    channel = RCOPS_CHANNEL;
  }
  else {
    channel = RUNCLT_CHANNEL;
  }
  ERS_DEBUG(0,"Callback received for channel " << channel << " with state " << eor.Flag << ".");
  try {
    db->openDatabase();
    cool::IFolderPtr folder = db->getFolder( Folder_Name );
    const cool::IRecordSpecification & spec = folder->payloadSpecification();
   
    coral::AttributeList payload( cool::Record( spec ).attributeList() );
    payload["Code"].data<cool::Int32>() = eor.Flag;
    payload["deadFrac"].data<cool::Float>() = 0;
    payload["Thrust"].data<cool::Float>() = 0;
    cool::UInt63 a = eor.RunNumber;
    cool::ValidityKey since = a <<32;
    cool::ValidityKey until = (a <<32) | 0xffffffff;
    ERS_DEBUG(3, "the COOL channel is " << channel << " the IOV is [" << since << "  " << until << "[.");
    cool::Record rec(spec, payload);
    folder -> storeObject( since, until, rec, channel );
  }
  catch (std::exception &e){
      daq::rc::BadCoolDB i(ERS_HERE, e.what());
      ers::error(i);
  }
  s_OngoingCallback = false;
  db->closeDatabase();
}

// throws daq::rc::BadCoolDb
void openDB(const cool::DatabaseId& db_Id, const cool::IDatabaseSvc& db_Svc) {

  try {
    db = db_Svc.openDatabase(db_Id, false); // false = !read_only
    ERS_DEBUG(3, "DB opened.");
  }
  catch (std::exception &ex) {
    ERS_LOG(ex.what());

    try {
      db = db_Svc.createDatabase(db_Id);
      ERS_DEBUG(3, "DB created.");
    }
    catch (std::exception &e) {
      ERS_LOG(e.what());
      daq::rc::BadCoolDB i(ERS_HERE, e.what());
      throw i;
    }
  }
}

//--------------------- MAIN ----------------------------//
int main(int argc, char ** argv)
{

  try{
    IPCCore::init(argc,argv);
  }catch(daq::ipc::CannotInitialize& e){
    ers::fatal(e);
    abort();
  }catch(daq::ipc::AlreadyInitialized& e){
    ers::warning(e);
  }
  // parse commandline parameters
  boost::program_options::options_description desc("This program retrieves some run parameters from IS and inserts them into COOL.");
  desc.add_options()
    ("help,h", "help message")
    ("coolDb,d",   boost::program_options::value<std::string>(), "Name of COOL database")
    ;

  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::parse_command_line(argc,argv,desc), vm);
  boost::program_options::notify(vm);

  std::string coolDb;

  if(vm.count("help")) {
    std::cout << desc << std::endl;
    return EXIT_SUCCESS;
  }
  if ( vm.count("coolDb")) {
    coolDb = vm["coolDb"].as<std::string>();
  }
  else {
    std::cout << desc << std::endl;
    daq::rc::CmdLineError issue(ERS_HERE,"the name of the COOL database cannot be empty");
    ers::fatal(issue);
    return EXIT_FAILURE;
  }

  // Retrieve TDAQ_PARTITION from environment
  std::string partitionName("");
  const char * pn = getenv("TDAQ_PARTITION");
  if (pn)
    partitionName = pn;
  else {
    daq::rc::EnvironmentMissing e(ERS_HERE,"TDAQ_PARTITION");
    ers::fatal(e);
    return EXIT_FAILURE;
  }

  cool::DatabaseId dbId = coolDb;

  // Open DB and check folder existence
  cool::IDatabaseSvc& dbSvc = cool::DatabaseSvcFactory::databaseService();
  ERS_DEBUG(3, "Retrieved DB service.");

  try {
    openDB(dbId, dbSvc);
    if (!db->existsFolder(Folder_Name)) {
      ers::fatal(daq::rc::NotExistingFolder(ERS_HERE,Folder_Name.c_str() ));
      db->closeDatabase();
      return EXIT_FAILURE;
    }
    db->closeDatabase();
  }
  catch(daq::rc::BadCoolDB& ex) {
    ers::fatal(ex);
    return EXIT_FAILURE;
  }


  // Subscribe to RunParams EOR
  IPCPartition ipcP(partitionName);

  infoRec = new ISInfoReceiver(ipcP);
  try {
    infoRec->subscribe("RunParams.TDAQ_DQ_Flags", DQcb);
  }
  catch ( daq::is::Exception & ex ) {
      ers::fatal(ex);
      return EXIT_FAILURE;
    }
  // Install signal handler
  signal(SIGINT,  signalhandler);
  signal(SIGTERM, signalhandler);

  // Tell PMG that now we are ready!
  pmg_initSync();

  sem.wait();

  while(s_OngoingCallback) {
        usleep(100000);
  }

  try {
    infoRec->unsubscribe("RunParams.TDAQ_DQ_Flags");
    delete infoRec;
  }
  catch(ers::Issue & e) {
  }
  return EXIT_SUCCESS;

}
