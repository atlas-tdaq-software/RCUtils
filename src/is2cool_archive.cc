// $Id$
// Author: Giovanna.Lehmann@cern.ch , August 2007

#include <sstream>
#include <stdlib.h> // for getenv
#include <signal.h>
#include <atomic>

#include <boost/program_options.hpp>

#include <CoralBase/Attribute.h>
#include <CoolKernel/Exception.h>
#include <CoolKernel/ValidityKey.h>
#include <CoolKernel/IDatabaseSvc.h>
#include <CoolKernel/IDatabase.h>
#include <CoolKernel/IFolder.h>
#include <CoolKernel/IRecordSpecification.h>
#include <CoolKernel/Record.h>
#include <CoolApplication/DatabaseSvcFactory.h>

#include "ers/ers.h"
#include "owl/time.h"

#include "ipc/core.h"
#include "ipc/partition.h"

#include "pmg/pmg_initSync.h"

#include "dal/Partition.h"
#include "dal/util.h"
#include "dal/IS_InformationSources.h"
#include "dal/IS_EventsAndRates.h"
#include "is/info.h"
#include "is/infoT.h"
#include "is/infodynany.h"
#include "is/infodictionary.h"
#include "is/infodocument.h"
#include "is/inforeceiver.h"
#include "is/type.h"
#include "rc/RunParams.h"
#include "TTCInfo/LumiBlockNamed.h"
#include "rc/StopWhileBeamInfo.h"
#include "rc/HoldTriggerInfo.h"
#include "rc/Ready4PhysicsInfo.h"
#include "RunControl/Common/Exceptions.h"
#include "owl/semaphore.h"
#include "Exceptions.h"


//***********************************************************************/
// Custom ERS exceptions

namespace daq {
ERS_DECLARE_ISSUE(	rc,
			BadCoolDB,
			"Archiving to COOL failed because " << explanation,
                        ((const char*) explanation)
		)

}
//***********************************************************************/
OWLSemaphore sem;
std::recursive_mutex x;

// A few Constants

// COOL Folder Names
const std::string SOR_Name_Old = "/TDAQ/RunCtrl/SOR_Params";
const std::string EOR_Name_Old = "/TDAQ/RunCtrl/EOR_Params";
const std::string SOR_Name = "/TDAQ/RunCtrl/SOR";
const std::string EOR_Name = "/TDAQ/RunCtrl/EOR";

const std::string EC_Name  = "/TDAQ/RunCtrl/EventCounters";
const std::string STOPR_Name = "/TDAQ/RunCtrl/StopReason";
const std::string HOLDT_Name = "/TDAQ/RunCtrl/HoldTriggerReason";
const std::string READY4PH_Name = "/TDAQ/RunCtrl/DataTakingMode";
// IS Information Names

const std::string IS_SOR_Name = "RunParams.SOR_RunParams";
const std::string IS_EOR_Name = "RunParams.EOR_RunParams";
const std::string IS_CV_Name  = "RunParams.ConfigVersions";
const std::string IS_STOPR_Name = "RunParams.StopWhileBeam";
const std::string IS_HOLDT_Name = "RunParams.HoldTrigger";
const std::string IS_READY4PH_Name = "RunParams.Ready4Physics";

//***********************************************************************************/
// Helper class to deal with DB connections

namespace daq {
  namespace rc {
    class CoolDB {
    public:
      CoolDB(std::string dbName);
      ~CoolDB();
      void close();
      cool::IDatabasePtr getValidDb();

      cool::RecordSpecification SOR_Spec_Old;
      cool::RecordSpecification EOR_Spec_Old;
      cool::RecordSpecification SOR_Spec;
      cool::RecordSpecification EOR_Spec;
      cool::RecordSpecification EC_Spec;
      cool::RecordSpecification STOPR_Spec;
      cool::RecordSpecification HOLDT_Spec;
      cool::RecordSpecification READY4PH_Spec;

    private:
      cool::DatabaseId m_mainDbId;
      cool::IDatabasePtr m_db;
    };
  }
}

daq::rc::CoolDB::CoolDB(std::string dbName): m_mainDbId(dbName),m_db(0) {

  // Create Folder Specifications
  SOR_Spec_Old.extend("RunNumber",cool::StorageType::UInt32);
  SOR_Spec_Old.extend("SORTime",cool::StorageType::UInt63);
  SOR_Spec_Old.extend("RunType",cool::StorageType::String255);
  SOR_Spec_Old.extend("DAQConfiguration", cool::StorageType::String255);
  SOR_Spec_Old.extend("DetectorMask", cool::StorageType::UInt63);
  SOR_Spec_Old.extend("FilenameTag", cool::StorageType::String255);
  SOR_Spec_Old.extend("RecordingEnabled",cool::StorageType::Bool);
  SOR_Spec_Old.extend("DataSource", cool::StorageType::String255);

  EOR_Spec_Old.extend("RunNumber",cool::StorageType::UInt32);
  EOR_Spec_Old.extend("SORTime",cool::StorageType::UInt63);
  EOR_Spec_Old.extend("RunType",cool::StorageType::String255);
  EOR_Spec_Old.extend("DAQConfiguration", cool::StorageType::String255);
  EOR_Spec_Old.extend("DetectorMask", cool::StorageType::UInt63);
  EOR_Spec_Old.extend("FilenameTag", cool::StorageType::String255);
  EOR_Spec_Old.extend("RecordingEnabled",cool::StorageType::Bool);
  EOR_Spec_Old.extend("DataSource", cool::StorageType::String255);
  EOR_Spec_Old.extend("EORTime",cool::StorageType::UInt63);
  EOR_Spec_Old.extend("TotalTime",cool::StorageType::UInt32);
  EOR_Spec_Old.extend("CleanStop",cool::StorageType::Bool);

  SOR_Spec.extend("RunNumber",cool::StorageType::UInt32);
  SOR_Spec.extend("SORTime",cool::StorageType::UInt63);
  SOR_Spec.extend("RunType",cool::StorageType::String255);
  //SOR_Spec.extend("DAQConfiguration", cool::StorageType::String255);
  SOR_Spec.extend("DetectorMask", cool::StorageType::String255);
  SOR_Spec.extend("T0ProjectTag", cool::StorageType::String255);
  SOR_Spec.extend("RecordingEnabled",cool::StorageType::Bool);
  //SOR_Spec.extend("DataSource", cool::StorageType::String255);

  EOR_Spec.extend("RunNumber",cool::StorageType::UInt32);
  EOR_Spec.extend("SORTime",cool::StorageType::UInt63);
  EOR_Spec.extend("RunType",cool::StorageType::String255);
  EOR_Spec.extend("DAQConfiguration", cool::StorageType::String255);
  EOR_Spec.extend("DetectorMask", cool::StorageType::String255);
  EOR_Spec.extend("T0ProjectTag", cool::StorageType::String255);
  EOR_Spec.extend("RecordingEnabled",cool::StorageType::Bool);
  //R_Spec.extend("DataSource", cool::StorageType::String255);
  EOR_Spec.extend("EORTime",cool::StorageType::UInt63);
  EOR_Spec.extend("TotalTime",cool::StorageType::UInt32);
  EOR_Spec.extend("CleanStop",cool::StorageType::Bool);

  EC_Spec.extend("PartitionName",cool::StorageType::String255);
  EC_Spec.extend("L1Events",cool::StorageType::UInt63);
  EC_Spec.extend("L2Events",cool::StorageType::UInt63);
  EC_Spec.extend("EFEvents",cool::StorageType::UInt63);
  EC_Spec.extend("RecordedEvents",cool::StorageType::UInt63);

  STOPR_Spec.extend("SubSystems", cool::StorageType::String255);
  STOPR_Spec.extend("Reason", cool::StorageType::String255);

  HOLDT_Spec.extend("SubSystems", cool::StorageType::String255);
  HOLDT_Spec.extend("Reason", cool::StorageType::String255);

  READY4PH_Spec.extend("ReadyForPhysics", cool::StorageType::UInt32);

  // Open DB
  try {
    m_db = cool::DatabaseSvcFactory::databaseService().openDatabase(m_mainDbId, false); // false = !read_only    
  }
  catch (cool::DatabaseDoesNotExist &e) {
    try {
      m_db = cool::DatabaseSvcFactory::databaseService().createDatabase(m_mainDbId);
    }
    catch(std::exception &ex) {
      daq::rc::BadCoolDB i(ERS_HERE, ex.what());
      ers::error(i);
    }
  }
  catch (std::exception &ex) {
      daq::rc::BadCoolDB i(ERS_HERE, ex.what());
      ers::error(i);    
  }
  

}

daq::rc::CoolDB::~CoolDB() {
  this->close();
}

cool::IDatabasePtr daq::rc::CoolDB::getValidDb() {
    m_db = cool::DatabaseSvcFactory::databaseService().openDatabase(m_mainDbId, false); 
    return m_db;
}

void daq::rc::CoolDB::close() {
  //return;
  if (m_db) m_db->closeDatabase();
}


//**************************************************************
// More statics used across main and IS callbacks... FIXME

daq::rc::CoolDB *s_dbHandler = NULL;
std::string s_partitionName = ""; // name of the IPC partition
ISInfoReceiver * infoRec = 0;

// Event counters stuff
std::string s_LVL1InfoSource;
std::string s_HLTInfoSource;
std::string s_RecordedEventsInfoSource;

std::string s_LVL1Attr;
std::string s_HLTAttr;
std::string s_RecordedEventsAttr;

int s_LVL1Pos=-1;
int s_HLTPos=-1;
int s_RecordedEventsPos=-1;

std::atomic<bool> s_vmeBusy;
std::atomic<bool> s_ready4Physics;
std::atomic<bool> s_trigOnHold;
bool s_oldFolders;
bool s_newFolders;

//***********************************
// Signal Handler
//***********************************
extern "C" void signalhandler(int)
{
   signal(SIGINT,  SIG_DFL);
   signal(SIGTERM, SIG_DFL);
   sem.post();
   ERS_LOG("Terminating rc_is2cool_archive...");
}

//************************************************************************
// Helper functions to extract the IS information of event counters
//************************************************************************
uint64_t extractNumber(std::string & info, std::string & name, int& number) {
  uint64_t retCode=0;

  if (info.empty() || (name.empty() && (number == -1)))
    return retCode;

  ISInfoDynAny isInfo;
  IPCPartition p(s_partitionName);
  ISInfoDictionary dict(p);
  try {
    dict.getValue(info, isInfo);
    if (number != -1) {
      ERS_DEBUG(3, "Extract attribute with index " << number);
      ISType::Basic t = isInfo.getAttributeType((size_t)number);
      if (t == ISType::S32) {
	retCode = isInfo.getAttributeValue<int32_t>((size_t)number);
	ERS_DEBUG(3, "Value of attribute at position " << number << " in IS info " << info << " is " << retCode);
      }
      else if (t == ISType::U32) {
	retCode = isInfo.getAttributeValue<uint32_t>((size_t)number);
      ERS_DEBUG(3, "Value of attribute at position " << number << " in IS info " << info << " is " << retCode);
      }
      else if (t == ISType::S64) {
        retCode = isInfo.getAttributeValue<int64_t>((size_t)number);
        ERS_DEBUG(3, "Value of attribute at position " << number << " in IS info " << info << " is " << retCode);
      }
      else if (t == ISType::U64) {
        retCode = isInfo.getAttributeValue<uint64_t>((size_t)number);
      ERS_DEBUG(3, "Value of attribute at position " << number << " in IS info " << info << " is " << retCode);
      }
      else {
	std::ostringstream text;
	text << info << ":" << number << " isn't a signed or unsigned 32 or 64 bit integer.";
	ERS_INFO(text.str().c_str());
      }
    }
    else {
      ERS_DEBUG(3, "Extract attribute with name " << name);
      ISInfoDocument::Attribute a = isInfo.getAttributeDescription(name);
      if (a.typeCode() == ISType::S32) {
	retCode = isInfo.getAttributeValue<int32_t>(name);
	ERS_DEBUG(3, "Value of attribute with name " << name << " in IS info " << info << " is " << retCode);
      }
      else if (a.typeCode() == ISType::U32) {
	retCode = isInfo.getAttributeValue<uint32_t>(name);
	ERS_DEBUG(3, "Value of attribute with name " << name << " in IS info " << info << " is " << retCode);
      }
      else if (a.typeCode() == ISType::S64) {
        retCode = isInfo.getAttributeValue<int64_t>(name);
        ERS_DEBUG(3, "Value of attribute with name " << name << " in IS info " << info << " is " << retCode);
      }
      else if (a.typeCode() == ISType::U64) {
        retCode = isInfo.getAttributeValue<uint64_t>(name);
        ERS_DEBUG(3, "Value of attribute with name " << name << " in IS info " << info << " is " << retCode);
      }
      else {
	std::ostringstream text;
	text << info << "." << name << "isn't a signed or unsigned integer.";
	ERS_INFO(text.str().c_str());
      }
    }
  }
  catch (ers::Issue &e) {
    ers::error(e);
  }
  return retCode;
}

void getEventCounters (uint64_t& l1c, uint64_t & hlt, uint64_t & rc) {

  l1c = extractNumber(s_LVL1InfoSource, s_LVL1Attr, s_LVL1Pos);
  hlt = extractNumber(s_HLTInfoSource, s_HLTAttr, s_HLTPos);
  rc = extractNumber(s_RecordedEventsInfoSource, s_RecordedEventsAttr, s_RecordedEventsPos);

  ERS_DEBUG(3,"Got L1c " << l1c << " HLT " << hlt << " Recc " << rc);
}

std::string getConfigVersion() {
  ISInfoDynAny isInfo;
  IPCPartition p(s_partitionName);
  ISInfoDictionary dict(p);
  try {
    dict.getValue(IS_CV_Name, isInfo);

    int32_t schema, data;
    std::ostringstream t;
    schema = isInfo.getAttributeValue<int32_t>(0);
    data = isInfo.getAttributeValue<int32_t>(1);
    t << "Schema=" << schema << ":Data=" << data ;
    return t.str().c_str();
  }
  catch(ers::Issue &e) {
    return "";
  }
}

//******************************************************************
//IS callback functions
//******************************************************************


void SORcb(ISCallbackInfo *isc) {
  ERS_LOG("SOR callback received.");

  // Read info from IS
  RunParams sor;
  if (isc->reason() != is::Deleted)
    isc->value(sor);
  else {
    ERS_LOG("SOR info deleted");
    return;
  }
  ERS_DEBUG(0,"SOR callback received for run " << sor.run_number << ".");
  // Open DB and retrieve folder
  std::lock_guard<std::recursive_mutex> lk(x);

  cool::IDatabasePtr db;
  try {
    db = s_dbHandler->getValidDb();
    ERS_DEBUG(3, "DB opened.");
  }
  catch (daq::rc::BadCoolDB &e){
    ers::error(e);
    return;
  }

  // Index by run number and lumi block

  cool::UInt63 a = sor.run_number;
  cool::UInt63 z = sor.run_number+1;
  cool::ValidityKey since = a <<32;
  cool::ValidityKey until = z <<32;

  // Fill in the data
  
  
  coral::AttributeList payload_old(cool::Record(s_dbHandler->SOR_Spec_Old).attributeList());
  payload_old["RunNumber"].data<cool::UInt32>() = sor.run_number;
  payload_old["SORTime"].data<cool::UInt63>() = sor.timeSOR.total_mksec_utc() * 1000 ; // convert from microseconds into nanoseconds
  payload_old["RunType"].data<cool::String255>() = sor.run_type;
  payload_old["DAQConfiguration"].data<cool::String255>() = ""; // dummy
  payload_old["DetectorMask"].data<cool::UInt63>() = 0;
  payload_old["FilenameTag"].data<cool::String255>() = sor.T0_project_tag;
  payload_old["RecordingEnabled"].data<cool::Bool>() = sor.recording_enabled;
  payload_old["DataSource"].data<cool::String255>() = "no LHC"; // should come from LHC

  coral::AttributeList payload(cool::Record(s_dbHandler->SOR_Spec).attributeList());
  payload["RunNumber"].data<cool::UInt32>() = sor.run_number;
  payload["SORTime"].data<cool::UInt63>() = sor.timeSOR.total_mksec_utc() * 1000 ; // convert from microseconds into nanoseconds
  payload["RunType"].data<cool::String255>() = sor.run_type;
  //payload["DAQConfiguration"].data<cool::String255>() = ""; // dummy
  payload["DetectorMask"].data<cool::String255>() = sor.det_mask;
  payload["T0ProjectTag"].data<cool::String255>() = sor.T0_project_tag;
  payload["RecordingEnabled"].data<cool::Bool>() = sor.recording_enabled;
  //payload["DataSource"].data<cool::String255>() = "no LHC"; // should come from LHC

  ERS_LOG( "SOR info filled.");
  // Commit to DB
  if(s_oldFolders) {
    try {
      cool::Record rec(s_dbHandler->SOR_Spec_Old, payload_old);
      cool::IFolderPtr SOR_Folder = db->getFolder(SOR_Name_Old);
      ERS_LOG("Old SOR_Folder retrieved.");
      SOR_Folder->storeObject(since, until, rec,0);
      ERS_LOG("Old SOR folder stored.");
    }
    catch (std::exception &e){
        daq::rc::BadCoolDB i(ERS_HERE, e.what());

        std::string txt = e.what();
        if (txt.find("Back-insertion") == std::string::npos) {
            ers::error(i);
        } else {
            ers::log(i);
        }
    }
  }
  if(s_newFolders) {
    try {
      cool::IFolderPtr SOR_Folder = db->getFolder(SOR_Name);
      ERS_LOG("SOR_Folder retrieved.");
      cool::Record rec(s_dbHandler->SOR_Spec, payload);
      SOR_Folder->storeObject(since, until, rec,0);
      ERS_LOG("SOR folder stored.");
    }
    catch (std::exception &e){
        daq::rc::BadCoolDB i(ERS_HERE, e.what());

        std::string txt = e.what();
        if (txt.find("Back-insertion") == std::string::npos) {
            ers::error(i);
        } else {
            ers::log(i);
        }
    }
  }
  // Close the STOPR IOV for ATLAS
  if (s_partitionName == "ATLAS") {

       // Index by time
     cool::ValidityKey since = sor.time().total_mksec_utc()  * 1000 ;
     cool::ValidityKey until = sor.time().total_mksec_utc()  * 1000 + 1;

     // Fill in the data
     coral::AttributeList payload(cool::Record(s_dbHandler->STOPR_Spec).attributeList());


     payload["SubSystems"].data<cool::String255>() = "None";
     payload["Reason"].data<cool::String255>() = "None";

     ERS_LOG("STOPR info filled.");

     // Commit to DB
     try {
       cool::IFolderPtr STOPR_Folder = db->getFolder(STOPR_Name);
       ERS_LOG("STOPR_Folder retrieved.");
       cool::Record rec(s_dbHandler->STOPR_Spec, payload);     
       STOPR_Folder->storeObject(since, until, rec, 0);
       ERS_LOG("STOPR folder stored.");
     }
     catch (std::exception &e) {
         daq::rc::BadCoolDB i(ERS_HERE, e.what());

         std::string txt = e.what();
         if (txt.find("Back-insertion") == std::string::npos) {
             ers::error(i);
         } else {
             ers::log(i);
         }
     }  
  }

  s_dbHandler->close();
  return;
}

void EORcb(ISCallbackInfo *isc) {
  ERS_LOG("EOR callback received.");

  // Read info from IS
  RunParams eor;
  if (isc->reason() != is::Deleted)
    isc->value(eor);
  else {
    return;
  }

  std::lock_guard<std::recursive_mutex> lk(x);
  cool::IDatabasePtr db;
  try {
    db = s_dbHandler->getValidDb();
    ERS_DEBUG(3, "DB opened.");
  }
  catch (daq::rc::BadCoolDB &e){
    ers::error(e);
    return;
  }

  // Get Lumi Block
  uint32_t lumiblocknumber;
  try {
    IPCPartition p(s_partitionName);
    LumiBlockNamed lumiInfo(p, "RunParams.LumiBlock");
    lumiInfo.checkout();
    lumiblocknumber = lumiInfo.LumiBlockNumber;
  }
  catch (ers::Issue &e){
    lumiblocknumber = UINT_MAX-1;
  }

  // Index by run number and lumi block
  uint64_t a = eor.run_number ;
  cool::ValidityKey since = a<<32;
  cool::ValidityKey until = ((a<<32) | lumiblocknumber) +1;

  // Fill in the data
  coral::AttributeList payload_old(cool::Record(s_dbHandler->EOR_Spec_Old).attributeList());
  payload_old["RunNumber"].data<cool::UInt32>() = eor.run_number;
  payload_old["SORTime"].data<cool::UInt63>() = eor.timeSOR.total_mksec_utc()  * 1000 ; // convert from microseconds into nanoseconds
  payload_old["RunType"].data<cool::String255>() = eor.run_type;
  payload_old["DAQConfiguration"].data<cool::String255>() = getConfigVersion();
  payload_old["DetectorMask"].data<cool::UInt63>() = 0;
  payload_old["FilenameTag"].data<cool::String255>() = eor.T0_project_tag;
  payload_old["RecordingEnabled"].data<cool::Bool>() = eor.recording_enabled;
  payload_old["DataSource"].data<cool::String255>() = "no LHC"; // should come from LHC?
  payload_old["EORTime"].data<cool::UInt63>() = eor.timeEOR.total_mksec_utc() * 1000 ; // convert from seconds into nanoseconds
  payload_old["TotalTime"].data<cool::UInt32>() = eor.totalTime ;
  payload_old["CleanStop"].data<cool::Bool>() = true;

  coral::AttributeList payload(cool::Record(s_dbHandler->EOR_Spec).attributeList());
  payload["RunNumber"].data<cool::UInt32>() = eor.run_number;
  payload["SORTime"].data<cool::UInt63>() = eor.timeSOR.total_mksec_utc()  * 1000 ; // convert from microseconds into nanoseconds
  payload["RunType"].data<cool::String255>() = eor.run_type;
  payload["DAQConfiguration"].data<cool::String255>() = getConfigVersion();
  payload["DetectorMask"].data<cool::String255>() = eor.det_mask;
  payload["T0ProjectTag"].data<cool::String255>() = eor.T0_project_tag;
  payload["RecordingEnabled"].data<cool::Bool>() = eor.recording_enabled;
  //payload["DataSource"].data<cool::String255>() = "no LHC"; // should come from LHC?
  payload["EORTime"].data<cool::UInt63>() = eor.timeEOR.total_mksec_utc() * 1000 ; // convert from seconds into nanoseconds
  payload["TotalTime"].data<cool::UInt32>() = eor.totalTime ;
  payload["CleanStop"].data<cool::Bool>() = true;

  ERS_LOG("EOR info filled.");

  // Commit to DB

  if(s_oldFolders) {
    try {
      cool::IFolderPtr EOR_Folder = db->getFolder(EOR_Name_Old);
      ERS_LOG( "EOR_Folder retrieved.");
       cool::Record rec(s_dbHandler->EOR_Spec_Old, payload_old);     

      EOR_Folder->storeObject(since, until, rec, 0);
      ERS_LOG("EOR folder stored.");
    }
    catch (std::exception &e) {
        daq::rc::BadCoolDB i(ERS_HERE, e.what());

        std::string txt = e.what();
        if (txt.find("Back-insertion") == std::string::npos) {
            ers::error(i);
        } else {
            ers::log(i);
        }
    }
  }
  if(s_newFolders) {
    try {
      cool::IFolderPtr EOR_Folder = db->getFolder(EOR_Name);
      ERS_LOG( "EOR_Folder retrieved.");
      cool::Record rec(s_dbHandler->EOR_Spec, payload);     
      EOR_Folder->storeObject(since, until, rec, 0);
      ERS_LOG("EOR folder stored.");
    }
    catch (std::exception &e) {
        daq::rc::BadCoolDB i(ERS_HERE, e.what());

        std::string txt = e.what();
        if (txt.find("Back-insertion") == std::string::npos) {
            ers::error(i);
        } else {
            ers::log(i);
        }
    }
  }
  // Store Event counters
  uint64_t l1c, hlt, rc;

  getEventCounters(l1c, hlt, rc);

  coral::AttributeList ec_payload(cool::Record(s_dbHandler->EC_Spec).attributeList());
  ec_payload["PartitionName"].data<cool::String255>() = s_partitionName;
  ec_payload["L1Events"].data<cool::UInt63>() = l1c ;
  ec_payload["L2Events"].data<cool::UInt63>() = 0 ;
  ec_payload["EFEvents"].data<cool::UInt63>() = hlt ;
  ec_payload["RecordedEvents"].data<cool::UInt63>() = rc ;

  ERS_LOG("Event Counters info filled.");

  // Commit to DB
  try {
    cool::IFolderPtr EC_Folder = db->getFolder(EC_Name);
    ERS_LOG("EC_Folder retrieved.");
    cool::Record rec(s_dbHandler->EC_Spec, ec_payload);     

    EC_Folder->storeObject(since, until, rec, 0);
    ERS_LOG("Event Counters folder stored.");
  }
  catch (std::exception &e) {
      daq::rc::BadCoolDB i(ERS_HERE, e.what());

      std::string txt = e.what();
      if (txt.find("Back-insertion") == std::string::npos) {
          ers::error(i);
      } else {
          ers::log(i);
      }
  }
  
  s_dbHandler->close();
  return;
}

void STOPRcb(ISCallbackInfo *isc) {
  
  // Read info from IS
  StopWhileBeamInfo swb;
  if (isc->reason() != is::Deleted)
    isc->value(swb);
  else {
    return;
  }

  ERS_LOG("StopWhileBeam callback received.");
  std::lock_guard<std::recursive_mutex> lk(x);
  cool::IDatabasePtr db;
  try {
    db = s_dbHandler->getValidDb();
    ERS_DEBUG(3, "DB opened.");
  }
  catch (daq::rc::BadCoolDB &e){
    ers::error(e);
    return;
  }

  // Index by time
  cool::ValidityKey since = swb.time().total_mksec_utc()  * 1000 ;
  cool::ValidityKey until = cool::ValidityKeyMax;

  // Fill in the data
  coral::AttributeList payload(cool::Record(s_dbHandler->STOPR_Spec).attributeList());
  
  std::string cause="";
  for (size_t i = 0 ; i < swb.causedByString.size(); ++i) {
    cause += swb.causedByString[i];
    cause += ", ";
  }
  
  std::string reason="";
  for (size_t i = 0 ; i < swb.reasonString.size(); ++i) {
    reason += swb.reasonString[i];
    reason += ", ";
  }
  
  payload["SubSystems"].data<cool::String255>() = cause;
  payload["Reason"].data<cool::String255>() = reason;

  ERS_LOG("STOPR info filled.");

  // Commit to DB
  try {
    cool::IFolderPtr STOPR_Folder = db->getFolder(STOPR_Name);
    ERS_LOG( "STOPR_Folder retrieved.");
    cool::Record rec(s_dbHandler->STOPR_Spec, payload);     
    STOPR_Folder->storeObject(since, until, rec, 0);
    ERS_LOG( "STOPR folder stored.");
  }
  catch (std::exception &e) {
      daq::rc::BadCoolDB i(ERS_HERE, e.what());

      std::string txt = e.what();
      if (txt.find("Back-insertion") == std::string::npos) {
          ers::error(i);
      } else {
          ers::log(i);
      }
  }

  s_dbHandler->close();
  return;
  
}


void READYcb(ISCallbackInfo *isc) {
  
  ERS_LOG("READY callback received.");
  Ready4PhysicsInfo ready;
  if (isc->reason() != is::Deleted)
    isc->value(ready);
  else {
    return;
  }

  s_ready4Physics = ready.ready4physics;
  std::lock_guard<std::recursive_mutex> lk(x);
  cool::IDatabasePtr db;
  try {
    db = s_dbHandler->getValidDb();
    ERS_DEBUG(3, "DB opened.");
  }
  catch (daq::rc::BadCoolDB &e){
    ers::error(e);
    return;
  }

  // Fill in the data
  coral::AttributeList payload(cool::Record(s_dbHandler->READY4PH_Spec).attributeList());
   payload["ReadyForPhysics"].data<cool::UInt32>() = ready.ready4physics;
   uint64_t rn = ready.run_number, lb=ready.lumi_block;
   cool::ValidityKey since = rn <<32 | lb ;
   try {
     ERS_LOG("Writing into the COOL Folder the Ready4Physics flag: " << ready.ready4physics << " for run: " << ready.run_number << " and LB: " << ready.lumi_block);
    cool::IFolderPtr READY4PH_Folder = db->getFolder(READY4PH_Name);
    cool::Record rec(s_dbHandler->READY4PH_Spec, payload);     

    READY4PH_Folder->storeObject(since, cool::ValidityKeyMax, rec, 0); // open ended entry, channel = 0
    ERS_LOG("READY folder stored");
   }
   catch (std::exception &e) {
       daq::rc::BadCoolDB i(ERS_HERE, e.what());

       std::string txt = e.what();
       if (txt.find("Back-insertion") == std::string::npos) {
           ers::error(i);
       } else {
           ers::log(i);
       }
   }

   s_dbHandler->close();
   return;
  
}

void BSYcb(ISCallbackInfo *isc) {
  
  ISInfoDynAny busyInfo;
  IPCPartition p(s_partitionName);
  ISInfoDictionary dict(p);

  if (isc->reason() != is::Deleted)
    isc->value(busyInfo);
  else {
    return;
  }

  int bsy = s_vmeBusy;
  try {
    bsy = busyInfo.getAttributeValue<uint32_t>("MasterTriggerBusy") +
      busyInfo.getAttributeValue<uint32_t>("RunControlBusy");
  }
  catch (ers::Issue &e) {
    ers::error(e);
  }

  if(s_vmeBusy && (bsy==0) && s_trigOnHold) {
  ERS_LOG("BSY callback received.");
  std::lock_guard<std::recursive_mutex> lk(x);
    // close hold trigger interval here
    s_trigOnHold = false;

    // Close the STOPR IOV for ATLAS
    cool::IDatabasePtr db;
    try {
      db = s_dbHandler->getValidDb();
      ERS_DEBUG(3, "DB opened.");
    }
    catch (daq::rc::BadCoolDB &e){
      ers::error(e);
      return;
    }
    
    // Index by time
    cool::ValidityKey since = busyInfo.time().total_mksec_utc()  * 1000 ;
    cool::ValidityKey until = busyInfo.time().total_mksec_utc()  * 1000 + 1;
    
    // Fill in the data
    coral::AttributeList payload(cool::Record(s_dbHandler->HOLDT_Spec).attributeList());      
    
    payload["SubSystems"].data<cool::String255>() = "None";
    payload["Reason"].data<cool::String255>() = "None";
      
    ERS_LOG("HOLDT info filled from BSY callback.");
    
    // Commit to DB
    try {
      cool::IFolderPtr HOLDT_Folder = db->getFolder(HOLDT_Name);
       cool::Record rec(s_dbHandler->HOLDT_Spec, payload);     
      ERS_LOG("HOLDT_Folder retrieved.");
      HOLDT_Folder->storeObject(since, until, rec, 0);
      ERS_LOG("HOLDT folder stored.");
    }
    catch (std::exception &e) {
        daq::rc::BadCoolDB i(ERS_HERE, e.what());

        std::string txt = e.what();
        if (txt.find("Back-insertion") == std::string::npos) {
            ers::error(i);
        } else {
            ers::log(i);
        }
    } 
  }

  s_vmeBusy = bsy;
  
}

void HOLDTcb(ISCallbackInfo *isc) {
  
  // Read info from IS
  HoldTriggerInfo swb;
  if (isc->reason() != is::Deleted)
    isc->value(swb);
  else {
    return;
  }

  ERS_DEBUG(0,"HoldTrigger callback received.");
  std::lock_guard<std::recursive_mutex> lk(x);
  cool::IDatabasePtr db;
  try {
    db = s_dbHandler->getValidDb();
    ERS_DEBUG(3, "DB opened.");
  }
  catch (daq::rc::BadCoolDB &e){
    ers::error(e);
    return;
  }

  ERS_LOG("Hold Trigger callback received.");

  s_trigOnHold = true;
  // Index by time
  cool::ValidityKey since = swb.time().total_mksec_utc()  * 1000 ;
  cool::ValidityKey until = cool::ValidityKeyMax;

  // Fill in the data
  coral::AttributeList payload(cool::Record(s_dbHandler->HOLDT_Spec).attributeList());
  
  std::string cause=swb.causedByString;
  std::string reason=swb.reasonString;

  
  payload["SubSystems"].data<cool::String255>() = cause;
  payload["Reason"].data<cool::String255>() = reason;

  ERS_LOG("HOLDT info filled from HOLDcb.");

  // Commit to DB
  try {
    cool::IFolderPtr HOLDT_Folder = db->getFolder(HOLDT_Name);
    ERS_LOG( "HOLDT_Folder retrieved.");
    cool::Record rec(s_dbHandler->HOLDT_Spec, payload);     

    HOLDT_Folder->storeObject(since, until, rec, 0);
    ERS_LOG( "HOLDT_folder stored.");
  }
  catch (std::exception &e) {
      daq::rc::BadCoolDB i(ERS_HERE, e.what());

      std::string txt = e.what();
      if (txt.find("Back-insertion") == std::string::npos) {
          ers::error(i);
      } else {
          ers::log(i);
      }
  }

  s_dbHandler->close();
  return;
  
}

//***************************************************************************************
// string splitting
//***************************************************************************************
void splitter(std::string all, std::string & info, std::string & name, int & number) {
  info = "";
  name = "";
  number = -1;

  if (all != "") {
    // take into account also numbered notation for IS
    auto j = all.find_last_of(":");
    auto i = all.find_last_of(".");

    if ( ( i != all.npos ) && ( (j== all.npos) ||  (j < i) ) ) {
      info = all.substr(0,i);
      name = all.substr(i+1);
    }
    else if ( (j != all.npos) && ( j> i ) ) {
      info = all.substr(0,j);
      std::istringstream buf(all.substr(j+1));
      buf >> number;
    }
  }
  ERS_DEBUG(3, "IS Info is " << info << " and Name is " << name);
  return;
}



//--------------------- MAIN ----------------------------//
int main(int argc, char ** argv)
{
  try{
    IPCCore::init(argc,argv);
  }catch(daq::ipc::CannotInitialize& e){
    ers::fatal(e);
    abort();
  }catch(daq::ipc::AlreadyInitialized& e){
    ers::warning(e);
  }

  // parse commandline parameters
  boost::program_options::options_description desc("This program retrieves some run parameters from IS and inserts them into COOL.");
  desc.add_options()
    ("help,h",         "help message")
    ("coolDb,d",       boost::program_options::value<std::string>()                                  , "Name of COOL database")
    ("OldFolders,o",   boost::program_options::bool_switch(&s_oldFolders)                              , "Write to old folders ")
    ("NewFolders,n",   boost::program_options::bool_switch(&s_newFolders)                              , "Write to new folders")
    ;

  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::parse_command_line(argc,argv,desc), vm);
  boost::program_options::notify(vm);

  std::string folder, coolDb;

  if(vm.count("help")) {
    std::cout << desc << std::endl;
    return EXIT_SUCCESS;
  }
  if (vm.count("coolDb") ) {
    coolDb = vm["coolDb"].as<std::string>();
  }
  else {
    std::cout << desc << std::endl;
    daq::rc::CmdLineError issue(ERS_HERE,"");
    ers::fatal(issue);
    return EXIT_FAILURE;
  }

  // Retrieve TDAQ_PARTITION from environment
  std::string partitionName("");
  const char * pn = getenv("TDAQ_PARTITION");
  if (pn)
    s_partitionName = pn;
  else {
    daq::rc::EnvironmentMissing e(ERS_HERE,"TDAQ_PARTITION");
    ers::fatal(e);
    return EXIT_FAILURE;
  }


  // Initialize all global atomics;
  s_vmeBusy = false;
  s_ready4Physics = false;
  s_trigOnHold = false;


  // Retrieve data from DB
  const daq::core::Partition * part = 0;
  const daq::core::IS_InformationSources *infoIS;
  try {
    Configuration config("");
    part = daq::core::get_partition(config, s_partitionName);
    if(!part){
      std::string message = "Partition does not exist!" ;
      daq::rc::ConfigurationIssue issue(ERS_HERE,message.c_str());
      ers::fatal(issue);
      return EXIT_FAILURE;
    }
    infoIS = part->get_IS_InformationSource();
    // Get IS strings from DB
    try {
      std::string tmp("");
      if (infoIS && infoIS->get_LVL1()) {
       	tmp = infoIS->get_LVL1()->get_EventCounter();
      }
      splitter(tmp, s_LVL1InfoSource, s_LVL1Attr, s_LVL1Pos);
    }
    catch(ers::Issue & e) {
      // nothing
    }
    try {
      std::string tmp("");
      if (infoIS && infoIS->get_HLT()) {
       	tmp = infoIS->get_HLT()->get_EventCounter();
      }
      splitter(tmp, s_HLTInfoSource, s_HLTAttr, s_HLTPos);
    }
    catch(ers::Issue & e) {
      // nothing
    }
    try {
      std::string tmp("");
      if (infoIS && infoIS->get_Recording()) {
       	tmp = infoIS->get_Recording()->get_EventCounter();
      }
      splitter(tmp, s_RecordedEventsInfoSource, s_RecordedEventsAttr, s_RecordedEventsPos);
    }
    catch(ers::Issue & e) {
      // nothing
    }
  }
  catch(daq::config::Exception& e) {
    ers::fatal(e);
    return EXIT_FAILURE;
  }

  s_dbHandler = new daq::rc::CoolDB(coolDb);

  // Close the DBs; reopen everytime there is an entry to be inserted...
  s_dbHandler->close();
  ERS_DEBUG(3, "DB closed.");

  // Subscribe to RunParams SOR, RunParams EOR

  IPCPartition ipcP(s_partitionName);

  infoRec = new ISInfoReceiver(ipcP, true);
  try {
    infoRec->subscribe(IS_SOR_Name, SORcb);
    infoRec->subscribe(IS_EOR_Name, EORcb);
    if(s_partitionName == "ATLAS") {
       infoRec->subscribe(IS_STOPR_Name, STOPRcb);
       infoRec->subscribe(IS_HOLDT_Name, HOLDTcb);
       infoRec->subscribe("RunParams.Ready4Physics", READYcb);
       infoRec->subscribe("RunParams.GlobalBusy", BSYcb);
    }

  }
  catch ( daq::is::Exception & ex )
    {
      ers::fatal(ex);
      return EXIT_FAILURE;
    }

  // Install signal handler
  signal(SIGINT,  signalhandler);
  signal(SIGTERM, signalhandler);

  ERS_DEBUG(3, "Installed signal handlers.");

  // Tell PMG that now we are ready!
  pmg_initSync();

  ERS_DEBUG(3, "Waiting for IS callbacks....");

  sem.wait();

  try {
    infoRec->unsubscribe(IS_SOR_Name);
    infoRec->unsubscribe(IS_EOR_Name);
    if(s_partitionName == "ATLAS") {
       infoRec->unsubscribe(IS_STOPR_Name);
       infoRec->unsubscribe(IS_HOLDT_Name);
       infoRec->unsubscribe("RunParams.Ready4Physics");
       infoRec->unsubscribe("RunParams.GlobalBusy");
    }
    std::lock_guard<std::recursive_mutex> lk(x);
    delete s_dbHandler;
    delete infoRec;
  }
  catch(ers::Issue & e) {
  }

  return EXIT_SUCCESS;
}

