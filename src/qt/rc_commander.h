#ifndef RC_COMMANDER_H_
#define RC_COMMANDER_H_
#include "rc_commander.ui.h"
#include <QMutex>
#include <QTimer>

class QTextStream;
class QTreeWidgetItem;
/*! \brief This class define Main Window layout and functionality. The layout defined by Qt designer is provided by Ui:MayinWindows
    \ingroup public
    \sa
 */
class RCCommander : public QMainWindow, private Ui::rccommander
{

    //! The QObject class is the base class of all Qt objects.Take care for signal slotes etc.
    /*!
      \param  parent Widget which will be parent of MainWindow
    */
    Q_OBJECT
public:
    RCCommander()
    {
        init();
    }
private:
    void init();
private slots:
    void loadPartitions();
    void getControllers(QString s);
    void SendCommand();
};
#endif /*RC_COMMANDER_H_*/
