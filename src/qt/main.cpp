#include <qapplication.h>
#include "rc_commander.h"
#include <ipc/core.h>

int main( int argc, char ** argv )
{
    IPCCore::init(argc,argv);
    QApplication a( argc, argv );
    RCCommander w;
    w.show();
    a.connect( &a, SIGNAL( lastWindowClosed() ), &a, SLOT( quit() ) );
    return a.exec();
}
