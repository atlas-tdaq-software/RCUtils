#include "rc_commander.h"
#include "RunController/Commander.h"
#include <list>
#include "ipc/partition.h"
#include <ers/ers.h>
#include <map>
#include <QDateTime>
#include <QMap>
#include <QFile>
#include <QTextStream>
#include <QFileDialog>

void RCCommander::init()
{
	setupUi(this);//setup UI layout defined by designer
    loadPartitions();
    connect(this->partitions,SIGNAL(activated(QString)),this,SLOT(getControllers(QString)));
    connect(this->Send,SIGNAL(clicked()),this,SLOT(SendCommand()));
        
}

/**
 * Loads all available partitions
 */
void RCCommander::loadPartitions()
{
    this->partitions->clear();
    std::list<IPCPartition> pl;
    IPCPartition partition("initial");
    if(partition.isValid())
    {
        this->partitions->addItem(QString(partition.name().c_str()));
        partition.getPartitions(pl);
        ERS_DEBUG(0, "Found " << pl.size() << " partitions");
        for( std::list<IPCPartition>::iterator it = pl.begin(); it !=  pl.end();it++)
        {
            ERS_DEBUG(0,"Inserting partition = " << (*it).name());
            this->partitions->addItem(QString((*it).name().c_str()));
        }
        getControllers(this->partitions->currentText());

    }
    else
    {
        ERS_DEBUG(0,"Partition " << partition.name() << " does not exist");
    }
}


/**
 * Gets a list of all the controller objects
 */
void RCCommander::getControllers(QString pName)
{
    controllers->clear();
    try
    {
        IPCPartition partition(pName.toStdString());
        std::map<std::string,ipc::servant_var> objects;
        partition.getObjects<ipc::servant,ipc::use_cache,ipc::unchecked_narrow>(objects,"CS/commander");
        std::map<std::string,ipc::servant_var>::iterator it = objects.begin();
        while(it != objects.end())
        {
            if(!CORBA::is_nil((*it).second))
            {
                try
                {
                    this->controllers->addItem(QString((*it).first.c_str()));
                }
                catch(...)
                {}
                it++;
            }
        }
    }
    catch(ers::Issue &e)
    {
        ers::error(e);
    }
}


void RCCommander::SendCommand()
{
    IPCPartition p(this->partitions->currentText().toStdString());
    if(!p.isValid())
    {
        ERS_DEBUG(1,"Send failed!");
        return;
    }
    if(this->controllers->currentText() == "")
    {
        ERS_DEBUG(1,"Send failed!");
        return;
    }
    daq::rc::Commander commander(p,"CmdIgui");
    try
    {
        ERS_DEBUG(1,"Sending command to the controller");
        commander.send(
            this->controllers->currentText().toStdString(), //name of the controller
            this->command->currentText().toStdString(), //command name
            this->args->text().toStdString()); ///arguments
        ERS_DEBUG(1,"Send succeeded");
        return;
    }
    catch(daq::rc::CorbaException& e)
    {
        ers::warning(e);
    }
    catch(daq::rc::IPCLookup& e)
    {
        ers::warning(e);
    }
    catch(ers::Issue& e)
    {
        ers::warning(e);
    }
    ERS_DEBUG(1,"Send failed!");
}

