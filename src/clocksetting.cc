// $Id: sendcommand.cc 45895 2008-02-23 16:48:32Z glehmann $
//
// Command sender to supervisor and run control
//
// Author: Giovanna.Lehmann@cern.ch

#include <boost/program_options.hpp>

#include "ers/ers.h"
#include "ipc/core.h"
#include "ipc/partition.h"

#include <RunControl/Common/CommandSender.h>
#include <RunControl/Common/Exceptions.h>
#include <TriggerCommander/TriggerCommander.h>

#include "is/infodictionary.h"
#include "is/infoT.h"
#include "rc/AutomaticClockNamed.h"
#include "rc/RCStateInfoNamed.h"

#include "config/Configuration.h"
#include "dal/Partition.h"
#include "dal/MasterTrigger.h"
#include "dal/RunControlApplicationBase.h"
#include "dal/util.h"

inline std::string str(const char * word) {
    return word ? word : "";
}

namespace daq {
ERS_DECLARE_ISSUE_BASE( rc,
                       HoldingTriggerFailed,
                       rc::Exception,
                       "Could not contact Master Trigger",
                       ERS_EMPTY,
                       ((const char *)reason))

ERS_DECLARE_ISSUE_BASE( rc,
                       ResumingTriggerFailed,
                       rc::Exception,
                       "Could not contact Master Trigger",
                       ERS_EMPTY,
                       ((const char *)reason))

//Issues as Online Recovery
ERS_DECLARE_ISSUE_BASE( OnlRec, ClockChange, rc::Exception, mesg, ERS_EMPTY, ((const char*) mesg))
}

int main(int argc, char ** argv) {
    try {
        IPCCore::init(argc, argv);
    }
    catch(daq::ipc::CannotInitialize& e) {
        ers::fatal(e);
        abort();
    }
    catch(daq::ipc::AlreadyInitialized& e) {
        ers::warning(e);
    }

    int wTime = 5;
    std::string clock, partition, tdaq_db;

    // Set defaults if available in the environment
    if(getenv("TDAQ_PARTITION")) {
        partition = getenv("TDAQ_PARTITION");
    }
    if(getenv("TDAQ_DB"))
        tdaq_db = getenv("TDAQ_DB");

    boost::program_options::options_description desc("This program switches the CLOCK on the RF2TTC", 128);
    desc.add_options()("help,h", "help message")
                      ("partition,p", boost::program_options::value<std::string>(&partition)->default_value(partition), "partition name")
                      ("database,d", boost::program_options::value<std::string>(&tdaq_db)->default_value(tdaq_db), "database (TDAQ_DB)")
                      ("waiting,w", boost::program_options::value<int>(&wTime)->default_value(wTime), "seconds to wait after/before acting on trigger")
                      ("action,a", boost::program_options::value<std::string>(), "possible values: INTERNAL, LHC, BCREF, AUTOMATIC ");

    boost::program_options::variables_map vm;
    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);

    if(vm.count("help")) {
        std::cout << desc << std::endl;
        return EXIT_SUCCESS;
    }

    if(vm.count("action")) {
        clock = vm["action"].as<std::string>();
    } else {
        std::cout << desc << std::endl;
        daq::rc::CmdLineError issue(ERS_HERE, "the action to take has not been defined");
        ers::fatal(issue);
        return EXIT_FAILURE;
    }

    if(partition.empty() || tdaq_db.empty()) {
        daq::rc::CmdLineError issue(ERS_HERE, "the partition and database options cannot be empty");
        ers::error(issue);
        return EXIT_FAILURE;
    }

    IPCPartition p(partition);
    IPCPartition l1ctp("initialL1CT");
    IPCPartition initp("initial");

    daq::rc::CommandSender rf2ttc(l1ctp.name(), "ClockSettingUtility");

    try {
        RCStateInfoNamed rcState(p, "RunCtrl.RootController");
        AutomaticClockNamed ac(p, "RunParams.AutomaticClock");
        if(clock == "AUTOMATIC") {
            ERS_INFO("Clock switching will be handled by Expert System.");
            ac.value = true;
            ac.checkin();
        } else {
            ac.value = false;
            ac.checkin();

            rcState.checkout();
            if(rcState.state != "RUNNING") {
                ers::fatal(daq::rc::BadCommand(ERS_HERE,
                                               "Clock Switching",
                                               "The clock can only be switched in the RUNNING state!"));
                return EXIT_FAILURE;
            }

            // Get the master trigger name
            Configuration config(tdaq_db);

            if(!config.loaded()) {
                daq::rc::ConfigurationIssue issue(ERS_HERE, "the database cannot be loaded");
                ers::fatal(issue);
                return EXIT_FAILURE;
            }

            // find root controller in the root partition

            const daq::core::Partition * root = daq::core::get_partition(config, partition);
            if(!root) {
                std::string message = "Partition does not exist!";
                daq::rc::ConfigurationIssue issue(ERS_HERE, message.c_str());
                ers::fatal(issue);
                std::exit(EXIT_FAILURE);
            }

            const daq::core::MasterTrigger* masterTrigger = root->get_MasterTrigger();
            if(masterTrigger == NULL) {
                ers::fatal(daq::rc::MasterTriggerNotDefined(ERS_HERE, "Can't possibly send commands to it!"));
                std::exit(EXIT_FAILURE);
            }

            std::string masterTriggerName(masterTrigger->get_Controller()->UID());
            daq::trigger::TriggerCommander t(p, masterTriggerName);

	    std::vector<std::string> cmd;
            if(clock == "INTERNAL") {
                ERS_INFO("Clock will be switched to INTERNAL.");
                cmd.push_back("INT");
                cmd.push_back("INT");
            } else if(clock == "LHC") {
                ISInfoDictionary dict(initp);
                ISInfoString s;
                dict.getValue("LHC.ExternalClockChoice", s);
		std::stringstream ss(s);
		std::string item;
		while(std::getline(ss, item, ' ')) {
		  if(!item.empty()) {
		    cmd.push_back(item);
		  }
		}
                ERS_INFO("Clock will be switched to " << s);
            } else if(clock == "BCREF") {
                ERS_INFO("Clock will be switched to BCREF.");
                cmd.push_back("BCREF");
                cmd.push_back("INT");
            } else {
                std::ostringstream reason;
                reason << "Wrong type of clock: " << clock;
                ers::error(daq::rc::CmdLineError(ERS_HERE, reason.str()));
                exit(1);
            }

            ERS_INFO("Holding Trigger");
            try {
                t.hold();
            }
            catch(ers::Issue &e) {
                ers::fatal(daq::rc::HoldingTriggerFailed(ERS_HERE, "", e));
                std::exit(EXIT_FAILURE);
            }
            std::ostringstream msg;
            msg << wTime << " seconds before changing clock to " << clock;
            ers::info(daq::OnlRec::ClockChange(ERS_HERE, msg.str().c_str()));
            sleep(wTime); // leave time for systems to flush their buffers
            ERS_INFO("Sending clock switch to RF2TTCApp.");
            try {
                // FIXME: is this correct? A direct command (not an user command) was sent before
                rf2ttc.userCommand("RF2TTCApp", "SELECT", std::vector<std::string>{cmd});
                sleep(wTime); // leave time to systems to adapt to new clock
            }
            catch(ers::Issue &e) {
                ers::fatal(daq::rc::ExecutionFailed(ERS_HERE, "change of clock to RF2TTCApp", e.message(), e));
            }

            ERS_INFO("Resuming Trigger");
            try {
                t.resume();
            }
            catch(ers::Issue &e) {
                ers::fatal(daq::rc::ResumingTriggerFailed(ERS_HERE, "", e));
            }
        }
    }
    catch(ers::Issue &e) {
        ers::fatal(e);
    }
    return EXIT_SUCCESS;
}
