#include <signal.h>
#include <stdlib.h>
#include <fstream>
#include <vector>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <thread>
#include <list>
#include <algorithm>
#include <numeric>
#include <cmath>

#include <boost/program_options.hpp>

#include <CoralBase/Attribute.h>
#include <CoolKernel/Exception.h>
#include <CoolKernel/ValidityKey.h>
#include <CoolKernel/IDatabaseSvc.h>
#include <CoolKernel/IDatabase.h>
#include <CoolKernel/IFolder.h>
#include <CoolKernel/IRecordSpecification.h>
#include <CoolKernel/Record.h>
#include <CoolApplication/DatabaseSvcFactory.h>
#include <CoolKernel/IObjectIterator.h>
#include <CoolKernel/IObject.h>
#include <CoolKernel/ChannelSelection.h>
#include <CoolApplication/Application.h>

#include "owl/time.h"

#include "TROOT.h"
#include "TFile.h"
#include "TDatime.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TGaxis.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TLatex.h"
#include "TLegend.h"

//#include "ers/ers.h"
#include "RunControl/Common/Exceptions.h"

namespace daq {
	ERS_DECLARE_ISSUE(rc,
		BadCoolDb,
		"Retrieving data from COOL failed because " << explanation,
		((const char*)explanation)
	)
}

extern "C" void signalhandler(int)
{
	signal(SIGINT, SIG_DFL);
	signal(SIGTERM, SIG_DFL);
	std::cout << "Terminating daq_eff_page..." << std::endl;
}

static     cool::IDatabasePtr db;
static     cool::IDatabasePtr dbd;
static     cool::IDatabasePtr dbt;
static     cool::IDatabasePtr dbm;
static     cool::Application cool_app;

static     std::string dbNameTDAQ = "COOLONL_TDAQ/CONDBR2";
static     std::string dbNameDCS = "COOLOFL_DCS/CONDBR2";
static     std::string dbNameTRIGGER = "COOLONL_TRIGGER/CONDBR2";
static     std::string dbNameMONITOR = "COOLONL_TRIGGER/MONP200";
static     std::string FillState_Name = "/LHC/DCS/FILLSTATE";
static     std::string L1Counters_Name = "/TRIGGER/LUMI/LVL1COUNTERS";
static     std::string L1Menu_Name = "/TRIGGER/LVL1/Menu";
static     std::string LB_Name = "/TRIGGER/LUMI/LBLB";
static     std::string LBT_Name = "/TRIGGER/LUMI/LBTIME";
static     std::string DataTakingMode_Name = "/TDAQ/RunCtrl/DataTakingMode";
static     std::string BusyConf_Name = "/TRIGGER/LVL1/BUSYCONF";
static     std::string BusyRate_Name = "/TRIGGER/LVL1/BUSYRATES_RUN2";
static     std::string StopReason_Name = "/TDAQ/RunCtrl/StopReason";
static     std::string HoldTrigger_Name = "/TDAQ/RunCtrl/HoldTriggerReason";
static	   std::string t0Project_Name = "/TDAQ/RunCtrl/SOR";
static     std::string EOR_Name = "/TDAQ/RunCtrl/EOR";
static     std::string SOR_Name = "/TDAQ/RunCtrl/SOR";
static     std::string TotalInt_Name = "/LHC/DCS/BPTX/TOTALINT";
static     int yearToDo;
static     cool::IFolderPtr FillState_Folder;
static     cool::IFolderPtr LBT_Folder;
static     cool::IFolderPtr DataTakingMode_Folder;
static     cool::IFolderPtr BusyRate_Folder;
static	   cool::IFolderPtr EOR_Folder;
static	   cool::IFolderPtr SOR_Folder;
static	   cool::IFolderPtr TotalInt_Folder;

static unsigned long long globalStartofYear = 0;
static std::vector<std::string>* weeksInYear = new std::vector<std::string>;
static std::vector<std::pair<unsigned long, unsigned long>>* hourlyTimes = new std::vector<std::pair<unsigned long, unsigned long>>;
static std::vector<std::pair<unsigned long, unsigned long>>* hourlyBeamTimes = new std::vector<std::pair<unsigned long, unsigned long>>;
static std::vector<std::pair<unsigned long, unsigned long>>* dailyTimes = new std::vector<std::pair<unsigned long, unsigned long>>;
static std::vector<std::pair<unsigned long, unsigned long>>* runTimes = new std::vector<std::pair<unsigned long, unsigned long>>;
static std::vector<std::pair<unsigned long, unsigned long>>* runTimesRFP = new std::vector<std::pair<unsigned long, unsigned long>>;
static std::vector<unsigned long long>* runTimeHourly = new std::vector<unsigned long long>;
static std::vector<unsigned long long>* runTimeDaily = new std::vector<unsigned long long>;
static std::vector<std::pair<unsigned long, unsigned long>>* beamTimes = new std::vector<std::pair<unsigned long, unsigned long>>;
static std::vector<unsigned long>* beamTimesWOPresence = new std::vector<unsigned long>;
static std::vector<unsigned long>* beamTimeHourly = new std::vector<unsigned long>;
static std::vector<unsigned long>* beamTimeDaily = new std::vector<unsigned long>;
static std::vector<unsigned long>* effTimes = new std::vector<unsigned long>;
static std::vector<unsigned long>* effTimeHourly = new std::vector<unsigned long>;
static std::vector<unsigned long long>* effTimeDaily = new std::vector<unsigned long long>;
static std::vector<unsigned long>* effTimesRFP = new std::vector<unsigned long>;
static std::vector<unsigned long>* effTimeHourlyRFP = new std::vector<unsigned long>;
static std::vector<unsigned long>* effTimeDailyRFP = new std::vector<unsigned long>;
static std::vector<unsigned long>* effTimesEndDelay = new std::vector<unsigned long>;

//totalizers
static double totalBeamHours = 0;
static double totalEffHours = 0;
static double totalEffReadyHours = 0;
static double totalEffNoEndDelay = 0;

//yearly plots
static std::vector<unsigned int>* secondsY = new std::vector<unsigned int>;
static std::vector<double>* beamY = new std::vector<double>;
static std::vector<double>* runEffY = new std::vector<double>;
static std::vector<double>* runEffReadyY = new std::vector<double>;
static std::vector<double>* beamTimeY = new std::vector<double>;
static std::vector<double>* runTimeY = new std::vector<double>;
static std::vector<double>* runTimeReadyY = new std::vector<double>;
static TGraph* beamGraph = new TGraph();
static TGraph* runEffGraph = new TGraph();
static TGraph* runEffReadyGraph = new TGraph();
static TGraph* beamTimeGraph = new TGraph();
static TGraph* runTimeGraph = new TGraph();
static TGraph* runTimeReadyGraph = new TGraph();
//daily plot
static std::vector<unsigned int>* secondsD = new std::vector<unsigned int>;
static std::vector<double>* runEffD = new std::vector<double>;
static std::vector<double>* runEffReadyD = new std::vector<double>;
static TGraph* runEffGraphD = new TGraph();
static TGraph* runEffReadyGraphD = new TGraph();
//weekly plots
static std::vector<unsigned int>* secondsW = new std::vector<unsigned int>;
static std::vector<double>* beamTimeW = new std::vector<double>;
static std::vector<double>* runEffW = new std::vector<double>;
static std::vector<double>* runEffReadyW = new std::vector<double>;
static TGraph* beamTimeGraphW = new TGraph();
static TGraph* runEffGraphW = new TGraph();
static TGraph* runEffReadyGraphW = new TGraph();
//week plot
static std::vector<unsigned int>* seconds_beams = new std::vector<unsigned int>;//time points for beam graph (all)
static std::vector<unsigned int>* seconds_runs = new std::vector<unsigned int>;//time points for runs graph (all)
static std::vector<unsigned int>* seconds_eff = new std::vector<unsigned int>;//time points for efficiencies (hourly w/ beam)
static std::vector<double>* run_week = new std::vector<double>; //run happening or not (1/0)
static std::vector<double>* beam_week = new std::vector<double>; //beam stable or not (1/0)
static std::vector<double>* runEffH = new std::vector<double>;
static std::vector<double>* runEffReadyH = new std::vector<double>;
static TGraph* beamTimeGraphWeek = new TGraph();
static TGraph* runTimeGraphWeek = new TGraph();
static TGraph* runEffGraphWeek = new TGraph();
static TGraph* runEffReadyGraphWeek = new TGraph();

//HTML stuff
static std::string weekBYweekHTML = "";

//methods
void partYear(std::vector<std::string>* weeksString);
void getHourlyTimes(unsigned long long starttime_ns, unsigned long long endtime_ns);
void getDailyTimes(unsigned long long starttime_ns, unsigned long long endtime_ns);
void getRunTimes(unsigned long long starttime_ns, unsigned long long endtime_ns);
void getRunTimesRFP(unsigned long long starttime_ns, unsigned long long endtime_ns);
void getBeamTimes(unsigned long long starttime_ns, unsigned long long endtime_ns);
void getEfficiencies();
std::string get_date_OWL(long long nseconds, std::string mode = "output");
void processYearly(unsigned long long starttime_ns, unsigned long long endtime_ns);
void processWeekly(unsigned long long starttime_ns, unsigned long long endtime_ns);
void processDaily(unsigned long long starttime_ns, unsigned long long endtime_ns);
void processWeek(unsigned long long starttime_ns, unsigned long long endtime_ns);
void drawYearly();
void drawDaily();
void drawWeekly();
void drawWeek(unsigned long long starttime_ns, unsigned long long endtime_ns);
TStyle* getAtlasStyle();
void doHTML(unsigned long long starttime_ns, unsigned long long endtime_ns);
std::string tellMeTheMonth(int m);

static TStyle* atlas = getAtlasStyle();

int main(int argc, char** argv) {

	std::string startTime, endTime;

	boost::program_options::options_description desc("This program retrieves info from COOL conditions database, evaluates the data taking efficiency and the inneficiency sources for selected time intervals and outputs the results in the standard output stream. The granularity of the information is per lumi block.");
	desc.add_options()
		("help,h", "help message")
		("busyFolder,f", boost::program_options::value<std::string>(&BusyRate_Name)->default_value(BusyRate_Name), "Busy folder name (default /TRIGGER/LVL1/BUSYRATES_RUN2")
		("year,y", boost::program_options::value<int>(&yearToDo)->default_value(0), "The year to be indexed (default is current year)")
		;

	boost::program_options::variables_map vm;
	boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
	boost::program_options::notify(vm);
	unsigned long long StartTimeNS = 0;
	unsigned long long EndTimeNS = 0;

	if (vm.count("help")) {
		std::cout << desc << std::endl;
		return EXIT_SUCCESS;
	}

	partYear(weeksInYear);

	try {
		auto iterWeeks = weeksInYear->begin();
		for (; iterWeeks < (weeksInYear->end() - 1); ++iterWeeks) {
			startTime = *iterWeeks + " 00:00:00";
			endTime = *(iterWeeks + 1) + " 00:00:00";
			OWLTime start_time(startTime.c_str());
			OWLTime end_time(endTime.c_str());
			StartTimeNS = start_time.total_mksec_utc();
			EndTimeNS = end_time.total_mksec_utc();
			StartTimeNS *= 1000;
			EndTimeNS *= 1000;
			getHourlyTimes(StartTimeNS, EndTimeNS);
			getDailyTimes(StartTimeNS, EndTimeNS);
			getBeamTimes(StartTimeNS, EndTimeNS);
			if (!beamTimes->empty()) {
				getRunTimes(StartTimeNS, EndTimeNS);
				getEfficiencies();
				processYearly(StartTimeNS, EndTimeNS);
				processDaily(StartTimeNS, EndTimeNS);
				processWeekly(StartTimeNS, EndTimeNS);
				processWeek(StartTimeNS, EndTimeNS);
				drawWeek(StartTimeNS, EndTimeNS);
				doHTML(StartTimeNS, EndTimeNS);
			}
		}
		if (!secondsY->empty()) {
			drawYearly();
			drawDaily();
			drawWeekly();
		}
	}
	catch (const std::runtime_error& e) {
		std::cerr << e.what() << std::endl;
		delete dailyTimes;
		delete hourlyTimes;
		delete runTimes;
		delete runTimeHourly;
		delete beamTimes;
		delete beamTimeHourly;
		delete effTimes;
		delete effTimeHourly;
		delete effTimesRFP;
		delete effTimeHourlyRFP;
		delete atlas;
		return EXIT_FAILURE;
	}
	delete dailyTimes;
	delete hourlyTimes;
	delete runTimes;
	delete runTimeHourly;
	delete beamTimes;
	delete beamTimeHourly;
	delete effTimes;
	delete effTimeHourly;
	delete effTimesRFP;
	delete effTimeHourlyRFP;
	delete atlas;
	return EXIT_SUCCESS;
}

void getHourlyTimes(unsigned long long starttime_ns, unsigned long long endtime_ns) {
	hourlyTimes->clear();
	unsigned long buffTime = starttime_ns;
	while (buffTime <= endtime_ns) {
		std::pair<long, long> buffPair;
		buffPair.first = buffTime;
		buffTime += 3'600'000'000'000;
		buffPair.second = buffTime;
		hourlyTimes->push_back(buffPair);
	}
	hourlyTimes->pop_back();
}

void getDailyTimes(unsigned long long starttime_ns, unsigned long long endtime_ns) {
	dailyTimes->clear();
	unsigned long buffTime = starttime_ns;
	while (buffTime <= endtime_ns) {
		std::pair<unsigned long, unsigned long> buffPair;
		buffPair.first = buffTime;
		buffTime += 86'400'000'000'000;
		buffPair.second = buffTime;
		dailyTimes->push_back(buffPair);
	}
	dailyTimes->pop_back();
}

void getRunTimes(unsigned long long starttime_ns, unsigned long long endtime_ns) {
	runTimes->clear();
	runTimeDaily->clear();
	runTimeHourly->clear();
	try {
		db = cool_app.databaseService().openDatabase(dbNameTDAQ);
		dbt = cool_app.databaseService().openDatabase(dbNameTRIGGER);
		LBT_Folder = dbt->getFolder(LBT_Name);
		EOR_Folder = db->getFolder(EOR_Name);
		SOR_Folder = db->getFolder(SOR_Name);
	}
	catch (const std::exception& ex) {
		std::string error = "getRunTimes(..) says: DATABASE ACCESS PROBLEM";
		throw(std::runtime_error(error));
	}

	try {
		int buffRN = 0;
		unsigned long long sor = 0;
		unsigned long long eor = 0;
		cool::IObjectIteratorPtr rl_objects = LBT_Folder->browseObjects(cool::ValidityKey(starttime_ns), cool::ValidityKey(endtime_ns), 0);
		while (rl_objects->goToNext()) {
			const cool::IObject& obj = rl_objects->currentRef();
			if (buffRN != atoi(obj.payloadValue("Run").c_str())) {
				buffRN = atoi(obj.payloadValue("Run").c_str());
				cool::UInt63 runlumi_key = atoi(obj.payloadValue("Run").c_str());
				runlumi_key = (runlumi_key << 32);
				cool::IObjectPtr obj_lb;
				try {
					obj_lb = EOR_Folder->findObject(runlumi_key, 0);
					sor = atoll(obj_lb->payloadValue("SORTime").c_str());
					if (sor < starttime_ns)
						sor = starttime_ns;
					eor = atoll(obj_lb->payloadValue("EORTime").c_str());
					if (eor > endtime_ns)
						eor = endtime_ns;
					runTimes->push_back({ sor,eor });
				}
				catch (const std::exception& e) {
					std::cout << "RUN " << buffRN << " STILL RUNNING" << std::endl;
					obj_lb = SOR_Folder->findObject(runlumi_key, 0);
					sor = atoll(obj_lb->payloadValue("SORTime").c_str());
					eor = endtime_ns;
					runTimes->push_back({ sor,eor });
				}
			}
		}
	}
	catch (const std::exception& e) {
		std::string error = "getRunTimes(..) says: DATA ACCESS PROBLEM";
		throw(std::runtime_error(error));
	}
	db->closeDatabase();
	dbt->closeDatabase();

	//intersect with hourly vector to get runtime per hour
	for (auto& valHT : *hourlyTimes) {
		long sum = 0;
		for (auto& valRT : *runTimes) {
			if (valRT.first >= valHT.second || valRT.second <= valHT.first)
				continue;
			if (valRT.first <= valHT.first && valRT.second <= valHT.second)
				sum += valRT.second - valHT.first;
			else if (valRT.first <= valHT.first && valRT.second >= valHT.second)
				sum += valHT.second - valHT.first;
			else if (valRT.first >= valHT.first && valRT.second <= valHT.second)
				sum += valRT.second - valRT.first;
			else if (valRT.first >= valHT.first && valRT.second >= valHT.second)
				sum += valHT.second - valRT.first;
		}
		runTimeHourly->push_back(sum);
	}

	//intersect with daily vector to get runtime per day
	for (auto& valHT : *dailyTimes) {
		long sum = 0;
		for (auto& valRT : *runTimes) {
			if (valRT.first >= valHT.second || valRT.second <= valHT.first)
				continue;
			if (valRT.first <= valHT.first && valRT.second <= valHT.second)
				sum += valRT.second - valHT.first;
			else if (valRT.first <= valHT.first && valRT.second >= valHT.second)
				sum += valHT.second - valHT.first;
			else if (valRT.first >= valHT.first && valRT.second <= valHT.second)
				sum += valRT.second - valRT.first;
			else if (valRT.first >= valHT.first && valRT.second >= valHT.second)
				sum += valHT.second - valRT.first;
		}
		runTimeDaily->push_back(sum);
	}
}

void getRunTimesRFP(unsigned long long starttime_ns, unsigned long long endtime_ns) {
	runTimesRFP->clear();
	try {
		db = cool_app.databaseService().openDatabase(dbNameTDAQ);
		dbt = cool_app.databaseService().openDatabase(dbNameTRIGGER);
		LBT_Folder = dbt->getFolder(LBT_Name);
		EOR_Folder = db->getFolder(EOR_Name);
		DataTakingMode_Folder = db->getFolder(DataTakingMode_Name);
	}
	catch (const std::exception& ex) {
		std::string error = "getRunTimesRFP(..) says: DATABASE ACCESS PROBLEM";
		throw(std::runtime_error(error));
	}

	try {
		int buffRFP = 0;
		std::pair<unsigned long long, unsigned long long> buffPair;
		cool::IObjectIteratorPtr rl_objects = LBT_Folder->browseObjects(cool::ValidityKey(starttime_ns), cool::ValidityKey(endtime_ns), 0);
		while (rl_objects->goToNext()) {
			const cool::IObject& obj = rl_objects->currentRef();
			cool::UInt63 runlumi_key = atoi(obj.payloadValue("Run").c_str());
			runlumi_key = (runlumi_key << 32);
			runlumi_key += atoi(obj.payloadValue("LumiBlock").c_str());
			cool::IObjectPtr obj_lb = DataTakingMode_Folder->findObject(runlumi_key, 0);
			if (obj_lb->payloadValue("ReadyForPhysics").compare("1") == 0 && buffRFP == 0) {
				buffPair.first = obj.since();
			}
			else if (obj_lb->payloadValue("ReadyForPhysics").compare("0") == 0 && buffRFP == 1) {
				buffPair.second = obj.since();
				runTimesRFP->push_back(buffPair);
			}
			buffRFP = (obj_lb->payloadValue("ReadyForPhysics").compare("1") == 0) ? 1 : 0;
		}
	}
	catch (const std::exception& e) {
		std::string error = "getRunTimesRFP(..) says: DATA ACCESS PROBLEM";
		throw(std::runtime_error(error));
	}
	db->closeDatabase();
	dbt->closeDatabase();
}

void getBeamTimes(unsigned long long starttime_ns, unsigned long long endtime_ns) {
	beamTimes->clear();
	beamTimeHourly->clear();
	beamTimeDaily->clear();
	beamTimesWOPresence->clear();
	hourlyBeamTimes->clear();
	try {
		dbd = cool_app.databaseService().openDatabase(dbNameDCS);
		FillState_Folder = dbd->getFolder(FillState_Name);
		TotalInt_Folder = dbd->getFolder(TotalInt_Name);
	}
	catch (const std::exception& ex) {
		std::string error = "getBeamTimes(..) says: DATABASE ACCESS PROBLEM";
		throw(std::runtime_error(error));
	}

	std::pair<unsigned long, unsigned long> buffPair;
	bool buffFlag = false;
	try {
		cool::IObjectIteratorPtr objects = FillState_Folder->browseObjects(cool::ValidityKey(starttime_ns), cool::ValidityKey(endtime_ns), cool::ChannelSelection::all());
		while (objects->goToNext()) {
			const cool::IObject& obj = objects->currentRef();
			bool sb_flag = false;
			std::string sb_string = obj.payloadValue("StableBeams");
			std::string beam_mode = obj.payloadValue("BeamMode");
			if (beam_mode.compare("STABLE BEAMS") == 0 && sb_string.compare("TRUE") == 0)
				sb_flag = true;
			if (sb_flag && !buffFlag) {
				buffPair.first = (obj.since() < starttime_ns) ? static_cast<unsigned long>(starttime_ns) : static_cast<long>(obj.since());
				buffFlag = true;
			}
			else if (!sb_flag && buffFlag) {
				buffPair.second = (obj.until() > endtime_ns) ? static_cast<unsigned long>(endtime_ns) : static_cast<unsigned long>(obj.until());
				buffFlag = false;
				beamTimes->push_back(buffPair);
			}
			else if (obj.until() > endtime_ns && sb_flag) {
				buffPair.second = endtime_ns;
				beamTimes->push_back(buffPair);
				break;
			}
		}
		auto iter_BT = beamTimes->begin();
		unsigned long long st, et;
		for (; iter_BT != beamTimes->end(); ++iter_BT) {
			st = iter_BT->first;
			et = iter_BT->second;
			unsigned long long sumOoB = 0;
			objects = TotalInt_Folder->browseObjects(cool::ValidityKey(st), cool::ValidityKey(et), cool::ChannelSelection::all());
			while (objects->goToNext()) {
				const cool::IObject& obj = objects->currentRef();
				std::string beam1 = obj.payloadValue("Beam1_BeamPresent");
				std::string beam2 = obj.payloadValue("Beam2_BeamPresent");
				if (beam1.compare("TRUE") == 0 && beam2.compare("TRUE") == 0) {
					continue;
				}
				else {
					sumOoB += (obj.until() - obj.since());
				}
			}
			beamTimesWOPresence->push_back(sumOoB);
		}
	}
	catch (const std::exception& e) {
		std::string error = "getBeamTimes(..) says: DATA ACCESS PROBLEM";
		throw(std::runtime_error(error));
	}
	dbd->closeDatabase();

	auto iterWOtimes = beamTimesWOPresence->begin();
	auto iter_BT = beamTimes->begin();
	for (; iter_BT != beamTimes->end(); ++iter_BT) {
		if ((iter_BT->second - iter_BT->first) <= *iterWOtimes) {
			//std::cout << "SB without beam presence!" << iter_BT->first << "\t" << iter_BT->second << std::endl;
			beamTimes->erase(iter_BT);
			--iter_BT;
		}
		++iterWOtimes;
	}

	if (globalStartofYear == 0)
		globalStartofYear = starttime_ns;

	//divide beam periods into hours
	for (auto& valBT : *beamTimes) {
		unsigned long long buffHrEnd = valBT.first;
		while (buffHrEnd < valBT.second) {
			hourlyBeamTimes->push_back(std::make_pair(buffHrEnd, buffHrEnd + 3'600'000'000'000));
			buffHrEnd += 3'600'000'000'000;
		}
		hourlyBeamTimes->pop_back();
		if (hourlyBeamTimes->back().second < valBT.first)
			hourlyBeamTimes->push_back(std::make_pair(valBT.first, valBT.second));
		else
			hourlyBeamTimes->push_back(std::make_pair(hourlyBeamTimes->back().second, valBT.second));
	}

	//intersect with hourly vector to get beamtime per hour
	for (auto& valHT : *hourlyTimes) {
		unsigned long long sum = 0;
		for (auto& valBT : *beamTimes) {
			if (valBT.first >= valHT.second || valBT.second <= valHT.first)
				continue;
			if (valBT.first <= valHT.first && valBT.second <= valHT.second)
				sum += valBT.second - valHT.first;
			else if (valBT.first <= valHT.first && valBT.second >= valHT.second)
				sum += valHT.second - valHT.first;
			else if (valBT.first >= valHT.first && valBT.second <= valHT.second)
				sum += valBT.second - valBT.first;
			else if (valBT.first >= valHT.first && valBT.second >= valHT.second)
				sum += valHT.second - valBT.first;
		}
		beamTimeHourly->push_back(sum);
	}

	//intersect with daily vector to get beamtime per day
	for (auto& valHT : *dailyTimes) {
		unsigned long long sum = 0;
		for (auto& valBT : *beamTimes) {
			if (valBT.first >= valHT.second || valBT.second <= valHT.first)
				continue;
			if (valBT.first <= valHT.first && valBT.second <= valHT.second)
				sum += valBT.second - valHT.first;
			else if (valBT.first <= valHT.first && valBT.second >= valHT.second)
				sum += valHT.second - valHT.first;
			else if (valBT.first >= valHT.first && valBT.second <= valHT.second)
				sum += valBT.second - valBT.first;
			else if (valBT.first >= valHT.first && valBT.second >= valHT.second)
				sum += valHT.second - valBT.first;
		}
		beamTimeDaily->push_back(sum);
	}
}

void getEfficiencies() {
	effTimes->clear();
	effTimeHourly->clear();
	effTimeDaily->clear();
	effTimeDailyRFP->clear();
	effTimesRFP->clear();
	effTimeHourlyRFP->clear();
	effTimesEndDelay->clear();
	double buffEff = 0;
	double r4p = 0;
	try {
		db = cool_app.databaseService().openDatabase(dbNameTDAQ);
		dbt = cool_app.databaseService().openDatabase(dbNameTRIGGER);
		dbm = cool_app.databaseService().openDatabase(dbNameMONITOR);
		BusyRate_Folder = dbm->getFolder(BusyRate_Name);
		LBT_Folder = dbt->getFolder(LBT_Name);
		DataTakingMode_Folder = db->getFolder(DataTakingMode_Name);
	}
	catch (const std::exception& e) {
		std::string error = "getEfficiencies(..) says: DATABASE ACCESS PROBLEM";
		throw(std::runtime_error(error));
	}
	unsigned long long sum = 0;
	const unsigned int sz = hourlyBeamTimes->size();
	const unsigned int sd = dailyTimes->size();
	std::vector<unsigned long long> sumH(sz, 0);
	std::vector<unsigned long long> sumD(sd, 0);
	unsigned long long sumRFP = 0;
	std::vector<unsigned long long> sumRFPH(sz, 0);
	std::vector<unsigned long long> sumRFPD(sd, 0);
	unsigned long long sumEndDelay = 0;
	for (auto& valBT : *beamTimes) {
		sum = 0;
		sumRFP = 0;
		sumEndDelay = 0;
		cool::IObjectIteratorPtr rl_objects = LBT_Folder->browseObjects(cool::ValidityKey(valBT.first), cool::ValidityKey(valBT.second), 0);
		while (rl_objects->goToNext()) {
			const cool::IObject& obj = rl_objects->currentRef();
			cool::UInt63 runlumi_key = atoi(obj.payloadValue("Run").c_str());
			runlumi_key = (runlumi_key << 32);
			runlumi_key += atoi(obj.payloadValue("LumiBlock").c_str());
			try {
				cool::IObjectPtr objectEff = BusyRate_Folder->findObject(cool::ValidityKey(runlumi_key), 0);
				buffEff = 1 - atof(objectEff->payloadValue("ctpcore_moni0_rate").c_str());
			}
			catch (const std::exception& e) {
				std::cout << "getEfficiencies(..) says: EFF OBJECT NOT IN DB" << std::endl;
				buffEff = 0;
			}
			try {
				cool::IObjectPtr objectR4P = DataTakingMode_Folder->findObject(cool::ValidityKey(runlumi_key), 0);
				r4p = (objectR4P->payloadValue("ReadyForPhysics").compare("1") == 0) ? 1 : 0;
			}
			catch (const std::exception& e) {
				std::cout << "getEfficiencies(..) says: RFP OBJECT NOT IN DB" << std::endl;
				r4p = 0;
			}
			sum += (obj.until() - obj.since()) * static_cast<long>(buffEff * 10000) / 10000;
			sumRFP += (obj.until() - obj.since()) * static_cast<long>(buffEff * 10000) / 10000 * static_cast<long>(r4p);
			if (static_cast<long>(r4p) == 0)
				sumEndDelay += (obj.until() - obj.since());

			for (unsigned int i = 0; i < hourlyBeamTimes->size(); i++) {
				if (obj.since() >= hourlyBeamTimes->at(i).second || obj.until() <= hourlyBeamTimes->at(i).first)
					continue;
				if (obj.since() <= hourlyBeamTimes->at(i).first && obj.until() <= hourlyBeamTimes->at(i).second) {
					sumH[i] += (obj.until() - hourlyBeamTimes->at(i).first) * static_cast<long>(buffEff * 10000) / 10000;
					sumRFPH[i] += (obj.until() - hourlyBeamTimes->at(i).first) * static_cast<long>(buffEff * 10000) / 10000 * static_cast<long>(r4p);
				}
				else if (obj.since() >= hourlyBeamTimes->at(i).first && obj.until() <= hourlyBeamTimes->at(i).second) {
					sumH[i] += (obj.until() - obj.since()) * static_cast<long>(buffEff * 10000) / 10000;
					sumRFPH[i] += (obj.until() - obj.since()) * static_cast<long>(buffEff * 10000) / 10000 * static_cast<long>(r4p);
				}
				else if (obj.since() >= hourlyBeamTimes->at(i).first && obj.until() >= hourlyBeamTimes->at(i).second) {
					sumH[i] += (hourlyBeamTimes->at(i).second - obj.since()) * static_cast<long>(buffEff * 10000) / 10000;
					sumRFPH[i] += (hourlyBeamTimes->at(i).second - obj.since()) * static_cast<long>(buffEff * 10000) / 10000 * static_cast<long>(r4p);
				}
				else if (obj.since() <= hourlyBeamTimes->at(i).first && obj.until() >= hourlyBeamTimes->at(i).second) {
					sumH[i] += (hourlyBeamTimes->at(i).second - hourlyBeamTimes->at(i).first) * static_cast<long>(buffEff * 10000) / 10000;
					sumRFPH[i] += (hourlyBeamTimes->at(i).second - hourlyBeamTimes->at(i).first) * static_cast<long>(buffEff * 10000) / 10000 * static_cast<long>(r4p);
				}
			}

			for (unsigned int i = 0; i < dailyTimes->size(); i++) {
				if (obj.since() >= dailyTimes->at(i).second || obj.until() <= dailyTimes->at(i).first)
					continue;
				unsigned long long sinceBT = (obj.since() <= valBT.first) ? valBT.first : obj.since();
				unsigned long long untilBT = (obj.until() >= valBT.second) ? valBT.second : obj.until();
				if (sinceBT <= dailyTimes->at(i).first && untilBT <= dailyTimes->at(i).second) {
					sumD[i] += (untilBT - dailyTimes->at(i).first) * static_cast<unsigned long long>(buffEff * 10000) / 10000;
					sumRFPD[i] += (untilBT - dailyTimes->at(i).first) * static_cast<unsigned long long>(buffEff * 10000) / 10000 * static_cast<long>(r4p);
				}
				else if (sinceBT >= dailyTimes->at(i).first && untilBT <= dailyTimes->at(i).second) {
					sumD[i] += (untilBT - sinceBT) * static_cast<unsigned long long>(buffEff * 10000) / 10000;
					sumRFPD[i] += (untilBT - sinceBT) * static_cast<unsigned long long>(buffEff * 10000) / 10000 * static_cast<long>(r4p);
				}
				else if (sinceBT >= dailyTimes->at(i).first && untilBT >= dailyTimes->at(i).second) {
					sumD[i] += (dailyTimes->at(i).second - sinceBT) * static_cast<unsigned long long>(buffEff * 10000) / 10000;
					sumRFPD[i] += (dailyTimes->at(i).second - sinceBT) * static_cast<unsigned long long>(buffEff * 10000) / 10000 * static_cast<long>(r4p);
				}
				else if (sinceBT <= dailyTimes->at(i).first && untilBT >= dailyTimes->at(i).second) {
					sumD[i] += (dailyTimes->at(i).second - dailyTimes->at(i).first) * static_cast<unsigned long long>(buffEff * 10000) / 10000;
					sumRFPD[i] += (dailyTimes->at(i).second - dailyTimes->at(i).first) * static_cast<unsigned long long>(buffEff * 10000) / 10000 * static_cast<long>(r4p);
				}
			}
		}
		effTimes->push_back(sum);
		effTimesRFP->push_back(sumRFP);
		effTimesEndDelay->push_back(sumEndDelay);
	}
	effTimeHourly->insert(effTimeHourly->begin(), sumH.begin(), sumH.end());
	effTimeHourlyRFP->insert(effTimeHourlyRFP->begin(), sumRFPH.begin(), sumRFPH.end());
	effTimeDaily->insert(effTimeDaily->begin(), sumD.begin(), sumD.end());
	effTimeDailyRFP->insert(effTimeDailyRFP->begin(), sumRFPD.begin(), sumRFPD.end());

	dbm->closeDatabase();
	dbt->closeDatabase();
	db->closeDatabase();
}

std::string get_date_OWL(long long nseconds, std::string mode) {
	int seconds = nseconds / 1000000000;
	OWLTime owl_date = OWLTime(seconds);
	long year = owl_date.year();
	unsigned short month = owl_date.month();
	unsigned short day = owl_date.day();
	std::string date_string = std::string(owl_date.c_str());
	size_t lastIndex = date_string.size();
	std::string time_string = date_string.substr(lastIndex - 8, lastIndex);
	std::ostringstream stm;

	if (mode.compare("OWL") == 0) {
		if (day < 10)
			stm << "0";
		stm << day << "/";
		if (month < 10)
			stm << "0";
		stm << month << "/";
		stm << year;
		stm << " ";
		stm << time_string;
	}
	else {
		stm << year;
		stm << "-";
		if (month < 10)
			stm << "0";
		stm << month << "-";
		if (day < 10)
			stm << "0";
		stm << day;
		stm << " ";
		stm << time_string;
	}

	return stm.str();
}

void processDaily(unsigned long long starttime_ns, unsigned long long endtime_ns) {
	//process efficiencies for daily plot
	TDatime T1(get_date_OWL(globalStartofYear).c_str());
	int X1 = T1.Convert();

	auto iterET = effTimeDaily->begin();
	auto iterETR = effTimeDailyRFP->begin();
	auto iterBT = beamTimeDaily->begin();
	secondsD->push_back(starttime_ns / 1'000'000'000 - X1);
	runEffD->push_back(0);
	runEffReadyD->push_back(0);
	for (auto& valDay : *dailyTimes) {
		secondsD->push_back(valDay.first / 1'000'000'000 - X1);
		runEffD->push_back(0);
		runEffReadyD->push_back(0);
		secondsD->push_back(valDay.first / 1'000'000'000 - X1);
		if (*iterBT > 0) {
			runEffD->push_back(static_cast<double>(*iterET) / static_cast<double>(*iterBT));
			runEffReadyD->push_back(static_cast<double>(*iterETR) / static_cast<double>(*iterBT));
		}
		else {
			runEffD->push_back(0);
			runEffReadyD->push_back(0);
		}
		secondsD->push_back(valDay.second / 1'000'000'000 - X1);
		if (*iterBT > 0) {
			runEffD->push_back(static_cast<double>(*iterET) / static_cast<double>(*iterBT));
			runEffReadyD->push_back(static_cast<double>(*iterETR) / static_cast<double>(*iterBT));
		}
		else {
			runEffD->push_back(0);
			runEffReadyD->push_back(0);
		}
		secondsD->push_back(valDay.second / 1'000'000'000 - X1);
		runEffD->push_back(0);
		runEffReadyD->push_back(0);
		++iterET;
		++iterETR;
		++iterBT;
	}
	secondsD->push_back(endtime_ns / 1'000'000'000 - X1);
	runEffD->push_back(0);
	runEffReadyD->push_back(0);
}

void processYearly(unsigned long long starttime_ns, unsigned long long endtime_ns) {
	//process times and efficiencies
	//all combined graphs start at the first week with beam of the year
	TDatime T1(get_date_OWL(globalStartofYear).c_str());
	int X1 = T1.Convert();

	double sumBeamTime = beamTimeY->empty() ? 0 : beamTimeY->back(),
		sumRunTime = runTimeY->empty() ? 0 : runTimeY->back(),
		sumRunTimeReady = runTimeReadyY->empty() ? 0 : runTimeReadyY->back();
	auto iterET = effTimes->begin();
	auto iterETR = effTimesRFP->begin();
	auto iterBnP = beamTimesWOPresence->begin();
	secondsY->push_back(starttime_ns / 1'000'000'000 - X1);
	beamY->push_back(0);
	runEffY->push_back(0);
	runEffReadyY->push_back(0);
	beamTimeY->push_back(sumBeamTime);
	runTimeY->push_back(sumRunTime);
	runTimeReadyY->push_back(sumRunTimeReady);
	for (auto& valBT : *beamTimes) {
		secondsY->push_back(valBT.first / 1'000'000'000 - X1);// -1);
		beamTimeY->push_back(sumBeamTime);
		runTimeY->push_back(sumRunTime);
		runTimeReadyY->push_back(sumRunTimeReady);
		beamY->push_back(0);
		runEffY->push_back(0);
		runEffReadyY->push_back(0);
		secondsY->push_back((valBT.first / 1'000'000'000 - X1));
		beamTimeY->push_back(sumBeamTime);
		runTimeY->push_back(sumRunTime);
		runTimeReadyY->push_back(sumRunTimeReady);
		beamY->push_back(1);
		runEffY->push_back(static_cast<double>(*iterET) / static_cast<double>(valBT.second - valBT.first));// - *iterBnP));
		runEffReadyY->push_back(static_cast<double>(*iterETR) / static_cast<double>(valBT.second - valBT.first));// - *iterBnP));
		secondsY->push_back((valBT.second / 1'000'000'000 - X1));
		sumBeamTime = beamTimeY->back() + static_cast<double>((valBT.second - valBT.first) / 1'000'000'000) / 3600;
		sumRunTime = runTimeY->back() + static_cast<double>(*iterET / 1'000'000'000) / 3600;
		sumRunTimeReady = runTimeReadyY->back() + static_cast<double>(*iterETR / 1'000'000'000) / 3600;
		beamTimeY->push_back(sumBeamTime);
		runTimeY->push_back(sumRunTime);
		runTimeReadyY->push_back(sumRunTimeReady);
		beamY->push_back(1);
		runEffY->push_back(static_cast<double>(*iterET) / static_cast<double>(valBT.second - valBT.first));
		runEffReadyY->push_back(static_cast<double>(*iterETR) / static_cast<double>(valBT.second - valBT.first));
		secondsY->push_back(valBT.second / 1'000'000'000 - X1);// +1);
		beamTimeY->push_back(sumBeamTime);
		runTimeY->push_back(sumRunTime);
		runTimeReadyY->push_back(sumRunTimeReady);
		beamY->push_back(0);
		runEffY->push_back(0);
		runEffReadyY->push_back(0);
		++iterET;
		++iterETR;
		++iterBnP;
	}
	secondsY->push_back(endtime_ns / 1'000'000'000 - X1);
	beamTimeY->push_back(sumBeamTime);
	runTimeY->push_back(sumRunTime);
	runTimeReadyY->push_back(sumRunTimeReady);
	beamY->push_back(0);
	runEffY->push_back(0);
	runEffReadyY->push_back(0);
}

void processWeekly(unsigned long long starttime_ns, unsigned long long endtime_ns) {
	//process info for weekly plot
	TDatime T1(get_date_OWL(globalStartofYear).c_str());
	int X1 = T1.Convert();

	auto iterET = effTimeDaily->begin();
	auto iterETR = effTimeDailyRFP->begin();
	double sumBT = 0, sumET = 0, sumETR = 0;
	for (auto& valBT : *beamTimeDaily) {
		sumBT += static_cast<double>(valBT / 1'000'000'000) / 3600;
		sumET += static_cast<double>(*iterET / 1'000'000'000) / 3600;
		sumETR += static_cast<double>(*iterETR / 1'000'000'000) / 3600;
		++iterET;
		++iterETR;
	}
	if (!secondsW->empty() && secondsW->back() < (starttime_ns / 1'000'000'000 - X1)) {
		secondsW->push_back(secondsW->back());
		beamTimeW->push_back(0);
		runEffW->push_back(0);
		runEffReadyW->push_back(0);
		secondsW->push_back(starttime_ns / 1'000'000'000 - X1);
		beamTimeW->push_back(0);
		runEffW->push_back(0);
		runEffReadyW->push_back(0);
	}
	secondsW->push_back(starttime_ns / 1'000'000'000 - X1);
	secondsW->push_back(endtime_ns / 1'000'000'000 - X1);
	beamTimeW->push_back(sumBT);
	beamTimeW->push_back(sumBT);
	runEffW->push_back(sumET / sumBT);
	runEffW->push_back(sumET / sumBT);
	runEffReadyW->push_back(sumETR / sumBT);
	runEffReadyW->push_back(sumETR / sumBT);
}

void processWeek(unsigned long long starttime_ns, unsigned long long endtime_ns) {
	//method to process the week-by-week data
	//we need beam time periods (start-finish), run time periods (start-finish) and run efficiency per hour during beam time
	//this will be used to make the week by week plots (old java charts)
	seconds_beams->clear();
	seconds_runs->clear();
	seconds_eff->clear();
	run_week->clear();
	beam_week->clear();
	runEffH->clear();
	runEffReadyH->clear();

	TDatime T1(get_date_OWL(starttime_ns).c_str());
	int X1 = T1.Convert();

	//beam parameters
	seconds_beams->push_back(starttime_ns / 1'000'000'000 - X1);
	beam_week->push_back(0);
	for (auto& valBT : *beamTimes) {
		seconds_beams->push_back(valBT.first / 1'000'000'000 - X1);
		beam_week->push_back(0);
		seconds_beams->push_back(valBT.first / 1'000'000'000 - X1);
		beam_week->push_back(1);
		seconds_beams->push_back(valBT.second / 1'000'000'000 - X1);
		beam_week->push_back(1);
		seconds_beams->push_back(valBT.second / 1'000'000'000 - X1);
		beam_week->push_back(0);
	}
	seconds_beams->push_back(endtime_ns / 1'000'000'000 - X1);
	beam_week->push_back(0);

	//run parameters
	seconds_runs->push_back(starttime_ns / 1'000'000'000 - X1);
	run_week->push_back(0);
	for (auto& valRT : *runTimes) {
		seconds_runs->push_back(valRT.first / 1'000'000'000 - X1);
		run_week->push_back(0);
		seconds_runs->push_back(valRT.first / 1'000'000'000 - X1);
		run_week->push_back(0.5);
		seconds_runs->push_back(valRT.second / 1'000'000'000 - X1);
		run_week->push_back(0.5);
		seconds_runs->push_back(valRT.second / 1'000'000'000 - X1);
		run_week->push_back(0);
	}
	seconds_runs->push_back(endtime_ns / 1'000'000'000 - X1);
	run_week->push_back(0);

	//efficiency parameters
	auto iterET = effTimeHourly->begin();
	auto iterETR = effTimeHourlyRFP->begin();
	seconds_eff->push_back(hourlyBeamTimes->front().first / 1'000'000'000 - X1);
	runEffH->push_back(0);
	runEffReadyH->push_back(0);
	for (auto& valHr : *hourlyBeamTimes) {
		if (seconds_eff->back() != (valHr.first / 1'000'000'000 - X1)) {
			seconds_eff->push_back(seconds_eff->back());
			runEffH->push_back(0);
			runEffReadyH->push_back(0);
			seconds_eff->push_back(valHr.first / 1'000'000'000 - X1);
			runEffH->push_back(0);
			runEffReadyH->push_back(0);
		}
		seconds_eff->push_back(valHr.first / 1'000'000'000 - X1);
		runEffH->push_back(static_cast<double>(*iterET) / static_cast<double>(valHr.second - valHr.first));
		runEffReadyH->push_back(static_cast<double>(*iterETR) / static_cast<double>(valHr.second - valHr.first));
		seconds_eff->push_back(valHr.second / 1'000'000'000 - X1);
		runEffH->push_back(static_cast<double>(*iterET) / static_cast<double>(valHr.second - valHr.first));
		runEffReadyH->push_back(static_cast<double>(*iterETR) / static_cast<double>(valHr.second - valHr.first));
		++iterET;
		++iterETR;
	}
	seconds_eff->push_back(hourlyBeamTimes->back().second / 1'000'000'000 - X1);
	runEffH->push_back(0);
	runEffReadyH->push_back(0);
}

void partYear(std::vector<std::string>* weeksString) {
	weeksString->clear();
	std::string yearInput = std::to_string(yearToDo);
	std::string firstDayInput;
	bool isBisect = false;
	bool isCurrentY = false;

	OWLTime currentTime;
	std::string currentYear = std::to_string(currentTime.year());
	if (yearToDo == 0) {
		yearInput = currentYear;
		yearToDo = currentTime.year();
		isCurrentY = true;
	}
	else if (currentYear.compare(yearInput) == 0)
		isCurrentY = true;

	//automatically find the 1st day-in-week of the year
	int d = ((yearToDo - 1) + (yearToDo - 1) / 4 - (yearToDo - 1) / 100 + (yearToDo - 1) / 400 + 1) % 7;
	int dayInWeek = ((d - 1) >= 0 ? d - 1 : 6) + 1;

	//check if it's a bisect (leap) year
	if ((atoi(yearInput.c_str()) % 4) == 0)
		isBisect = true;

	int dayInMonth = 1;
	int monthInYear = 1;
	std::string date;

	int daysInYear = (isBisect) ? 366 : 365;
	weeksString->push_back("01/01/" + yearInput);
	while (daysInYear > 0) {
		int maxDays = (monthInYear == 1) ? 31 : (monthInYear == 2 && !isBisect) ? 28 : (monthInYear == 2 && isBisect) ? 29 : (monthInYear == 3) ? 31 : (monthInYear == 4) ? 30 : (monthInYear == 5) ? 31 : (monthInYear == 6) ? 30 : (monthInYear == 7) ? 31 : (monthInYear == 8) ? 31 : (monthInYear == 9) ? 30 : (monthInYear == 10) ? 31 : (monthInYear == 11) ? 30 : 31;
		if (dayInMonth > maxDays) {
			dayInMonth = 1;
			monthInYear++;
		}
		if (isCurrentY && currentTime.month() == monthInYear && currentTime.day() < dayInMonth)
			break;
		if (dayInWeek == 8) {
			date = (dayInMonth < 10) ? "0" + std::to_string(dayInMonth) : std::to_string(dayInMonth);
			date += "/";
			date += (monthInYear < 10) ? "0" + std::to_string(monthInYear) : std::to_string(monthInYear);
			date += "/" + yearInput;
			weeksString->push_back(date);
			dayInWeek -= 7;
			date.clear();
		}
		dayInWeek++;
		dayInMonth++;
		daysInYear--;
	}
	if (weeksString->back().find("31/12/") == std::string::npos && !isCurrentY)
		weeksString->push_back("31/12/" + yearInput);

	return;
}

TStyle* getAtlasStyle() {
	//SET ATLAS STYLE
	TStyle* atlasStyle = new TStyle("ATLAS", "Atlas style");
	// use plain black on white colors
	Int_t icol = 0; // WHITE
	atlasStyle->SetFrameBorderMode(icol);
	atlasStyle->SetFrameFillColor(icol);
	atlasStyle->SetCanvasBorderMode(icol);
	atlasStyle->SetCanvasColor(icol);
	atlasStyle->SetPadBorderMode(icol);
	atlasStyle->SetPadColor(icol);
	atlasStyle->SetStatColor(icol);
	// set the paper & margin sizes
	atlasStyle->SetPaperSize(20, 26);
	// set margin sizes
	atlasStyle->SetPadTopMargin(0.05);
	atlasStyle->SetPadRightMargin(0.05);
	atlasStyle->SetPadBottomMargin(0.16);
	atlasStyle->SetPadLeftMargin(0.16);
	// set title offsets (for axis label)
	atlasStyle->SetTitleXOffset(1.4);
	atlasStyle->SetTitleYOffset(1.4);
	// use large fonts
	Int_t font = 42; // Helvetica
	Double_t tsize = 0.05;
	atlasStyle->SetTextFont(font);
	atlasStyle->SetTextSize(tsize);
	atlasStyle->SetLabelFont(font, "x");
	atlasStyle->SetTitleFont(font, "x");
	atlasStyle->SetLabelFont(font, "y");
	atlasStyle->SetTitleFont(font, "y");
	atlasStyle->SetLabelFont(font, "z");
	atlasStyle->SetTitleFont(font, "z");
	atlasStyle->SetLabelSize(tsize, "x");
	atlasStyle->SetTitleSize(tsize, "x");
	atlasStyle->SetLabelSize(tsize, "y");
	atlasStyle->SetTitleSize(tsize, "y");
	atlasStyle->SetLabelSize(tsize, "z");
	atlasStyle->SetTitleSize(tsize, "z");
	// use bold lines and markers
	atlasStyle->SetMarkerStyle(20);
	atlasStyle->SetMarkerSize(1.2);
	atlasStyle->SetHistLineWidth(2.);
	atlasStyle->SetLineStyleString(2, "[12 12]"); // postscript dashes
	// get rid of error bar caps
	atlasStyle->SetEndErrorSize(0.);
	// do not display any of the standard histogram decorations
	atlasStyle->SetOptTitle(0);
	atlasStyle->SetOptStat(0);
	atlasStyle->SetOptFit(0);
	// put tick marks on top and RHS of plots
	atlasStyle->SetPadTickX(1);
	atlasStyle->SetPadTickY(1);
	return atlasStyle;
}

void drawYearly() {
	TDatime T1(get_date_OWL(globalStartofYear).c_str());
	std::string year = get_date_OWL(globalStartofYear).substr(0, 4);
	std::string Xtitle = "Date in " + year;
	int X1 = T1.Convert();
	double timeMax = static_cast<double>(secondsY->back());

	TCanvas* cEff = new TCanvas("cEff", "daq efficiency", 50, 50, 896, 572);
	beamGraph = new TGraph(secondsY->size());
	runEffGraph = new TGraph(secondsY->size());
	runEffReadyGraph = new TGraph(secondsY->size());
	beamTimeGraph = new TGraph(secondsY->size() + 1);
	runTimeGraph = new TGraph(secondsY->size() + 1);
	runTimeReadyGraph = new TGraph(secondsY->size() + 1);
	int i = 0;
	auto iterRE = runEffY->begin();
	auto iterRER = runEffReadyY->begin();
	auto iterBT = beamTimeY->begin();
	auto iterRT = runTimeY->begin();
	auto iterRTR = runTimeReadyY->begin();
	for (auto& valSec : *secondsY) {
		beamGraph->SetPoint(i, valSec, beamY->at(i));
		runEffGraph->SetPoint(i, valSec, *iterRE);
		runEffReadyGraph->SetPoint(i, valSec, *iterRER);
		beamTimeGraph->SetPoint(i, valSec, *iterBT);
		runTimeGraph->SetPoint(i, valSec, *iterRT);
		runTimeReadyGraph->SetPoint(i, valSec, *iterRTR);
		i++;
		++iterRE;
		++iterRER;
		++iterBT;
		++iterRT;
		++iterRTR;
	}
	beamTimeGraph->SetPoint(i, secondsY->back(), 0);
	runTimeGraph->SetPoint(i, secondsY->back(), 0);
	runTimeReadyGraph->SetPoint(i, secondsY->back(), 0);

	//CREATE EFF PLOT
	beamGraph->SetName("BeamGraph");
	beamGraph->GetXaxis()->SetTimeDisplay(1);
	beamGraph->GetXaxis()->SetTimeOffset(X1);
	beamGraph->GetXaxis()->SetTimeFormat("%d/%m");
	beamGraph->GetXaxis()->SetTitle(Xtitle.c_str());
	beamGraph->GetYaxis()->SetTitle("Run Efficiency");
	runEffGraph->SetName("RunEffGraph");
	runEffGraph->GetXaxis()->SetTimeDisplay(1);
	runEffGraph->GetXaxis()->SetTimeOffset(X1);
	runEffGraph->GetXaxis()->SetTimeFormat("%d/%m");
	runEffGraph->GetXaxis()->SetTitle(Xtitle.c_str());
	runEffGraph->GetYaxis()->SetTitle("Run Efficiency");
	runEffReadyGraph->SetName("RunEffReadyGraph");
	runEffReadyGraph->GetXaxis()->SetTimeDisplay(1);
	runEffReadyGraph->GetXaxis()->SetTimeOffset(X1);
	runEffReadyGraph->GetXaxis()->SetTimeFormat("%d/%m");
	runEffReadyGraph->GetXaxis()->SetTitle(Xtitle.c_str());
	runEffReadyGraph->GetYaxis()->SetTitle("Run Efficiency");

	TH2F* htY = new TH2F("htY", " ", 3000, 0., timeMax, 10, 0.40, 1.10);
	htY->GetXaxis()->SetTimeOffset(X1);
	htY->GetXaxis()->SetTimeFormat("%d/%m");
	htY->GetXaxis()->SetTitle(Xtitle.c_str());
	htY->GetYaxis()->SetTitle("Run Efficiency");
	htY->GetXaxis()->SetTitleOffset(1.2);
	htY->GetXaxis()->SetTimeDisplay(1);
	htY->GetXaxis()->SetNdivisions(8);

	//CREATE EFF PLOT
	gROOT->SetStyle("ATLAS");
	gROOT->ForceStyle();
	beamGraph->SetFillColor(kYellow);
	beamGraph->SetLineWidth(1);
	runEffGraph->SetFillColor(kGreen);
	runEffGraph->SetLineWidth(1);
	runEffReadyGraph->SetFillColor(kGray + 1);
	runEffReadyGraph->SetLineWidth(1);
	htY->GetXaxis()->SetNdivisions(-407);
	htY->GetXaxis()->SetLabelOffset(0.015);
	htY->Draw();
	beamGraph->Draw("f");
	runEffGraph->Draw("f");
	runEffReadyGraph->Draw("f");
	TLegend* leg = new TLegend(0.20, 0.84, 0.70, 0.89);
	leg->AddEntry(runEffGraph, "Run Eff", "f");
	leg->AddEntry(runEffReadyGraph, "Run Eff Physics", "f");
	leg->AddEntry(beamGraph, "Beam", "f");
	leg->SetShadowColor(0);
	leg->SetNColumns(3);
	leg->SetFillColor(0);
	leg->SetBorderSize(0);
	leg->Draw();
	Color_t color = 1;
	TLatex l;
	l.SetNDC();
	l.SetTextFont(72);
	l.SetTextColor(color);
	Double_t x = 0.49;
	Double_t y = 0.90;
	l.DrawLatex(x, y, "ATLAS");
	l.Draw();
	TLatex ll;
	ll.SetNDC();
	ll.SetTextColor(color);
	Double_t x1 = 0.60;
	Double_t y3 = 0.90;
	ll.DrawLatex(x1, y3, "DAQ Operations");
	ll.Draw();
	gPad->RedrawAxis();
	gPad->SetTicks(0, 0);
	cEff->Print("eff.png");
	//----------------------------------------------
	delete leg;
	delete cEff;

	//CREATE TIME PLOT
	TCanvas* cTime = new TCanvas("cTime", "daq efficiency", 50, 50, 796, 572);
	gROOT->SetStyle("ATLAS");
	gROOT->ForceStyle();
	beamTimeGraph->SetName("BeamTimeGraph");
	beamTimeGraph->GetXaxis()->SetTimeDisplay(1);
	beamTimeGraph->GetXaxis()->SetLabelOffset(0.015);
	beamTimeGraph->GetXaxis()->SetTimeDisplay(1);
	beamTimeGraph->GetXaxis()->SetTimeOffset(X1);
	beamTimeGraph->GetXaxis()->SetTimeFormat("%d/%m");
	beamTimeGraph->GetXaxis()->SetTitle(Xtitle.c_str());
	beamTimeGraph->GetYaxis()->SetTitle("Time (hours)");
	runTimeGraph->SetName("RunTimeGraph");
	runTimeGraph->GetXaxis()->SetTimeDisplay(1);
	runTimeGraph->GetXaxis()->SetTimeOffset(X1);
	runTimeGraph->GetXaxis()->SetTimeFormat("%d/%m");
	runTimeGraph->GetXaxis()->SetTitle(Xtitle.c_str());
	runTimeGraph->GetYaxis()->SetTitle("Time (hours)");
	runTimeReadyGraph->SetName("RunTimeReadyGraph");
	runTimeReadyGraph->GetXaxis()->SetTimeDisplay(1);
	runTimeReadyGraph->GetXaxis()->SetTimeOffset(X1);
	runTimeReadyGraph->GetXaxis()->SetTimeFormat("%d/%m");
	runTimeReadyGraph->GetXaxis()->SetTitle(Xtitle.c_str());
	runTimeReadyGraph->GetYaxis()->SetTitle("Time (hours)");
	beamTimeGraph->SetFillColor(kYellow);
	beamTimeGraph->SetLineWidth(1);
	beamTimeGraph->SetLineColor(kBlack);
	runTimeGraph->SetFillColor(kGreen);
	runTimeGraph->SetLineWidth(1);
	runTimeReadyGraph->SetFillColor(kGray);
	runTimeReadyGraph->SetLineWidth(1);
	runTimeReadyGraph->SetLineColor(kBlack);
	beamTimeGraph->GetXaxis()->SetNdivisions(-407);
	beamTimeGraph->Draw("af");
	beamTimeGraph->Draw("l");
	runTimeGraph->Draw("f");
	runTimeGraph->Draw("l");
	runTimeReadyGraph->Draw("f");
	runTimeReadyGraph->Draw("l");
	leg = new TLegend(0.22, 0.70, 0.37, 0.88);
	leg->AddEntry(beamTimeGraph, "Beam", "f");
	leg->AddEntry(runTimeGraph, "Run", "f");
	leg->AddEntry(runTimeReadyGraph, "Physics", "f");
	leg->SetFillColor(0);
	leg->SetShadowColor(0);
	leg->SetBorderSize(0);
	leg->Draw();
	l.SetNDC();
	l.SetTextFont(72);
	l.SetTextColor(color);
	l.DrawLatex(x, y, "ATLAS");
	l.Draw();
	ll.SetNDC();
	ll.SetTextColor(color);
	x1 = 0.615;
	y3 = 0.90;
	ll.DrawLatex(x1, y3, "DAQ Operations");
	ll.Draw();
	gPad->RedrawAxis();
	gPad->SetTicks(0, 0);
	cTime->Print("time.png");
	//----------------------------------------------
	delete leg;
	delete cTime;
}

void drawDaily() {
	TDatime T1(get_date_OWL(globalStartofYear).c_str());
	std::string year = get_date_OWL(globalStartofYear).substr(0, 4);
	std::string Xtitle = "Date in " + year;
	int X1 = T1.Convert();
	double timeMax = static_cast<double>(secondsD->back());

	TCanvas* cDaily = new TCanvas("cDaily", "daq efficiency", 50, 50, 796, 572);
	runEffGraphD = new TGraph(secondsD->size());
	runEffReadyGraphD = new TGraph(secondsD->size());
	int i = 0;
	auto iterRE = runEffD->begin();
	auto iterRER = runEffReadyD->begin();
	for (auto& valSec : *secondsD) {
		runEffGraphD->SetPoint(i, valSec, *iterRE);
		runEffReadyGraphD->SetPoint(i, valSec, *iterRER);
		i++;
		++iterRE;
		++iterRER;
	}

	runEffGraphD->SetName("RunEffGraph");
	runEffGraphD->GetXaxis()->SetTimeDisplay(1);
	runEffGraphD->GetXaxis()->SetTimeOffset(X1);
	runEffGraphD->GetXaxis()->SetTimeFormat("%d/%m");
	runEffGraphD->GetXaxis()->SetTitle(Xtitle.c_str());
	runEffGraphD->GetYaxis()->SetTitle("Run Efficiency");
	runEffReadyGraphD->SetName("RunEffReadyGraph");
	runEffReadyGraphD->GetXaxis()->SetTimeDisplay(1);
	runEffReadyGraphD->GetXaxis()->SetTimeOffset(X1);
	runEffReadyGraphD->GetXaxis()->SetTimeFormat("%d/%m");
	runEffReadyGraphD->GetXaxis()->SetTitle(Xtitle.c_str());
	runEffReadyGraphD->GetYaxis()->SetTitle("Run Efficiency");
	TH2F* htD = new TH2F("htD", " ", 3000, 0., timeMax, 10, 0.40, 1.10);
	htD->GetXaxis()->SetTimeOffset(X1);
	htD->GetXaxis()->SetTimeFormat("%d/%m");
	htD->GetXaxis()->SetTitle(Xtitle.c_str());
	htD->GetYaxis()->SetTitle("Run Efficiency");
	htD->GetXaxis()->SetTitleOffset(1.2);
	htD->GetXaxis()->SetTimeDisplay(1);
	htD->GetXaxis()->SetNdivisions(8);

	gROOT->SetStyle("ATLAS");
	gROOT->ForceStyle();
	runEffGraphD->SetFillColor(kGreen);
	runEffGraphD->SetLineWidth(1);
	runEffReadyGraphD->SetFillColor(kGray + 1);
	runEffReadyGraphD->SetLineWidth(1);
	htD->GetXaxis()->SetNdivisions(-407);
	htD->GetXaxis()->SetLabelOffset(0.015);
	htD->Draw();
	runEffGraphD->Draw("f");
	runEffReadyGraphD->Draw("f");
	TLegend* leg = new TLegend(0.20, 0.84, 0.70, 0.89);
	leg->AddEntry(runEffGraphD, "Run Eff", "f");
	leg->AddEntry(runEffReadyGraphD, "Run Eff Physics", "f");
	leg->SetShadowColor(0);
	leg->SetNColumns(3);
	leg->SetFillColor(0);
	leg->SetBorderSize(0);
	leg->Draw();
	Color_t color = 1;
	TLatex l;
	l.SetNDC();
	l.SetTextFont(72);
	l.SetTextColor(color);
	Double_t x = 0.49;
	Double_t y = 0.90;
	l.DrawLatex(x, y, "ATLAS");
	l.Draw();
	TLatex ll;
	ll.SetNDC();
	ll.SetTextColor(color);
	Double_t x1 = 0.60;
	Double_t y3 = 0.90;
	ll.DrawLatex(x1, y3, "DAQ Operations");
	ll.Draw();
	gPad->RedrawAxis();
	gPad->SetTicks(0, 0);
	cDaily->Print("daily.png");
	delete leg;
	delete cDaily;
}

void drawWeekly() {
	TDatime T1(get_date_OWL(globalStartofYear).c_str());
	std::string year = get_date_OWL(globalStartofYear).substr(0, 4);
	std::string Xtitle = "Date in " + year;
	int X1 = T1.Convert();
	double timeMax = static_cast<double>(secondsW->back());
	double beamTimeMax = *std::max_element(beamTimeW->begin(), beamTimeW->end());

	TH2F* ht = new TH2F("ht", " ", 3000, 0., timeMax + 86400, 10, 0.70, 1.05);
	ht->GetXaxis()->SetTimeOffset(X1);
	ht->GetXaxis()->SetTimeFormat("%d/%m");
	ht->GetXaxis()->SetTitle(Xtitle.c_str());
	ht->GetYaxis()->SetTitle("Run Efficiency");
	ht->GetXaxis()->SetTitleOffset(1.2);
	ht->GetXaxis()->SetTimeDisplay(1);

	TGaxis* axis_hours = new TGaxis(timeMax + 86400, 0.7, timeMax + 86400, 1.05, 0, 0.35 * beamTimeMax / 0.3, 50100, "+L");
	axis_hours->SetTitle("Beam Time (hours)");
	axis_hours->SetLabelSize(0.05);
	axis_hours->SetLabelFont(42);
	//axis_hours->SetTitleOffset(1);
	axis_hours->SetTitleSize(0.045);
	axis_hours->SetTitleFont(42);
	axis_hours->SetLineColor(kRed);
	axis_hours->SetLabelColor(kRed);
	axis_hours->SetTitleColor(kRed);
	axis_hours->SetNdivisions(1005);
	axis_hours->SetName("axis_hours");

	TCanvas* cWeekly = new TCanvas("cWeekly", "daq efficiency", 50, 50, 800, 600);
	cWeekly->SetRightMargin(0.09);
	cWeekly->SetLeftMargin(0.15);
	cWeekly->SetBottomMargin(0.15);
	beamTimeGraphW = new TGraph(secondsW->size() + 2);
	runEffGraphW = new TGraph(secondsW->size() + 2);
	runEffReadyGraphW = new TGraph(secondsW->size() + 2);
	runEffGraphW->SetPoint(0, secondsW->front(), 0);
	runEffReadyGraphW->SetPoint(0, secondsW->front(), 0);
	beamTimeGraphW->SetPoint(0, secondsW->front(), 0);
	int i = 1;
	auto iterRE = runEffW->begin();
	auto iterRER = runEffReadyW->begin();
	auto iterBT = beamTimeW->begin();
	for (auto& valSec : *secondsW) {
		runEffGraphW->SetPoint(i, valSec, *iterRE);
		runEffReadyGraphW->SetPoint(i, valSec, *iterRER);
		beamTimeGraphW->SetPoint(i, valSec, 0.7 + 0.3 * *iterBT / beamTimeMax);
		i++;
		++iterRE;
		++iterRER;
		++iterBT;
	}
	runEffGraphW->SetPoint(i, secondsW->back(), 0);
	runEffReadyGraphW->SetPoint(i, secondsW->back(), 0);
	beamTimeGraphW->SetPoint(i, secondsW->back(), 0);

	gROOT->SetStyle("ATLAS");
	gROOT->ForceStyle();
	ht->GetXaxis()->SetNdivisions(-407);
	ht->GetXaxis()->SetLabelOffset(0.015);
	ht->Draw();
	runEffGraphW->SetName("RunTimeGraph");
	runEffGraphW->GetXaxis()->SetTimeDisplay(1);
	runEffGraphW->GetXaxis()->SetTimeOffset(X1);
	runEffGraphW->GetXaxis()->SetTimeFormat("%d/%m");
	runEffGraphW->GetXaxis()->SetTitle(Xtitle.c_str());
	runEffGraphW->GetYaxis()->SetTitle("Time (hours)");
	runEffGraphW->SetFillColor(kGreen);
	runEffReadyGraphW->SetName("RunTimeReadyGraph");
	runEffReadyGraphW->GetXaxis()->SetTimeDisplay(1);
	runEffReadyGraphW->GetXaxis()->SetTimeOffset(X1);
	runEffReadyGraphW->GetXaxis()->SetTimeFormat("%d/%m");
	runEffReadyGraphW->GetXaxis()->SetTitle(Xtitle.c_str());
	runEffReadyGraphW->GetYaxis()->SetTitle("Time (hours)");
	runEffReadyGraphW->SetFillColor(kGray + 1);
	runEffGraphW->Draw("f");
	runEffGraphW->Draw("l");
	runEffReadyGraphW->Draw("f");
	runEffReadyGraphW->Draw("l");
	beamTimeGraphW->GetXaxis()->SetTimeDisplay(1);
	beamTimeGraphW->GetXaxis()->SetTimeOffset(X1);
	beamTimeGraphW->GetXaxis()->SetTimeFormat("%d/%m");
	beamTimeGraphW->GetXaxis()->SetTitle(Xtitle.c_str());
	beamTimeGraphW->GetYaxis()->SetTitle("Run Efficiency");
	beamTimeGraphW->SetLineWidth(3);
	beamTimeGraphW->SetLineColor(kRed);
	beamTimeGraphW->GetXaxis()->SetNdivisions(-407);
	beamTimeGraphW->Draw("l");
	axis_hours->Draw();

	TLegend* leg = new TLegend(0.20, 0.84, 0.80, 0.89);
	leg->AddEntry(runEffGraphW, "Run Eff", "f");
	leg->AddEntry(runEffReadyGraphW, "Run Eff Physics", "f");
	leg->AddEntry(beamTimeGraphW, "Beam Time", "l");
	leg->SetShadowColor(0);
	leg->SetNColumns(3);
	leg->SetFillColor(0);
	leg->SetBorderSize(0);
	leg->Draw();
	Color_t color = 1;
	TLatex l;
	l.SetNDC();
	l.SetTextFont(72);
	l.SetTextColor(color);
	Double_t x = 0.49;
	Double_t y = 0.90;
	l.DrawLatex(x, y, "ATLAS");
	l.Draw();
	TLatex ll;
	ll.SetNDC();
	ll.SetTextColor(color);
	Double_t x1 = 0.615;
	Double_t y3 = 0.90;
	ll.DrawLatex(x1, y3, "DAQ Operations");
	ll.Draw();
	gPad->RedrawAxis();
	gPad->SetTicks(0, 0);
	cWeekly->Print("weekly.png");
	delete leg;
	delete cWeekly;
}

void drawWeek(unsigned long long starttime_ns, unsigned long long endtime_ns) {
	TDatime T1(get_date_OWL(starttime_ns).c_str());
	std::string year = get_date_OWL(starttime_ns).substr(0, 4);
	std::string Xtitle = "Time";
	std::string Ytitle = "Efficiency";
	int X1 = T1.Convert();
	double timeMax = static_cast<double>(seconds_beams->back());

	beamTimeGraphWeek = new TGraph(seconds_beams->size());
	runEffGraphWeek = new TGraph(seconds_eff->size());
	runEffReadyGraphWeek = new TGraph(seconds_eff->size());
	runTimeGraphWeek = new TGraph(seconds_runs->size() + 1);
	int i = 0;
	auto iterBeam = beam_week->begin();
	for (auto& valBT : *seconds_beams) {
		beamTimeGraphWeek->SetPoint(i, valBT, *iterBeam);
		i++;
		++iterBeam;
	}
	i = 0;
	auto iterRun = run_week->begin();
	for (auto& valRT : *seconds_runs) {
		runTimeGraphWeek->SetPoint(i, valRT, *iterRun);
		i++;
		++iterRun;
	}
	i = 0;
	auto iterET = runEffH->begin();
	auto iterETR = runEffReadyH->begin();
	for (auto& valE : *seconds_eff) {
		runEffGraphWeek->SetPoint(i, valE, *iterET);
		runEffReadyGraphWeek->SetPoint(i, valE, *iterETR);
		i++;
		++iterET;
		++iterETR;
	}

	TCanvas* cWeek = new TCanvas("cWeek", "daq efficiency", 50, 50, 1000, 380);
	cWeek->SetFrameFillColor(kGray);
	cWeek->SetGrid();
	gROOT->SetStyle("ATLAS");
	gROOT->ForceStyle();

	TH2F* htW = new TH2F("htW", " ", 3000, 0., timeMax, 10, 0.0, 1.10);
	htW->GetXaxis()->SetTimeOffset(X1);
	htW->GetXaxis()->SetTimeFormat("%d/%m");
	htW->GetXaxis()->SetTitle(Xtitle.c_str());
	htW->GetYaxis()->SetTitle(Ytitle.c_str());
	htW->GetXaxis()->SetTitleOffset(1.2);
	htW->GetXaxis()->SetTimeDisplay(1);
	htW->GetXaxis()->SetNdivisions(8);
	htW->GetXaxis()->SetNdivisions(-407);
	htW->GetXaxis()->SetLabelOffset(0.015);
	htW->Draw();

	beamTimeGraphWeek->GetXaxis()->SetTimeDisplay(1);
	beamTimeGraphWeek->GetXaxis()->SetTimeOffset(X1);
	beamTimeGraphWeek->GetXaxis()->SetTimeFormat("%d/%m");
	beamTimeGraphWeek->GetXaxis()->SetTitle(Xtitle.c_str());
	beamTimeGraphWeek->GetYaxis()->SetTitle(Ytitle.c_str());
	beamTimeGraphWeek->SetLineWidth(1);
	beamTimeGraphWeek->SetLineColor(kBlue);
	beamTimeGraphWeek->GetXaxis()->SetNdivisions(-407);
	beamTimeGraphWeek->Draw("l");

	runTimeGraphWeek->GetXaxis()->SetTimeDisplay(1);
	runTimeGraphWeek->GetXaxis()->SetTimeOffset(X1);
	runTimeGraphWeek->GetXaxis()->SetTimeFormat("%d/%m");
	runTimeGraphWeek->GetXaxis()->SetTitle(Xtitle.c_str());
	runTimeGraphWeek->SetLineWidth(1);
	runTimeGraphWeek->SetLineColor(kGreen);
	runTimeGraphWeek->Draw("l");

	runEffGraphWeek->GetXaxis()->SetTimeDisplay(1);
	runEffGraphWeek->GetXaxis()->SetTimeOffset(X1);
	runEffGraphWeek->GetXaxis()->SetTimeFormat("%d/%m");
	runEffGraphWeek->GetXaxis()->SetTitle(Xtitle.c_str());
	runEffGraphWeek->SetLineWidth(1);
	runEffGraphWeek->SetLineColor(kRed);
	runEffGraphWeek->Draw("l");

	TLegend* leg = new TLegend(0.20, 0.012, 0.80, 0.052);
	leg->AddEntry(runEffGraphWeek, "Run Eff", "l");
	leg->AddEntry(runTimeGraphWeek, "Run Time", "l");
	leg->AddEntry(beamTimeGraphWeek, "Beam Time", "l");
	leg->SetShadowColor(0);
	leg->SetNColumns(3);
	leg->SetFillColor(0);
	leg->SetBorderSize(0);
	leg->Draw();

	gPad->RedrawAxis();
	gPad->SetTicks(0, 0);

	std::string picTitle = get_date_OWL(starttime_ns).substr(8, 2) + get_date_OWL(starttime_ns).substr(5, 2) + "_" + get_date_OWL(endtime_ns).substr(8, 2) + get_date_OWL(endtime_ns).substr(5, 2) + ".png";
	cWeek->Print(picTitle.c_str());

	cWeek->Clear();
	delete cWeek;
	delete leg;

	TCanvas* cWeekReady = new TCanvas("cWeekReady", "daq efficiency", 50, 50, 1000, 380);
	cWeekReady->SetFrameFillColor(kGray);
	cWeekReady->SetGrid();
	gROOT->SetStyle("ATLAS");
	gROOT->ForceStyle();

	htW->Draw();
	beamTimeGraphWeek->Draw("l");
	runTimeGraphWeek->Draw("l");

	runEffReadyGraphWeek->GetXaxis()->SetTimeDisplay(1);
	runEffReadyGraphWeek->GetXaxis()->SetTimeOffset(X1);
	runEffReadyGraphWeek->GetXaxis()->SetTimeFormat("%d/%m");
	runEffReadyGraphWeek->GetXaxis()->SetTitle(Xtitle.c_str());
	runEffReadyGraphWeek->GetYaxis()->SetTitle("Run Eff RfP");
	runEffReadyGraphWeek->SetLineWidth(1);
	runEffReadyGraphWeek->SetLineColor(kRed);
	runEffReadyGraphWeek->Draw("l");

	leg = new TLegend(0.20, 0.012, 0.80, 0.052);
	leg->AddEntry(runEffGraphWeek, "Run Eff", "l");
	leg->AddEntry(runTimeGraphWeek, "Run Time", "l");
	leg->AddEntry(beamTimeGraphWeek, "Beam Time", "l");
	leg->SetShadowColor(0);
	leg->SetNColumns(3);
	leg->SetFillColor(0);
	leg->SetBorderSize(0);
	leg->Draw();

	gPad->RedrawAxis();
	gPad->SetTicks(0, 0);

	picTitle = get_date_OWL(starttime_ns).substr(8, 2) + get_date_OWL(starttime_ns).substr(5, 2) + "_" + get_date_OWL(endtime_ns).substr(8, 2) + get_date_OWL(endtime_ns).substr(5, 2) + "_rfp.png";
	cWeekReady->Print(picTitle.c_str());

	cWeekReady->Clear();
	delete cWeekReady;
	delete leg;
	delete htW;
}

void doHTML(unsigned long long starttime_ns, unsigned long long endtime_ns) {
	//get total beam time
	double totalWeekBeamHours = 0;
	for (auto valBT : *beamTimes) {
		totalBeamHours += static_cast<double>((valBT.second - valBT.first) / 1'000'000'000) / 3600;
		totalWeekBeamHours += static_cast<double>((valBT.second - valBT.first) / 1'000'000'000) / 3600;
	}
	auto iterETR = effTimesRFP->begin();
	auto iterED = effTimesEndDelay->begin();
	for (auto valET : *effTimes) {
		totalEffHours += static_cast<double>(valET / 1'000'000'000) / 3600;
		totalEffReadyHours += static_cast<double>(*iterETR / 1'000'000'000) / 3600;
		totalEffNoEndDelay += static_cast<double>(*iterED / 1'000'000'000) / 3600;
		++iterETR;
		++iterED;
	}

	std::ofstream outputHTML("index.html", std::ios::out);
	outputHTML << std::setprecision(2) << std::fixed;
	outputHTML << "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" << std::endl;
	outputHTML << "<html xmlns = \"http://www.w3.org/1999/xhtml\" xml:lang = \"en\" lang = \"en\">" << std::endl;
	outputHTML << "<head>" << std::endl;
	outputHTML << "<title>ATLAS DAQ Efficiency</title>" << std::endl;
	outputHTML << "</head>" << std::endl;
	outputHTML << "<body>" << std::endl;
	outputHTML << "<p><h3>ATLAS DAQ Efficiency " << get_date_OWL(globalStartofYear).substr(0, 4)
		<< "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href=\"https://atlasdaq.cern.ch/daq_eff/2023\">ATLAS DAQ Efficiency 2023</a>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href=\"https://atlasdaq.cern.ch/daq_eff/2018\">ATLAS DAQ Efficiency Run 2</a>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href=\"https://atlasdaq.cern.ch/daq_eff/2013\">ATLAS DAQ Efficiency Run 1</a></h3></p>" << std::endl;
	outputHTML << "<p><a href = \"https://atlasdaq.cern.ch/daq_eff_summary\">DAQ Efficiency Summary</a>(efficiency per fill, inefficiency sources)</p>" << std::endl;
	outputHTML << "<p><a href = \"https://vm-daq-eff.cern.ch/en-US/app/daq_eff/run_efficiency_v3/\">Run Efficiency</a>(efficiency per run and per fill, dead time sources)</p>" << std::endl;
	outputHTML << "<table><tr><td>" << std::endl;
	outputHTML << "<p><b>" << get_date_OWL(globalStartofYear).substr(8, 2) << " "
		<< tellMeTheMonth(atoi(get_date_OWL(globalStartofYear).substr(5, 2).c_str())) << " - "
		<< get_date_OWL(endtime_ns).substr(8, 2) << " "
		<< tellMeTheMonth(atoi(get_date_OWL(endtime_ns).substr(5, 2).c_str())) << " "
		<< get_date_OWL(globalStartofYear).substr(0, 4) << "</b></p>" << std::endl;
	outputHTML << "<table><tr><td>" << std::endl;
	outputHTML << "<table border=\"1\">" << std::endl;
	outputHTML << "<tr><th></th><th>Hours</th><th>Percent</th></tr>" << std::endl;
	outputHTML << "<tr><td style=\"text-align:center\">Stable beams</td>" << std::endl;
	outputHTML << "<td style=\"text-align:center\">" << totalBeamHours << std::endl;
	outputHTML << "</td><td></td></tr>" << std::endl;
	outputHTML << "<tr><td style=\"text-align:center\">Running and not busy</td>" << std::endl;
	outputHTML << "<td style=\"text-align:center\">" << totalEffHours << std::endl;
	outputHTML << "</td>" << std::endl;
	outputHTML << "<td style=\"text-align:center\">" << totalEffHours / totalBeamHours * 100 << std::endl;
	outputHTML << "</td></tr>" << std::endl;
	outputHTML << "<tr><td style=\"text-align:center\">Running with RfP flag</td>" << std::endl;
	outputHTML << "<td style=\"text-align:center\">" << totalEffReadyHours << std::endl;
	outputHTML << "</td>" << std::endl;
	outputHTML << "<td style=\"text-align:center\">" << totalEffReadyHours / totalBeamHours * 100 << std::endl;
	outputHTML << "</td></tr>" << std::endl;
	outputHTML << "</table>" << std::endl;

	outputHTML << "</td><td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</td>" << std::endl;
	outputHTML << "<td><table border=\"1\">" << std::endl;
	outputHTML << "<tr><th></th><th>Hours</th><th>Percent</th></tr>" << std::endl;
	outputHTML << "<tr><td style=\"text-align:center\">Stable beams before WarmStop</td>" << std::endl;
	outputHTML << "<td style=\"text-align:center\">" << totalBeamHours - totalEffNoEndDelay << std::endl;
	outputHTML << "</td><td></td></tr>" << std::endl;
	outputHTML << "<tr><td style=\"text-align:center\">Running with RfP flag</td>" << std::endl;
	outputHTML << "<td style=\"text-align:center\">" << totalEffReadyHours << std::endl;
	outputHTML << "</td>" << std::endl;
	outputHTML << "<td style=\"text-align:center\">" << totalEffReadyHours / (totalBeamHours - totalEffNoEndDelay) * 100 << std::endl;
	outputHTML << "</td></tr>" << std::endl;
	outputHTML << "</table>" << std::endl;
	outputHTML << "</td></tr></table>" << std::endl;

	outputHTML << "<table>" << std::endl;
	outputHTML << "<tr>" << std::endl;
	outputHTML << "<td style=\"text-align:center\">" << std::endl;
	outputHTML << "<b>Run Efficiency(per fill)</b>" << std::endl;
	outputHTML << "<a href=\"eff.png\"><img src=\"eff.png\" alt=\"RunEff\" width=\"225px\"/></a>" << std::endl;
	outputHTML << "</td>" << std::endl;
	outputHTML << "<td style=\"text-align:center\">" << std::endl;
	outputHTML << "<b>Running Time(integral)</b>" << std::endl;
	outputHTML << "<a href=\"time.png\"><img src=\"time.png\" alt=\"RunTime\" width=\"225px\"/></a>" << std::endl;
	outputHTML << "</td>" << std::endl;
	outputHTML << "<td style=\"text-align:center\">" << std::endl;
	outputHTML << "<b>Run Efficiency(daily)</b>" << std::endl;
	outputHTML << "<a href=\"daily.png\"><img src=\"daily.png\" alt=\"Daily\" width=\"225px\"/></a>" << std::endl;
	outputHTML << "</td>" << std::endl;
	outputHTML << "<td style=\"text-align:center\">" << std::endl;
	outputHTML << "<b>Run Efficiency(weekly)</b>" << std::endl;
	outputHTML << "<a href=\"weekly.png\"><img src=\"weekly.png\" alt=\"Weekly\" width=\"225px\"/></a>" << std::endl;
	outputHTML << "</td>" << std::endl;
	outputHTML << "</tr>" << std::endl;
	outputHTML << "</table>" << std::endl;

	outputHTML << "<p><b>Weekly summaries and plots</b></p>" << std::endl;
	outputHTML << "<table border=\"1\">" << std::endl;
	outputHTML << "<tr>" << std::endl;
	outputHTML << "<th width=\"12%\">Week</th>" << std::endl;
	outputHTML << "<th width=\"28%\">Summary</th>" << std::endl;
	outputHTML << "<th width=\"30%\">Efficiency without RfP flag</th>" << std::endl;
	outputHTML << "<th width=\"30%\">Efficiency with RfP flag</th>" << std::endl;

	std::string smartWeekDates = get_date_OWL(starttime_ns).substr(8, 2);
	std::stringstream buffSS;
	smartWeekDates += (atoi(get_date_OWL(starttime_ns).substr(5, 2).c_str()) == atoi(get_date_OWL(endtime_ns).substr(5, 2).c_str())) ?
		" - " + get_date_OWL(endtime_ns).substr(8, 2) + " " + tellMeTheMonth(atoi(get_date_OWL(endtime_ns).substr(5, 2).c_str())) :
		" " + tellMeTheMonth(atoi(get_date_OWL(starttime_ns).substr(5, 2).c_str())) + " - " + get_date_OWL(endtime_ns).substr(8, 2) + " " + tellMeTheMonth(atoi(get_date_OWL(endtime_ns).substr(5, 2).c_str()));
	weekBYweekHTML += "</tr><tr><td style=\"text-align:center\">" + smartWeekDates + "\n";
	weekBYweekHTML += "<td><table border=\"1\">\n";
	weekBYweekHTML += "<tr><th></th><th>Hours</th><th>Percent</th></tr>\n";
	weekBYweekHTML += "<tr><td style=\"text-align:center\">Stable beams</td>\n";
	buffSS << std::fixed << std::setprecision(2) << totalWeekBeamHours;
	weekBYweekHTML += "<td style=\"text-align:center\">" + buffSS.str() + "\n";
	buffSS.str(std::string());//this is how you clear a stream
	weekBYweekHTML += "</td>\n";
	weekBYweekHTML += "<td></td></tr>\n";
	weekBYweekHTML += "<tr><td style=\"text-align:center\">Running and not busy</td>\n";
	buffSS << std::fixed << std::setprecision(2) << static_cast<double>(std::accumulate(effTimes->begin(), effTimes->end(), static_cast<unsigned long long>(0)) / 1'000'000'000) / 3600;
	weekBYweekHTML += "<td style=\"text-align:center\">" + buffSS.str() + "\n";
	buffSS.str(std::string());
	weekBYweekHTML += "</td>\n";
	buffSS << std::fixed << std::setprecision(2) << (static_cast<double>(std::accumulate(effTimes->begin(), effTimes->end(), static_cast<unsigned long long>(0)) / 1'000'000'000) / 3600) / totalWeekBeamHours * 100;
	weekBYweekHTML += "<td style=\"text-align:center\">" + buffSS.str() + "\n";
	buffSS.str(std::string());
	weekBYweekHTML += "</td></tr>\n";
	weekBYweekHTML += "<tr><td style=\"text-align:center\">Running with RfP flag</td>\n";
	buffSS << std::fixed << std::setprecision(2) << static_cast<double>(std::accumulate(effTimesRFP->begin(), effTimesRFP->end(), static_cast<unsigned long long>(0)) / 1'000'000'000) / 3600;
	weekBYweekHTML += "<td style=\"text-align:center\">" + buffSS.str() + "\n";
	buffSS.str(std::string());
	weekBYweekHTML += "</td>\n";
	buffSS << std::fixed << std::setprecision(2) << (static_cast<double>(std::accumulate(effTimesRFP->begin(), effTimesRFP->end(), static_cast<unsigned long long>(0)) / 1'000'000'000) / 3600) / totalWeekBeamHours * 100;
	weekBYweekHTML += "<td style=\"text-align:center\">" + buffSS.str() + "\n";
	buffSS.str(std::string());
	weekBYweekHTML += "</td></tr>\n";
	weekBYweekHTML += "</table></td>\n";
	weekBYweekHTML += "<td style=\"text-align:center\"><a href=\"" + get_date_OWL(starttime_ns).substr(8, 2) + get_date_OWL(starttime_ns).substr(5, 2) + "_" + get_date_OWL(endtime_ns).substr(8, 2) + get_date_OWL(endtime_ns).substr(5, 2) + ".png\"><img src=\"" + get_date_OWL(starttime_ns).substr(8, 2) + get_date_OWL(starttime_ns).substr(5, 2) + "_" + get_date_OWL(endtime_ns).substr(8, 2) + get_date_OWL(endtime_ns).substr(5, 2) + ".png\" alt=\"Run eff\" width=\"320px\"/></a></td>\n";
	weekBYweekHTML += "<td style=\"text-align:center\"><a href=\"" + get_date_OWL(starttime_ns).substr(8, 2) + get_date_OWL(starttime_ns).substr(5, 2) + "_" + get_date_OWL(endtime_ns).substr(8, 2) + get_date_OWL(endtime_ns).substr(5, 2) + "_rfp.png\"><img src=\"" + get_date_OWL(starttime_ns).substr(8, 2) + get_date_OWL(starttime_ns).substr(5, 2) + "_" + get_date_OWL(endtime_ns).substr(8, 2) + get_date_OWL(endtime_ns).substr(5, 2) + "_rfp.png\" alt=\"Run eff with Ready\" width=\"320px\"/></a></td></tr>\n";

	outputHTML << weekBYweekHTML;
	outputHTML << "</table>" << std::endl;
	outputHTML << "<p><i>Contact and support: " << std::endl;
	outputHTML << "<a href=\"https://atlas-glance.cern.ch/atlas/membership/members/profile?id=5334\">Adrian Chitan</a>" << std::endl;
	outputHTML << "</i></p>" << std::endl;
	outputHTML << "</body>" << std::endl;
	outputHTML << "</html>" << std::endl;

	outputHTML.close();
}

std::string tellMeTheMonth(int m) {
	switch (m) {
	case 1:
		return "January";
	case 2:
		return "February";
	case 3:
		return "March";
	case 4:
		return "April";
	case 5:
		return "May";
	case 6:
		return "June";
	case 7:
		return "July";
	case 8:
		return "August";
	case 9:
		return "September";
	case 10:
		return "October";
	case 11:
		return "November";
	case 12:
		return "December";
	}
	return "";
}
