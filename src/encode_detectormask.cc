#include <iostream>

#include "ers/ers.h"

#include "config/Configuration.h"

#include "dal/util.h"
#include "dal/Segment.h"
#include "dal/Partition.h"
#include "dal/Detector.h"
#include "dal/BaseApplication.h"

#include "eformat/DetectorMask.h"

#include "DFdal/ROS.h"
#include "DFdal/PreloadedReadoutModule.h"
#include "DFdal/InputChannel.h"

int main() {
    Configuration* config = new Configuration("");
    const char *partName = std::getenv("TDAQ_PARTITION");
    if(partName == NULL) {
        std::cerr << "${TDAQ_PARTITION} is not set." << std::endl;
        exit(EXIT_FAILURE);
    }
    const daq::core::Partition * partition = daq::core::get_partition(*config, partName);

    std::set<std::string> app_types;
    app_types.insert("ROS");
    app_types.insert("SwRodApplication");
    app_types.insert("HLTSVApplication");
    app_types.insert("SFOngApplication");

    std::vector<const daq::core::BaseApplication*> appConfigs = partition->get_all_applications(&app_types);

    eformat::helper::DetectorMask detMask;

    //loop over all ROS to extract Detector IDs; if we have emulated ROSs the detector ID is kept in the ROBs

    for(unsigned int i = 0; i < appConfigs.size(); i++) {
        if(appConfigs[i]->class_name() == "ROS" || appConfigs[i]->class_name() == "SwRodApplication") {
            const daq::df::ROS * ros = config->cast<daq::df::ROS>(appConfigs[i]);
            if(ros->get_Detector()->get_LogicalId() != 0) {
                uint32_t detectorId = (ros->get_Detector()->get_LogicalId()) << 16;
                eformat::helper::SourceIdentifier sid(detectorId);
                detMask.set(sid.subdetector_id());
            } else {
                std::vector<const daq::core::ResourceBase *> robins = ros->get_Contains();

                for(size_t i = 0; i < robins.size(); i++) {
                    if(!robins[i]->disabled(*partition)) {
                        const daq::df::PreloadedReadoutModule * robin =
                                config->cast<daq::df::PreloadedReadoutModule>(robins[i]);
                        if(robin) {
                            std::vector<const daq::core::ResourceBase *> robs = robin->get_Contains();
                            for(size_t j = 0; j < robs.size(); j++) {
                                if(!robs[j]->disabled(*partition)) {
                                    const daq::df::InputChannel * rob = config->cast<daq::df::InputChannel>(robs[j]);
                                    if(rob) {
                                        eformat::helper::SourceIdentifier sid(rob->get_Id());
                                        detMask.set(sid.subdetector_id());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else if(appConfigs[i]->class_name() == "HLTSVApplication") {
            detMask.set(eformat::TDAQ_HLT);
        } else if(appConfigs[i]->class_name() == "SFOngApplication") {
            detMask.set(eformat::TDAQ_SFO);
        }
    }
    std::cout << detMask.mask() << "   " << detMask.string() << std::endl;

    return 0;
}
