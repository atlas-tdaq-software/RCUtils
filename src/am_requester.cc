// File: am_requester.cc
// Purpose: asks the Access Manager Server whether operationg the DAQ is permitted to a user
// Return codes: 0 = accepted; 1 = denied
// Author: G. Lehmann Miotto

#include "ers/ers.h"
#include "system/User.h"
#include "system/Host.h"

#include <AccessManager/xacml/impl/PMGResource.h>
#include "AccessManager/xacml/impl/RunControlResource.h"
#include "AccessManager/client/RequestorInfo.h"
#include "AccessManager/client/ServerInterrogator.h"
#include "AccessManager/util/ErsIssues.h"

int main() {

  bool allowed = false;

  System::LocalHost host;
  System::User u;

  std::string partition="BOH";
  if (getenv("TDAQ_PARTITION"))
    partition = getenv("TDAQ_PARTITION");

  // Ask AM for authorization
  daq::am::PMGResource amResource("/bin/ls", host.full_name(), "", true, partition);
  daq::am::ServerInterrogator amInterrogator;
  daq::am::RequestorInfo      amReqInfo(u.name_safe(), host.full_name());
  amResource.setStartAction();
  
  try {
    allowed = amInterrogator.isAuthorizationGranted(amResource, amReqInfo);
  }
  catch(daq::am::Exception & ex) {
    ers::error(ex);
    allowed = false;
  }

  if (allowed) {
    return 0;
  }

  std::cerr << "You, " << u.name_safe() << ", are not allowed to start processes by the AM in partition " 
	    << partition<< "; check your enabled roles!" << std::endl;
  return 1;

}
