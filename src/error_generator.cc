/*
 * error_generator.cc
 *
 * This utility reproduce different kinds of application error on user demand.
 * It runs interactively, the supported errors are prompted in a menu and the chosen
 * one is reproduced and propagated.
 *
 *  Author: lmagnoni
 */

#include <iostream>
#include <unistd.h>
#include "ers/ers.h"
#include "ipc/core.h"
#include "RunControl/Common/UserExceptions.h"

ERS_DECLARE_ISSUE( ers, HLTMessage, , )

int main(int argc, char **argv)
{


  int errortype;
  std::string applicationID = "test_application_ID";
  std::string channelId = "test_channel_ID";
  int waitTimeInSecond = 1; //1 second

  IPCCore::init(argc, argv);

  std::cout<<"Error generation utility.\n"
	   <<"If you want the error to propagate on MRS set $TDAQ_ERS_ERROR=\"mts\" and define $TDAQ_PARTITION."<< std::endl;

  if ( argc < 2 )
    std::cout<<"Using fake application ID = '" <<  applicationID << "' and channel ID = '" << channelId <<"'." <<std::endl;
  else {
    applicationID = argv[1];
    if ( applicationID == "-h" || applicationID == "--help") {
      std::cout << "Usage: rc_error_generator [application ID] [channel ID]" << std::endl;
      exit(0);
    }
    std::cout<<"Using user specified applicationID = "+ applicationID+"\n";
  }

  if ( argc > 2 ) {
    channelId=argv[2];
    std::cout<<"Using user specified channelID = "+ channelId+"\n";
  }

  //Defining User Error Type
  daq::rc::ApplicationNotResponding issueNR(ERS_HERE ,applicationID.c_str());
  daq::rc::ApplicationDead issueAD(ERS_HERE ,applicationID.c_str());
  daq::rc::ApplicationFatalError issueAF(ERS_HERE ,applicationID.c_str());
  daq::rc::ProcessingTimeout issuePT(ERS_HERE ,applicationID.c_str());
  daq::rc::ApplicationError issueAR(ERS_HERE ,applicationID.c_str());
  daq::rc::HardwareError issueHW(ERS_HERE,channelId.c_str(),applicationID.c_str());
  daq::rc::Recovery issueR(ERS_HERE,applicationID.c_str());
  daq::rc::UserException issueUE(ERS_HERE,applicationID.c_str());
  daq::rc::MAX_EVT_DONE issueMED(ERS_HERE,applicationID.c_str());
  daq::rc::HardwareSynchronization issueHS(ERS_HERE,applicationID.c_str());
  daq::rc::HardwareRecovered issueHR(ERS_HERE,channelId.c_str(),applicationID.c_str());
  daq::rc::PixelUp issuePU(ERS_HERE,applicationID.c_str());
  daq::rc::PixelDown issuePD(ERS_HERE,applicationID.c_str());
  daq::rc::ModulesEnabled issueMD(ERS_HERE,channelId.c_str(), false);
  daq::rc::ModulesDisabled issueME(ERS_HERE,channelId.c_str(), false);
  daq::rc::ReadyForHardwareRecovery issueRFHR(ERS_HERE,channelId.c_str(),applicationID.c_str());
  l1calo::rc::L1CaloZeroPpmLut issueCALO_ZPL(ERS_HERE,1);
  l1calo::rc::L1CaloRestorePpmLut issueCALO_RPL(ERS_HERE,1);
  l1calo::rc::L1CaloSetPpmNoiseCut issueCALO_PNC(ERS_HERE,1,0);
  //  daq::rc::ReadyForModuleRecovery issueRFMHR(ERS_HERE,applicationID.c_str(),channelId.c_str());
  ers::HLTMessage issueHLT(ERS_HERE," ToolSvc.TrigTSerializer: Errors while decoding TrigInDetTrackCollection_tlp2 any further ROOT messages for this class will be...");
  //Interactive mode
  std::cout<<"Select the Error to generate: \n"
 		  " 1. ApplicationNotResponding \n"
 		  " 2. ApplicationDead \n"
 		  " 3. ApplicationFatalError \n"
 		  " 4. ProcessingTimeout\n"
 		  " 5. ApplicationError \n"
 		  " 6. HardwareError \n"
 		  " 7. Recovery \n"
 		  " 8. UserException \n"
		  " 9. MAX_EVT_DONE\n"
		  "10. HardwareSynchronization\n"
		  "11. HardwareRecovered\n"
		  "12. PixelUp\n"
		  "13. PixelDown\n"
		  "14. ModulesEnabled (no LB change)\n"
		  "15. ModulesDisabled (no LB change)\n"
		  "16. ReadyForHardwareRecovery\n"
		  "17. HLTMessage error\n"
		  "18. HLTMessage warning\n"
		  "19. L1Calo ZeroPpmLut\n"
		  "20. L1Calo RestorePpmLut\n"
		  "21. L1Calo SetPpmNoiseCut\n"
          "22. HardwareError (for SWROD)\n"
          "23. ReadyForHardwareRecovery (for SWROD)\n"
          "24. HardwareRecovered (for SWROD)\n"
		  ": ";

   std::cin>>errortype;

   switch (errortype) {
	case 1:
		//Application Not responding
		std::cout << "Generating ApplicationNotResponding error..." << std::endl;
		ers::error(issueNR);
		break;

	case 2:
		//Application Dead
		std::cout << "Generating ApplicationDead error..." << std::endl;
		ers::error(issueAD);
		break;

	case 3:
		//ApplicationFatalError
		std::cout << "Generating ApplicationFatal error..." << std::endl;
		ers::error(issueAF);
		break;

	case 4:
		//Processing Timeout
		std::cout << "Generating ProcessingTimeout error..." << std::endl;
		ers::error(issuePT);
		break;

	case 5:
		//ApplicationError
		std::cout << "Generating ApplicationError base error..." << std::endl;
		ers::error(issueAR);
		break;

	case 6:
		//ApplicationError
		std::cout << "Generating HardwareError base error..." << std::endl;
		ers::error(issueHW);
		break;

	case 7:
		//Recovery
		std::cout << "Generating Recovery base error..." << std::endl;
		ers::error(issueR);
		break;

	case 8:
		//UserExcpetion
		std::cout << "Generating UserException base error..." << std::endl;
		ers::error(issueUE);
		break;

	case 9:
		//MAX_EVT_DONE
		std::cout << "Generating MAX_EVT_DONE error..." << std::endl;
		ers::error(issueMED);
		break;

	case 10:
		//HardwareSynchronization
		std::cout << "Generating HardwareSynchronization error..." << std::endl;
		ers::error(issueHS);
		break;

	case 11:
		//HardwareRecovered
		std::cout << "Generating HardwareRecovered error..." << std::endl;
		ers::error(issueHR);
		break;

	case 12:
        //PixelUp
        std::cout << "Generating PixelUp..." << std::endl;
        ers::error(issuePU);
        break;

    case 13:
        //PixelDown
        std::cout << "Generating PixelDown..." << std::endl;
        ers::error(issuePD);
        break;

    case 14:
        //ModulesDisabled
        std::cout << "Generating ModulesDisabled..." << std::endl;
        ers::error(issueMD);
        break;

    case 15:
        //ModulesEnabled
        std::cout << "Generating ModulesEnabled..." << std::endl;
        ers::error(issueME);
        break;

    case 16:
        //ReadyForHardwareRecovery
        std::cout << "Generating ReadyForHardwareRecovery..." << std::endl;
        ers::error(issueRFHR);
        break;

    case 17:
        //HLTMessage err
        std::cout << "Generating HLTMessage..." << std::endl;
        ers::error(issueHLT);
        break;

    case 18:
        //HLTMessage warning
        std::cout << "Generating HLTMessage..." << std::endl;
        ers::warning(issueHLT);
        break;

    case 19:
        //L1CaloZeroPpmLut err
        std::cout << "Generating L1CaloZeroPpmLut..." << std::endl;
        ers::warning(issueCALO_ZPL);
        break;
	
	case 20:
		//L1CaloRestorePpmLut err
		std::cout << "Generating  L1CaloRestorePpmLut..." << std::endl;
		ers::warning(issueCALO_RPL);
		break;

	case 21:
		//L1CaloSetPpmNoiseCut err
		std::cout << "Generating L1CaloSetPpmNoiseCut..." << std::endl;
		ers::warning(issueCALO_PNC);
		break;

	case 22:
	    //HardwareError for SWROD
	    std::cout << "Generating HardwareError (for SWROD) base error..." << std::endl;
	    issueHW.add_qualifier("SWROD");
	    ers::error(issueHW);
	    break;

	case 23:
	    //ReadyForHardwareRecovery
	    std::cout << "Generating ReadyForHardwareRecovery (for SWROD)..." << std::endl;
	    issueRFHR.add_qualifier("SWROD");
	    ers::error(issueRFHR);
	    break;

	case 24:
        //HardwareRecovered
        std::cout << "Generating HardwareRecovered error (for SWROD))..." << std::endl;
        issueHR.add_qualifier("SWROD");
        ers::error(issueHR);
        break;

	default:
		std::cout << "Invalid input specified. No error generated." << std::endl;
		exit(1);
		break;
  }

    //sleep to allow error propagation
	sleep(waitTimeInSecond);
}
