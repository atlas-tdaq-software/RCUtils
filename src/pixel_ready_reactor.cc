// $Id: sendcommand.cc 45895 2008-02-23 16:48:32Z glehmann $
//
// Command sender to supervisor and run control
//
// Author: Giovanna.Lehmann@cern.ch

#include <signal.h>

#include "ers/ers.h"
#include "ipc/core.h"
#include "ipc/partition.h"
#include "is/inforeceiver.h"
#include "is/infoT.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/UserExceptions.h"
#include "ddc/DdcIntInfo.h"
#include "ddc/DdcIntInfoNamed.h"
#include "rc/RunParamsNamed.h"
#include "eformat/DetectorMask.h"

namespace daq {
  ERS_DECLARE_ISSUE_BASE( rc,
                          PixelCanTurnOn,
                          rc::Exception,
                          "Pixel partition " << excp << " can turn on pre-amplifiers." ,
                          ERS_EMPTY,
                          ((const char*) excp)
                        )
  ERS_DECLARE_ISSUE_BASE( rc,
                          CannotGetDetectorMask,
                          rc::Exception,
                          "Cannot determine which detectors are active. Is RunParams.RunParams published in IS?" ,
                          ERS_EMPTY,
                          ((const char*) excp)
                        )
}

std::string partition = "UNKNOWN";
ISInfoReceiver* infoRec = NULL;
ddc::DdcIntInfoNamed * pixBarrel = NULL;
ddc::DdcIntInfoNamed * pixDisks = NULL;
ddc::DdcIntInfoNamed * pixBLayer = NULL;
bool    barrelInUse = false;
bool    disksInUse = false;
bool    bLayerInUse = false;
bool    running = false;
 
void checkIfReady() {
  if((pixBarrel->value == 0 && barrelInUse) || (pixDisks->value == 0 && disksInUse) || (pixBLayer->value == 0 && bLayerInUse)) {
    ERS_LOG("Barrel = " << pixBarrel->value << " " << barrelInUse 
	    << ", Disks = " << pixDisks->value << " " << disksInUse
	    << ", B-Layer = " << pixBLayer->value << " " << bLayerInUse);
    return;
  }
  
  ers::info(daq::rc::PixelUp(ERS_HERE, "Pixel"));
  
  //ERS_LOG("Would request Pixel UP");
  return;
}

extern "C" void signalhandler(int)
{
   signal(SIGINT,  SIG_DFL);
   signal(SIGTERM, SIG_DFL);
   running = false;
}

void DQcb(ISCallbackInfo *isc) {
  ERS_DEBUG(0, "Callback received for " << isc->name());
  ddc::DdcIntInfo res;
  if (isc->reason() != is::Deleted)
    isc->value(res);
  else {
    return;
  }

  if (!strcmp(isc->name(),"DDC.PIX_BARREL_onAfterGotoReady")) {
    if(pixBarrel->value == res.value) return; // Trigger only on changes!
    pixBarrel->value = res.value;
    if(pixBarrel->value > 0) ers::info(daq::rc::PixelCanTurnOn(ERS_HERE, "Barrel"));
  }
  else if (!strcmp(isc->name(), "DDC.PIX_DISKS_onAfterGotoReady")) {
    if(pixDisks->value == res.value) return; // Trigger only on changes!
    pixDisks->value = res.value;
    if(pixDisks->value > 0) ers::info(daq::rc::PixelCanTurnOn(ERS_HERE, "Disks"));    
  }
  else if (!strcmp(isc->name(), "DDC.PIX_BLAYER_onAfterGotoReady")) {
    if(pixBLayer->value == res.value) return; // Trigger only on changes!
    pixBLayer->value = res.value;
    if(pixBLayer->value > 0) ers::info(daq::rc::PixelCanTurnOn(ERS_HERE, "B-Layer"));
  }

  checkIfReady();

}

int main(int argc, char ** argv)
{
   try{
   	IPCCore::init(argc,argv);
   }catch(daq::ipc::CannotInitialize& e){
   	ers::fatal(e);
	abort();
   }catch(daq::ipc::AlreadyInitialized& e){
   	ers::warning(e);
   }

   if (std::getenv("TDAQ_PARTITION")) {         
   	partition=std::getenv("TDAQ_PARTITION");
   }

  IPCPartition part(partition);
  // Get the detector mask to understand what Pisel Partitions are in the run
  try {
    RunParamsNamed runP(part, "RunParams.RunParams");
    runP.checkout();
    eformat::helper::DetectorMask detMask(runP.det_mask);
    if (detMask.is_set(eformat::PIXEL_BARREL)) {
      barrelInUse = true;
      ERS_LOG("PIXEL_BARREL active");
    }
    if (detMask.is_set(eformat::PIXEL_DISK)) {
      disksInUse = true;
      ERS_LOG("PIXEL_DISK active");
    }
    if (detMask.is_set(eformat::PIXEL_B_LAYER)) {
      bLayerInUse = true;
      ERS_LOG("PIXEL_B_LAYER active");
    }    
  }
  catch(ers::Issue &e) {
    ers::fatal(daq::rc::CannotGetDetectorMask(ERS_HERE, "", e));
    sleep(1);
    exit(1);
  }

  infoRec = new ISInfoReceiver(part);
  try {
    pixBarrel = new ddc::DdcIntInfoNamed(part, "DDC.PIX_BARREL_onAfterGotoReady");
    pixDisks = new ddc::DdcIntInfoNamed(part, "DDC.PIX_DISKS_onAfterGotoReady");
    pixBLayer = new ddc::DdcIntInfoNamed(part, "DDC.PIX_BLAYER_onAfterGotoReady");
    pixBarrel->checkout();
    pixDisks->checkout();
    pixBLayer->checkout();
    infoRec->subscribe("DDC", ~ddc::DdcIntInfo::type() && ".*onAfterGotoReady", DQcb);
  }
  catch ( daq::is::Exception & ex ) {
      ers::fatal(ex);
      return EXIT_FAILURE;
    }
  // Install signal handler
  signal(SIGINT,  signalhandler);
  signal(SIGTERM, signalhandler);

  checkIfReady();
   running = true;
   // sleep forever
   while (running) {
     sleep(1);
   }

   infoRec->unsubscribe("DDC", ~ddc::DdcIntInfo::type() && ".*onAfterGotoReady");
   return EXIT_SUCCESS;
}
