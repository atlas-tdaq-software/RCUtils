// $Id$
//
// ATLAS Run Control
//
// Author: Dietrich Liko, Giovanna Lehmann

// Brings a controller from NONE/BOOTED to INITIAL (used in setup_daq for initial partition)

#include <chrono>
#include <memory>
#include <thread>
#include <unistd.h>
#include <sstream>
#include <cstdlib>
#include <boost/program_options.hpp>

#include "ers/ers.h"
#include "ipc/core.h"
#include "tmgr/tmresult.h"
#include "rc/RCStateInfoNamed.h"

#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/CommandSender.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/FSM/FSMStates.h"
#include "RunControl/FSM/FSMCommands.h"

#include "Exceptions.h"

int main(int argc, char ** argv) {
    std::string partition = (std::getenv("TDAQ_PARTITION") == nullptr) ? "" : std::getenv("TDAQ_PARTITION");
    std::string name = "RootController";
    std::string final = daq::rc::FSMStates::INITIAL_STATE;
    unsigned int timeout = 10;
    daq::rc::FSM_STATE finalState = daq::rc::FSM_STATE::INITIAL;

    // Parse command-line parameters
    try {
        boost::program_options::options_description desc("Allowed options", 128);
        desc.add_options()
            ("help,h", "help message")
            ("partition,p", boost::program_options::value<std::string>(&partition)->default_value(partition), "Partition name ($TDAQ_PARTITION)")
            ("name,n", boost::program_options::value<std::string>(&name)->default_value(name), "Controller name")
            ("finalState,f", boost::program_options::value<std::string>(&final)->default_value(final), "Final state")
            ("timeout,T", boost::program_options::value<unsigned int>(&timeout)->default_value(timeout), "Timeout before giving up");

        boost::program_options::variables_map vm;
        boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
        boost::program_options::notify(vm);

        if(vm.count("help")) {
            desc.print(std::cout);
            std::cout << std::endl;
            return (daq::tmgr::TmUnresolved);
        }

        if(partition.empty() || name.empty() || final.empty()) {
            throw daq::rc::CmdLineError(ERS_HERE, "the partition, controller and final state names cannot be empty");
        }

        // Check that the passed state is the right one
        try {
            finalState = daq::rc::FSMStates::stringToState(final);
        }
        catch(daq::rc::InvalidState& ex) {
            throw daq::rc::CmdLineError(ERS_HERE,
                                        std::string("The final state \"") + final + "\" is not a valid FSM state",
                                        ex);
        }

        // Initialize IPC
        try {
            IPCCore::init(argc, argv);
        }
        catch(daq::ipc::AlreadyInitialized& e) {
            ers::warning(e);
        }

        // The commander used to send commands to the controller
        daq::rc::CommandSender commander(partition, "CmdlUtility");

        // This is the end time-point given the timeout
        auto endTime = std::chrono::system_clock::now() + std::chrono::seconds(timeout);

        // Wait for the application to reach at least the NONE state
        daq::rc::FSM_STATE currState = daq::rc::FSM_STATE::ABSENT;
        do {
            const auto& appStatus = commander.status(name);
            currState = daq::rc::FSMStates::stringToState(appStatus->fsmState());
            if(static_cast<unsigned int>(currState) >= static_cast<unsigned int>(daq::rc::FSM_STATE::NONE)) {
                break;
            }

            const bool timeoutElapsed = (std::chrono::system_clock::now() >= endTime);
            if(timeoutElapsed == true) {
                const std::string msg = std::string("Did not reach state") +  daq::rc::FSMStates::NONE_STATE + " on time.";
                throw daq::rc::Timeout(ERS_HERE, msg.c_str());
            }

            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
        while(true);

        // Check that the state has not already been reached
        if(static_cast<unsigned int>(finalState) < static_cast<unsigned int>(currState)) {
            throw daq::rc::Exception(ERS_HERE, " You specified a final state which is lower than the present one.");
        }

        // Start sending commands in order to reach the final state
        while(currState != finalState) {
            const daq::rc::FSM_STATE tmp = currState;
            commander.makeTransition(name, daq::rc::TransitionCmd(daq::rc::FSM_COMMAND::NEXT_TRANSITION));

            while(tmp == currState) {
                const auto& appStatus = commander.status(name);
                currState = daq::rc::FSMStates::stringToState(appStatus->fsmState());

                const bool timeoutElapsed = (std::chrono::system_clock::now() >= endTime);
                if(timeoutElapsed == true) {
                    throw daq::rc::Timeout(ERS_HERE, "Did not reach the final state on time");
                }

                std::this_thread::sleep_for(std::chrono::milliseconds(100));
            }
        }
    }
    catch(daq::rc::CmdLineError& ex) {
        ers::fatal(ex);
        return (daq::tmgr::TmUnresolved);
    }
    catch(boost::program_options::error& ex) {
        ers::fatal(daq::rc::CmdLineError(ERS_HERE, ex.what(), ex));
        return (daq::tmgr::TmUnresolved);
    }
    catch(ers::Issue& ex) {
        ers::fatal(ex);
        return (daq::tmgr::TmFail);
    }

    return (daq::tmgr::TmPass);
}
