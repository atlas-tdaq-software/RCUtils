// $Id:  checkapps.cc dumps all apps in a partition with minimal information
//
// ATLAS Run Control
//
// Current: G. Lehmann Miotto, March 2008 

#include <iostream>
#include <iomanip>
#include <sstream>
#include <boost/program_options.hpp>

#include "ipc/core.h"
#include "ers/ers.h"

#include "config/Configuration.h"

#include "RunControl/Common/Exceptions.h"

#include "dal/Partition.h"
#include "dal/util.h"
#include "dal/Computer.h"
#include "dal/OnlineSegment.h"
#include "dal/BaseApplication.h"
#include "dal/CustomLifetimeApplicationBase.h"

int main(int argc, char ** argv) {

    try {
        IPCCore::init(argc, argv);
    }
    catch(daq::ipc::CannotInitialize& e) {
        ers::fatal(e);
        abort();
    }
    catch(daq::ipc::AlreadyInitialized& e) {
        ers::warning(e);
    }

    // take defaults from environment
    std::string tdaq_partition(""), tdaq_db("");
    //  std::string def_p, def_d, def_a, def_l;
    if(getenv("TDAQ_PARTITION"))
        tdaq_partition = getenv("TDAQ_PARTITION");
    if(getenv("TDAQ_DB"))
        tdaq_db = getenv("TDAQ_DB");

    // parse commandline parameters

    boost::program_options::options_description desc("Allowed options", 128);
    desc.add_options()
         ("help,h", "help message")
         ("partition,p", boost::program_options::value<std::string>(&tdaq_partition)->default_value(tdaq_partition), "partition name (TDAQ_PARTITION)")
         ("database,d", boost::program_options::value<std::string>(&tdaq_db)->default_value(tdaq_db), "database (TDAQ_DB)");

    boost::program_options::variables_map vm;
    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);

    if(vm.count("help")) {
        desc.print(std::cout);
        std::cout << std::endl;
        return EXIT_SUCCESS;
    }

    if(tdaq_partition.empty() || tdaq_db.empty()) {
        daq::rc::CmdLineError issue(ERS_HERE, "partition name and database cannot be empy");
        ers::error(issue);
        return EXIT_FAILURE;
    }

    Configuration config(tdaq_db);

    if(!config.loaded()) {
        daq::rc::ConfigurationIssue issue(ERS_HERE, "the database could not be loaded");
        ers::fatal(issue);
        return EXIT_FAILURE;
    }

    // find root controller in the root partition

    const daq::core::Partition * root = daq::core::get_partition(config, tdaq_partition);
    if(!root) {
        std::string message = "Partition does not exist!";
        daq::rc::ConfigurationIssue issue(ERS_HERE, message);
        ers::fatal(issue);
        std::exit(EXIT_FAILURE);
    }

    std::vector<const daq::core::BaseApplication*> out;

    try {
        out = root->get_all_applications();
    }
    catch(ers::Issue &e) {
        daq::rc::ConfigurationIssue issue(ERS_HERE, "the configuration is inconsistent!", e);
        ers::fatal(issue);
        std::exit(EXIT_FAILURE);
    }
    std::cout << std::setfill('-') << std::setw(110) << "-" << std::setfill(' ') << std::endl;
    for(unsigned int i = 0; i < out.size(); i++) {
        std::string startAt = "BOOT";
        std::string stopAt = "SHUTDOWN";

        const daq::core::CustomLifetimeApplicationBase* cltApp = config.cast<daq::core::CustomLifetimeApplicationBase>(out[i]);
        if(cltApp != nullptr) {
            const std::string& lifetime = cltApp->get_Lifetime();
            if(lifetime == daq::core::CustomLifetimeApplicationBase::Lifetime::SOR_EOR) {
                startAt = "SOR";
                stopAt = "EOR";
            } else if(lifetime == daq::core::CustomLifetimeApplicationBase::Lifetime::EOR_SOR) {
                startAt = "EOR";
                stopAt = "SOR";
            } else if(lifetime == daq::core::CustomLifetimeApplicationBase::Lifetime::UserDefined_Shutdown) {
                startAt = "UserDefined";
                stopAt = "SHUTDOWN";
            } else if(lifetime == daq::core::CustomLifetimeApplicationBase::Lifetime::Configure_Unconfigure) {
                startAt = "CONFIGURE";
                stopAt = "UNCONFIGURE";
            }
        }

        std::cout << "| " << std::setw(50) << out[i]->UID() << " | " << std::setw(20) << out[i]->get_host()->UID()
                << " | " << std::setw(14) << startAt << " | " << std::setw(14) << stopAt << "|" << std::endl;

    }
    std::cout << std::setfill('-') << std::setw(110) << "-" << std::setfill(' ') << std::endl;
    return EXIT_SUCCESS;
}
