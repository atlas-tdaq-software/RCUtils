// $Id$
//
// ATLAS Run Control
//
// Author: Bob Jones, Sarah Wheeler, Dietmar Schweiger
// Current: Dietrich Liko, September 2002 

#ifndef RC_TIMETEST_H_INCLUDED
#define RC_TIMETEST_H_INCLUDED

#include <string>
#include <chrono>

#include "is/infodictionary.h"
#include "is/inforeceiver.h"
#include "rc/RCStateInfo.h"
#include "RunControl/Common/CommandSender.h"

class IPCAlarm;

namespace daq {
namespace RCUtils {

class TimeTest 
{
public:

   TimeTest(IPCPartition & partition,const std::string & controller,const std::string & isServer);
   ~TimeTest();
   void timeout(double timeout);
   void verbosity(int verbosity);
   int cycle(int initial,int final,int cycles=1);
   static void stateChangeCallback(ISCallbackInfo * isc);
   static bool timeoutCallback(void * parameter);
   double averageTime() const;
	static int state2Index(const std::string & state);
	static const std::string & index2State(int state);
private:
   bool nextStep(RCStateInfo & stateInfo);
   const std::string & nextCommand(int state);
   void startTimeout();
   void clearTimeout();
         
   ISInfoReceiver     isInfoReceiver_;
   ISInfoDictionary   isInfoDictionary_;
   daq::rc::CommandSender          commander_;
   IPCAlarm *         timeoutAlarm_;

   std::string controller_;
   std::string stateInfoName_;
   
   std::chrono::time_point<std::chrono::steady_clock> tp_;

   double timeout_;
   int verbosity_;

	static const std::string s_states [];
	
   int    initial_;
   int    final_;
   int    cycles_;
   int    current_;
   bool   reverse_;
   double totalTime_;
   int    returnCode_;  
};

} //RCUtils namespace
} // daq namespace

#endif /* RC_TIMETEST_H_INCLUDED */
