#ifndef RUN_RESOURCES_PLUGIN_INCLUDED
#define RUN_RESOURCES_PLUGIN_INCLUDED

#include <wmi/pluginbase.h>
#include <wmi/table.h>
#include <wmi/link.h>
#include <wmi/br.h>
#include <wmi/hr.h>
#include <ipc/partition.h>

#include <string>
#include <map>
#include <set>
#include <vector>

namespace daq {
  namespace ccwmi {
    
    class ResourcesPlugin : public daq::wmi::PluginBase {
      
    public:
      
      ResourcesPlugin();
      virtual ~ResourcesPlugin();
      
      void periodicAction();
      void stop();
      void configure(const ::wmi::InfoParameter& params);
      struct T_subDet {
	std::string name;
	std::string primaryFolder;
	std::set<std::string> primaryChannels;
	std::string secondaryName;
	std::string secondaryFolder;
	std::set<std::string> secondaryChannels;
      };	
    private:
      void addColorCode(int fd);
      void prepareMainTable(int fd);
      daq::wmi::Link* prepareSecondaryTable(const std::string &linkName, const std::string &folderName, const std::string &title, const std::set<std::string> & theSet, int &, int &, int &);
      std::vector<T_subDet> m_detectors;
      std::vector<std::string> m_folderNames;
      std::map<std::string, uint32_t> m_ISEnabled;
      std::map<std::string, uint32_t> m_ISDisabled;
      mutable daq::wmi::BR br;
      mutable daq::wmi::HR hr;

    };

    extern "C" daq::wmi::PluginBase* create() {
      return new ResourcesPlugin();
    }
    
    extern "C" void destroy(daq::wmi::PluginBase* p) {
      delete p;
    }
  } // end ccwmi namespace
} // end daq namespace

#endif

