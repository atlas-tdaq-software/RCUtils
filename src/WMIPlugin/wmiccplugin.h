#ifndef RUN_PARAMS_PLUGIN_INCLUDED
#define RUN_PARAMS_PLUGIN_INCLUDED

#include <wmi/pluginbase.h>
#include <wmi/table.h>
#include <wmi/br.h>
#include <wmi/hr.h>
#include <ipc/partition.h>

#include <string>
#include <map>
#include <vector>

#include <boost/regex.hpp>

namespace daq {
  namespace ccwmi {
    
    class RunParamsPlugin : public daq::wmi::PluginBase {
      
    public:

      RunParamsPlugin();
      virtual ~RunParamsPlugin();

      void periodicAction();
      void stop();
      void configure(const ::wmi::InfoParameter& params);
    
    protected:

      class PartitionData;

      daq::wmi::Table* createMainTable() const throw();
      void fillMainTable(daq::wmi::Table* const tb, 
			 const PartitionData* const  p,
			 const daq::wmi::Color& rcStateColor,
			 unsigned int row) const throw();
	
      daq::wmi::Table* createSystemSpecificTable() const throw();
      void fillSystemSpecificTable(daq::wmi::Table* const systemTable,
				   const PartitionData* const  p,
				   const std::string& systemMask,
				   const daq::wmi::Color& rcStateColor,
				   unsigned int row) const throw();

      void updatePartitions() throw();
      void buildPartitionPages() const throw();
      void buildTables() const throw();
      void buildPlots(const IPCPartition& p, PartitionData* const pData) const;
      void setTableColors(daq::wmi::Table& tb, int colNo, int lineNo, bool hasLabelColumn, bool hasHeaderColumn) const throw();
      void fillInfoCell(daq::wmi::Table& tb, int colNo, int lineNo, std::string value, std::string defaultValue) const throw();
      void fillLabelCell(daq::wmi::Table& tb, int colNo, int lineNo, std::string value) const throw();
      void fillBusyCell(daq::wmi::Table& tb, int colNo, int lineNo, std::string value, std::string defaultValue) const throw();
      void fillColorCell(daq::wmi::Table& tb, int colNo, int lineNo, std::string value, std::string defaultValue, int textSize, daq::wmi::Color textColor, std::string textOptions) const throw();


      void setLineColors(daq::wmi::Table& tb, int colNo, int row, bool hasLabelColumn) const throw();
      const std::string extractServerName(const std::string& fullName) const throw();
      const std::string extractAttributeName(const std::string& fullName) const throw();
      double counterFromIS(const IPCPartition& p, const std::string& isSource) const;
 
      class PartitionData {

	friend class RunParamsPlugin;

      public:
	
	virtual ~PartitionData() throw();
	void updateL1Counter(long long xValue, double yValue) throw();
	void updateL1Rate(long long xValue, double rate) throw();
	void updateL2Counter(long long xValue, double yValue) throw();
	void updateL2Rate(long long xValue, double rate) throw();
	void updateEBCounter(long long xValue, double yValue) throw();
	void updateEBRate(long long xValue, double rate) throw();
	void updateEFCounter(long long xValue, double yValue) throw();
	void updateEFRate(long long xValue, double rate) throw();
	void updateRECCounter(long long xValue, double yValue) throw();
	void updateRECRate(long long xValue, double rate) throw();
	void printGraphs(daq::wmi::Table& tb) const throw();
	
      protected:

	typedef std::vector< std::pair<unsigned long, float> > graph;
	
	explicit PartitionData(const std::string& partName, unsigned int maxValues) throw();
	PartitionData(const PartitionData& rhs);
	void createGraphGroups(const std::string& graphGroupName) throw();
	void updateGraph(long long xValue, double yValue, graph& gr) const throw();
	void cleanGraphs() throw();

      private:

	static const std::string L1;
	static const std::string L2;
	static const std::string EB;
	static const std::string EF;
	static const std::string REC;
	static  std::string L1_ACC;
	static 	std::string L2_ACC;
	static  std::string EF_ACC;
	//static std::string DQBaseName;
	static unsigned int graphWidth;
	static unsigned int graphHeight;
	static const unsigned int graphTableColNum;

	const std::string partitionName;
	const unsigned int graphPoints;

	typedef std::map< std::string, std::vector<graph> > graphGroupMap; // Size 2 vector: counter + rate
	graphGroupMap grMap;
	
	typedef std::list< std::pair<unsigned int, double> > ratePairList;
	ratePairList l1Rate; // temporary for rate calculation
	ratePairList l2Rate; // temporary for rate calculation
	ratePairList ebRate; // temporary for rate calculation
	ratePairList efRate; // temporary for rate calculation
	ratePairList recRate; // temporary for rate calculation
	void updateRateGraphs(unsigned int xValue, double yValue, graph& gr, ratePairList& rpl) const throw();
	
	std::string _runNumber;
	std::string _rcState;
	std::string _rcError;
	bool _rcFault;
	std::string _recStatus;
	std::string _lbSwitchingMode;
	std::string _lbInterval;
        std::string _lbValue;
	std::string _l2AverageSize;
	std::string _partitionPageName; 
	std::string _startTime; 
	std::string _stopTime;  
	std::string _runTime;
	std::string _runType; 
	std::string _projectTag; 
	std::string _fullDetMask;
	
	std::string _vme;
	std::string _ecr;
	std::string _veto0;
	std::string _veto1;
	std::string _bckp;
        std::string _bckpl;
        std::string _rslt;
        std::string _rdt;
        std::string _mon;
        std::string _smpl;
        std::string _cmplx0;
        std::string _cmplx1;
        std::string _rslt2;
        std::string _ctpout12[5];
	std::string _ctpout13[5];
	std::string _ctpout14[5];
	std::string _ctpout15[5];
	std::string _connectorName12[5], _connectorName13[5], _connectorName14[5], _connectorName15[5];
        std::string _connectorIn12[5], _connectorIn13[5], _connectorIn14[5], _connectorIn15[5];
	bool _existBusyStatistics;
	
	//trigger info
	std::string _confSmKey, _confSmComment, _confL1PrescaleKey, _confL1PrescaleComment, _L1BunchGroupKey, _L1BunchGroupComment, _hltPrescaleKey, _hltPrescaleComment, _hltReleaseVersion;
	bool _existTrigPsKey, _existTrigL1BgKey, _existTrigConfL1PsKey, _existTrigConfSmKey, _existTrigConfRelease;

	std::string _savedEventsRate;
	
	std::string _TTC2LAN;
	std::string _Pixel;
	std::string _SCT;
	std::string _TRT;
	std::string _L1Calo;
	std::string _BCM;
	std::string _LArHFC;
	std::string _LArHFA;
	std::string _LArEMEC;
	std::string _LArEMB;
	std::string _LHCf;
	std::string _MDTB;
	std::string _MDTEC;
	std::string _TileEB;
	std::string _TileLB;
	std::string _CSC;
	std::string _TGCC;
	std::string _TCGA;
	std::string _RPC;
	std::string _MUCTPI;
	std::string _VME;
	std::string _ECR;
	std::string _Veto0;
	std::string _Veto1;
	std::string _Result;	
        // temporary since I don't understand graphs
	std::string _L1A;
	std::string _L2A;
	std::string _EFA;
	std::string _RECA;

      }; // PartitionData

      class DetectorInfo {
	
      public:
	typedef struct {
	  //std::string Muon;
	  std::string MuonMDT, MuonRPC, MuonCSC, MuonTGC;
	  //std::string Id;
	  std::string IdPixel, IdSCT, IdTRT;
	  std::string LAr;
	  std::string Til;
	  std::string Lumi;
	} subDetectors;

	DetectorInfo() throw();
	virtual ~DetectorInfo() throw();
	void decodeDetMask(std::vector<std::string>& detMaskElements, uint64_t detMask) const throw();
	void detectors(subDetectors& sd, const std::vector<std::string>& detMaskElemnts) const throw();
	
      private:

	typedef struct {
	  short dId;
	  std::string dName;
	} T_DetectorInfo;

	T_DetectorInfo dl[64];
	
	const std::string pix;
	const std::string sct;
	const std::string trt;
	const std::string lar;
	const std::string til;
	const std::string mdt;
	const std::string rpc;
	const std::string tgc;
	const std::string csc;
	const std::string bcm;
	const std::string lucid;
	const std::string zdc;
	const std::string alfa;

	//const boost::regex muonExpr;
        const boost::regex muonMDTExpr;
        const boost::regex muonRPCExpr;
        const boost::regex muonCSCExpr;
        const boost::regex muonTGCExpr;
	const boost::regex idPixelExpr;
        const boost::regex idSCTExpr;
        const boost::regex idTRTExpr;
	const boost::regex tilExpr;
	const boost::regex larExpr;
	const boost::regex lumiExpr;

	const std::string detSeparator;
	
	void initDetMask() throw();

      }; // DetectorInfo

    private:
	
      std::map<std::string, int> confMap;
      static std::string DQBaseName;
      static std::string RCMessagePage;
      static std::string TRPPage;
      static std::string RunEffPage;
      static std::string RunResPage;
      static std::string RatesPlot;
      static std::string RunSummaryPage;
      static std::string RemoteMonitoringPage;

      std::map<IPCPartition, PartitionData*> partMap;
      mutable daq::wmi::BR br;
      mutable daq::wmi::HR hr;
      static const DetectorInfo detInfo;
      static char* DQPageName;

      //static const std::string muonPageName;
      static const std::string mdtPageName;
      static const std::string rpcPageName;
      static const std::string cscPageName;
      static const std::string tgcPageName;
      static const std::string larPageName;
      static const std::string tilPageName;
      //static const std::string idPageName;
      static const std::string pixelPageName;
      static const std::string sctPageName;
      static const std::string trtPageName;
      static const std::string lumiPageName;
      static const unsigned int generalTableColNum;
      static const unsigned int systemTableColNum;
      static const std::string mirrorPartitionFilter;

      static const std::string RunNumberLabel;
      static const std::string RunTypeLabel;
      static const std::string DetLabel;
      static const std::string RootControllerStateLabel;
      static const std::string RootControllerErrorLabel;
      static const std::string RecordingStateLabel;
      static const std::string PartitionNameLabel;
      static const std::string RunStartTimeLabel;
      static const std::string RunStopTimeLabel;
      static const std::string TotalRunTimeLabel;
    };
    
    extern "C" daq::wmi::PluginBase* create() {
      return new RunParamsPlugin();
    }
    
    extern "C" void destroy(daq::wmi::PluginBase* p) {
      delete p;
    }
    
  } // end ccwmi namespace
} // end daq namespace

#endif

