// Run Status Plugin
// Configurable parameters: dq_page, rc_message_page, trp_page, rates_plot, run_summary_page, remote_monitoring_page, run_eff_page

#include "wmi/iframe.h"

//BUSY Information
#include "wmiccplugin.h"
#include "ISCTPBUSYNamed.h"
#include "ISCTPOUTNamed.h"

// Dead time info
#include "ISCTPCORENamed.h"

//Trigger information
#include "TrigConfHltPsKeyNamed.h"
#include "TrigConfL1BgKeyNamed.h"
#include "TrigConfL1PsKeyNamed.h"
#include "TrigConfReleaseNamed.h"
#include "TrigConfSmKeyNamed.h"


#include <wmi/link.h>
#include <wmi/text.h>
#include <wmi/picture.h>
#include <wmi/font.h>
#include <wmi/timegraph.h>
#include <wmi/tree.h>
#include <is/infodynany.h>
#include <is/infodictionary.h>
#include <rc/RunParamsNamed.h>
#include <rc/LBSettingsNamed.h>
#include <rc/LumiBlockNamed.h>
#include <rc/RunInfoNamed.h>
#include <rc/RCStateInfoNamed.h>
#include <DF_IS_Info/SFONamed.h>
#include <DF_IS_Info/SFINamed.h>
#include <DF_IS_Info/L2SVNamed.h>
#include <ddc/DdcStringInfoNamed.h>
#include <ddc/DdcIntInfoNamed.h>
#include <ddc/DdcFloatInfoNamed.h>

#include <config/Configuration.h>
#include <dal/Partition.h>
#include "dal/IS_InformationSources.h"
#include "dal/IS_EventsAndRates.h"
#include <ers/Issue.h>

#include <boost/date_time/posix_time/posix_time.hpp>

#include <utility>
#include <list>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>
using daq::wmi::Text;
using daq::wmi::Table;
using daq::wmi::TextOption;
using daq::wmi::Link;
using daq::wmi::Color;
using daq::wmi::Picture;
using daq::wmi::TimeGraph;
using daq::wmi::IFrame;
using daq::wmi::Tree;
using daq::wmi::Node;

std::string runComPage = "AtlasRunCom.html";
//Data Quality Page Name
//std::string daq::ccwmi::RunParamsPlugin::DQBaseName("https://atlasop.cern.ch/atlas-point1/wmi/current/Data%20Quality%20Monitoring_wmi/");
std::string daq::ccwmi::RunParamsPlugin::RCMessagePage("https://atlasop.cern.ch/twiki/bin/view/Main/RCWhiteBoard#DailyPlan");
std::string daq::ccwmi::RunParamsPlugin::RunSummaryPage("http://atlas-service-db-runlist.web.cern.ch/atlas-service-db-runlist/query.html");
std::string daq::ccwmi::RunParamsPlugin::RemoteMonitoringPage("https://twiki.cern.ch/twiki/bin/view/Atlas/TDAQPoint1RemoteMonitoring#Using_a_Web_Browser");

std::string daq::ccwmi::RunParamsPlugin::DQBaseName("../Data%20Quality%20Monitoring_wmi/");
std::string daq::ccwmi::RunParamsPlugin::TRPPage("../WTRP_wmi/");
std::string daq::ccwmi::RunParamsPlugin::RatesPlot("../WTRP_wmi/pot_globalRates_1.png");
std::string daq::ccwmi::RunParamsPlugin::RunEffPage("../Run_Eff_wmi/");
std::string daq::ccwmi::RunParamsPlugin::RunResPage("../Run Resources_wmi/ATLAS_Resources.html");

const std::string daq::ccwmi::RunParamsPlugin::PartitionData::L1("l1");
const std::string daq::ccwmi::RunParamsPlugin::PartitionData::L2("l2");
const std::string daq::ccwmi::RunParamsPlugin::PartitionData::EB("eb");
const std::string daq::ccwmi::RunParamsPlugin::PartitionData::EF("ef");
const std::string daq::ccwmi::RunParamsPlugin::PartitionData::REC("rec");
unsigned int daq::ccwmi::RunParamsPlugin::PartitionData::graphWidth(300);
unsigned int daq::ccwmi::RunParamsPlugin::PartitionData::graphHeight(150);
const unsigned int daq::ccwmi::RunParamsPlugin::PartitionData::graphTableColNum(3);

//const std::string daq::ccwmi::RunParamsPlugin::muonPageName("Muon.html");
const std::string daq::ccwmi::RunParamsPlugin::mdtPageName("MDTsubdet.html");
const std::string daq::ccwmi::RunParamsPlugin::rpcPageName("RPCsubdet.html");
const std::string daq::ccwmi::RunParamsPlugin::cscPageName("CSCsubdet.html");
const std::string daq::ccwmi::RunParamsPlugin::tgcPageName("TGCsubdet.html");

const std::string daq::ccwmi::RunParamsPlugin::larPageName("LAr.html");
//const std::string daq::ccwmi::RunParamsPlugin::idPageName("ID.html");
const std::string daq::ccwmi::RunParamsPlugin::pixelPageName("Pixelsubdet.html");
const std::string daq::ccwmi::RunParamsPlugin::sctPageName("SCTsubdet.html");
const std::string daq::ccwmi::RunParamsPlugin::trtPageName("TRTsubdet.html");

const std::string daq::ccwmi::RunParamsPlugin::tilPageName("Til.html");
const std::string daq::ccwmi::RunParamsPlugin::lumiPageName("Lumi.html");
const std::string daq::ccwmi::RunParamsPlugin::mirrorPartitionFilter("_mirror");
const unsigned int daq::ccwmi::RunParamsPlugin::generalTableColNum(9);
const unsigned int daq::ccwmi::RunParamsPlugin::systemTableColNum(5);

const std::string daq::ccwmi::RunParamsPlugin::RunNumberLabel("Run Number");
const std::string daq::ccwmi::RunParamsPlugin::RunTypeLabel("Run Type");
const std::string daq::ccwmi::RunParamsPlugin::DetLabel("Active Detectors");
const std::string daq::ccwmi::RunParamsPlugin::RootControllerStateLabel("Root Controller State");
//const std::string daq::ccwmi::RunParamsPlugin::RootControllerErrorLabel("Root Controller Error");
const std::string daq::ccwmi::RunParamsPlugin::RecordingStateLabel("Recording State");
const std::string daq::ccwmi::RunParamsPlugin::PartitionNameLabel("Partition Name");
const std::string daq::ccwmi::RunParamsPlugin::RunStartTimeLabel("Run Start Time");
const std::string daq::ccwmi::RunParamsPlugin::RunStopTimeLabel("Run Stop Time");
const std::string daq::ccwmi::RunParamsPlugin::TotalRunTimeLabel("Total Run Time");

const daq::ccwmi::RunParamsPlugin::DetectorInfo daq::ccwmi::RunParamsPlugin::detInfo;
const daq::wmi::Color RED(255, 0, 0);
const daq::wmi::Color DARK_RED(183, 60, 60);
const daq::wmi::Color DARK_BLUE(60, 60, 183);
const daq::wmi::Color WHITE(255, 255, 255);
const daq::wmi::Color GREY(198, 198, 198);
const daq::wmi::Color DARK_GREY(150, 151, 150);
const daq::wmi::Color CELL_BG(219, 239, 251);
const daq::wmi::Color CAPTION_BG(0, 64, 128);
const daq::wmi::Color LIGHT_GREY(240, 240, 240);
const daq::wmi::Color TABLE_BG(107, 127, 147); //Color found as background color of the tables from LAR page: http://pcatdwww.cern.ch/atlas-point1/lar/geninfo/lar.php?subdet=LAR : blueish gray...
const daq::wmi::Color BLACK(0, 0, 0);
#include <sstream>

std::string intToString(unsigned int value){
  std::ostringstream intStream;
  intStream << value;
  return intStream.str();
}

std::string floatToString(float value){
  std::ostringstream intStream;
  intStream << value;
  return intStream.str();
}

std::string doubleToString(double value){
  //especially made for values between 0 and 100. For values outside this range it will return an integer - float with 0 precision
  std::ostringstream doubleStream;
  if (value > 99.999 || value < 0.001) {
    doubleStream << std::fixed << std::setprecision(0) << value;
  }
  else {
    doubleStream << std::fixed << std::setprecision(3) << value;
  }
  return doubleStream.str();
}

namespace daq {
  namespace ccwmi {

    /***********************************/
    /** RunParamsPlugin::DetectorInfo **/
    /***********************************/

    RunParamsPlugin::DetectorInfo::DetectorInfo() throw() 
      : pix("Pix"), sct("SCT"), trt("TRT"), lar("LAr"), til("Til"), mdt("MDT"), rpc("RPC"), tgc("TGC"), csc("CSC"),
	bcm("BCM"), lucid("Lucid"), zdc("ZDC"), alfa("Alfa"), 
	muonMDTExpr(".*" + mdt + ".*"),
	muonRPCExpr(".*" + rpc + ".*"),
	muonCSCExpr(".*" + csc + ".*"),
	muonTGCExpr(".*" + tgc + ".*"),
	idPixelExpr(".*" + pix + ".*"),
	idSCTExpr(".*" + sct + ".*"),
	idTRTExpr(".*" + trt + ".*"),
	tilExpr(".*" + til + ".*"),
	larExpr(".*" + lar + ".*"),
	lumiExpr(".*" + bcm + ".*|" + ".*" + lucid + ".*|" + ".*" + zdc + ".*|" + ".*" + alfa + ".*"),
	detSeparator(" - ")
    {
      this->initDetMask();
    }

    RunParamsPlugin::DetectorInfo::~DetectorInfo()
      throw()
    {
    }
    
    void 
    RunParamsPlugin::DetectorInfo::detectors(subDetectors& sd, const std::vector<std::string>& detMaskElements) const 
      throw()
    {
      sd.MuonMDT.clear();
      sd.MuonRPC.clear();
      sd.MuonCSC.clear();
      sd.MuonTGC.clear();

      sd.IdPixel.clear();
      sd.IdSCT.clear();
      sd.IdTRT.clear();

      sd.LAr.clear();
      sd.Til.clear();
      sd.Lumi.clear();

      std::vector<std::string>::const_iterator it;
      for(it = detMaskElements.begin(); it != detMaskElements.end(); ++it) {
	try {
	  if(boost::regex_match(*it, this->muonMDTExpr)) {
	    sd.MuonMDT.append(*it + this->detSeparator);

	  } else if(boost::regex_match(*it, this->muonRPCExpr)) {
	    sd.MuonRPC.append(*it + this->detSeparator);

	  } else if(boost::regex_match(*it, this->muonTGCExpr)) {
	    sd.MuonTGC.append(*it + this->detSeparator);

	  } else if(boost::regex_match(*it, this->muonCSCExpr)) {
	    sd.MuonCSC.append(*it + this->detSeparator);

	  } else if(boost::regex_match(*it, this->idPixelExpr)) {
	    sd.IdPixel.append(*it + this->detSeparator);

	  } else if(boost::regex_match(*it, this->idSCTExpr)) {
	    sd.IdSCT.append(*it + this->detSeparator);

	  } else if(boost::regex_match(*it, this->idTRTExpr)) {
	    sd.IdTRT.append(*it + this->detSeparator);

	  } else if(boost::regex_match(*it, this->tilExpr)) {
	    sd.Til.append(*it + this->detSeparator);
	  } else if(boost::regex_match(*it, this->larExpr)) {
	    sd.LAr.append(*it + this->detSeparator);
	  } else if(boost::regex_match(*it, this->lumiExpr)) {
	    sd.Lumi.append(*it + this->detSeparator);
	  }
	}
	catch(std::exception& ex) {
	  ERS_LOG("Error parsing detector mask: " << ex.what());
	}
      }
    }

    void 
    RunParamsPlugin::DetectorInfo::initDetMask() 
      throw()
    {
      this->dl[0].dId = 0x11; this->dl[0].dName = this->pix + " Barrel";
      this->dl[1].dId = 0x12; this->dl[1].dName = this->pix + " Disk";
      this->dl[2].dId = 0x13; this->dl[2].dName = this->pix + " B-Layer";
      this->dl[3].dId = 0x14; this->dl[3].dName = "REMOVED";
      
      this->dl[4].dId = 0x21; this->dl[4].dName = this->sct + " BA";
      this->dl[5].dId = 0x22; this->dl[5].dName = this->sct + " BC"; 
      this->dl[6].dId = 0x23; this->dl[6].dName = this->sct + " EA"; 
      this->dl[7].dId = 0x24; this->dl[7].dName = this->sct + " EC"; 
      
      this->dl[8].dId = 0x31; this->dl[8].dName = this->trt + " BA";
      this->dl[9].dId = 0x32; this->dl[9].dName = this->trt +" BC";
      this->dl[10].dId = 0x33; this->dl[10].dName = this->trt + " EA";
      this->dl[11].dId = 0x34; this->dl[11].dName = this->trt + " EC";
      
      this->dl[12].dId = 0x41; this->dl[12].dName = this->lar + " EMBA";
      this->dl[13].dId = 0x42; this->dl[13].dName = this->lar + " EMBC";
      this->dl[14].dId = 0x43; this->dl[14].dName = this->lar + " EMECA";
      this->dl[15].dId = 0x44; this->dl[15].dName = this->lar + " EMECC";
      this->dl[16].dId = 0x45; this->dl[16].dName = this->lar + " HECA";
      this->dl[17].dId = 0x46; this->dl[17].dName = this->lar + " HECC";
      this->dl[18].dId = 0x47; this->dl[18].dName = this->lar + " FCALA";
      this->dl[19].dId = 0x48; this->dl[19].dName = this->lar + " FCALC";
      
      this->dl[20].dId = 0x51; this->dl[20].dName = this->til + " BA"; 
      this->dl[21].dId = 0x52; this->dl[21].dName = this->til + " BC";
      this->dl[22].dId = 0x53; this->dl[22].dName = this->til + " EA";
      this->dl[23].dId = 0x54; this->dl[23].dName = this->til + " EC";
      
      this->dl[24].dId = 0x61; this->dl[24].dName = this->mdt + " BA";
      this->dl[25].dId = 0x62; this->dl[25].dName = this->mdt + " BC";
      this->dl[26].dId = 0x63; this->dl[26].dName = this->mdt + " EA";
      this->dl[27].dId = 0x64; this->dl[27].dName = this->mdt + " EC";
      this->dl[28].dId = 0x65; this->dl[28].dName = this->rpc + " BA";
      this->dl[29].dId = 0x66; this->dl[29].dName = this->rpc + " BC";
      this->dl[30].dId = 0x67; this->dl[30].dName = this->tgc + " EA";
      this->dl[31].dId = 0x68; this->dl[31].dName = this->tgc + " EC";
      
      this->dl[32].dId = 0x69; this->dl[32].dName = this->csc + " EA";
      this->dl[33].dId = 0x6a; this->dl[33].dName = this->csc + " EC";
      
      this->dl[34].dId = 0x71; this->dl[34].dName = "L1 calo preprocessor";
      this->dl[35].dId = 0x72; this->dl[35].dName = "L1 calo cluster DAQ";
      this->dl[36].dId = 0x73; this->dl[36].dName = "L1 calo cluster RoI";
      this->dl[37].dId = 0x74; this->dl[37].dName = "L1 calo Jet/E DAQ";
      this->dl[38].dId = 0x75; this->dl[38].dName = "L1 calo Jet/E RoI";
      this->dl[39].dId = 0x76; this->dl[39].dName = "MUCTPI";
      this->dl[40].dId = 0x77; this->dl[40].dName = "CTP";
      this->dl[41].dId = 0x78; this->dl[41].dName = "L2SV";
      this->dl[42].dId = 0x79; this->dl[42].dName = "SFI";
      this->dl[43].dId = 0x7a; this->dl[43].dName = "SFO";
      this->dl[44].dId = 0x7b; this->dl[44].dName = "LVL2";
      this->dl[45].dId = 0x7c; this->dl[45].dName = "EF";
      
      this->dl[46].dId = 0x81; this->dl[46].dName = this->bcm;
      this->dl[47].dId = 0x82; this->dl[47].dName = this->lucid;
      this->dl[48].dId = 0x83; this->dl[48].dName = this->zdc;
      this->dl[49].dId = 0x84; this->dl[49].dName = this->alfa;
      
      for (unsigned int i=50; i<64; i++) {
	this->dl[i].dId = 0x00;
	this->dl[i].dName = "unknown";
      }
    }

    void
    RunParamsPlugin::DetectorInfo::decodeDetMask(std::vector<std::string>& detMaskElements, uint64_t detMask) const
      throw() 
    {
      detMaskElements.clear();
      uint64_t offset = 1;
      
      if(detMask != 0) { 
	for(int n = 0; n < 51; ++n) {
	  if(detMask & (offset << n)) {
	    detMaskElements.push_back(this->dl[n].dName); 
	  }
	}
      } else {
	detMaskElements.push_back("N/A");
      }
    }

    /************************************/
    /** RunParamsPlugin::PartitionData **/
    /************************************/

    RunParamsPlugin::PartitionData::PartitionData(const std::string& partName, unsigned int maxValues) 
      throw() : 
      partitionName(partName), 
      graphPoints(maxValues)
    {
      this->createGraphGroups(L1);
      this->createGraphGroups(L2);
      this->createGraphGroups(EB);
      this->createGraphGroups(EF);
      this->createGraphGroups(REC);
    }

    RunParamsPlugin::PartitionData::~PartitionData()
      throw()
    {
      this->cleanGraphs();
    } 

    void
    RunParamsPlugin::PartitionData::createGraphGroups(const std::string& graphGroupName)
      throw() 
    {
      std::vector<graph> vec(2);
      this->grMap[graphGroupName] = vec;
    }

    void 
    RunParamsPlugin::PartitionData::updateGraph(long long xValue, double yValue, graph& gr) const
      throw()
    {
      if(!(gr.size() < this->graphPoints)) {
	// If the graph already contains the maximum number of points
	// then remove the first point and append the new one
	gr.erase(gr.begin());	
      }
      gr.push_back(std::make_pair((unsigned long)xValue, (float)yValue));      
    } 

    void
    RunParamsPlugin::PartitionData::cleanGraphs()
      throw()
    {
      (this->grMap[L1])[0].clear();
      (this->grMap[L1])[1].clear();
      (this->grMap[L2])[0].clear();
      (this->grMap[L2])[1].clear();
      (this->grMap[EB])[0].clear();
      (this->grMap[EB])[1].clear();
      (this->grMap[EF])[0].clear();
      (this->grMap[EF])[1].clear();
      (this->grMap[REC])[0].clear();
      (this->grMap[REC])[1].clear();
    }

    void 
    RunParamsPlugin::PartitionData::updateL1Counter(long long xValue, double yValue)
      throw()
    {
      this->updateGraph(xValue, yValue, (this->grMap[L1])[0]);
    } 

    void 
    RunParamsPlugin::PartitionData::updateL1Rate(long long xValue, double rate)
      throw()
    {
      this->updateGraph(xValue, rate, (this->grMap[L1])[1]);
    } 

    void 
    RunParamsPlugin::PartitionData::updateL2Counter(long long xValue, double yValue)
      throw()
    {
      this->updateGraph(xValue, yValue, (this->grMap[L2])[0]);
    }    
   
    void 
    RunParamsPlugin::PartitionData::updateL2Rate(long long xValue, double rate)
      throw()
    {
      this->updateGraph(xValue, rate, (this->grMap[L2])[1]);
    }
    
    void 
    RunParamsPlugin::PartitionData::updateEBCounter(long long xValue, double yValue)
      throw()
    {
      this->updateGraph(xValue, yValue, (this->grMap[EB])[0]);
    }

    void 
    RunParamsPlugin::PartitionData::updateEBRate(long long xValue,  double rate)
      throw()
    {
      this->updateGraph(xValue, rate, (this->grMap[EB])[1]);
    }
    
    void 
    RunParamsPlugin::PartitionData::updateEFCounter(long long xValue, double yValue)
      throw()
    {
      this->updateGraph(xValue, yValue, (this->grMap[EF])[0]);
    } 

    void 
    RunParamsPlugin::PartitionData::updateEFRate(long long xValue, double rate)
      throw()
    {
      this->updateGraph(xValue, rate, (this->grMap[EF])[1]);
    } 

    void
    RunParamsPlugin::PartitionData::updateRECCounter(long long xValue, double yValue)
      throw()
    {
      this->updateGraph(xValue, yValue, (this->grMap[REC])[0]);
    } 

    void 
    RunParamsPlugin::PartitionData::updateRECRate(long long xValue, double rate)
      throw()
    {
      this->updateGraph(xValue, rate, (this->grMap[REC])[1]);
          
    } 

    void 
    RunParamsPlugin::PartitionData::printGraphs(Table& tb) const
      throw()
    {

      static std::string titleLVL1Rate("L1 Rate[Hz] - Accepts: ");
      std::string l1accepts = (RunParamsPlugin::PartitionData::_L1A.empty())?"N/A":RunParamsPlugin::PartitionData::_L1A;
      std::string tmp1 = titleLVL1Rate;
      tmp1.append(l1accepts, 0 ,l1accepts.size());

      static std::string titleLVL2Rate("L2 Rate[Hz] - Accepts: ");
      std::string l2accepts = (RunParamsPlugin::PartitionData::_L2A.empty())?"N/A":RunParamsPlugin::PartitionData::_L2A;
      std::string tmp2 = titleLVL2Rate;
      tmp2.append(l2accepts, 0 ,l2accepts.size());

      static  std::string titleEFRate("EF Rate[Hz] - Accepts: ");
      std::string efaccepts = (RunParamsPlugin::PartitionData::_EFA.empty())?"N/A":RunParamsPlugin::PartitionData::_EFA;
      std::string tmp3 = titleEFRate;
      tmp3.append(efaccepts, 0 ,efaccepts.size());

      const unsigned int pWidth(RunParamsPlugin::PartitionData::graphWidth);
      const unsigned int pHeight(RunParamsPlugin::PartitionData::graphHeight);
  		
      tb.setWidth(3*pWidth);
      tb.setTDWidth(1, pWidth);
      tb.setTDWidth(2, pWidth);  		
      tb.setTDWidth(3, pWidth);  		
      
      tb.setAlignCell(1,1,2);
      tb.setAlignCell(2,1,2);
      tb.setAlignCell(3,1,2);
      tb.addElement(1, 1, new Text(tmp1, 3, WHITE, TextOption("b")));
      tb.addElement(2, 1, new Text(tmp2, 3, WHITE, TextOption("b")));
      tb.addElement(3, 1, new Text(tmp3, 3, WHITE, TextOption("b")));
      tb.addRow();
      tb.addElement(1, 2, new TimeGraph("", "", ((this->grMap.find(L1))->second)[1], pWidth, pHeight));
      tb.addElement(2, 2, new TimeGraph("", "", ((this->grMap.find(L2))->second)[1], pWidth, pHeight)); 
      tb.addElement(3, 2, new TimeGraph("", "", ((this->grMap.find(EF))->second)[1], pWidth, pHeight));


    }
      
    /*********************/
    /** RunParamsPlugin **/
    /*********************/
    
    RunParamsPlugin::RunParamsPlugin() : 
      PluginBase()
    {
      
    }

    RunParamsPlugin::~RunParamsPlugin() {
      std::map<IPCPartition, PartitionData*>::iterator mapIt;
      for(mapIt = this->partMap.begin(); mapIt != this->partMap.end(); ) {
	delete mapIt->second;
	this->partMap.erase(mapIt++);
      }
      delete m_outStream;
    }

    void RunParamsPlugin::periodicAction() {      
      // Update the partition list
      this->updatePartitions();
     
      // Build the partition info table
      this->buildTables();

      // Build partition specific pages
      this->buildPartitionPages();

      // Close all the ouput streams
      m_outStream->closeAll();      
    }

    void 
    RunParamsPlugin::stop() 
    {
      // stop()
      m_outStream->closeAll();      
    }

    void 
    RunParamsPlugin::configure(const ::wmi::InfoParameter& params) 
    {
      this->confMap["maxPlotEntries"] = 50; // default value

      // Read settings from the config file
      unsigned int nums = params.length();
      for(unsigned int i = 0; i < nums; ++i) {
	const char* const parName = params[i].name;
	const char* const parVal = params[i].value;
	if (!strcmp(parName,"dq_page")){
	  std::string paramVal(parVal);
	  this->DQBaseName = paramVal;
	}
	else if (!strcmp(parName, "rc_message_page")) {
	  std::string paramVal(parVal);
	  this->RCMessagePage = paramVal;
	}
	else if(!strcmp(parName, "trp_page")) {
	  std::string paramVal(parVal);
	  this->TRPPage = paramVal;	  
	}
	else if(!strcmp(parName, "run_eff_page")) {
	  std::string paramVal(parVal);
	  this->RunEffPage = paramVal;	  
	}
	else if(!strcmp(parName, "run_res_page")) {
	  std::string paramVal(parVal);
	  this->RunResPage = paramVal;	  
	}	
	else if (!strcmp(parName, "rates_plot")) {
	  std::string paramVal(parVal);
	  this->RatesPlot = paramVal;
	}
	else if (!strcmp(parName, "run_summary_page")) {
	  std::string paramVal(parVal);
	  this->RunSummaryPage = paramVal;
	}	
	else if (!strcmp(parName, "remote_monitoring_page")) {
	  std::string paramVal(parVal);
	  this->RemoteMonitoringPage = paramVal;
	}
	else if (!strcmp(parName, "picture_width"))
	    PartitionData::graphWidth = ::atoi(parVal);
	else if (!strcmp(parName, "picture_height"))
	  PartitionData::graphHeight = ::atoi(parVal);
	else {
	  this->confMap[parName] = ::atoi(parVal);	
	}
      }      
    }

    void
    RunParamsPlugin::updatePartitions()
      throw()
    {
      std::list<IPCPartition> partList;
      IPCPartition::getPartitions(partList);
      
      // Look for partitions in map: if not present add it
      std::list<IPCPartition>::const_iterator listIt;
      for(listIt = partList.begin(); listIt != partList.end(); ++listIt) {
	if((this->partMap.find(*listIt) == this->partMap.end()) && (listIt->name().find(RunParamsPlugin::mirrorPartitionFilter) == std::string::npos)) {
	  // Partitions whose name contains "RunParamsPlugin::mirrorPartitionFilter" are filtered
	  this->partMap[(*listIt)] = new PartitionData(listIt->name(), this->confMap["maxPlotEntries"]);
	  ERS_DEBUG(1, "Added partition " << listIt->name());
	}
      }
      
      // Remove from map no more existing partition
      std::map<IPCPartition, PartitionData*>::iterator mapIt;
      for(mapIt = this->partMap.begin(); mapIt != this->partMap.end(); ) {
	std::list<IPCPartition>::const_iterator p = std::find(partList.begin(), partList.end(), mapIt->first);
	if(p == partList.end()) {
	  ERS_DEBUG(1, "Removed partition " << (mapIt->first).name());
	  delete mapIt->second;
	  this->partMap.erase(mapIt++);
	} else {
	  ++mapIt;
	}
      }
    }

    void
    RunParamsPlugin::buildPartitionPages() const
      throw()
    {
      std::map<IPCPartition, PartitionData*>::const_iterator mapIt;      
      std::map<IPCPartition, PartitionData*>::const_iterator mapItBegin(this->partMap.begin());
      std::map<IPCPartition, PartitionData*>::const_iterator mapItEnd(this->partMap.end());
      
      bool atlasExists = false;
      for(mapIt = mapItBegin; mapIt != mapItEnd; ++mapIt) { 
	const std::string currentPartitionName((mapIt->first).name());
	if (currentPartitionName == "ATLAS") atlasExists = true;

	const std::string partitionPageName(currentPartitionName + ".html");
	
	// Open the partition page
	int fileIndex = m_outStream->open(partitionPageName);

	// Write page title
	Table mainTitle(1, 2);
	mainTitle.setWidth (100, true);
	mainTitle.setAlignCell(1, 1, 2);
	mainTitle.setAlignCell(1, 2, 2);
	const std::string Part("Partition " + currentPartitionName);
	const std::string TTCPart("TTC Partitions: " + mapIt->second->_fullDetMask);
	//Text PartTx(Part, 5, DARK_RED, TextOption("b"));
	mainTitle.addElement(1, 1, new Text(Part, 6, DARK_RED, TextOption("b")));
	mainTitle.addElement(1, 2, new Text(TTCPart, 1, DARK_BLUE, TextOption("i")));

	m_outStream->write(mainTitle);

	m_outStream->write(this->hr);
	//m_outStream->write(this->br);
	// Link the previously generated page to top
	std::ostringstream lhctxt;
	lhctxt << "LHC page 1 message: ";
	try {
	  IPCPartition initP("initial");
	  ddc::DdcStringInfoNamed lhcmsg(initP, "LHC.LHCPage1Msg");
	  lhcmsg.checkout();
	  lhctxt << lhcmsg.value;
	}
	catch(ers::Issue & e) {
	  // happily ignore
	}
	
	Text LHCP1(lhctxt.str().c_str(),3, DARK_RED);
	m_outStream->write(LHCP1);
	m_outStream->write(this->br);
	Link RunComLk(runComPage, "Check today's program here!");
	m_outStream->write(RunComLk);
	if(currentPartitionName == "ATLAS") {
	  Text space1("*   *",3, DARK_RED, TextOption("b"));
	  m_outStream->write(space1);
	  Link RunEffLk(RunEffPage, "Data taking efficiency");
	  m_outStream->write(RunEffLk);
	  Text space2("*   *",3, DARK_RED, TextOption("b"));
	  m_outStream->write(space2);
	  Link RunResLk(RunResPage, "Status of resources");
	  m_outStream->write(RunResLk);
	  Text space3("*   *",3, DARK_RED, TextOption("b"));
	  m_outStream->write(space3);
	  Link WebSrvLk(this->RemoteMonitoringPage, "Other Web Based Services", false);
	  m_outStream->write(WebSrvLk);
	}
	m_outStream->write(this->br);
	Link OtherPartsLk("index.html", "Other active partitions can be seen here.");
	m_outStream->write(OtherPartsLk);
	m_outStream->write(this->br);
	{
	  /********************************/
	  /** composedTable contains: 2 tables containing run information: infoTable - RC state, run number and type and luminosity value and secondInfoTable - error state, run tag and time, changing interval of the luminosity value; 1 table containing event information: average size, number of recorded events and the throughput to disk; one table with trigger information and a last one with beam info. These 4 tables are displayed in a row **/
	  /********************************/

	  Table * composedTable = new Table(4, 2);
	  setTableColors(*composedTable, 4, 2, true, true);
	  composedTable->setBGColor(LIGHT_GREY);

	  // First Table
	  Table* infoTable = new Table(2, 5);	  
	  infoTable->setWidth(100, true);
	  infoTable->setHeight(100, true);
	  setTableColors(*infoTable, 2, 5, true, false);
	  infoTable->setBGColor(LIGHT_GREY);

	  fillLabelCell(*infoTable, 1, 1, "Run State");
	  
	  std::unique_ptr<Color> runStateColor;
	  if(mapIt->second->_rcFault) {
	    runStateColor = std::unique_ptr<Color>(new Color(255, 0, 0));
	  } else if(mapIt->second->_rcState == "N/A" || mapIt->second->_rcState.empty()) {
	    runStateColor = std::unique_ptr<Color>(new Color(190, 190, 190));
	  } else if(mapIt->second->_rcState == "RUNNING") {
	    runStateColor = std::unique_ptr<Color>(new Color(0, 255, 0));
	  } else {
	    runStateColor = std::unique_ptr<Color>(new Color(34, 139, 34));
	  }

	  fillColorCell(*infoTable, 2, 1, mapIt->second->_rcState, "", 4, *(runStateColor.get()), "");

	  fillLabelCell(*infoTable, 1, 2, "Run Tag");
	  fillInfoCell(*infoTable, 2, 2, mapIt->second->_projectTag, "");
	  fillLabelCell(*infoTable, 1, 3, "Run Type");
	  fillInfoCell(*infoTable, 2, 3, mapIt->second->_runType, "");	
	  fillLabelCell(*infoTable, 1, 4, "Run Number");
	  fillInfoCell(*infoTable, 2, 4, mapIt->second->_runNumber, ""); 


	  if (currentPartitionName == "ATLAS") {
	    fillLabelCell(*infoTable, 1, 5, "Run Mode");

	    ISInfoBool ready4phys=false;
	    IPCPartition p(currentPartitionName);
	    ISInfoDictionary dict(p);
	    try {
	      dict.getValue("RunParams.Ready4Physics", ready4phys );
	    }
	    catch (ers::Issue &e) {
	      //ignore - leave flag as false
	    }
	    fillInfoCell(*infoTable, 2, 5, ready4phys ? "Ready":"Standby", ""); 
	    
	  }
	  else {
	     fillLabelCell(*infoTable, 1, 5, "Run Mode");
	     fillInfoCell(*infoTable, 2, 5, "N/A", "");
	  }
	  

	  composedTable->addElement(1, 1, new Text("Run Info", 5, WHITE, TextOption("b")));
	  composedTable->addElement(1, 2, infoTable);

	  // Second Table
	  Table* secondInfoTable = new Table(2, 5);
	  secondInfoTable->setWidth(100, true);
	  secondInfoTable->setHeight(100, true);
	  setTableColors(*secondInfoTable, 2, 5, true, false);
	  secondInfoTable->setBGColor(LIGHT_GREY);

	  fillLabelCell(*secondInfoTable, 1, 1, "RunTime");
	  fillInfoCell(*secondInfoTable, 2, 1, mapIt->second->_runTime, "");	  
	  fillLabelCell(*secondInfoTable, 1, 2, "Luminosity Block");
	  fillInfoCell(*secondInfoTable, 2, 2, mapIt->second->_lbValue, "");
	  fillLabelCell(*secondInfoTable, 1, 3, "LB changes every");
	  if (!mapIt->second->_lbInterval.empty())
	    fillInfoCell(*secondInfoTable, 2, 3, mapIt->second->_lbInterval + " " + mapIt->second->_lbSwitchingMode, "");
	  else
	    fillInfoCell(*secondInfoTable, 2, 3, "N/A", ""); 
	  
	  fillLabelCell(*secondInfoTable, 1, 4, "Average Event Size [MB]");
	  fillInfoCell(*secondInfoTable, 2, 4, mapIt->second->_l2AverageSize, "");
	  fillLabelCell(*secondInfoTable, 1, 5, "Throughput to Disk [MB/s]");
	  fillInfoCell(*secondInfoTable, 2, 5, mapIt->second->_savedEventsRate, "");
	  composedTable->addElement(2, 1, new Text("Run Statistics", 5, WHITE, TextOption("b")));	  
	  composedTable->addElement(2, 2, secondInfoTable);

	  /***********  New VALUEs taken from trigger status ***/

	  /************* Trigger Table ************/
	  Table* triggerTable = new Table(2, 5);
	  triggerTable->setWidth(100, true);
	  triggerTable->setHeight(100, true);

	  if (mapIt->second->_existTrigPsKey || mapIt->second->_existTrigL1BgKey || mapIt->second->_existTrigConfL1PsKey || mapIt->second->_existTrigConfSmKey){
	    triggerTable->setBGColor(LIGHT_GREY);
	    setTableColors(*triggerTable, 2, 5, true, false);

	    std::string trgURL = "http://atlas-trigconf.cern.ch/run/smkey/" + mapIt->second->_confSmKey 
	      + "/l1key/" + mapIt->second->_confL1PrescaleKey + "/hltkey/" + mapIt->second->_hltPrescaleKey + "/";

	    std::string trgLable = mapIt->second->_confSmKey + "' " + mapIt->second->_confL1PrescaleKey + "' " + mapIt->second->_hltPrescaleKey;
	    Link *SmkLink = new Link(trgURL, trgLable );

	    fillLabelCell(*triggerTable, 1, 1, "Master & Prescale Keys");
	    triggerTable->addElement(2, 1, SmkLink);
	    /*
	    Link *L1pLink = new Link(trgURL, mapIt->second->_confL1PrescaleKey);
	    fillLabelCell(*triggerTable, 1, 2, "L1 Prescales");
	    triggerTable->addElement(2, 2, L1pLink);

	    Link *HltpLink = new Link(trgURL, mapIt->second->_hltPrescaleKey);
	    fillLabelCell(*triggerTable, 1, 3, "HLT Prescales");
	    triggerTable->addElement(2, 3, HltpLink);
	    */

	    IPCPartition p(currentPartitionName);
	    ISCTPCORENamed deadTimeInfo(currentPartitionName, "L1CT.ISCTPCORE");
	    try {
	      deadTimeInfo.checkout();
	      fillLabelCell(*triggerTable, 1, 3, "Simple Deadtime");
	      fillInfoCell(*triggerTable, 2, 3, intToString(deadTimeInfo.Simple_Deadtime), "") ;
	      fillLabelCell(*triggerTable, 1, 4, "Complex Deadtime");
	      fillInfoCell(*triggerTable, 2, 4, intToString(deadTimeInfo.Complex_Deadtime2_Level)+"/"+intToString(deadTimeInfo.Complex_Deadtime2_Rate), "") ;	      
	    }
	    catch (ers::Issue &e) {
	      
	      fillLabelCell(*triggerTable, 1, 3, "Simple Deadtime");
	      fillInfoCell(*triggerTable, 2, 3, "N/A", "");
	      fillLabelCell(*triggerTable, 1, 4, "Complex Deadtime");
	      fillInfoCell(*triggerTable, 2, 4, "N/A", "");
	    }

	    Link *BgLink = new Link("http://atlas-trigconf.cern.ch/bunchgroups?key="+mapIt->second->_L1BunchGroupKey , mapIt->second->_L1BunchGroupKey);
	    fillLabelCell(*triggerTable, 1, 2, "L1 Bunch Group");
	    triggerTable->addElement(2, 2, BgLink);



	    fillLabelCell(*triggerTable, 1, 5, "HLT Release Version");
	    fillInfoCell(*triggerTable, 2, 5, mapIt->second->_hltReleaseVersion, "");

	    composedTable->addElement(3, 1, new Text("Trigger Info", 5, WHITE, TextOption("b")));
	    composedTable->addElement(3, 2, triggerTable);
	  }

	  // Beam Table
	  Table* beamTable = new Table(2, 5);
	  beamTable->setWidth(100, true);
	  beamTable->setHeight(100, true);
	  beamTable->setBGColor(LIGHT_GREY);
	  setTableColors(*beamTable, 2, 5, true, false);

	  composedTable->addElement(4, 1, new Text("Beam Info", 5, WHITE, TextOption("b")));

	  try {
	    IPCPartition initP("initial");
	    ddc::DdcStringInfoNamed beamModeInfo(initP, "LHC.BeamMode");
	    beamModeInfo.checkout();
	    fillLabelCell(*beamTable, 1, 1, "Beam Mode");
	    fillInfoCell(*beamTable, 2, 1, beamModeInfo.value.c_str() , "");

	    ddc::DdcIntInfoNamed beamOneInfo(initP, "LHC.BeamStatus.value");
	    beamOneInfo.checkout();
	    fillLabelCell(*beamTable, 1, 2, "Beam 1 Status");
	    std::string bOneStat;
	    if(beamOneInfo.value & 1) {
	      bOneStat = "Present";
	    }
	    else {
	      bOneStat = "No beam";
	    }
	    if(beamOneInfo.value & 2) {
	      bOneStat += " & Safe";
	    }
	    if(beamOneInfo.value & 4) {
	      bOneStat += " & Stable";
	    }
	    if(beamOneInfo.value & 8) {
	      bOneStat += " & Dev allowed in";
	    }
	    fillInfoCell(*beamTable, 2, 2, bOneStat.c_str(), "");

	    ddc::DdcIntInfoNamed beamTwoInfo(initP, "LHC.Beam2Status.value");
	    fillLabelCell(*beamTable, 1, 3, "Beam 2 Status");
            std::string bTwoStat;
	    if(beamOneInfo.value & 16) {
	      bTwoStat = "Present";
	    }
	    else {
	      bTwoStat = "No beam";
	    }
	    if(beamOneInfo.value & 32) {
	      bTwoStat += " & Safe";
	    }
	    if(beamOneInfo.value & 4) {
	      bTwoStat += " & Stable";
	    }
	    if(beamOneInfo.value & 8) {
	      bTwoStat += " & Dev allowed in";
	    }
            fillInfoCell(*beamTable, 2, 3, bTwoStat.c_str(), "");
	    ddc::DdcIntInfoNamed stableBeamsInfo(initP, "LHC.StableBeamsFlag");
	    stableBeamsInfo.checkout();
	    fillLabelCell(*beamTable, 1, 4, "Stable Beams");
	    fillInfoCell(*beamTable, 2, 4, stableBeamsInfo.value > 0 ? "TRUE":"FALSE", "");

	    ddc::DdcFloatInfoNamed beamEnergy(initP, "LHC.BeamEnergy");
	    beamEnergy.checkout();
	    fillLabelCell(*beamTable, 1, 5, "Beam Energy");
	    fillInfoCell(*beamTable, 2, 5, floatToString(beamEnergy.value), "");

	  }
	  catch(ers::Issue &e) {
	    // ignore
	  }

	  composedTable->addElement(4, 2, beamTable);

	  std::string DQLink(DQBaseName  + partitionPageName);
	  
	  if (currentPartitionName != "ATLAS") {
	    // write out composed table
	    m_outStream->write(*composedTable);
	    delete composedTable;
	    try {
	      // Build the plots
	      ERS_DEBUG(1, "Building plots for partition " << currentPartitionName);
	      this->buildPlots(mapIt->first, mapIt->second);
	    }
	    catch(daq::config::Exception& ex) {
	      std::string errStr("WARNING, error getting partition information: " + ex.message() + ". Plots cannot be created!");
	      Text err(errStr);
	      m_outStream->write(err);
	      m_outStream->write(this->br);
	    }
	    // DK Link
	    m_outStream->write(this->br);
	    Link DQLk(DQLink, "Click here to browse the data quality page");
	    m_outStream->write(DQLk);
	    
	  }
	  else { // ATLAS

	    Table superTab(1,4);
	    superTab.setWidth (100, true);
	    superTab.setAlignCell(1, 1, 2);
	    superTab.addElement(1, 1, composedTable);

	    /*****************Busy Table***************/
	  
	    std::unique_ptr<Color> redColor;
	    redColor = std::unique_ptr<Color>(new Color(255, 0, 0));
	  
	    if (mapIt->second->_existBusyStatistics){
	      Table *busyTable = new Table(12, 6);
	    
	      setTableColors(*busyTable, 12, 6, true, true);
	      busyTable->setBGColor(LIGHT_GREY);
	      busyTable->setAlignCell(4, 2, 2);
	      busyTable->setAlignCell(4, 3, 2);	    
	    
	      fillColorCell(*busyTable, 1, 1, "CTPMI", "", 2, WHITE, "b");
	    
	      fillLabelCell(*busyTable, 1, 2, "VME");
	      fillBusyCell(*busyTable, 2, 2, mapIt->second->_vme, "");
	    
	      fillLabelCell(*busyTable, 1, 3, "ECR");	
	      fillBusyCell(*busyTable, 2, 3, mapIt->second->_ecr, ""); 
	    
	      fillLabelCell(*busyTable, 1, 4, "Veto 0");
	      fillBusyCell(*busyTable, 2, 4, mapIt->second->_veto0, "");
	    
	      fillLabelCell(*busyTable, 1, 5, "Veto 1");
	      fillBusyCell(*busyTable, 2, 5, mapIt->second->_veto1, "");
	    
	      fillLabelCell(*busyTable, 1, 6, "Backplane");
	      fillBusyCell(*busyTable, 2, 6, mapIt->second->_bckp, "");
	    
	      fillColorCell(*busyTable, 3, 1, "CTPCORE", "", 2, WHITE, "b");
	    
	      fillLabelCell(*busyTable, 3, 2, "Backplane");
	      fillBusyCell(*busyTable, 4, 2, mapIt->second->_bckpl, "");
	    
	      fillLabelCell(*busyTable, 3, 3, "Result");
	      fillBusyCell(*busyTable, 4, 3, mapIt->second->_rslt, "");
	    
	      fillColorCell(*busyTable, 5, 1, "CTPOUT 12", "", 2, WHITE, "b");
	    
	      int i;
	      for (i = 0; i < 5; i ++) {
		try { 
		  if (mapIt->second->_connectorIn12[i] == "ON")
		    fillBusyCell(*busyTable, 6, i+2, mapIt->second->_ctpout12[i], "");
		  else
		    fillBusyCell(*busyTable, 6, i+2, "OUT", "");
		}
		catch (std::exception& ex) {
		  fillBusyCell(*busyTable, 6, i+2, "N/A", "");
		}
		fillLabelCell(*busyTable, 5, i+2, mapIt->second->_connectorName12[i]);
	      }
	    
	      fillColorCell(*busyTable, 7, 1, "CTPOUT 13", "", 2, WHITE, "b");
	    
	    
	      for (i = 0; i < 5; i ++) {
		try {
		
		  if (mapIt->second->_connectorIn13[i] == "ON")
		    fillBusyCell(*busyTable, 8, i+2, mapIt->second->_ctpout13[i], "");
		  else
		    fillBusyCell(*busyTable, 8, i+2, "OUT", "");
		}
		catch (std::exception& ex) {
		  fillBusyCell(*busyTable, 8, i+2, "N/A", "");
		}
		fillLabelCell(*busyTable, 7, i+2, mapIt->second->_connectorName13[i]);
	      }
	    
	      fillColorCell(*busyTable, 9, 1, "CTPOUT 14", "", 2, WHITE, "b");
	    
	      for (i = 0; i < 5; i ++) {
		try { 
		  if (mapIt->second->_connectorIn14[i] == "ON")
		    fillBusyCell(*busyTable, 10, i+2, mapIt->second->_ctpout14[i], "");
		  else
		    fillBusyCell(*busyTable, 10, i+2, "OUT", "");
		}
		catch (std::exception& ex) {
		  fillBusyCell(*busyTable, 10, i+2, "N/A", "");
		}
	      
		fillLabelCell(*busyTable, 9, i+2, mapIt->second->_connectorName14[i]);
	      }
	    
	      fillColorCell(*busyTable, 11, 1, "CTPOUT 15", "", 2, WHITE, "b");
	    
	    
	      for (i = 0; i < 5; i ++) {
		try {
		  if (mapIt->second->_connectorIn15[i] == "ON")
		    fillBusyCell(*busyTable, 12, i+2, mapIt->second->_ctpout15[i], "");
		  else
		    fillBusyCell(*busyTable, 12, i+2, "OUT", "");
		}
		catch (std::exception& ex) {
		  fillBusyCell(*busyTable, 12, i+2, "N/A", "");
		}
	      
		fillLabelCell(*busyTable, 11, i+2, mapIt->second->_connectorName15[i]);
	      }
	      
	      superTab.setAlignCell(1,2,2);
	      superTab.addElement(1,2, new Text ("Busy Status", 5, DARK_BLUE, TextOption("b")));
	      superTab.setAlignCell(1,3,2);
	      superTab.addElement(1,3, busyTable);
	      
	    }

	    Picture * TRP = new Picture(RatesPlot, 1000, 380); 
	    
	    superTab.setAlignCell(1,4,2);
	    superTab.addElement(1, 4, TRP);
	    m_outStream->write(superTab);
	    
	    m_outStream->write(this->br);	    
	    Link TRPLk(TRPPage, "Click here for more complete Trigger information.");
	    m_outStream->write(TRPLk);
	    m_outStream->write(this->hr);
	    Text DQTitle("Data Quality Monitoring Page", 5, DARK_BLUE, TextOption("b"));
	    m_outStream->write(DQTitle);
	    IFrame DQ(DQLink, "DQ", 100, true, 500, false);
	    m_outStream->write(DQ);
	  }

	}		
	// Close the partition page
	m_outStream->close(fileIndex);
      }
      
      if (!atlasExists) {
	const std::string partitionPageName("ATLAS.html");
	// Open the partition page
	int fileIndex = m_outStream->open(partitionPageName);

	// Write page title
	Table mainTitle(1, 1);
	mainTitle.setWidth (100, true);
	mainTitle.setAlignCell(1, 1, 2);
	const std::string Part("Partition ATLAS Not Up");
	//Text PartTx(Part, 5, DARK_RED, TextOption("b"));
	mainTitle.addElement(1, 1, new Text(Part, 6, DARK_RED, TextOption("b")));
	m_outStream->write(mainTitle);
	m_outStream->write(this->hr);
	//m_outStream->write(this->br);
	// Link the previously generated page to top
	Link RunComLk(runComPage, "Check today's program here!");
	m_outStream->write(RunComLk);
	m_outStream->write(this->br);
	Link OtherPartsLk("index.html", "Other active partitions can be seen here.");
	m_outStream->write(OtherPartsLk);
	m_outStream->write(this->br);
	m_outStream->close(fileIndex);
      }

    }
    
    Table*
    RunParamsPlugin::createMainTable() const
      throw()     
    {
      Table* tb = new Table(RunParamsPlugin::generalTableColNum, this->partMap.size() + 1);
      setTableColors(*tb, (int)RunParamsPlugin::generalTableColNum, this->partMap.size() + 1, false, true);
      tb->setBorder(3);

      int i;
      for (i = 1; i < 10; i++)
	tb->setAlignCell(i, 1, 2);

      fillColorCell(*tb, 1, 1, RunParamsPlugin::PartitionNameLabel, "", 3, WHITE, "b");
      fillColorCell(*tb, 2, 1, RunParamsPlugin::RunNumberLabel, "", 3, WHITE, "b");
      fillColorCell(*tb, 3, 1, RunParamsPlugin::RunTypeLabel, "", 3, WHITE, "b");
      fillColorCell(*tb, 4, 1, RunParamsPlugin::DetLabel, "", 3, WHITE, "b");
      fillColorCell(*tb, 5, 1, RunParamsPlugin::RootControllerStateLabel, "", 3, WHITE, "b");
      fillColorCell(*tb, 6, 1, RunParamsPlugin::RecordingStateLabel, "", 3, WHITE, "b");
      fillColorCell(*tb, 7, 1, RunParamsPlugin::RunStartTimeLabel, "", 3, WHITE, "b");
      fillColorCell(*tb, 8, 1, RunParamsPlugin::RunStopTimeLabel, "", 3, WHITE, "b");
      fillColorCell(*tb, 9, 1, RunParamsPlugin::TotalRunTimeLabel, "", 3, WHITE, "b");

      return tb;
    }
    
    void
    RunParamsPlugin::fillMainTable(Table* const tb, 
				   const PartitionData* const p,
				   const Color& rcStateColor,
				   unsigned int row) const 
      throw() 
    {
      Link* lk = new Link(p->_partitionPageName, p->partitionName);	  

      int i;
      for (i = 1; i < 10; i++)
	tb->setAlignCell(i, row, 2);

      tb->addElement(1, row, lk);
      
      fillInfoCell(*tb, 2, (int)row, p->_runNumber, "");
      fillInfoCell(*tb, 3, (int)row, p->_runType, "");
      fillInfoCell(*tb, 4, (int)row, p->_fullDetMask, "");
      fillColorCell(*tb, 5, (int)row, p->_rcState, "", 4, rcStateColor, "");
      fillInfoCell(*tb, 6, (int)row, p->_recStatus, "");
      fillInfoCell(*tb, 7, (int)row, p->_startTime, "");      
      fillInfoCell(*tb, 8, (int)row, p->_stopTime, "");  
      fillInfoCell(*tb, 9, (int)row, p->_runTime, "");
    }

    Table* 
    RunParamsPlugin::createSystemSpecificTable() const
      throw() 
    {
      Table* tb = new Table(RunParamsPlugin::systemTableColNum, 1);//this->partMap.size() + 1 has been used previously for defining the number of lines, giving the table its maximum possible size - assuming that all the partitions contain the given subdetector. This meant no lines needed to be added on the way, but the table contained very often (see always) unnecessary empty lines
      tb->setBGColor(LIGHT_GREY);
      tb->setBorder(2);

      unsigned int i;

      setTableColors(*tb, (int)RunParamsPlugin::systemTableColNum, 1, false, true);

      for (i = 1; i < 6; i++)
	tb->setAlignCell(i, 1, 2);

      fillColorCell(*tb, 1, 1, RunParamsPlugin::PartitionNameLabel, "", 3, WHITE, "b");
      fillColorCell(*tb, 2, 1, RunParamsPlugin::RunNumberLabel, "", 3, WHITE, "b");
      fillColorCell(*tb, 3, 1, RunParamsPlugin::RunTypeLabel, "", 3, WHITE, "b");
      fillColorCell(*tb, 4, 1, RunParamsPlugin::DetLabel, "", 3, WHITE, "b");
      fillColorCell(*tb, 5, 1, RunParamsPlugin::RootControllerStateLabel, "", 3, WHITE, "b");

      return tb;
    }


    void RunParamsPlugin::fillColorCell(Table& tb, int colNo, int lineNo, std::string value, std::string defaultValue, int textSize, Color textColor, std::string textOptions)
      const throw()
    {

      if (defaultValue.empty())
	defaultValue = "N/A";

      std::string stringValue;
      if (value.empty())
	stringValue = defaultValue;
      else
	stringValue = value;

      if (textSize == 0)
	textSize = 2;

      if (textOptions.empty())
	tb.addElement(colNo, lineNo, new Text(stringValue, textSize, textColor));
      else
	tb.addElement(colNo, lineNo, new Text(stringValue, textSize, textColor, TextOption(textOptions)));

    }


    void RunParamsPlugin::fillBusyCell(Table& tb, int colNo, int lineNo, std::string value, std::string defaultValue)
      const throw()
    {

      if (defaultValue.empty())
	defaultValue = "N/A";

      std::string stringValue;
      if (!value.empty())
	stringValue = value;
      else
	stringValue = defaultValue;

      if (stringValue != "100")
	if (stringValue != "OUT" && stringValue != "N/A" && stringValue != defaultValue)
	  tb.addElement(colNo, lineNo, new Text(stringValue + "%"));
	else
	  if (stringValue == "OUT")
	    tb.addElement(colNo, lineNo, new Text(stringValue, 4, DARK_GREY));
	  else 
	    tb.addElement(colNo, lineNo, new Text(stringValue));
      else {
	tb.addElement(colNo, lineNo, new Text(stringValue + "%", 4, WHITE));                        	
	tb.setBGColorCell(colNo, lineNo, RED);
      }
	
    }
   

    void RunParamsPlugin::fillInfoCell(Table& tb, int colNo, int lineNo, std::string value, std::string defaultValue)
      const throw()
    {

      if (defaultValue.empty())
	defaultValue = "N/A";

      Text* textValue;
      if (!value.empty() && value != " ")
	textValue = new Text(value);
      else textValue = new Text(defaultValue);

      tb.addElement(colNo, lineNo, textValue);

    }     

    void RunParamsPlugin::fillLabelCell(Table& tb, int colNo, int lineNo, std::string value)
      const throw()
    {
      tb.addElement(colNo, lineNo, new Text(value, TextOption("b")));
    }



    /**
     * The background colors of the cells of the table are set according to the rules:
     * 	 1. if the table has a header (hasLabelHeader) the first line has the background color = CAPTION_BG (dark blue)
     *   2. the cells containing information have background color = WHITE
     *   3. the cells containing labels have background color = CELL_BG (light cyan)
     *   4. label vs information cells: hasLabelColumn = true means that it is expected that the odd columns of the table contain labels (painted in light cyan) and the even columns contain values (painted in white). 
     *   5. Any other particular formats need to be treated locally in order to set the background color of the cells
     */

    void RunParamsPlugin::setTableColors(Table& tb, int colNo, int lineNo, bool hasLabelColumn, bool hasLabelHeader)
      const throw()
    {
      int i, j, start_line;
      if (hasLabelHeader)
	start_line = 2;
      else
	start_line = 1;

      for (i = 2; i <= colNo; i += 2)
	for (j = start_line; j <= lineNo; j++)
	  tb.setBGColorCell(i, j, WHITE);

      for (i = 1; i <= colNo; i++)
	for (j = 1; j < start_line; j++)
	  tb.setBGColorCell(i, j, CAPTION_BG);


      if (hasLabelColumn) 
	for (i = 1; i <= colNo; i += 2)
	  for (j = start_line; j <= lineNo; j++)
	    tb.setBGColorCell(i, j, CELL_BG);
      else
	for (i = 1; i <= colNo; i += 2)
	  for (j = start_line; j <= lineNo; j++)
	    tb.setBGColorCell(i, j, WHITE);
      return;
    }


    /**
     * Sets the background colors for a line that is added on the way to a table. The same protocol as for the setTableColors(...) method is used, except that the hasHeaderLine does no longer count, since it is supposed that the table is already colored except for the current line
     */
    void RunParamsPlugin::setLineColors(Table &tb, int colNo, int row, bool hasLabelColumn = true) const throw()

    {
      int i;
      for (i = 2; i <= colNo; i += 2)
	tb.setBGColorCell(i, row, WHITE);
      if (hasLabelColumn)
	for (i = 1; i <= colNo; i +=2)
	  tb.setBGColorCell(i, row, CELL_BG);
      else
	for (i =1; i < colNo; i += 2)
	  tb.setBGColorCell(i, row, WHITE);
	
    }


    /***************************************/
    /**         Subdetectors pages         */
    /***************************************/
    void
    RunParamsPlugin::fillSystemSpecificTable(Table* const tb, 
					     const PartitionData* const  p,
					     const std::string& systemMask,
					     const daq::wmi::Color& rcStateColor,
					     unsigned int row) const 
      throw() 
    {

      tb->addRow();
      unsigned int i;
      setLineColors(*tb, (int)RunParamsPlugin::systemTableColNum, (int)row, false);

      Link* lk = new Link(p->_partitionPageName, p->partitionName);

      for (i = 1; i < 6; i++)
	tb->setAlignCell(i, row, 2);

      tb->addElement(1, row, lk);
      
      fillInfoCell(*tb, 2, (int)row, p->_runNumber, "");
      
      fillInfoCell(*tb, 3, (int)row, p->_runType, "");
      
      std::string tempString = systemMask;
      fillInfoCell(*tb, 4, (int)row, tempString, "");
      
      fillColorCell(*tb, 5, (int)row, p->_rcState, "", 4, rcStateColor, "");
      
    }

    void
    RunParamsPlugin::buildTables() const
      throw()
    {
      int muonMDTFileIndex = m_outStream->open(RunParamsPlugin::mdtPageName);
      int muonRPCFileIndex = m_outStream->open(RunParamsPlugin::rpcPageName);
      int muonCSCFileIndex = m_outStream->open(RunParamsPlugin::cscPageName);
      int muonTGCFileIndex = m_outStream->open(RunParamsPlugin::tgcPageName);



      int idPixelFileIndex = m_outStream->open(RunParamsPlugin::pixelPageName);
      int idSCTFileIndex = m_outStream->open(RunParamsPlugin::sctPageName);
      int idTRTFileIndex = m_outStream->open(RunParamsPlugin::trtPageName);



      int larFileIndex = m_outStream->open(RunParamsPlugin::larPageName);
      int tilFileIndex = m_outStream->open(RunParamsPlugin::tilPageName);

      /*******************************************/
      /**  Message from white board commission  **/
      /*******************************************/
      int runComFileIndex = m_outStream->open(runComPage);

      std::ifstream::pos_type size;
      char * memblock;
      std::ifstream file ("/det/tdaq/RunComWhiteBoard/last_white_board_message.txt", std::ios::in|std::ios::binary|std::ios::ate);
      if (file.is_open())
	{
	  Text runCommissionTitle("Program Of The Day", 5, DARK_BLUE, TextOption("b"));
	  size = file.tellg();
	  memblock = new char [size];
	  file.seekg (0, std::ios::beg);
	  m_outStream->write(runCommissionTitle, runComFileIndex);
	  m_outStream->write(this->br, runComFileIndex);
	  while (!file.eof())
	    {	
	      m_outStream->write(this->br, runComFileIndex);
	      file.getline(memblock, size);
	      Text runComWhiteBoardMess(memblock);
	      m_outStream->write(runComWhiteBoardMess,runComFileIndex);
	    }
	  delete(memblock);
	  file.close();
	}
	
      else 
	{
	  Text runComWhiteBoardMess("No Run Com White Board message available", 5, DARK_BLUE, TextOption("b"));
	  m_outStream->write(runComWhiteBoardMess,runComFileIndex);
	  m_outStream->write(this->hr, runComFileIndex);
	}

      /************************************************/
      /** Links to OKS archives and log manager page **/
      /************************************************/

      // Add web links
      m_outStream->write(this->br, runComFileIndex);
      m_outStream->write(this->hr, runComFileIndex);
      Text linkTitle("Useful Links", 5, DARK_BLUE, TextOption("b"));
      Link lkRC(this->RCMessagePage, "Run Schedule", false);
      Link runSumLk(this->RunSummaryPage, "Run Descriptions in COOL");
      Link oksLk(this->RemoteMonitoringPage, "TDAQ Web Based Services", false);
          
      m_outStream->write(linkTitle, runComFileIndex);
      m_outStream->write(this->br, runComFileIndex);
      m_outStream->write(lkRC, runComFileIndex);
      m_outStream->write(this->br, runComFileIndex);
      m_outStream->write(runSumLk, runComFileIndex);
      m_outStream->write(this->br, runComFileIndex);
      m_outStream->write(oksLk, runComFileIndex);
      m_outStream->write(this->br, runComFileIndex);
      m_outStream->close(runComFileIndex);

      int fileIndex = m_outStream->open();
      std::vector<std::string> errMsgVect;

      {

	// Link the previously generated page to top
	Link RunComLk(runComPage, "Check today's program here!");
	m_outStream->write(RunComLk);
	m_outStream->write(this->hr, fileIndex);
 
	// Write page title
	static const std::string AvPart("Available Partitions");
	Text AvPartTx(AvPart, 5, DARK_BLUE, TextOption("b"));
	m_outStream->write(AvPartTx, fileIndex);
      }

      const unsigned int partNum(this->partMap.size()); 
      
      if(partNum > 0) {
	std::map<IPCPartition, PartitionData*>::const_iterator mapIt;
	std::map<IPCPartition, PartitionData*>::const_iterator mapItBegin(this->partMap.begin());
	std::map<IPCPartition, PartitionData*>::const_iterator mapItEnd(this->partMap.end());
	
	// System specific tables
	const std::unique_ptr<Table> tbMuonMDT(this->createSystemSpecificTable());
	const std::unique_ptr<Table> tbMuonRPC(this->createSystemSpecificTable());
	const std::unique_ptr<Table> tbMuonCSC(this->createSystemSpecificTable());
	const std::unique_ptr<Table> tbMuonTGC(this->createSystemSpecificTable());

	const std::unique_ptr<Table> tbLAr(this->createSystemSpecificTable());
	const std::unique_ptr<Table> tbTil(this->createSystemSpecificTable());
	const std::unique_ptr<Table> tbIDPixel(this->createSystemSpecificTable());
	const std::unique_ptr<Table> tbIDSCT(this->createSystemSpecificTable());
	const std::unique_ptr<Table> tbIDTRT(this->createSystemSpecificTable());

	// Main table
	const std::unique_ptr<Table> tb(this->createMainTable());

	// Loop over all available partitions
	unsigned int counter(1);
	unsigned int muonMDTCounter(1);
	unsigned int muonRPCCounter(1);
	unsigned int muonCSCCounter(1);
	unsigned int muonTGCCounter(1);

	unsigned int larCounter(1);
	unsigned int tilCounter(1);
	unsigned int idPixelCounter(1);
	unsigned int idSCTCounter(1);
	unsigned int idTRTCounter(1);

	for(mapIt = mapItBegin; mapIt != mapItEnd; ++mapIt) {
	  counter++;

	  const IPCPartition& p(mapIt->first); 	  
	  const std::string currentPartitionName(p.name());
	  const std::string currentPartitionPageName(currentPartitionName + ".html");
	  
	  ERS_DEBUG(1, "Populating info table for partition " << currentPartitionName);

	  // Warning: the m_outStream has to be opened to the right file
	  static const std::string runParamsIS("RunParams.RunParams");
	  static const std::string lbSettingsIS("RunParams.LBSettings");
	  static const std::string lbInfoIS("RunParams.LumiBlock");
	  static const std::string runInfoIS("RunParams.RunInfo");
	  static const std::string rcIS("RunCtrl.RootController");
	  static const std::string sfoStatsIS("RunCtrlStatistics.SFO-SUM");

	  unsigned int _totalTime(0);
	  try {
	    ERS_DEBUG(2, "Asking IS server " << runInfoIS << " in partition " << currentPartitionName);
	    RunInfoNamed runInfo(p, runInfoIS);
	    runInfo.checkout();
	    _totalTime = runInfo.activeTime;
	  }
	  catch(daq::is::Exception& ex) {
	    errMsgVect.push_back("WARNING, error getting information for partition " + currentPartitionName + ": " + ex.message() + ". Shown information may be incomplete.\n");
	  }

	  /**************L2 average event size*************/
	  double _l2AverageRate = 0;
	  try{
	    DF_IS_Info::SFINamed sfiAverageInfo(p, "RunCtrlStatistics.SFI-AVERAGE");
	    sfiAverageInfo.checkout();
	    _l2AverageRate = sfiAverageInfo.EventPayload;
	  }
	  catch (daq::is::Exception& ex) {}
	  
	  /************************************************/


	  /*************LB interval and Switching Mode******************/
	  std::string _switchingMode("");
	  std::string _interval("");
	  try {
	    ERS_DEBUG(2, "Asking for LBSettings server " << runInfoIS << " in partition " << currentPartitionName);
	    LBSettingsNamed lbSettings(p, lbSettingsIS);
	    lbSettings.checkout();
	    _switchingMode = lbSettings.SwitchingMode;
	    if(_switchingMode == "TIME") {
	      _switchingMode = "seconds";
	    }
	    _interval = intToString(lbSettings.Interval);
	  }
	  catch(daq::is::Exception& ex) {
	    errMsgVect.push_back("WARNING, error getting information for partition " + currentPartitionName + ": " + ex.message() + ". Shown information may be incomplete.\n");
	  }

	  /**********************************/

	  /******************Busy rates**************/
	  ISCTPOUTNamed isctpoutInfo12(p, "L1CT.ISCTPOUT_12");
	  std::string connectorsNames12[5], connectorsIn12[5];
	  try{
	    isctpoutInfo12.checkout();
	    connectorsNames12[0] = isctpoutInfo12.connector0_name;
	    connectorsNames12[1] = isctpoutInfo12.connector1_name;
	    connectorsNames12[2] = isctpoutInfo12.connector2_name;
	    connectorsNames12[3] = isctpoutInfo12.connector3_name;
	    connectorsNames12[4] = isctpoutInfo12.connector4_name;
	    connectorsIn12[0] =isctpoutInfo12.connector0_busy;
	    connectorsIn12[1] = isctpoutInfo12.connector1_busy;
	    connectorsIn12[2] = isctpoutInfo12.connector2_busy;
	    connectorsIn12[3] = isctpoutInfo12.connector3_busy; 
	    connectorsIn12[4] = isctpoutInfo12.connector4_busy;
	  }
	  catch(daq::is::Exception& ex) {
	    errMsgVect.push_back("WARNING, error getting information for partition " + currentPartitionName + ": " + ex.message() + ". Shown information may be incomplete.\n");
	  }

	  ISCTPOUTNamed isctpoutInfo13(p, "L1CT.ISCTPOUT_13");
	  std::string connectorsNames13[5], connectorsIn13[5];
	  try {

	    isctpoutInfo13.checkout();
	    connectorsNames13[0] = isctpoutInfo13.connector0_name;
	    connectorsNames13[1] = isctpoutInfo13.connector1_name;
	    connectorsNames13[2] = isctpoutInfo13.connector2_name;
	    connectorsNames13[3] = isctpoutInfo13.connector3_name;
	    connectorsNames13[4] = isctpoutInfo13.connector4_name;
	    connectorsIn13[0] = isctpoutInfo13.connector0_busy;
	    connectorsIn13[1] = isctpoutInfo13.connector1_busy;
	    connectorsIn13[2] = isctpoutInfo13.connector2_busy;
	    connectorsIn13[3] = isctpoutInfo13.connector3_busy;
	    connectorsIn13[4] = isctpoutInfo13.connector4_busy;
	  }
	  catch(daq::is::Exception& ex) {
	    errMsgVect.push_back("WARNING, error getting information for partition " + currentPartitionName + ": " + ex.message() + ". Shown information may be incomplete.\n");
	  }


	  ISCTPOUTNamed isctpoutInfo14(p, "L1CT.ISCTPOUT_14");
	  std::string connectorsNames14[5], connectorsIn14[5];
	  try {

	    isctpoutInfo14.checkout();
	    connectorsNames14[0] = isctpoutInfo14.connector0_name;
	    connectorsNames14[1] = isctpoutInfo14.connector1_name;
	    connectorsNames14[2] = isctpoutInfo14.connector2_name;
	    connectorsNames14[3] = isctpoutInfo14.connector3_name;
	    connectorsNames14[4] = isctpoutInfo14.connector4_name;
	    connectorsIn14[0] = isctpoutInfo14.connector0_busy;
	    connectorsIn14[1] = isctpoutInfo14.connector1_busy;
	    connectorsIn14[2] = isctpoutInfo14.connector2_busy;
	    connectorsIn14[3] = isctpoutInfo14.connector3_busy;
	    connectorsIn14[4] = isctpoutInfo14.connector4_busy;
	  }
	  catch(daq::is::Exception& ex) {
	    errMsgVect.push_back("WARNING, error getting information for partition " + currentPartitionName + ": " + ex.message() + ". Shown information may be incomplete.\n");
	  }


	  ISCTPOUTNamed isctpoutInfo15(p, "L1CT.ISCTPOUT_15");
	  std::string connectorsNames15[5], connectorsIn15[5];
	  try {
	    isctpoutInfo15.checkout();
	    connectorsNames15[0] = isctpoutInfo15.connector0_name;
	    connectorsNames15[1] = isctpoutInfo15.connector1_name;
	    connectorsNames15[2] = isctpoutInfo15.connector2_name;
	    connectorsNames15[3] = isctpoutInfo15.connector3_name;
	    connectorsNames15[4] = isctpoutInfo15.connector4_name;
	    connectorsIn15[0] = isctpoutInfo15.connector0_busy;
	    connectorsIn15[1] = isctpoutInfo15.connector1_busy;
	    connectorsIn15[2] = isctpoutInfo15.connector2_busy;
	    connectorsIn15[3] = isctpoutInfo15.connector3_busy;
	    connectorsIn15[4] = isctpoutInfo15.connector4_busy;
	  }
	  catch(daq::is::Exception& ex) {
	    errMsgVect.push_back("WARNING, error getting information for partition " + currentPartitionName + ": " + ex.message() + ". Shown information may be incomplete.\n");
	  }

	  ISCTPBUSYNamed busyInfo(p, "L1CT-History.ISCTPBUSY");
	  bool _existBusy(true);

	  std::string _vme, _ecr, _veto0, _veto1, _bckp;
	  std::string _ctpout12[5], _ctpout13[5], _ctpout14[5], _ctpout15[5];
	  std::string _bckpl, _rslt, _rdt, _mon;//, _smpl, _cmplx0, _cmplx1, _rslt2;

	  int i;

	  try {
	    busyInfo.checkout();
	    _vme = doubleToString(busyInfo.ctpmi_vme_rate);
	    _ecr = doubleToString(busyInfo.ctpmi_ecr_rate);
	    _veto0 = doubleToString(busyInfo.ctpmi_vto0_rate);
	    _veto1 = doubleToString(busyInfo.ctpmi_vto1_rate);
	    _bckp = doubleToString(busyInfo.ctpmi_bckp_rate);
	    _bckpl = doubleToString(busyInfo.ctpcore_bckp_rate);
	    _rslt = doubleToString(busyInfo.ctpcore_rslt_rate);
	    _rdt = doubleToString(busyInfo.ctpcore_rdt_rate);
	    _mon = doubleToString(busyInfo.ctpcore_mon_rate);
	    /*_smpl = doubleToString(busyInfo.ctpcore_smple_rate);
	      _cmplx0 = doubleToString(busyInfo.ctpcore_cmplx0_rate);
	      _cmplx1 = doubleToString(busyInfo.ctpcore_cmplx1_rate);
	      _rslt2 = doubleToString(busyInfo.ctpcore_rslt2_rate);*/

	    for (i = 0; i < 5; i++){
	      _ctpout12[i]= doubleToString(busyInfo.ctpout_12[i]);
	      _ctpout13[i] = doubleToString(busyInfo.ctpout_13[i]);
	      _ctpout14[i] = doubleToString(busyInfo.ctpout_14[i]);
	      _ctpout15[i] = doubleToString(busyInfo.ctpout_15[i]);
	    }

	  }
	  catch (daq::is::Exception& ex) {
	    _existBusy = false;
	  }

	  /************LB Value*********************/
	  std::string _lbValue("");
	  try {
	    ERS_DEBUG(2, "Asking for LBInfo server " << runInfoIS << " in partition " << currentPartitionName);
	    LumiBlockNamed luminosityInfo(p, lbInfoIS);
	    luminosityInfo.checkout();
	    _lbValue = intToString(luminosityInfo.LumiBlockNumber);
	  }
	  catch(daq::is::Exception& ex) {
	    errMsgVect.push_back("WARNING, error getting information for partition " + currentPartitionName + ": " + ex.message() + ". Shown information may be incomplete.\n");
	  }

	  /***************Trigger information***************/
	  TrigConfHltPsKeyNamed trigHltPsKeyInfo(p, "RunParams.TrigConfHltPsKey");
	  bool _existTrigPsKey(true);

	  std::string _hltPrescaleKey, _hltPrescaleComment;

	  try {
	    trigHltPsKeyInfo.checkout();
	    _hltPrescaleKey = intToString(trigHltPsKeyInfo.HltPrescaleKey);
	    _hltPrescaleComment = trigHltPsKeyInfo.HltPrescaleComment;
	  }
	  catch (daq::is::Exception& ex) {_existTrigPsKey = false;}


	  TrigConfReleaseNamed trigConfReleaseInfo(p, "RunParams.TrigConfRelease");
	  bool _existTrigConfRelease(true);
	  std::string _hltReleaseVersion;

	  try {
	    trigConfReleaseInfo.checkout();
	    _hltReleaseVersion = trigConfReleaseInfo.HLTReleaseVersion;
	  }
	  catch (daq::is::Exception& ex) {_existTrigConfRelease = false;}

	  TrigConfL1BgKeyNamed trigL1BgKeyInfo(p, "RunParams.TrigConfL1BgKey");
	  bool _existTrigL1BgKey(true);

	  std::string _L1BunchGroupKey, _L1BunchGroupComment;

	  try {
	    trigL1BgKeyInfo.checkout();
	    _L1BunchGroupKey = intToString(trigL1BgKeyInfo.L1BunchGroupKey);
	    _L1BunchGroupComment = trigL1BgKeyInfo.L1BunchGroupComment;
	  }
	  catch (daq::is::Exception& ex) {_existTrigL1BgKey = false;}

	  TrigConfL1PsKeyNamed trigConfL1PsKeyInfo(p, "RunParams.TrigConfL1PsKey");
	  bool _existTrigConfL1PsKey(true);

	  std::string _confL1PrescaleKey, _confL1PrescaleComment;

	  try {
	    trigConfL1PsKeyInfo.checkout();
	    _confL1PrescaleKey = intToString(trigConfL1PsKeyInfo.L1PrescaleKey);
	    _confL1PrescaleComment = trigConfL1PsKeyInfo.L1PrescaleComment;
	  }
	  catch (daq::is::Exception& ex) {_existTrigConfL1PsKey = false;}


	  TrigConfSmKeyNamed trigConfSmKeyInfo(p, "RunParams.TrigConfSmKey");
	  bool _existTrigConfSmKey(true);

	  std::string _confSmKey, _confSmComment;

	  try {
	    trigConfSmKeyInfo.checkout();
	    _confSmKey = intToString(trigConfSmKeyInfo.SuperMasterKey);
	    _confSmComment = trigConfSmKeyInfo.SuperMasterComment;
	  }
	  catch (daq::is::Exception& ex) {_existTrigConfSmKey = false;}

	  /************************************************/

	  std::string _savedEventsRate("");
	  try {
	    ERS_DEBUG(2, "Asking IS server " << sfoStatsIS << " in partition " << currentPartitionName);
	    DF_IS_Info::SFONamed sfoInfo(p, sfoStatsIS);
	    sfoInfo.checkout();
	    std::ostringstream ostr;
	    ostr << sfoInfo.CurrentDataSavedRate;
	    _savedEventsRate = ostr.str().c_str();
	    ostr.str(""); // Clean ostr
	  }
	  catch(daq::is::Exception& ex) {
	    errMsgVect.push_back("WARNING, error getting information for partition " + currentPartitionName + ": " + ex.message() + ". Shown information may be incomplete.\n");
	  }
	  uint64_t _detMask(0);
	  unsigned int _runNumber(0);
	  unsigned int _recStatus(0);
	  std::string _startTime("");
	  std::string _stopTime("");
	  std::string _runType("");
	  std::string _projectTag("");
	  try {
	    ERS_DEBUG(2, "Asking IS server " << runParamsIS << " in partition " << currentPartitionName);
	    RunParamsNamed runParams(p, runParamsIS);
	    
	    runParams.checkout();
	    _detMask = runParams.detector_mask;
	    _runNumber = runParams.run_number;
	    _recStatus = runParams.recording_enabled;
	    _startTime = (runParams.timeSOR.c_time() == 0) ? "" : runParams.timeSOR.str();
	    _stopTime = (runParams.timeEOR.c_time() == 0) ? "" : runParams.timeEOR.str();
	    _runType = runParams.run_type;
	    _projectTag = runParams.T0_project_tag;
	  }
	  
	  catch(daq::is::Exception& ex) {
	    errMsgVect.push_back("WARNING, error getting information for partition " + currentPartitionName + ": " + ex.message() + ". Shown information may be incomplete.\n");
	  }
          
	  std::ostringstream ostr;
	  ostr << _runNumber;
	  const std::string runNumberString(ostr.str());
	  ostr.str(""); // Clean ostr

	  ostr << _recStatus;
	  const std::string recStatusString(ostr.str());
	  ostr.str(""); // Clean ostr

	  //TEST: fake detector mask
	  //_detMask = 111111122;
	  
	  std::string _rcState("");
	  std::string _rcError("");
	  bool _rcFault(false);

	  try {
	    ERS_DEBUG(2, "Asking IS server " << rcIS << " in partition " << currentPartitionName);
	    RCStateInfoNamed rcStatus(p, rcIS);
	    rcStatus.checkout();
	    _rcState = rcStatus.state;
	    _rcFault = rcStatus.fault;

	    for(const std::string& e : rcStatus.errorReasons) {
	        _rcError += e + "; ";
	    }
	  }
	  catch(daq::is::Exception& ex) {
	    errMsgVect.push_back("WARNING, error getting information for partition " + currentPartitionName + ": " + ex.message() + ". Shown information may be incomplete.\n");
	  }  

	  std::unique_ptr<Color> rcsColor;
	  if(_rcFault) {
	    rcsColor = std::unique_ptr<Color>(new Color(255, 0, 0));
	  } else if(_rcState == "N/A" || _rcState.empty()) {
	    rcsColor = std::unique_ptr<Color>(new Color(190, 190, 190));
	  } else if(_rcState == "RUNNING") {
	    rcsColor = std::unique_ptr<Color>(new Color(0, 255, 0));
	  } else {
	    rcsColor = std::unique_ptr<Color>(new Color(34, 139, 34));
	  }
	  
	  // Detector mask
	  std::vector<std::string> detMaskElements;
	  RunParamsPlugin::detInfo.decodeDetMask(detMaskElements, _detMask);
	  std::string fullDetMask;
	  std::vector<std::string>::const_iterator it;
	  for(it = detMaskElements.begin(); it != detMaskElements.end(); ++it) {
	    fullDetMask.append(*it + " - ");
	  }	 
	  
	  // Update partition info
	  mapIt->second->_runType = _runType;
	  mapIt->second->_projectTag = _projectTag;
	  mapIt->second->_stopTime = _stopTime;
	  mapIt->second->_startTime = _startTime;
	  mapIt->second->_runTime = boost::posix_time::to_simple_string(boost::posix_time::seconds(_totalTime));
	  mapIt->second->_partitionPageName = currentPartitionPageName;
	  mapIt->second->_rcState = _rcState;
	  mapIt->second->_rcError = _rcError;
	  mapIt->second->_rcFault = _rcFault;
	  mapIt->second->_runNumber = runNumberString;
	  mapIt->second->_recStatus = recStatusString;
	  mapIt->second->_fullDetMask = fullDetMask;
	  mapIt->second->_savedEventsRate = _savedEventsRate;
	  mapIt->second->_lbSwitchingMode = _switchingMode;
	  mapIt->second->_lbValue = _lbValue;
	  mapIt->second->_lbInterval = _interval;
	  if (_existBusy){
	    mapIt->second->_vme = _vme;
	    mapIt->second->_ecr = _ecr;
	    mapIt->second->_veto0 = _veto0;
	    mapIt->second->_veto1 = _veto1;
	    mapIt->second->_bckp = _bckp;
	    mapIt->second->_bckpl = _bckpl;
	    mapIt->second->_rslt = _rslt;
	    mapIt->second->_rdt = _rdt;
	    mapIt->second->_mon = _mon;
	    /*mapIt->second->_smpl = _smpl;
	      mapIt->second->_cmplx0 = _cmplx0;
	      mapIt->second->_cmplx1 = _cmplx1;
	      mapIt->second->_rslt2 = _rslt2;*/
	    for (i = 0; i < 5; i++) {
	      mapIt->second->_ctpout12[i] = _ctpout12[i];
	      mapIt->second->_ctpout13[i] = _ctpout13[i];
	      mapIt->second->_ctpout14[i] = _ctpout14[i];
	      mapIt->second->_ctpout15[i] = _ctpout15[i];
	      mapIt->second->_connectorName12[i] = connectorsNames12[i];
	      mapIt->second->_connectorIn12[i] = connectorsIn12[i];

	      mapIt->second->_connectorName13[i] = connectorsNames13[i];
	      mapIt->second->_connectorIn13[i] = connectorsIn13[i];

	      mapIt->second->_connectorName14[i] = connectorsNames14[i];
	      mapIt->second->_connectorIn14[i] = connectorsIn14[i];

	      mapIt->second->_connectorName15[i] = connectorsNames15[i];
	      mapIt->second->_connectorIn15[i] = connectorsIn15[i];
	    }
	  }
	  mapIt->second->_existBusyStatistics = _existBusy;

	  //trigger info
	  mapIt->second->_existTrigPsKey = _existTrigPsKey;
	  mapIt->second->_existTrigL1BgKey = _existTrigL1BgKey;
	  mapIt->second->_existTrigConfL1PsKey = _existTrigConfL1PsKey;
	  mapIt->second->_existTrigConfSmKey = _existTrigConfSmKey;
	  mapIt->second->_existTrigConfRelease = _existTrigConfRelease;


	  if (_existTrigConfSmKey) {
	    mapIt->second->_confSmKey = _confSmKey;
	    mapIt->second->_confSmComment = _confSmComment; 
	  }
	  else {
	    mapIt->second->_confSmKey = "";
	    mapIt->second->_confSmComment = "";
	  }


	  if (_existTrigConfRelease) {
	    mapIt->second->_hltReleaseVersion = _hltReleaseVersion;
	  }
	  else {
	    mapIt->second->_hltReleaseVersion = "";
	  }




	  if (_existTrigConfL1PsKey) {
	    mapIt->second->_confL1PrescaleKey = _confL1PrescaleKey;
	    mapIt->second->_confL1PrescaleComment = _confL1PrescaleComment;
	  }
	  else {
	    mapIt->second->_confL1PrescaleKey = "";
	    mapIt->second->_confL1PrescaleComment = "";
	  }



	  if (_existTrigL1BgKey) {
	    mapIt->second->_L1BunchGroupKey = _L1BunchGroupKey;
	    mapIt->second->_L1BunchGroupComment = _L1BunchGroupComment;
	  }
	  else {
	    mapIt->second->_L1BunchGroupKey = "";
	    mapIt->second->_L1BunchGroupComment = "";
	  }



	  if (_existTrigPsKey) {
	    mapIt->second->_hltPrescaleKey = _hltPrescaleKey;
	    mapIt->second->_hltPrescaleComment = _hltPrescaleComment;
	  }
	  else {
	    mapIt->second->_hltPrescaleKey = "";
	    mapIt->second->_hltPrescaleComment = "";
	  }


	  if (_l2AverageRate != 0)
	    mapIt->second->_l2AverageSize = doubleToString(_l2AverageRate/1000.0);
	  else 
	    mapIt->second->_l2AverageSize = "";
	 
	  // Fill subsystem tables
	  RunParamsPlugin::DetectorInfo::subDetectors sdMask;
	  RunParamsPlugin::detInfo.detectors(sdMask, detMaskElements);
	  if(!sdMask.MuonMDT.empty()) {
	    muonMDTCounter++;
	    this->fillSystemSpecificTable(tbMuonMDT.get(),
					  mapIt->second,
					  sdMask.MuonMDT,
					  *(rcsColor.get()), 
					  muonMDTCounter);	    
	  }

	  if(!sdMask.MuonRPC.empty()) {
	    muonRPCCounter++;
	    this->fillSystemSpecificTable(tbMuonRPC.get(),
					  mapIt->second,
					  sdMask.MuonRPC,
					  *(rcsColor.get()),
					  muonRPCCounter);
	  }

	  if(!sdMask.MuonCSC.empty()) {
	    muonCSCCounter++;
	    this->fillSystemSpecificTable(tbMuonCSC.get(),
					  mapIt->second,
					  sdMask.MuonCSC,
					  *(rcsColor.get()),
					  muonCSCCounter);
	  }

	  if(!sdMask.MuonTGC.empty()) {
	    muonTGCCounter++;
	    this->fillSystemSpecificTable(tbMuonTGC.get(),
					  mapIt->second,
					  sdMask.MuonTGC,
					  *(rcsColor.get()),
					  muonTGCCounter);
	  }

	  if(!sdMask.IdPixel.empty()) {
	    idPixelCounter++;
	    this->fillSystemSpecificTable(tbIDPixel.get(),
					  mapIt->second,
					  sdMask.IdPixel,
					  *(rcsColor.get()), 
					  idPixelCounter);	    
	  }

	  if(!sdMask.IdSCT.empty()) {
	    idSCTCounter++;
	    this->fillSystemSpecificTable(tbIDSCT.get(),
					  mapIt->second,
					  sdMask.IdSCT,
					  *(rcsColor.get()),
					  idSCTCounter);
	  }

	  if(!sdMask.IdTRT.empty()) {
	    idTRTCounter++;
	    this->fillSystemSpecificTable(tbIDTRT.get(),
					  mapIt->second,
					  sdMask.IdTRT,
					  *(rcsColor.get()),
					  idTRTCounter);
	  }





	  if(!sdMask.LAr.empty()) {
	    larCounter++;
	    this->fillSystemSpecificTable(tbLAr.get(), 
					  mapIt->second,
					  sdMask.LAr,
					  *(rcsColor.get()), 
					  larCounter);	    
	  }
	  if(!sdMask.Til.empty()) {
	    tilCounter++;
	    this->fillSystemSpecificTable(tbTil.get(), 
					  mapIt->second,
					  sdMask.Til,
					  *(rcsColor.get()), 
					  tilCounter);	    
	  }
	  
	  // Fill the main table
	  this->fillMainTable(tb.get(),
			      mapIt->second,
			      *(rcsColor.get()),
			      counter);
	}
	
	
	m_outStream->write(*(tb.get()), fileIndex);
	m_outStream->write(this->hr, fileIndex);
	
	m_outStream->write(*(tbMuonMDT.get()), muonMDTFileIndex);
	m_outStream->write(*(tbMuonRPC.get()), muonRPCFileIndex);
	m_outStream->write(*(tbMuonCSC.get()), muonCSCFileIndex);
	m_outStream->write(*(tbMuonTGC.get()), muonTGCFileIndex);
	m_outStream->write(*(tbLAr.get()), larFileIndex);
	m_outStream->write(*(tbTil.get()), tilFileIndex);
	m_outStream->write(*(tbIDPixel.get()), idPixelFileIndex);
	m_outStream->write(*(tbIDSCT.get()), idSCTFileIndex);
	m_outStream->write(*(tbIDTRT.get()), idTRTFileIndex);


      }
      m_outStream->close(fileIndex);
      m_outStream->close(muonMDTFileIndex);
      m_outStream->close(muonRPCFileIndex);
      m_outStream->close(muonCSCFileIndex);
      m_outStream->close(muonTGCFileIndex);
      m_outStream->close(idPixelFileIndex);
      m_outStream->close(idSCTFileIndex);
      m_outStream->close(idTRTFileIndex);
      m_outStream->close(larFileIndex);
      m_outStream->close(tilFileIndex);
    }
    
    void 
    RunParamsPlugin::buildPlots(const IPCPartition& p, PartitionData* const partData) const 
    {
      time_t now = time(0);
      const std::string partitionName(p.name());

      // Warning: the m_outStream has to be opened to the right file

      // Get counter names from RDB
      ERS_DEBUG(1, "Getting counter IS names from RDB for partition " << partitionName);
      const std::string confStr("rdbconfig:" + partitionName + "::RDB");
      ::Configuration db(confStr);
      const daq::core::Partition* dalPart = db.get<daq::core::Partition>(partitionName, false, true, 0, 0);
      if((dalPart != 0) && (dalPart->get_IS_InformationSource())) { 
	const daq::core::IS_InformationSources *infoIS= dalPart->get_IS_InformationSource();
	// Get counter values from IS and make plots
	if(infoIS->get_LVL1()){
	  ERS_DEBUG(2, "Getting L1 counters for partition " << partitionName);
	  try {
	    const std::string l1Source(infoIS->get_LVL1()->get_EventCounter());
	    const std::string l1RateSource(infoIS->get_LVL1()->get_Rate());
	    long long l1Value = (long long) this->counterFromIS(p, l1Source);
	    double l1Rate = this->counterFromIS(p, l1RateSource);
	    partData->updateL1Counter(now, l1Value);
	    partData->updateL1Rate(now, l1Rate);

	    std::ostringstream trans;
	    trans << l1Value;
	    partData->_L1A= trans.str();

	    ERS_DEBUG(2, "LVL1 events: " << l1Value << " (" << l1Source << ")");
	  }
	  catch(ers::Issue& ex) {
	  }
	}
	
	if(infoIS->get_LVL2()) {
	  ERS_DEBUG(2, "Getting L2 counters for partition " << partitionName);
	  try {
	    const std::string l2Source(infoIS->get_LVL2()->get_EventCounter());
	    const std::string l2RateSource(infoIS->get_LVL2()->get_Rate());
	    double l2Value = this->counterFromIS(p, l2Source);
	    double l2Rate = this->counterFromIS(p, l2RateSource);
	    partData->updateL2Counter(now, l2Value);
	    partData->updateL2Rate(now, l2Rate);

	    std::ostringstream trans;
	    trans << l2Value;
	    partData->_L2A= trans.str();
	
	    ERS_DEBUG(2, "LVL2 events: " << l2Value << " (" << l2Source << ")");
	  }
	  catch(ers::Issue& ex) {
	  }
	}
	
	if(infoIS->get_EF()){
	  ERS_DEBUG(2, "Getting EF counters for partition " << partitionName);
	  try {
	    const std::string efSource(infoIS->get_EF()->get_EventCounter());
	    const std::string efRateSource(infoIS->get_EF()->get_Rate());
	    long long efValue = (long long) this->counterFromIS(p, efSource);
	    double efRate = this->counterFromIS(p, efRateSource);
	    partData->updateEFCounter(now, efValue);   
	    partData->updateEFRate(now, efRate);
	    std::ostringstream trans;
	    trans << efValue;
	    partData->_EFA= trans.str();
   
	    ERS_DEBUG(2, "EF events: " << efValue << " (" << efSource << ")");
	  }
	  catch(ers::Issue& ex) {
	  }
	}	
	
	//m_outStream->write(this->hr);
	  Text plotsTitle("Trigger Rates", 5, DARK_BLUE, TextOption("b"));	
	  std::unique_ptr<Table> tb(new Table(RunParamsPlugin::PartitionData::graphTableColNum, 1));
	  tb->setBGColor(LIGHT_GREY);
	  setTableColors(*tb, 3, 1, true, true);
	  
	  m_outStream->write(plotsTitle);
	  partData->printGraphs(*tb);
	  
	  m_outStream->write(*(tb.get()));
	} else {
	  std::string errStr("WARNING, error getting partition information.\n Plots cannot be built.");
	  Text err(errStr);
	}
    }
    
    
    const std::string
    RunParamsPlugin::extractServerName(const std::string& fullName) const 
      throw()
    {
      size_t found = fullName.find_last_of(".");      
      return fullName.substr(0, found);
    }

    const std::string
    RunParamsPlugin::extractAttributeName(const std::string& fullName) const
      throw()
    {
      size_t found = fullName.find_last_of(".");      
      return fullName.substr(found + 1);
    }
    
    double
    RunParamsPlugin::counterFromIS(const IPCPartition& p, const std::string& isSource) const 
    {
      ERS_DEBUG(3, "Getting IS information with name " << isSource << " for partition " << p.name());

      const std::string attrName(this->extractAttributeName(isSource));
      
      ISInfoDynAny isInfo;
      ISInfoDictionary dict(p);
      dict.getValue(this->extractServerName(isSource), isInfo);
      
      ISInfoDocument isDoc(p, isInfo);
      size_t attrPos = isDoc.attributePosition(attrName);
      ISType::Basic attrType(isInfo.getAttributeType(attrPos));

      int retValue;
      switch(attrType) {
      case ISType::S16 :
	retValue = isInfo.getAttributeValue<short>(attrPos); //(double) isInfo.getAttributeValue<short>(attrPos); 
	break;
      case ISType::U16 : 
	retValue = isInfo.getAttributeValue<unsigned short>(attrPos); 
	break;
      case ISType::S32 : 
	retValue = isInfo.getAttributeValue<int>(attrPos); 
	break;
      case ISType::U32 :
	retValue = isInfo.getAttributeValue<unsigned int>(attrPos); 
	break;
      case ISType::S64 : 
	retValue =  isInfo.getAttributeValue<int64_t>(attrPos); 
	break;
      case ISType::U64 : 
	retValue = isInfo.getAttributeValue<uint64_t>(attrPos); 
	break;
      case ISType::Float : 
	retValue = (int) isInfo.getAttributeValue<float>(attrPos); 
	break;
      case ISType::Double : 
	retValue = (int) isInfo.getAttributeValue<double>(attrPos); 
	break;
      default :
	ERS_INFO("WARNING: Not forseen type for " << isSource);
	retValue = -1;
      }
      
      return retValue;
    }

  } // namespace ccwmi
} // namespace daq

