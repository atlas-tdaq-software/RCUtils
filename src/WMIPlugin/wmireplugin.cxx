// wmireplugin.cxx
//
// WMI Run Efficiency Plugin
//
//   Author: Mihai.Caprini@cern.ch, November 2009
//   Modifications:
//
//   Author  Date            Reason
//   MC      15/02/2010      Add runs and beam plots and run pages
//                           Include deatdime information
//   MC      12/04/2010      Use ReadyforPhysics flag
//   MC      10/05/2010      Use veto_fraction for deatdime evaluation
//   MC      20/08/2010      Fix exception catching
//   MC      07/02/2011      Use /TRIGGER/LUMI/LBTIME folder in getEfficiency
//                           Add Eff, Eff-Phys, RfP Delay, End Delay, 
//                               RunStopped and StopReason to Beam Table
//   MC      15/02/2011      If no busy info, let's just say we are not busy
//   MC      15/04/2011      Change name VME -> CTPMI_VME
//   MC      01/08/2011      Replace L1_MBTS_2 by L1_EM14
//   MC      03/04/2012      Replace L1_EM14 by L1_EM30
//                     


#include "wmi/iframe.h"

#include "wmireplugin.h"

#include <wmi/link.h>
#include <wmi/text.h>
#include <wmi/picture.h>
#include <wmi/font.h>
#include <wmi/timegraph.h>
#include <wmi/tree.h>
#include <ers/Issue.h>

#include <boost/date_time/posix_time/posix_time.hpp>

#include <utility>
#include <list>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>
#include <memory>

#include <CoralBase/Attribute.h>
#include <CoolKernel/Exception.h>
#include <CoolKernel/ValidityKey.h>
#include <CoolKernel/IDatabaseSvc.h>
#include <CoolKernel/IDatabase.h>
#include <CoolKernel/IFolder.h>
#include <CoolKernel/IRecordSpecification.h>
#include <CoolKernel/Record.h>
#include <CoolApplication/DatabaseSvcFactory.h>
#include <CoolKernel/IObjectIterator.h>
#include <CoolKernel/IObject.h>

using daq::wmi::Text;
using daq::wmi::Table;
using daq::wmi::TextOption;
using daq::wmi::Link;
using daq::wmi::Color;
using daq::wmi::Picture;
using daq::wmi::TimeGraph;
using daq::wmi::IFrame;
using daq::wmi::Tree;
using daq::wmi::Node;

unsigned int daq::rewmi::RunEffPlugin::hours(24);
long long daq::rewmi::RunEffPlugin::nowtime(0);
long long daq::rewmi::RunEffPlugin::start_time(0);
long long daq::rewmi::RunEffPlugin::end_time(0);
unsigned int daq::rewmi::RunEffPlugin::beamLimit(60);
unsigned int daq::rewmi::RunEffPlugin::writeBeamFile(0);
unsigned int daq::rewmi::RunEffPlugin::noRamp(0);
unsigned int daq::rewmi::RunEffPlugin::duration(60);
double  daq::rewmi::RunEffPlugin::rateLimit(1.0);
double  daq::rewmi::RunEffPlugin::lumiBlockBusyLimit(0.5);
double  daq::rewmi::RunEffPlugin::detBusyLimit(0.3);
unsigned int daq::rewmi::RunEffPlugin::intervalMinutes(60);
unsigned int daq::rewmi::RunEffPlugin::runningOffline(0);
unsigned int daq::rewmi::RunEffPlugin::beamType(3);
const unsigned int daq::rewmi::RunEffPlugin::graphTableColNum(2);
const unsigned int daq::rewmi::RunEffPlugin::graphPoints(48);
std::string daq::rewmi::RunEffPlugin::beamDataFile("/atlas-home/1/mcaprini/beam.log");
std::string daq::rewmi::RunEffPlugin::outFile("/tmp/eff.log");
std::string daq::rewmi::RunEffPlugin::triggerItem("L1_EM30");
unsigned int daq::rewmi::RunEffPlugin::readyForPhysics(0);
double  daq::rewmi::RunEffPlugin::busyLimit(0.03);

const std::string daq::rewmi::RunEffPlugin::RunNumberLabel("Run Number");
const std::string daq::rewmi::RunEffPlugin::NumberEventsLabel("Events");
const std::string daq::rewmi::RunEffPlugin::RunStartTimeLabel("Start Time");
const std::string daq::rewmi::RunEffPlugin::RunStopTimeLabel("Stop Time");
const std::string daq::rewmi::RunEffPlugin::LumiBlocksLabel("Lumi Blocks");
const std::string daq::rewmi::RunEffPlugin::LowRateLumiBlocksLabel("Low Rate Lumi Blocks");
const std::string daq::rewmi::RunEffPlugin::HighBusyLumiBlocksLabel("High Busy Lumi Blocks");
const unsigned int daq::rewmi::RunEffPlugin::generalTableColNum(8);
const unsigned int daq::rewmi::RunEffPlugin::busyTableColNum(4);

const std::string daq::rewmi::RunEffPlugin::BeamStartLabel("Start Time");
const std::string daq::rewmi::RunEffPlugin::FillNumberLabel("Fill Number");
const std::string daq::rewmi::RunEffPlugin::BeamDurationLabel("Duration (s)");
const std::string daq::rewmi::RunEffPlugin::RunLabel("Run Numbers");
const std::string daq::rewmi::RunEffPlugin::RunDurationLabel("Run Duration (s)");
const std::string daq::rewmi::RunEffPlugin::EffLabel("Eff");
const std::string daq::rewmi::RunEffPlugin::EffPhysLabel("Eff Physics");
const std::string daq::rewmi::RunEffPlugin::ReadyDelayLabel("RfP Delay (min)");
const std::string daq::rewmi::RunEffPlugin::EndDelayLabel("End Delay (min)");
const std::string daq::rewmi::RunEffPlugin::RunStoppedLabel("Run Stopped (min)");
const std::string daq::rewmi::RunEffPlugin::StopReasonLabel("Stop Reason");
const std::string daq::rewmi::RunEffPlugin::DelayLabel("Delay");

const std::string daq::rewmi::RunEffPlugin::LumiBlockLabel("Lumi Block");
const std::string daq::rewmi::RunEffPlugin::StartTimeLabel("Start Time");
const std::string daq::rewmi::RunEffPlugin::BusyFractionLabel("Busy Fraction");
const std::string daq::rewmi::RunEffPlugin::BusySourceLabel("Busy Source");

const std::string daq::rewmi::RunEffPlugin::EOR_Name("/TDAQ/RunCtrl/EOR_Params");
const std::string daq::rewmi::RunEffPlugin::SOR_Name("/TDAQ/RunCtrl/SOR_Params");
const std::string daq::rewmi::RunEffPlugin::EC_Name("/TDAQ/RunCtrl/EventCounters");
const std::string daq::rewmi::RunEffPlugin::StopReason_Name("/TDAQ/RunCtrl/StopReason");
const std::string daq::rewmi::RunEffPlugin::CTP_Name("/TRIGGER/LVL1/CTPCORELBDATA");
const std::string daq::rewmi::RunEffPlugin::LB_Name("/TRIGGER/LUMI/LBLB");
const std::string daq::rewmi::RunEffPlugin::LBT_Name("/TRIGGER/LUMI/LBTIME");
const std::string daq::rewmi::RunEffPlugin::BusyRate_Name("/TRIGGER/LVL1/BUSYRATES");
const std::string daq::rewmi::RunEffPlugin::BusyConf_Name("/TRIGGER/LVL1/BUSYCONF");
const std::string daq::rewmi::RunEffPlugin::FillState_Name("/LHC/DCS/FILLSTATE");
const std::string daq::rewmi::RunEffPlugin::TotalInt_Name("/LHC/DCS/BPTX/TOTALINT");
const std::string daq::rewmi::RunEffPlugin::L1Counters_Name("/TRIGGER/LUMI/LVL1COUNTERS");
const std::string daq::rewmi::RunEffPlugin::L1Menu_Name("/TRIGGER/LVL1/Menu");
const std::string daq::rewmi::RunEffPlugin::DataTakingMode_Name("/TDAQ/RunCtrl/DataTakingMode");

const daq::wmi::Color RED(255, 0, 0);
const daq::wmi::Color DARK_RED(183, 60, 60);
const daq::wmi::Color DARK_BLUE(60, 60, 183);
const daq::wmi::Color WHITE(255, 255, 255);
const daq::wmi::Color GREY(198, 198, 198);
const daq::wmi::Color DARK_GREY(150, 151, 150);
const daq::wmi::Color CELL_BG(219, 239, 251);
const daq::wmi::Color CAPTION_BG(0, 64, 128);
const daq::wmi::Color LIGHT_GREY(240, 240, 240);
const daq::wmi::Color TABLE_BG(107, 127, 147); //Color found as background color of the tables from LAR page: http://pcatdwww.cern.ch/atlas-point1/lar/geninfo/lar.php?subdet=LAR : blueish gray...
const daq::wmi::Color BLACK(0, 0, 0);
#include <sstream>

std::string intToString(unsigned int value){
	std::ostringstream intStream;
	intStream << value;
	return intStream.str();
}


std::string doubleToString(double value){
	//especially made for values between 0 and 100. For values outside this range it will return an integer - float with 0 precision
	std::ostringstream doubleStream;
	if (value > 99.999 || value < 0.001) {
        	doubleStream << std::fixed << std::setprecision(0) << value;
	} else if (value < 1) {
                doubleStream << std::fixed << std::setprecision(3) << value;
        } else {
		doubleStream << std::fixed << std::setprecision(2) << value;
	}
        return doubleStream.str();
}

namespace daq {
  namespace rewmi {

    void
    RunEffPlugin::printBeamsGraph(Table& tb) const
      throw()
    {

// std::cout << "Print beams graph ..." << std::endl;
      static std::string titleBeams("Beams");
      if (beamType == 0) {
        titleBeams = "Beams (Stable Beams)";
      }
      if (beamType == 1) {
        titleBeams = "Beams (Beam1 AND Beam2)";
      }
      if (beamType == 2) {
        titleBeams = "Beams (Beam1 OR Beam2)";
      }
      if (beamType == 3) {
        titleBeams = "Beams (Stable Beam1 AND Beam2)";
      }
      const unsigned int rWidth = 800;
      const unsigned int rHeight = 120;


      tb.setWidth(rWidth);
      tb.setTDWidth(1, rWidth);

      tb.setAlignCell(1,1,2);
      tb.addElement(1, 1, new Text(titleBeams, 3, WHITE, TextOption("b")));
      tb.addRow();
      tb.addElement(1, 2, new TimeGraph("", "", beamsGraph, rWidth, rHeight));

    }

    void
    RunEffPlugin::printRunsGraph(Table& tb) const
      throw()
    {

      static std::string titleRuns("Runs");

      const unsigned int rWidth = 800;
      const unsigned int rHeight = 120;


      tb.setWidth(rWidth);
      tb.setTDWidth(1, rWidth);

      tb.setAlignCell(1,1,2);
      tb.addElement(1, 1, new Text(titleRuns, 3, WHITE, TextOption("b")));
      tb.addRow();
      tb.addElement(1, 2, new TimeGraph("", "", runsGraph, rWidth, rHeight));

    }

    void
    RunEffPlugin::printEffGraph(Table& tb) const
      throw()
    {

      static std::string titleEff("RunTimeDuringBeam / BeamTime");
      if (beamType == 0) {
        titleEff = "RunTimeDuringBeam / BeamTime  (Stable Beams)  Mean value = " + meanBeamEff;
      }
      if (beamType == 1) {
        titleEff = "RunTimeDuringBeam / BeamTime  (Beam1 AND Beam2)  Mean value = " + meanBeamEff;
      }
      if (beamType == 2) {
        titleEff = "RunTimeDuringBeam / BeamTime   (Beam1 OR Beam2)  Mean value = " + meanBeamEff;
      }
      if (beamType == 3) {
        titleEff = "RunTimeDuringBeam / BeamTime  (Stable Beam1 AND Beam2)  Mean value = " + meanBeamEff;
      }
      const unsigned int rWidth = 800;
      const unsigned int rHeight = 200;

      tb.setWidth(rWidth);
      tb.setTDWidth(1, rWidth);

      tb.setAlignCell(1,1,2);
      tb.addElement(1, 1, new Text(titleEff, 3, WHITE, TextOption("b")));
      tb.addRow();
      tb.addElement(1, 2, new TimeGraph("", "", effGraph, rWidth, rHeight));

    }

    void
    RunEffPlugin::printRateGraph(Table& tb) const
      throw()
    {

      static std::string titleRate("LVL1 Rate (Hz)");

      const unsigned int rWidth = 800;
      const unsigned int rHeight = 200;


      tb.setWidth(rWidth);
      tb.setTDWidth(1, rWidth);

      tb.setAlignCell(1,1,2);
      tb.addElement(1, 1, new Text(titleRate, 3, WHITE, TextOption("b")));
      tb.addRow();
      tb.addElement(1, 2, new TimeGraph("", "", rateGraph, rWidth, rHeight));

    } 

    /************************************/
    /** RunEffPlugin::RunData **/
    /************************************/

    RunEffPlugin::RunData::RunData(const std::string& runNumber)
      throw() :
      runNumber(runNumber)
    {
    }

    RunEffPlugin::RunData::~RunData()
      throw()
    {
    }


    /*********************/
    /** RunEffPlugin **/
    /*********************/
    
    RunEffPlugin::RunEffPlugin() : 
      PluginBase()
    {
      
    }

    RunEffPlugin::~RunEffPlugin() {
      delete m_outStream;
    }

    void RunEffPlugin::periodicAction() {      
     
std::cout << OWLTime() << " Periodic action ..." << std::endl;
      rateGraph.clear();
      effGraph.clear();
      runsGraph.clear();
      beamsGraph.clear();
      takeTime();
      this->cleanRunsMap();
      openDatabasesAndFolders();
      this->updateEfficiencyData();
      this->updateGlobalEfficiency();
      this->buildTables();
      this->buildRunsPages();

      // Close all the ouput streams
      m_outStream->closeAll();  
      closeDatabases();    
    }

    void RunEffPlugin::takeTime() throw() {
      OWLTime now = OWLTime();
//      OWLTime now("26/11/2010 23:00:00"); 

      nowtime = now.total_mksec_utc();
      end_time = nowtime;
      //hours
      long long total_duration_mksec = ((long long) 3600) * ((long long) 1000000);
      total_duration_mksec = hours * total_duration_mksec;
      // day
//      long long duration_mksec = ((long long) 86400) * ((long long) 1000000);
//      duration_mksec = days * duration_mksec;
      // week
//      long long duration_mksec = (((long long) 604800) * ((long long) 1000000);
//      duration_mksec = weeks * duration_mksec;

      start_time = end_time - total_duration_mksec;
    }

    void RunEffPlugin::cleanRunsMap() throw() {
      std::map<std::string, RunData*>::iterator mapIt;
      for(mapIt = this->runsMap.begin(); mapIt != this->runsMap.end(); ) {
        delete mapIt->second;
        this->runsMap.erase(mapIt++);
      }
    }

    void 
    RunEffPlugin::stop() 
    {
      // stop()
      m_outStream->closeAll();      
    }

    void 
    RunEffPlugin::configure(const ::wmi::InfoParameter& params) 
    {
      // configure()
      unsigned int nums = params.length();
      for(unsigned int i = 0; i < nums; ++i) {
	const char* const parName = params[i].name;
        const char* const parVal = params[i].value;
	
        if (!strcmp(parName, "hours")) {
           hours = ::atoi(parVal);
        }
        if (!strcmp(parName, "beam_limit")) {
           beamLimit = ::atoi(parVal);
        }
        if (!strcmp(parName, "rate_limit")) {
           rateLimit = ::atof(parVal);
        }
        if (!strcmp(parName, "lumi_block_busy_limit")) {
           lumiBlockBusyLimit = ::atof(parVal);
        }
        if (!strcmp(parName, "det_busy_limit")) {
           detBusyLimit = ::atof(parVal);
        }
        if (!strcmp(parName, "duration_minutes")) {
           duration = ::atoi(parVal);
        }
        if (!strcmp(parName, "interval_minutes")) {
           intervalMinutes = ::atoi(parVal);
        }
        if (!strcmp(parName, "data_file")) {
           beamDataFile = parVal;
        }
        if (!strcmp(parName, "out_file")) {
           outFile = parVal;
        }
        if (!strcmp(parName, "running_offline")) {
          runningOffline = ::atoi(parVal);
        }
        if (!strcmp(parName, "beam_type")) {
          beamType = ::atoi(parVal);
        }
        if (!strcmp(parName, "write_beam_to_file")) {
           writeBeamFile = ::atoi(parVal);
        }
        if (!strcmp(parName, "no_ramp")) {
           noRamp = ::atoi(parVal);
        }
        if (!strcmp(parName, "ready_for_physics")) {
           readyForPhysics = ::atoi(parVal);
        }
        if (!strcmp(parName, "trigger_item")) {
           triggerItem = parVal;
        }
        if (!strcmp(parName, "busy_limit")) {
           busyLimit = ::atof(parVal);
        }
      }      
      if ((beamType == 0) || (beamType == 3)) {
        beamTableColNum = 10;
      } else {
        beamTableColNum = 5;
      }
    }

void RunEffPlugin::closeDatabases() {
  db->closeDatabase();
  dbm->closeDatabase();
  dbt->closeDatabase();
  dbd->closeDatabase();
  std::cout << "DB closed." << std::endl;
}

void RunEffPlugin::openDatabasesAndFolders() {
  std::string coolDb;
  std::string coolDbTrig;
  std::string coolDbMon;
  std::string coolDbDCS;

  if (runningOffline == 0) {
    coolDb = "oracle://ATONR_COOL;schema=ATLAS_COOLONL_TDAQ;dbname=COMP200";
  } else {
    coolDb = "oracle://ATLAS_COOLPROD;schema=ATLAS_COOLONL_TDAQ;dbname=COMP200;user=ATLAS_COOL_READER;password=COOLRED4PRO";
  }
  cool::DatabaseId dbId = coolDb;
  // Open DB
  cool::IDatabaseSvc& dbSvc = cool::DatabaseSvcFactory::databaseService();
  ERS_DEBUG(3, "Retrieved DB service.");

  try {
    db = dbSvc.openDatabase(dbId, false); // false = !read_only
  } catch (std::exception &ex) {
    std::cout << "Failure opening DB database - " << ex.what() << std::endl;
  }

  if (runningOffline == 0) {
    coolDbTrig = "oracle://ATONR_COOL;schema=ATLAS_COOLONL_TRIGGER;dbname=COMP200";
  } else {
    coolDbTrig = "oracle://ATLAS_COOLPROD;schema=ATLAS_COOLONL_TRIGGER;dbname=COMP200";
  }
  cool::DatabaseId dbIdTrig = coolDbTrig;
  cool::IDatabaseSvc& dbSvcTrig = cool::DatabaseSvcFactory::databaseService();
  try {
    dbt = dbSvcTrig.openDatabase(dbIdTrig, false); // false = !read_only
  } catch (std::exception &ex) {
    std::cout << "Failure opening DBT database - " << ex.what() << std::endl;
  }

  if (runningOffline == 0) {
    coolDbMon = "oracle://ATONR_COOL;schema=ATLAS_COOLONL_TRIGGER;dbname=MONP200";
  } else {
    coolDbMon = "oracle://ATLAS_COOLPROD;schema=ATLAS_COOLONL_TRIGGER;dbname=MONP200";
  }
  cool::DatabaseId dbIdMon = coolDbMon;
  cool::IDatabaseSvc& dbSvcMon = cool::DatabaseSvcFactory::databaseService();
  try {
    dbm = dbSvcMon.openDatabase(dbIdMon, false); // false = !read_only
  } catch (std::exception &ex) {
    std::cout << "Failure opening DBM database - " << ex.what() << std::endl;
  }

  if (runningOffline == 0) {
//    coolDbDCS = "oracle://ATONR_COOL;schema=ATLAS_COOLOFL_DCS;dbname=COMP200";
    coolDbDCS = "oracle://ATLAS_COOLPROD;schema=ATLAS_COOLOFL_DCS;dbname=COMP200;user=ATLAS_COOL_READER;password=COOLRED4PRO";
  } else {
    coolDbDCS = "oracle://ATLAS_COOLPROD;schema=ATLAS_COOLOFL_DCS;dbname=COMP200";
  }
  cool::DatabaseId dbIdDCS = coolDbDCS;
  cool::IDatabaseSvc& dbSvcDCS = cool::DatabaseSvcFactory::databaseService();
  try {
    dbd = dbSvcDCS.openDatabase(dbIdDCS, false); // false = !read_only
  } catch (std::exception &ex) {
    std::cout << "Failure opening DCS database - " << ex.what() << std::endl;
  }

  // check folders
  if (!db->existsFolder(EOR_Name)) {
    std::cout << "DB Folder EOR does not exist???" << std::endl;
  }
  if (!db->existsFolder(SOR_Name)) {
    std::cout << "DB Folder SOR does not exist???" << std::endl;
  }
  if (!db->existsFolder(StopReason_Name)) {
    std::cout << "DB Folder StopReason does not exist???" << std::endl;
  }
  try {
    if (!dbt->existsFolder(CTP_Name)) {
      std::cout << "DB Folder CTP does not exist???" << std::endl;
    }
  }
  catch (std::exception &e) {
     std::cout << "DB Folder CTP does not exist. " << e.what() << std::endl;
  }
  try {
     if (!dbm->existsFolder(BusyRate_Name)) {
        std::cout << "DB Folder BusyRate does not exist???" << std::endl;
     }
  } catch (std::exception &e) {
     std::cout << "DB Folder BusyRate does not exist. " << e.what() << std::endl;
  }
  try {
     if (!dbm->existsFolder(BusyConf_Name)) {
        std::cout << "DB Folder BusyConf does not exist???" << std::endl;
     }
  } catch (std::exception &e) {
     std::cout << "DB Folder BusyConf does not exist. " << e.what() << std::endl;
  }
  try {
     if (!dbd->existsFolder(FillState_Name)) {
        std::cout << "DB Folder FillState does not exist???" << std::endl;
     }
  } catch (std::exception &e) {
     std::cout << "DB Folder FillState does not exist. " << e.what() << std::endl;
  }
  try {
     if (!dbd->existsFolder(TotalInt_Name)) {
        std::cout << "DB Folder TotalInt does not exist???" << std::endl;
     }
  } catch (std::exception &e) {
     std::cout << "DB Folder TotalInt does not exist. " << e.what() << std::endl;
  }
  if (!dbt->existsFolder(LB_Name)) {
    std::cout << "DB Folder LB does not exist???" << std::endl;
  }
  if (!dbt->existsFolder(LBT_Name)) {
    std::cout << "DB Folder LBT does not exist???" << std::endl;
  }
  if (!dbt->existsFolder(L1Counters_Name)) {
    std::cout << "DB Folder L1Counters does not exist???" << std::endl;
  }
  if (!dbt->existsFolder(L1Menu_Name)) {
    std::cout << "DB Folder Menu does not exist???" << std::endl;
  }
  if (!db->existsFolder(DataTakingMode_Name)) {
    std::cout << "DB Folder Menu does not exist???" << std::endl;
  }
  //open folders

  EOR_Folder = db->getFolder(EOR_Name);
  SOR_Folder = db->getFolder(SOR_Name);
  EC_Folder = db->getFolder(EC_Name);
  StopReason_Folder = db->getFolder(StopReason_Name);

  try {
    CTP_Folder = dbt->getFolder(CTP_Name);
  }
  catch (std::exception &e) {
     std::cout << "Cannot open CTP folder " << e.what() << std::endl;
     db->closeDatabase();
     std::cout << "DBT closed." << std::endl;
  }

  try {
    BusyRate_Folder = dbm->getFolder(BusyRate_Name);
  }
  catch (std::exception &e) {
     std::cout << "Cannot open BusyRate folder " << e.what() << std::endl;
     dbm->closeDatabase();
     std::cout << "DBM closed." << std::endl;
  }

  try {
    BusyConf_Folder = dbm->getFolder(BusyConf_Name);
  }
  catch (std::exception &e) {
     std::cout << "Cannot open BusyConf folder " << e.what() << std::endl;
     dbm->closeDatabase();
     std::cout << "DBM closed." << std::endl;
  }

  LB_Folder = dbt->getFolder(LB_Name);
  LBT_Folder = dbt->getFolder(LBT_Name);
  L1Counters_Folder = dbt->getFolder(L1Counters_Name);
  L1Menu_Folder = dbt->getFolder(L1Menu_Name);
  DataTakingMode_Folder = db->getFolder(DataTakingMode_Name);

  try {
    FillState_Folder = dbd->getFolder(FillState_Name);
  }
  catch (std::exception &e) {
     std::cout << "Cannot open FillState folder " << e.what() << std::endl;
     dbd->closeDatabase();
     std::cout << "DBD closed." << std::endl;
  }

  try {
    TotalInt_Folder = dbd->getFolder(TotalInt_Name);
  }
  catch (std::exception &e) {
     std::cout << "Cannot open TotalInt folder " << e.what() << std::endl;
     dbd->closeDatabase();
     std::cout << "DBD closed." << std::endl;
  }
}

void RunEffPlugin::updateEfficiencyData() throw() {

  std::cout << "Update efficiency data ..." << std::endl;

  long long currenttime = start_time;
  long long interval_mksec = ((long long) 60) * ((long long) 1000000);
  interval_mksec = intervalMinutes * interval_mksec;

  // minute
  long long duration_mksec = ((long long) 60) * ((long long) 1000000);
  duration_mksec = duration * duration_mksec;
  // hour
//  long long duration_mksec = ((long long) 3600) * ((long long) 1000000);
//  duration_mksec = hours * duration_mksec;
  // day
//  long long duration_mksec = ((long long) 86400) * ((long long) 1000000);
//  duration_mksec = days * duration_mksec;
  // week
//  long long duration_mksec = (((long long) 604800) * ((long long) 1000000);
  int index = 0;
  while (currenttime <= end_time) {
    index = index + 1;
    long long endtime  = currenttime;
    long long starttime = endtime - duration_mksec;

    beamEff = getEfficiency(starttime, endtime, readyForPhysics);
    std::pair<unsigned long, float> values(currenttime/1000000, beamEff);
    effGraph.push_back(values);

    currenttime = currenttime + interval_mksec;
  }
}

void RunEffPlugin::updateGlobalEfficiency() throw() {

std::cout << "Update global efficiency ..." << std::endl;
  double rateLevel = rateLimit;
  rn_vector.clear();
  ev_vector.clear();
  sor_vector.clear();
  eor_vector.clear();
  lb_vector.clear();
  lrlb_vector.clear();
  hblb_vector.clear();

  beam_start_vector.clear();
  fill_number_vector.clear();
  beam_duration_vector.clear();
  beam_run_vector.clear();
  run_duration_vector.clear();
  beam_delay_vector.clear();
  eff_vector.clear();
  eff_phys_vector.clear();
  ready_delay_vector.clear();
  end_delay_vector.clear();
  run_stopped_vector.clear();
  stop_reason_vector.clear();

  std::string partitionName = "ATLAS";
  std::string startTime;

  long long starttime = start_time;
  long long endtime = end_time;
  OWLTime start = OWLTime(starttime/1000000);
  startTime = std::string(start.c_str());

  std::vector<int> delay_vector;
  std::vector<int> run_number_vector;

  int totalBeamTime = 0;

  // read beam data
  if (beamType == 0) {
    // stable beams
    get_beams_data (starttime*1000, endtime*1000, FillState_Folder);
    set_main_vectors(beams_start_vector, beams_end_vector);
    if (writeBeamFile == 1) {
      print_vectors(start_beam_vector, end_beam_vector, "/tmp/beams.log");
    }
  }
  if (beamType == 1) {
    // two beams present
    get_beam1_data (starttime*1000, endtime*1000, TotalInt_Folder);
    get_beam2_data (starttime*1000, endtime*1000, TotalInt_Folder);
    set_beam1and2_vectors();
    if (noRamp == 0) {
      set_main_vectors(beam1and2_start_vector, beam1and2_end_vector);
    }
    if (noRamp == 1) {
      get_ramp_data (starttime*1000, endtime*1000, FillState_Folder);
      set_beam1and2noRamp_vectors();
      set_main_vectors(beam1and2noRamp_start_vector, beam1and2noRamp_end_vector);
    }
    if (writeBeamFile == 1) {
      print_vectors(beam1_start_vector, beam1_end_vector, "/tmp/beam1.log");
      print_vectors(beam2_start_vector, beam2_end_vector, "/tmp/beam2.log");
      print_vectors(beam1and2_start_vector, beam1and2_end_vector, "/tmp/beam1and2.log");
      if (noRamp == 1) {
        print_vectors(ramp_start_vector, ramp_end_vector, "/tmp/ramp.log");
        print_vectors(start_beam_vector, end_beam_vector, "/tmp/beam1and2noRamp.log");
      }
    }
  }
  if (beamType == 2) {
    // one beam present
    get_beam1_data (starttime*1000, endtime*1000, TotalInt_Folder);
    get_beam2_data (starttime*1000, endtime*1000, TotalInt_Folder);
    set_beam1or2_vectors();
    if (noRamp == 0) {
      set_main_vectors(beam1or2_start_vector, beam1or2_end_vector);
    }
    if (noRamp == 1) {
      get_ramp_data (starttime*1000, endtime*1000, FillState_Folder);
      set_beam1or2noRamp_vectors();
      set_main_vectors(beam1or2noRamp_start_vector, beam1or2noRamp_end_vector);
    }
    if (writeBeamFile == 1) {
      print_vectors(beam1_start_vector, beam1_end_vector, "/tmp/beam1.log");
      print_vectors(beam2_start_vector, beam2_end_vector, "/tmp/beam2.log");
      print_vectors(beam1or2_start_vector, beam1or2_end_vector, "/tmp/beam1or2.log");
      if (noRamp == 1) {
        print_vectors(ramp_start_vector, ramp_end_vector, "/tmp/ramp.log");
        print_vectors(start_beam_vector, end_beam_vector, "/tmp/beam1or2noRamp.log");
      }
    }
  }
  if (beamType == 3) {
    // two beams present and stable
    get_beam1_data (starttime*1000, endtime*1000, TotalInt_Folder);
    get_beam2_data (starttime*1000, endtime*1000, TotalInt_Folder);
    get_beams_data (starttime*1000, endtime*1000, FillState_Folder);
    set_beam1and2_vectors();
    set_beam1and2andStable_vectors();
    set_main_vectors(beam1and2andStable_start_vector, beam1and2andStable_end_vector);
    if (writeBeamFile == 1) {
      print_vectors(beam1_start_vector, beam1_end_vector, "/tmp/beam1.log");
      print_vectors(beam2_start_vector, beam2_end_vector, "/tmp/beam2.log");
      print_vectors(beams_start_vector, beams_end_vector, "/tmp/beams.log");
      print_vectors(start_beam_vector, end_beam_vector, "/tmp/beam1and2andStable.log");
    }
  }
  if (beamType == 4) {
    // beam data from file
    get_beam_data_from_file (starttime, endtime);
    if (writeBeamFile == 1) {
      print_vectors(start_beam_vector, end_beam_vector, "/tmp/beamfromfile.log");
    }
  }
  if (start_beam_vector.size() > 0) {
    int valbeam = 0;
    for (unsigned int i = 0; i < start_beam_vector.size(); i++ ) {
      valbeam = end_beam_vector[i]/1000 - start_beam_vector[i]/1000;
      totalBeamTime = totalBeamTime + valbeam;
      delay_vector.push_back(valbeam);
      run_number_vector.push_back(0);
    }
  }

  // insert first points in graphs
  std::pair<unsigned long, float> vals(starttime/1000000, 0.0);
  beamsGraph.push_back(vals);
  runsGraph.push_back(vals);
  rateGraph.push_back(vals);

  // read run data
  int totalInBeamTime = 0;
  int totalInBeamTime_msec = 0;
  int no_data_time_in_beam_msec = 0;
  int totalRunNumber = 0;
  // find first run 
  int firstRunNumber = 173500; // default value, runs after Jan 2011
  try {
    long long start_time_nsec = ((long long)(starttime))*1000;
    long long end_time_nsec = ((long long)(endtime))*1000;
    cool::IObjectIteratorPtr rl_objects = LBT_Folder->browseObjects(cool::ValidityKey(start_time_nsec), cool::ValidityKey(end_time_nsec), 0);
    rl_objects->goToNext();
    const cool::IObject& obj = rl_objects->currentRef();
    std::string runNumber = obj.payloadValue("Run");
    firstRunNumber = atoi(runNumber.c_str());
//    std::cout << "firstRunNumber = " << firstRunNumber << std::endl;
  } catch (std::exception &e) {
    std::cout << "Warning: Retrieving firstRunNumber failed! -> " << e.what() << std::endl;
  } 
      
  try {
    // consider only runs with RN > 140000 (18-Nov-2009)
    // consider only runs with RN > 160000 (20-Jul-2010)
    // cool::UInt63 aa = 160000;
    cool::UInt63 aa = firstRunNumber;
    cool::ValidityKey initialKey = aa <<32;

    // start looking for runs in SOR_Folder
    cool::IObjectIteratorPtr objects = SOR_Folder->browseObjects( initialKey, cool::ValidityKeyMax, 0 );

    cool::ValidityKey key;
    cool::ValidityKey key1;
    cool::IObjectPtr obj_eor;
    cool::IObjectPtr obj_ec;
    cool::IObjectPtr obj_lb;
    while ( objects->goToNext() ) {
      const cool::IObject& obj = objects->currentRef();
      // read start of run time
      std::string sor = obj.payloadValue("SORTime").substr(0,16);
      long long sortime = atoll(sor.c_str());
      time_t sor_tt = time_t(sortime/1000000);
      OWLTime owlsor = OWLTime(sor_tt);
      // read run number
      std::string runNumber = obj.payloadValue("RunNumber");
      int run_number = atoi(runNumber.c_str());
      // prepare the since and until value for the search in EOR_Folder
      cool::UInt63 a = run_number;
      cool::UInt63 z = run_number+1;
      cool::ValidityKey sKey = a <<32;
      cool::ValidityKey uKey = (z <<32) - 1;
      // initial value for end of run (to be read later)
      long long eortime = 0;
      // check only for runs with start time three days before start of search
      // interval
      long long pre_check = ((long long)(72000))*((long long)(3600000));
      long long check_time = starttime - pre_check;
      if ((sortime > check_time) && (sortime < endtime)) {
        // try to find end of run time
        cool::IObjectIteratorPtr objects_eor = EOR_Folder->browseObjects(sKey, uKey, 0 );
        bool found = false;
        bool noEOR = false;
        std::string activeTime = "";
        while ( objects_eor->goToNext() ) {
          found = true;
          const cool::IObject& obj_eor = objects_eor->currentRef();
          std::string eor = obj_eor.payloadValue("EORTime").substr(0,16);
          activeTime = obj_eor.payloadValue("TotalTime");
          eortime = atoll(eor.c_str());
          key = obj.since();
        }
        bool currentRun = false;
        if (found == false) {
          noEOR = true;
          // consider that the run just started, to be checked later ??
          long long run_duration = checkRunInRNDB(partitionName, run_number);
          if (run_duration == 0) {
            currentRun = true;
            eortime = endtime;
            key = obj.since();
          } else if (run_duration > 0) {
              eortime = sortime + run_duration;
              key = obj.since();
          } else {
            // run was not found as a run for this partition in RN DB
            // skip this run
            continue;
          }
        }

        time_t eor_tt = time_t(eortime/1000000);
        OWLTime owleor = OWLTime(eor_tt);
        if (((sortime > check_time) && (sortime < endtime)) && (eortime > starttime))  {
          try {
           bool goodRun = false;
           std::string recordedEvents;
           int active_time = 0;
           if (!noEOR) {
             obj_ec = EC_Folder->findObject(key, 0);
             std::string partition = obj_ec->payloadValue("PartitionName");
             active_time = atoi(activeTime.c_str());
             if (partition.compare(partitionName) == 0) {
               recordedEvents = obj_ec->payloadValue("RecordedEvents");
               goodRun = true;
             }
           }
           if (noEOR || goodRun) {
              long long run_time = (eortime - sortime)/1000000;
              if (currentRun) {
                active_time = run_time;
              }
              long long sor_ms = 0;
              long long eor_ms = 0;
              bool toSkip = false;
              rn_vector.push_back(runNumber);
              this->runsMap[runNumber] = new RunData(runNumber);
              ev_vector.push_back(recordedEvents);
              sor_vector.push_back(owlsor.c_str());
              long long run_duration_mksec = 0;
              if (currentRun) {
                eor_vector.push_back("");
                run_duration_mksec = endtime - sortime;
              } else {
                eor_vector.push_back(owleor.c_str());
                run_duration_mksec = eortime - sortime;
              }
              int run_duration_sec = (int) (run_duration_mksec/1000000);
              run_duration_vector.push_back(intToString(run_duration_sec));
              if ((sortime >= starttime) && (eortime <= endtime)) {
                sor_ms = sortime/1000;
                eor_ms = eortime/1000;
              }
              if ((sortime < starttime) && (eortime <= endtime)) {
                sor_ms = starttime/1000;
                eor_ms = eortime/1000;
              }
              if ((sortime >= starttime) && (eortime > endtime)) {
                sor_ms = sortime/1000;
                eor_ms = endtime/1000;
              }
              if ((sortime < starttime) && (eortime > endtime)) {
                sor_ms = starttime/1000;
                eor_ms = endtime/1000;
              }
              if (!toSkip) {
                totalRunNumber++;

                std::map<std::string, std::string> ctpMap;
                // Populate ctp map for this run
                cool::UInt63 a = run_number;
                cool::UInt63 z = run_number+1;
                cool::ValidityKey sKey = (a <<32) + 1;
                cool::ValidityKey uKey = z <<32;;

                cool::IObjectIteratorPtr objects = BusyConf_Folder->browseObjects(sKey, uKey, cool::ChannelSelection::all());
                while ( objects->goToNext() ) {
                  const cool::IObject& obj = objects->currentRef();
                  std::string identifier = obj.payloadValue("identifier");
                  std::string description = obj.payloadValue("description");
                  std::string busyEnabled = obj.payloadValue("busyEnabled");
                  if (busyEnabled.compare("TRUE") == 0) {
                    ctpMap.insert(std::pair<std::string, std::string>(identifier, description) );
                  }
                }
                int ctpId = get_ctpid(sKey, uKey, triggerItem, L1Menu_Folder);                  if (ctpId == -1) {
                  std::cout << "For Run " << runNumber << " Trigger Item not found. Default value (227) will be used." << std::endl;
                  ctpId = 227;
                }
                if (rateLevel > 0) {
                  // use the same since and until keys as for EOR_Folder
                  cool::IObjectIteratorPtr objects_ctp = CTP_Folder->browseObjects(sKey, uKey, 0 );
                  int lumiBlocks = 0;
                  int lowRateLumiBlocks = 0;
                  int highBusyLumiBlocks = 0;
                  try {
                    while ( objects_ctp->goToNext() ) {
                      const cool::IObject& obj_ctp = objects_ctp->currentRef();
                      lumiBlocks = lumiBlocks + 1;
                      key1 = obj_ctp.since();
                      obj_lb = LB_Folder->findObject(key1, 0);
                      std::string st = obj_lb->payloadValue("StartTime");
                      std::string et = obj_lb->payloadValue("EndTime");
                      long long sT = atoll(st.c_str());
                      long long eT = atoll(et.c_str());
                      long long eT_msec = eT/1000000;
                      long long lb = eT - sT;
                      long long lb_msec = lb/1000000;
                      std::string lbLVL1 = obj_ctp.payloadValue("GlobalL1AcceptCounter");
                      long long lb_LVL1 = atoll(lbLVL1.c_str());
                      long long lb_LVL1_long = lb_LVL1*1000;
                      double lb_rate = (double)lb_LVL1_long/ (double)lb_msec;
                      std::pair<unsigned long, float> values(eT_msec/1000, lb_rate);
                      if ((eT_msec/1000 >= starttime/1000000) &&
                          (eT_msec/1000 <= endtime/1000000)) {
                        rateGraph.push_back(values);
                      }
                      ((this->runsMap[runNumber])->l1Graph).push_back(values);
                      int readyForPhysicsFlag = 1;
                      if (((beamType == 0) || (beamType == 3)) && (readyForPhysics == 1)) {
                        readyForPhysicsFlag = ready_for_physics(key1, DataTakingMode_Folder);
                      }
                      double vetofraction = veto_fraction(key1, ctpId, L1Counters_Folder);
                      double busyfraction = fraction(sT, eT, BusyRate_Folder );
                      if (busyfraction > lumiBlockBusyLimit) {
                        highBusyLumiBlocks = highBusyLumiBlocks + 1;
                        ((this->runsMap[runNumber])->lb_vector).push_back(intToString(lumiBlocks));
                        long long start_lb_sec = sT/1000000000;
                        OWLTime start_lb_owl = OWLTime(start_lb_sec);
                        ((this->runsMap[runNumber])->solb_vector).push_back(start_lb_owl.c_str());
                        ((this->runsMap[runNumber])->busy_fraction_vector).push_back(doubleToString(busyfraction));
                        std::ostringstream strm;
                        if (!no_busy_info) {
                          std::map<std::string,std::string>::iterator iter;
                          for ( iter=ctpMap.begin() ; iter != ctpMap.end(); iter++ ) {
                            double det_busy = busy(sT,eT, (*iter).first, BusyRate_Folder );
                            if (det_busy > detBusyLimit) {
                              strm << (*iter).second << " (" << det_busy << ") ";
                            }
                          }
                          double det_busy = busy(sT,eT, "ctpmi_vme_rate", BusyRate_Folder );
                          if (det_busy > detBusyLimit) {
                              strm << "CTPMI_VME (" << det_busy << ") ";
                          }
                        }
                        ((this->runsMap[runNumber])->busy_vector).push_back(strm.str());
                      }
                      if (lb_rate < rateLevel) {
                        lowRateLumiBlocks = lowRateLumiBlocks + 1;
                      }
                      if (start_beam_vector.size() > 0) {
                        for (unsigned int i = 0; i < start_beam_vector.size(); i++ ) {
                          int data_lumi_block_in_beam_msec = valInBeam(sT/1000000, eT/1000000, start_beam_vector[i], end_beam_vector[i]);
                          if (data_lumi_block_in_beam_msec > 0) {
                            if (readyForPhysicsFlag == 0) {
                              vetofraction = 1;
                            }
                            totalInBeamTime_msec = totalInBeamTime_msec + data_lumi_block_in_beam_msec;
                            int no_data_lumi_block_in_beam_msec = (int) (((double) data_lumi_block_in_beam_msec) * vetofraction);
                            no_data_time_in_beam_msec = no_data_time_in_beam_msec + no_data_lumi_block_in_beam_msec;
                          }
                        }
                      }
                      totalInBeamTime = totalInBeamTime_msec/1000 - no_data_time_in_beam_msec/1000;
                    }
                  } catch (std::exception &ex) {
                    std::cout << "Read trigger info failed: " << ex.what() << std::endl;
                  }
                  lb_vector.push_back(intToString(lumiBlocks));
                  lrlb_vector.push_back(intToString(lowRateLumiBlocks));
                  hblb_vector.push_back(intToString(highBusyLumiBlocks));
                }
                if (start_beam_vector.size() > 0) {
                  // this condition to be reviewed ...
                  if (active_time > 3600) {
                    for (unsigned int i = 0; i < start_beam_vector.size(); i++ ) {
                      if (((sor_ms < start_beam_vector[i]) && (eor_ms > start_beam_vector[i])) && ((eor_ms - start_beam_vector[i])/1000 > 3600)) {
                        delay_vector[i] = 0;
                        run_number_vector[i] = run_number;
                      }
                      if ((sor_ms >= start_beam_vector[i]) && (sor_ms < end_beam_vector[i])) {
                        if ((sor_ms - start_beam_vector[i])/1000 < delay_vector[i]) {
                          delay_vector[i] = (sor_ms - start_beam_vector[i])/1000;
                          run_number_vector[i] = run_number;
                        }
                      }
                    }
                  }
                }
              }
            }
          } catch (std::exception &e) {
            std::cout << "Warning: Retrieving data for RN = " << obj.payloadValue("RunNumber") << "  -> " << e.what() << std::endl;
          }
        }
      }
    }
  } catch (std::exception &e) {
     std::cout << "Read failed: " << e.what() << std::endl;
  }

  // print summary data
  double fractionBeam = 0;
  meanBeamEff = "";
  if (totalBeamTime > 0) {
    fractionBeam = (double)totalInBeamTime/(double)totalBeamTime;
    meanBeamEff = doubleToString(fractionBeam);
  }

  if (rn_vector.size() > 0) {
    generalTableRawNum = rn_vector.size() + 1;
  } else {
    generalTableRawNum = 1;
  }
  beamTableRawNum = 1;
  // prepare global beam vector
  std::vector<long long> global_start_beam_vector;
  std::vector<long long> global_end_beam_vector;
  if (start_beam_vector.size() > 0) {
    for (unsigned int i = 0; i < start_beam_vector.size(); i++ ) {
      global_start_beam_vector.push_back(start_beam_vector[i]);
      global_end_beam_vector.push_back(end_beam_vector[i]);
    }
  }
  if (global_start_beam_vector.size() > 0) {
    for (unsigned int i = 0; i < global_start_beam_vector.size(); i++ ) {
      long long global_start_beam_tmp = global_start_beam_vector[i];
      // get fill number
      long long start_search_nsec = ((long long)(global_start_beam_vector[i]/1000 - 1))*1000000000;
      long long end_search_nsec = ((long long)(global_start_beam_vector[i]/1000 + 1))*1000000000;
      int fill_number = 1000;
      cool::IObjectIteratorPtr objects = FillState_Folder->browseObjects( cool::ValidityKey(start_search_nsec),cool::ValidityKey(end_search_nsec), cool::ChannelSelection::all() );
      while ( objects->goToNext() ) {
        const cool::IObject& obj = objects->currentRef();
        std::string fillNumber = obj.payloadValue("FillNumber");
        fill_number = atoi(fillNumber.c_str());
      }
      fill_number_vector.push_back(intToString(fill_number));
      // if StableBeams flag was set at the beginning of the interval
      //   find real beam start time
      if ((beamType == 0) || (beamType == 3)) {
        if (global_start_beam_vector[i] == starttime/1000) {
          long long real_beam_start_time = get_beam_start_time(global_start_beam_vector[i]*1000000, fill_number, FillState_Folder);
          global_start_beam_vector[i] = real_beam_start_time/1000000;
//          OWLTime real_start_owl = OWLTime(real_beam_start_time/1000000000);
//          std::cout << " real_beam_start_time = " << real_start_owl.c_str() << std::endl;
        }
      }
      time_t sb_tt = time_t(global_start_beam_vector[i]/1000);
      OWLTime owlsb = OWLTime(sb_tt);
      beam_start_vector.push_back(owlsb.c_str());
      int beamDuration_sec = (global_end_beam_vector[i] - global_start_beam_vector[i])/1000;
      beam_duration_vector.push_back(intToString(beamDuration_sec));
      std::string runs = get_run_numbers(global_start_beam_vector[i], global_end_beam_vector[i]);
      beam_run_vector.push_back(runs);
      beam_delay_vector.push_back(intToString(delay_vector[i]));
      if ((beamType == 0) || (beamType == 3)) {
        double eff = getEfficiency(global_start_beam_vector[i]*1000, global_end_beam_vector[i]*1000, 0);
        eff_vector.push_back(doubleToString(eff));
        eff = getEfficiency(global_start_beam_vector[i]*1000, global_end_beam_vector[i]*1000, 1);
        eff_phys_vector.push_back(doubleToString(eff));
        runStopped_minutes = 0;
        endDelay_minutes = 0;
        startDelay_minutes = 0;
        std::string stopReason = getStopReason(global_start_beam_vector[i], global_end_beam_vector[i]);
        ready_delay_vector.push_back(doubleToString(startDelay_minutes));
        end_delay_vector.push_back(doubleToString(endDelay_minutes));
        run_stopped_vector.push_back(doubleToString(runStopped_minutes));
        stop_reason_vector.push_back(stopReason);
        // restore start_beam value for the interval
        global_start_beam_vector[i] = global_start_beam_tmp;
      }
    }
    beamTableRawNum = global_start_beam_vector.size() + 1;
    for (unsigned int i = 0; i < global_start_beam_vector.size(); i++ ) {
      std::pair<unsigned long, float> val1(global_start_beam_vector[i]/1000-1, 0.0);
      beamsGraph.push_back(val1);
      std::pair<unsigned long, float> val2(global_start_beam_vector[i]/1000, 1.0);
      beamsGraph.push_back(val2);
      std::pair<unsigned long, float> val3(global_end_beam_vector[i]/1000-1, 1.0);
      beamsGraph.push_back(val3);
      std::pair<unsigned long, float> val4(global_end_beam_vector[i]/1000, 0.0);
      beamsGraph.push_back(val4);
    }
  }
  if (sor_vector.size() > 0) {
    bool runOn = false;
    for (unsigned int i = 0; i < sor_vector.size(); i++ ) {
      OWLTime sTime(sor_vector[i].c_str());
      long st = sTime.c_time();
      if (st > starttime/1000000) {
        std::pair<unsigned long, float> val1(st-1, 0.0);
        runsGraph.push_back(val1);
        std::pair<unsigned long, float> val2(st, 1.0);
        runsGraph.push_back(val2);
      } else {
        std::pair<unsigned long, float> vals(starttime/1000000+1, 1.0);
        runsGraph.push_back(vals);
      }
      runOn = true;
      if (eor_vector[i].compare("") != 0) {
        OWLTime eTime(eor_vector[i].c_str());
        long et = eTime.c_time();
        std::pair<unsigned long, float> val3(et-1, 1.0);
        runsGraph.push_back(val3);
        std::pair<unsigned long, float> val4(et, 0.0);
        runsGraph.push_back(val4);
        runOn = false;
      }
    }
    if (runOn) {
      std::pair<unsigned long, float> val(endtime/1000000-1, 1.0);
      runsGraph.push_back(val);
    }
  }   
  std::pair<unsigned long, float> vale(endtime/1000000, 0.0);
  runsGraph.push_back(vale);
  beamsGraph.push_back(vale);  
  rateGraph.push_back(vale);
}

long long RunEffPlugin::checkRunInRNDB(std::string partitionName, int runNumber) {
  // returns -1 if it is not a run for this partition
  //          0 if it is an ongoing run for this partition
  //          run duration in microsec
  long long runDuration = -1;
  std::ostringstream stm;
  stm << runNumber;
  std::string run_number = stm.str();
  std::string command = "";
  if (runningOffline == 0) {
    if (beamType == 0) {
      command = "rn_ls -c \"oracle://atonr/rn_r\" -w atlas_run_number -p " + partitionName + " -n " + run_number + " -m " + run_number + " > /tmp/rnls0.out";
    }
    if (beamType == 1) {
      command = "rn_ls -c \"oracle://atonr/rn_r\" -w atlas_run_number -p " + partitionName + " -n " + run_number + " -m " + run_number + " > /tmp/rnls1.out";
    }
    if (beamType == 2) {
      command = "rn_ls -c \"oracle://atonr/rn_r\" -w atlas_run_number -p " + partitionName + " -n " + run_number + " -m " + run_number + " > /tmp/rnls2.out";
    }
    if (beamType == 3) {
      command = "rn_ls -c \"oracle://atonr/rn_r\" -w atlas_run_number -p " + partitionName + " -n " + run_number + " -m " + run_number + " > /tmp/rnls3.out";
    }
    if (beamType == 4) {
      command = "rn_ls -c \"oracle://atonr/rn_r\" -w atlas_run_number -p " + partitionName + " -n " + run_number + " -m " + run_number + " > /tmp/rnls4.out";
    }
  }
  if (runningOffline == 1) {
    if (beamType == 0) {
      command = "rn_ls -c \"oracle://atlr/rn_r\" -w atlas_run_number -p " + partitionName + " -n " + run_number + " -m " + run_number + " > /tmp/rnls0.out";
    }
    if (beamType == 1) {
      command = "rn_ls -c \"oracle://atlr/rn_r\" -w atlas_run_number -p " + partitionName + " -n " + run_number + " -m " + run_number + " > /tmp/rnls1.out";
    }
    if (beamType == 2) {
      command = "rn_ls -c \"oracle://atlr/rn_r\" -w atlas_run_number -p " + partitionName + " -n " + run_number + " -m " + run_number + " > /tmp/rnls2.out";
    }
    if (beamType == 3) {
      command = "rn_ls -c \"oracle://atlr/rn_r\" -w atlas_run_number -p " + partitionName + " -n " + run_number + " -m " + run_number + " > /tmp/rnls3.out";
    }
    if (beamType == 4) {
      command = "rn_ls -c \"oracle://atlr/rn_r\" -w atlas_run_number -p " + partitionName + " -n " + run_number + " -m " + run_number + " > /tmp/rnls4.out";
    }
  }
  system(command.c_str());
  std::filebuf fb;
  if (beamType == 0) {
    fb.open ("/tmp/rnls0.out",std::ios::in);
  }
if (beamType == 1) {
    fb.open ("/tmp/rnls1.out",std::ios::in);
  }
if (beamType == 2) {
    fb.open ("/tmp/rnls2.out",std::ios::in);
  }
  if (beamType == 3) {
    fb.open ("/tmp/rnls3.out",std::ios::in);
  }
  if (beamType == 4) {
    fb.open ("/tmp/rnls4.out",std::ios::in);
  }
  if (fb.is_open()) {
    std::istream is(&fb);
    std::string str;
    while (! is.eof()) {
      getline(is, str);
      if (str.find(partitionName) != std::string::npos) {
        std::string duration = str.substr(44, 8);
        if (duration.compare("        ") == 0) {
          runDuration = 0;
        } else {
          int hours = atoi((duration.substr(0, 2)).c_str());
          int minutes = atoi((duration.substr(3, 2)).c_str());
          int seconds = atoi((duration.substr(6, 2)).c_str());
          std::cout << "hours = " << hours << "  minutes = " << minutes << "  seconds = " << seconds << std::endl;
          int duration_sec = hours*3600 + minutes*60 + seconds;
          runDuration = duration_sec*1000000;
        }
      }
    }
  }
  fb.close();
  return runDuration;
}

int RunEffPlugin::get_ctpid(cool::ValidityKey sKey, cool::ValidityKey uKey, std::string itemName, cool::IFolderPtr & lvl1menu_folder) {
  int itemNo = -1;
  cool::IObjectIteratorPtr objects = lvl1menu_folder->browseObjects(sKey, uKey, cool::ChannelSelection::all());
  while ( objects->goToNext() ) {
    const cool::IObject& obj = objects->currentRef();
    std::string item_name = obj.payloadValue("ItemName");
    if (item_name.compare(itemName) == 0) {
      itemNo = obj.channelId();
      break;
    }
  }
  return itemNo;
}

int RunEffPlugin::ready_for_physics(cool::ValidityKey key, cool::IFolderPtr & datatakingmode_folder) {
  int flag = 1;
  cool::IObjectPtr obj = datatakingmode_folder->findObject(key, 0);
  std::string readyForPhysics = obj->payloadValue("ReadyForPhysics");
  flag = atoi(readyForPhysics.c_str());
  return flag;
}

std::string RunEffPlugin::get_run_numbers(long long starttime, long long endtime) {
  long long start_time_nsec = ((long long)(starttime))*1000000;
  long long end_time_nsec = ((long long)(endtime))*1000000;
  int current_run_number = 0;
  std::ostringstream strm;
  cool::IObjectIteratorPtr rl_objects = LBT_Folder->browseObjects(cool::ValidityKey(start_time_nsec), cool::ValidityKey(end_time_nsec), 0);
  while ( rl_objects->goToNext() ) {
    const cool::IObject& obj = rl_objects->currentRef();
    std::string runNumber = obj.payloadValue("Run");
    int run_nb = atoi(runNumber.c_str());
    if (run_nb != current_run_number) {
      strm << run_nb << " ";
      current_run_number = run_nb;
    }    
  }
  return strm.str();
}

void RunEffPlugin::set_beam_vectors(long long starttime, long long endtime) {
  if (beamType == 0) {
    // stable beams
    get_beams_data (starttime*1000, endtime*1000, FillState_Folder);
    set_main_vectors(beams_start_vector, beams_end_vector);
  }
  if (beamType == 1) {
    // two beams present
    get_beam1_data (starttime*1000, endtime*1000, TotalInt_Folder);
    get_beam2_data (starttime*1000, endtime*1000, TotalInt_Folder);
    set_beam1and2_vectors();
    if (noRamp == 0) {
      set_main_vectors(beam1and2_start_vector, beam1and2_end_vector);
    }
    if (noRamp == 1) {
      get_ramp_data (starttime*1000, endtime*1000, FillState_Folder);
      set_beam1and2noRamp_vectors();
      set_main_vectors(beam1and2noRamp_start_vector, beam1and2noRamp_end_vector);
    }
  }
  if (beamType == 2) {
    // one beam present
    get_beam1_data (starttime*1000, endtime*1000, TotalInt_Folder);
    get_beam2_data (starttime*1000, endtime*1000, TotalInt_Folder);
    set_beam1or2_vectors();
    if (noRamp == 0) {
      set_main_vectors(beam1or2_start_vector, beam1or2_end_vector);
    }
    if (noRamp == 1) {
      get_ramp_data (starttime*1000, endtime*1000, FillState_Folder);
      set_beam1or2noRamp_vectors();
      set_main_vectors(beam1or2noRamp_start_vector, beam1or2noRamp_end_vector);
    }
  }
  if (beamType == 3) {
    // two beams present and stable
    get_beam1_data (starttime*1000, endtime*1000, TotalInt_Folder);
    get_beam2_data (starttime*1000, endtime*1000, TotalInt_Folder);
    get_beams_data (starttime*1000, endtime*1000, FillState_Folder);
    set_beam1and2_vectors();
    set_beam1and2andStable_vectors();
    set_main_vectors(beam1and2andStable_start_vector, beam1and2andStable_end_vector);
  }
  if (beamType == 4) {
    // beam data from file
    get_beam_data_from_file (starttime, endtime);
  }
}

double RunEffPlugin::getEfficiency(long long starttime, long long endtime, int readyForPhysics) {
  // starttime in mksec
  int totalBeamTime = 0;
  int totalInBeamTime = 0;
  set_beam_vectors(starttime, endtime);
  totalBeamTime = get_total_beam_time();
 
  if (totalBeamTime > 0) {
    // read run data
    int totalInBeamTime_msec = 0;
    if (totalBeamTime > 0) {
      int no_data_time_in_beam_msec = 0;
      int current_run_number = 0;
      try {
        long long start_time_nsec = ((long long)(starttime))*1000;
        long long end_time_nsec = ((long long)(endtime))*1000;
        cool::IObjectIteratorPtr rl_objects = LBT_Folder->browseObjects(cool::ValidityKey(start_time_nsec), cool::ValidityKey(end_time_nsec), 0);
        int ctpId = 227; //default value, to be checked
        while ( rl_objects->goToNext() ) {
          const cool::IObject& obj = rl_objects->currentRef();
          std::string runNumber = obj.payloadValue("Run");
          std::string lumiBlock = obj.payloadValue("LumiBlock");
          int run_nb = atoi(runNumber.c_str());
          if (run_nb != current_run_number) {
            current_run_number = run_nb;
            cool::UInt63 a = run_nb;
            cool::UInt63 z = run_nb+1;
            cool::ValidityKey sKey = a <<32;
            cool::ValidityKey uKey = (z <<32) - 1;
            ctpId = get_ctpid(sKey, uKey, triggerItem, L1Menu_Folder);
            if (ctpId == -1) {
              std::cout << "For Run " << runNumber << " Trigger Item not found. Default value (227) will be used." << std::endl;
              ctpId = 227;
            }
          }
          cool::UInt63 rn = run_nb;
          cool::UInt63 lb = atoi(lumiBlock.c_str());
          cool::ValidityKey key1 = (rn<<32) + lb;
          cool::IObjectPtr obj_lb = LB_Folder->findObject(key1, 0);
          std::string st = obj_lb->payloadValue("StartTime");
          std::string et = obj_lb->payloadValue("EndTime");
          long long sT = atoll(st.c_str());
          long long eT = atoll(et.c_str());
          int readyForPhysicsFlag = 1;
          if (((beamType == 0) || (beamType == 3)) && (readyForPhysics == 1)) {
            readyForPhysicsFlag = ready_for_physics(key1, DataTakingMode_Folder);
          }
          double vetofraction = veto_fraction(key1, ctpId, L1Counters_Folder);
          if (readyForPhysicsFlag == 0) {
            vetofraction = 1;
          }
          if (start_beam_vector.size() > 0) {
            for (unsigned int i = 0; i < start_beam_vector.size(); i++ ) {
              int data_lumi_block_in_beam_msec = valInBeam(sT/1000000, eT/1000000, start_beam_vector[i], end_beam_vector[i]);
              if (data_lumi_block_in_beam_msec > 0) {
                totalInBeamTime_msec = totalInBeamTime_msec + data_lumi_block_in_beam_msec;
                int no_data_lumi_block_in_beam_msec = (int) (((double) data_lumi_block_in_beam_msec) * vetofraction);
                no_data_time_in_beam_msec = no_data_time_in_beam_msec + no_data_lumi_block_in_beam_msec;
              }
            }
          }
//          totalInBeamTime = (totalInBeamTime_msec - no_data_time_in_beam_msec)/1000;
          totalInBeamTime = totalInBeamTime_msec/1000 - no_data_time_in_beam_msec/1000;
        }
      } catch (std::exception &e) {
        std::cout << "Warning: Retrieving data failed for RN = " << current_run_number << "  -> " << e.what() << std::endl;
      }
    }
  }
  // evaluate efficiency in beam
  double beamEff = -0.2;
  if (totalBeamTime > 0) {
    beamEff = (double)totalInBeamTime/(double)totalBeamTime;
  }
  OWLTime current = OWLTime(endtime/1000000);
  std::cout << current.c_str() << "  " << beamEff << std::endl;
  return beamEff;
}
 
std::string RunEffPlugin::getStopReason(long long starttime, long long endtime) {
  // starttime in msec
  long long start_time_nsec = ((long long)(starttime))*1000000;
  long long end_time_nsec = ((long long)(endtime))*1000000;
  cool::IObjectIteratorPtr rl_objects = LBT_Folder->browseObjects(cool::ValidityKey(start_time_nsec), cool::ValidityKey(end_time_nsec), 0);
  long long start_of_first_LB_with_ready_sec = 0;
  long long end_of_last_LB_with_ready_sec = 0;
  bool firstRun = true;
  bool newRun = false;
  int current_run_number = 0;
  std::ostringstream reason;
  double end_of_last_run_sec = ((double) starttime)/1000;
  double runStopped_sec = 0;
  while ( rl_objects->goToNext() ) {
    const cool::IObject& obj = rl_objects->currentRef();
    std::string runNumber = obj.payloadValue("Run");
    std::string lumiBlock = obj.payloadValue("LumiBlock");
    int run_nb = atoi(runNumber.c_str());
    if (firstRun == true) {
      firstRun = false;
    }
    if (run_nb != current_run_number) {
      newRun = true;
      current_run_number = run_nb;
      end_of_last_LB_with_ready_sec = 0;
    }
    cool::UInt63 rn = run_nb;
    cool::UInt63 lb = atoi(lumiBlock.c_str());
    cool::ValidityKey key1 = (rn<<32) + lb;
    cool::IObjectPtr obj_lb = LB_Folder->findObject(key1, 0);
    std::string st = obj_lb->payloadValue("StartTime");
    std::string et = obj_lb->payloadValue("EndTime");
    long long sT = atoll(st.c_str());
    long long eT = atoll(et.c_str());
    if (newRun) {
      double start_of_new_run_sec = (double)sT/1000000000;
      if ((start_of_new_run_sec - end_of_last_run_sec) > 0) {
        runStopped_sec = runStopped_sec + (start_of_new_run_sec - end_of_last_run_sec);
        reason << get_stop_reason(((long long) end_of_last_run_sec)*1000, ((long long) start_of_new_run_sec)*1000) << " ";
      }
      newRun = false;
    }
    end_of_last_run_sec = (double)eT/1000000000;
    if (ready_for_physics(key1, DataTakingMode_Folder) == 1) {
      if (start_of_first_LB_with_ready_sec == 0) {
        start_of_first_LB_with_ready_sec = sT/1000000000;
      }
      end_of_last_LB_with_ready_sec = eT/1000000000;
    }
  }
  double delay_minutes = ((double) (endtime/1000 - starttime/1000))/60;
  if (start_of_first_LB_with_ready_sec > 0) {
    delay_minutes = ((double)(start_of_first_LB_with_ready_sec - (long long)(starttime/1000)))/60;
  }

  double end_delay_minutes = 0;
  if (end_of_last_LB_with_ready_sec > 0) {
    end_delay_minutes = ((double)(endtime/1000 - end_of_last_LB_with_ready_sec))/60;
    if (end_delay_minutes < 0) {
      end_delay_minutes = 0;
    }
  }

  runStopped_minutes = runStopped_sec/60;
  endDelay_minutes = end_delay_minutes;
  if (delay_minutes > 0) {
    startDelay_minutes = delay_minutes;
  } else {
    startDelay_minutes = 0;
  }
//  std::cout << "stopReason = " << reason.str() << std::endl;
  return reason.str();
}
  
std::string RunEffPlugin::get_stop_reason(long long starttime, long long endtime) {
  // starttime in msec
  long long start_time_nsec = ((long long)(starttime))*1000000;
  long long end_time_nsec = ((long long)(endtime))*1000000;
  cool::IObjectIteratorPtr sr_objects = StopReason_Folder->browseObjects(cool::ValidityKey(start_time_nsec), cool::ValidityKey(end_time_nsec), 0);
  std::ostringstream strm;
  while ( sr_objects->goToNext() ) {
    const cool::IObject& obj = sr_objects->currentRef();
    std::string subSystems = obj.payloadValue("SubSystems");
    std::string reason = obj.payloadValue("Reason");
    strm << subSystems << "/" << reason << ";";
  }
  return strm.str();
}

// find the real start time of StableBeams for the fill fillnumber
long long RunEffPlugin::get_beam_start_time(long long starttime, int fillnumber, cool::IFolderPtr & folder) {
  long long beam_start_time = 0;
  long long prestarttime = starttime - 35*24*3600*((long long)1000000000);
  cool::IObjectIteratorPtr objects = folder->browseObjects( cool::ValidityKey(prestarttime), cool::ValidityKey(starttime), cool::ChannelSelection::all() );
  while ( objects->goToNext() ) {
    const cool::IObject& obj = objects->currentRef();
    std::string fillNumber = obj.payloadValue("FillNumber");
    int fill_number = atoi(fillNumber.c_str());
    std::string beam = obj.payloadValue("StableBeams");
    if ((beam.compare("TRUE") == 0) && (fill_number == fillnumber)) {
      cool::ValidityKey key = obj.since();
      beam_start_time = key;
      break;
    }
  }
  return beam_start_time;
}

double RunEffPlugin::veto_fraction(cool::ValidityKey key, int ctpid, cool::IFolderPtr & l1counters_folder) {
  // for the moment overflow flags are not used
  cool::IObjectPtr obj_l1counters = l1counters_folder->findObject(key, ctpid);
  std::string l1Accept = obj_l1counters->payloadValue("L1Accept");
  std::string afterPrescale = obj_l1counters->payloadValue("AfterPrescale");
  long long l1_accept = atoll(l1Accept.c_str());
  long long after_prescale = atoll(afterPrescale.c_str());
  double fraction = 0;
  if (after_prescale > 0) {
    fraction = (double) (after_prescale - l1_accept) / (double) after_prescale;
  }
  return fraction;
}

double RunEffPlugin::fraction(long long startTime, long long endTime, cool::IFolderPtr & the_busy_folder ) {

  // Find all busy fraction values in this lumiblock:

  // Note: this also includes records that are partialy outside this lb

  no_busy_info = false;

  cool::IObjectIteratorPtr objects = the_busy_folder->browseObjects( cool::ValidityKey(startTime),cool::ValidityKey(endTime), 0 );
  double totaltime=0;
  double totalfraction=0;

  while ( objects->goToNext() ) {
    const cool::IObject& obj = objects->currentRef();

    std::string this_fraction_string= obj.payloadValue("ctpcore_moni0_rate");
    double this_fraction = atof(this_fraction_string.c_str());

    double this_starttime = obj.since();
    double this_stoptime = obj.until();
    totalfraction+=this_fraction*(this_stoptime-this_starttime);
    totaltime+=(this_stoptime-this_starttime);
  }

  if (totaltime>0) {
    totalfraction=0.01* totalfraction/totaltime;
  } else {
    no_busy_info = true;
//    totalfraction = 1; // if no info, let's just say we are busy
    totalfraction = 0; // if no info, let's just say we are not busy
  }

  return totalfraction;

}

double RunEffPlugin::busy(long long startTime, long long endTime, std::string identifier, cool::IFolderPtr & the_busy_folder ) {

  // Find the busy values for a specific detector in this lumiblock:

  // Note: this also includes records that are partialy outside this lb

  cool::IObjectIteratorPtr objects = the_busy_folder->browseObjects( cool::ValidityKey(startTime),cool::ValidityKey(endTime), 0 );
  double totaltime=0;
  double totalbusy=0;

  while ( objects->goToNext() ) {
    const cool::IObject& obj = objects->currentRef();

    std::string this_busy_string= obj.payloadValue(identifier.c_str());
    double this_busy = atof(this_busy_string.c_str());

    double this_starttime = obj.since();
    double this_stoptime = obj.until();
    totalbusy+=this_busy*(this_stoptime-this_starttime);
    totaltime+=(this_stoptime-this_starttime);
  }

  if (totaltime>0) {
    totalbusy = 0.01* totalbusy/totaltime;
  } else {
    totalbusy = 0; // if no info, let's just say this detector was not busy
  }

  return totalbusy;

}


int RunEffPlugin::valInBeam(long long startBlock, long long endBlock, long long startBeam, long long endBeam) {
  int result = 0;
  if ((startBlock <= startBeam) && (endBlock <= startBeam)) {
    // block before beam
    return result;
  }
  if ((startBlock <= startBeam) && (endBlock > startBeam) && (endBlock <= endBeam)) {
    result = endBlock - startBeam;
    return result;
  }
  if ((startBlock <= startBeam) && (endBlock >= endBeam)) {
    result = endBeam - startBeam;
    return result;
  }
  if ((startBlock >= startBeam) && (endBlock <= endBeam)) {
    result = endBlock - startBlock;
    return result;
  }
  if ((startBlock > startBeam) && (startBlock < endBeam) && (endBlock >= endBeam)) {
    result = endBeam - startBlock;
    return result;
  }
  if ((startBlock >= endBeam) && (endBlock > endBeam)) {
    // block after beam
    return result;
  }
  return result;
}

void RunEffPlugin::get_beam1_data(long long startTime, long long endTime, cool::IFolderPtr & folder) {
  beam1_start_vector.clear();
  beam1_end_vector.clear();
  cool::IObjectIteratorPtr objects = folder->browseObjects( cool::ValidityKey(startTime),cool::ValidityKey(endTime), cool::ChannelSelection::all());
  std::string old_beam = "FALSE";
  bool beamStarted = false;
  while ( objects->goToNext() ) {
    const cool::IObject& obj = objects->currentRef();
    std::string beam = obj.payloadValue("Beam1_BeamPresent");
    if (((beam.compare("TRUE") == 0) && (old_beam.compare("FALSE") == 0)) ||
        ((beam.compare("TRUE") == 0) && (old_beam.compare("NULL") == 0))) {
      // start of a new beam interval
      long long start_beam = (long long) (obj.since());
      if (start_beam < startTime) {
        start_beam = startTime;
      }
      beam1_start_vector.push_back(start_beam/1000000);
      old_beam = beam;
      beamStarted = true;
    } else if ((beam.compare("FALSE") == 0) && (old_beam.compare("TRUE") == 0)) {
      // end of a beam interval
      long long end_beam  = (long long) (obj.since());
      beam1_end_vector.push_back(end_beam/1000000);
      beamStarted = false;
      old_beam = beam;
    } else {
      old_beam = beam;
    }
  }
  if (beamStarted) {
    beam1_end_vector.push_back(endTime/1000000);
  }
}

void RunEffPlugin::get_beam2_data(long long startTime, long long endTime, cool::IFolderPtr & folder) {
  beam2_start_vector.clear();
  beam2_end_vector.clear();
  cool::IObjectIteratorPtr objects = folder->browseObjects( cool::ValidityKey(startTime),cool::ValidityKey(endTime), cool::ChannelSelection::all());
  std::string old_beam = "FALSE";
  bool beamStarted = false;
  while ( objects->goToNext() ) {
    const cool::IObject& obj = objects->currentRef();
    std::string beam = obj.payloadValue("Beam2_BeamPresent");
    if (((beam.compare("TRUE") == 0) && (old_beam.compare("FALSE") == 0)) ||
        ((beam.compare("TRUE") == 0) && (old_beam.compare("NULL") == 0))) {
      // start of a new beam interval
      long long start_beam = (long long) (obj.since());
      if (start_beam < startTime) {
        start_beam = startTime;
      }
      beam2_start_vector.push_back(start_beam/1000000);
      old_beam = beam;
      beamStarted = true;
    } else if ((beam.compare("FALSE") == 0) && (old_beam.compare("TRUE") == 0)) {
      // end of a beam interval
      long long end_beam  = (long long) (obj.since());
      beam2_end_vector.push_back(end_beam/1000000);
      beamStarted = false;
      old_beam = beam;
    } else {
      old_beam = beam;
    }
  }
  if (beamStarted) {
    beam2_end_vector.push_back(endTime/1000000);
  }
}

void RunEffPlugin::get_ramp_data(long long startTime, long long endTime, cool::IFolderPtr & folder) {
  // in fact the vectors contain NO RAMP UP intervals
  ramp_start_vector.clear();
  ramp_end_vector.clear();
  cool::IObjectIteratorPtr objects = folder->browseObjects( cool::ValidityKey(startTime),cool::ValidityKey(endTime), cool::ChannelSelection::all());
  std::string old_beam_mode = "RAMP";
  bool rampStarted = false;
  while ( objects->goToNext() ) {
    const cool::IObject& obj = objects->currentRef();
    std::string beam_mode = obj.payloadValue("BeamMode");
    // use the same conditions as Run Control to hold triggers
    // other conditions could be derived from the LHC state machine:
    //    INJECTION PROBE BEAM (?) -> PREPARE RAMP -> RAMP
    //    PREPARE RAMP -> BEAM DUMP
    //    RAMP -> BEAM DUMP
    //    RAMP -> FLAT TOP
    if (((beam_mode.compare("PREPARE RAMP") != 0) && (beam_mode.compare("RAMP") != 0)) && ((old_beam_mode.compare("PREPARE RAMP") == 0) || (old_beam_mode.compare("RAMP") == 0))) {
      // start of a new no ramp interval
      long long start_ramp = (long long) (obj.since());
      if (start_ramp < startTime) {
        start_ramp = startTime;
      }
      ramp_start_vector.push_back(start_ramp/1000000);
      old_beam_mode = beam_mode;
      rampStarted = true;
    } else if (((beam_mode.compare("PREPARE RAMP") == 0) || (beam_mode.compare("RAMP") == 0)) && ((old_beam_mode.compare("PREPARE RAMP") != 0) && (old_beam_mode.compare("RAMP") != 0))) {
      // end of a no ramp interval
      long long end_ramp  = (long long) (obj.since());
      ramp_end_vector.push_back(end_ramp/1000000);
      rampStarted = false;
      old_beam_mode = beam_mode;
    } else {
      old_beam_mode = beam_mode;
    }
  }
  if (rampStarted) {
    ramp_end_vector.push_back(endTime/1000000);
  }
}

void RunEffPlugin::get_beams_data(long long startTime, long long endTime, cool::IFolderPtr & folder) {
  beams_start_vector.clear();
  beams_end_vector.clear();
  cool::IObjectIteratorPtr objects = folder->browseObjects( cool::ValidityKey(startTime),cool::ValidityKey(endTime), cool::ChannelSelection::all());
  std::string old_beam = "FALSE";   bool beamStarted = false;
  while ( objects->goToNext() ) {
    const cool::IObject& obj = objects->currentRef();
    std::string beam = obj.payloadValue("StableBeams");
    if (((beam.compare("TRUE") == 0) && (old_beam.compare("FALSE") == 0)) ||
        ((beam.compare("TRUE") == 0) && (old_beam.compare("NULL") == 0))) {
      // start of a new beam interval
      long long start_beam = (long long) (obj.since());
      if (start_beam < startTime) {
        start_beam = startTime;
      }
      beams_start_vector.push_back(start_beam/1000000);
      old_beam = beam;
      beamStarted = true;
    } else if ((beam.compare("FALSE") == 0) && (old_beam.compare("TRUE") == 0)) {
      // end of a beam interval
      long long end_beam  = (long long) (obj.since());
      beams_end_vector.push_back(end_beam/1000000);
      beamStarted = false;
      old_beam = beam;
    } else {
      old_beam = beam;
    }
  }
  if (beamStarted) {
    beams_end_vector.push_back(endTime/1000000);
  }
}

void RunEffPlugin::get_beam_data_from_file(long long starttime, long long endtime) {
  std::filebuf fb;
  fb.open (beamDataFile.c_str(),std::ios::in);
  if (fb.is_open()) {
    std::istream is(&fb);
    std::string str;
    long long bLimit = ((long long) beamLimit) * ((long long) 1000000);
    while (!(str.compare("endbeam") == 0)) {
      getline(is, str);
      if (!(str.compare("endbeam") == 0)) {
        OWLTime startBeam(str.substr(0, 17).c_str());
        long long startbeam = startBeam.total_mksec_utc();
        OWLTime endBeam(str.substr(19, 17).c_str());
        long long endbeam = endBeam.total_mksec_utc();
        if ((endbeam - startbeam) < bLimit) {
//           std::cout << "short beam" << std::endl;
           continue;
        }
        if ((startbeam >= starttime) && (endbeam <= endtime)) {
          start_beam_vector.push_back(startbeam/1000);
          end_beam_vector.push_back(endbeam/1000);
        }
        if ((startbeam < starttime) && (endbeam > starttime) && (endbeam <= endtime)) {
          start_beam_vector.push_back(starttime/1000);
          end_beam_vector.push_back(endbeam/1000);
        }
        if ((startbeam < starttime) && (endbeam > endtime)) {
          start_beam_vector.push_back(starttime/1000);
          end_beam_vector.push_back(endtime/1000);
        }
        if ((startbeam >= starttime) && (startbeam < endtime) && (endbeam > endtime)) {
          start_beam_vector.push_back(startbeam/1000);
          end_beam_vector.push_back(endtime/1000);
        }
      }
    }
  } else {
    std::cout << "Warning: cannot open the beam data file" << std::endl;
  }
  fb.close();
}

void RunEffPlugin::print_vectors(std::vector<long long> start_vector, std::vector<long long> end_vector, std::string file_name) {
  std::ostream * m_lout = new std::ofstream (file_name.c_str(),std::ios::out | std::ios::trunc);
  if (!(*m_lout)) {
    std::cout << "Can't create the " << file_name << " log file " << std::endl;
    return;
  }
  std::ostringstream strm;
  if (start_vector.size() > 0) {
    for (unsigned int i = 0; i < start_vector.size(); i++ ) {
      long long start_beam_sec = start_vector[i]/1000;
      OWLTime start_owl = OWLTime(start_beam_sec);
      std::string start_string = std::string(start_owl.c_str());
      if (start_string.size() == 15) {
        strm << "  " << start_string;
      } else if (start_string.size() == 16) {
        strm << " " << start_string;
      } else {
        strm << start_string;
      }
      long long end_beam_sec = end_vector[i]/1000;
      OWLTime end_owl = OWLTime(end_beam_sec);
      std::string end_string = std::string(end_owl.c_str());
      strm << "  " << end_string << std::endl;
    }
    (*m_lout) << strm.str();
  }
  (*m_lout) << "endbeam" << std::endl;
  delete m_lout;
}

void RunEffPlugin::set_main_vectors(std::vector<long long> start_vector, std::vector<long long> end_vector) {
  start_beam_vector.clear();
  end_beam_vector.clear();
  long long bLimit = ((long long) beamLimit) * ((long long) 1000);
  if (start_vector.size() > 0) {
    for (unsigned int i = 0; i < start_vector.size(); i++ ) {
      if ((end_vector[i] - start_vector[i])  < bLimit) {
//        std::cout << "short beam" << std::endl;
         continue;
      }
      start_beam_vector.push_back(start_vector[i]);
      end_beam_vector.push_back(end_vector[i]);
    }
  }
}

int RunEffPlugin::get_total_beam_time() {
  int total_beam_time = 0;
  int valbeam = 0;
  if (start_beam_vector.size() > 0) {
    for (unsigned int i = 0; i < start_beam_vector.size(); i++ ) {
      valbeam = end_beam_vector[i]/1000 - start_beam_vector[i]/1000;
      total_beam_time = total_beam_time + valbeam;
    }
  }
  return total_beam_time;
}

void RunEffPlugin::set_beam1and2_vectors() {
  beam1and2_start_vector.clear();
  beam1and2_end_vector.clear();
  // set vectors
  if (beam1_start_vector.size() > 0) {
    for (unsigned int i = 0; i < beam1_start_vector.size(); i++ ) {
      if (beam2_start_vector.size() > 0) {
        for (unsigned int j = 0; j < beam2_start_vector.size(); j++ ) {
          if ((beam1_start_vector[i] <= beam2_start_vector[j]) &&
              (beam1_end_vector[i] > beam2_start_vector[j]) &&
              (beam1_end_vector[i] <= beam2_end_vector[j])) {
            beam1and2_start_vector.push_back(beam2_start_vector[j]);
            beam1and2_end_vector.push_back(beam1_end_vector[i]);
          } else {
            if ((beam1_start_vector[i] <= beam2_start_vector[j]) &&
                     (beam2_end_vector[j] <= beam1_end_vector[i])) {
              beam1and2_start_vector.push_back(beam2_start_vector[j]);
              beam1and2_end_vector.push_back(beam2_end_vector[j]);
            } else {
              if ((beam2_start_vector[j] <= beam1_start_vector[i]) &&
                   (beam1_start_vector[i] < beam2_end_vector[j]) &&
                   (beam2_end_vector[j] <= beam1_end_vector[i])) {
                 beam1and2_start_vector.push_back(beam1_start_vector[i]);
                 beam1and2_end_vector.push_back(beam2_end_vector[j]);
              } else {
                if ((beam2_start_vector[j] < beam1_start_vector[i]) &&
                    (beam1_end_vector[i] < beam2_end_vector[j])) {
                  beam1and2_start_vector.push_back(beam1_start_vector[i]);
                  beam1and2_end_vector.push_back(beam1_end_vector[i]);
                }
              }
            }
          }
        }
      }
    }
  }
}

bool RunEffPlugin::checkInBeam(long long time) {
  if (beam1_start_vector.size() > 0) {
    for (unsigned int i = 0; i < beam1_start_vector.size(); i++) {
      if ((beam1_start_vector[i] <= time) &&
          (time <= beam1_end_vector[i])) {
        return true;
      }
    }
  }
  if (beam2_start_vector.size() > 0) {
    for (unsigned int i = 0; i < beam2_start_vector.size(); i++) {
      if ((beam2_start_vector[i] <= time) &&
          (time <= beam2_end_vector[i])) {
        return true;
      }
    }
  }
  return false;
}

void RunEffPlugin::set_beam1or2_vectors() {
  beam1or2_start_vector.clear();
  beam1or2_end_vector.clear();
  long long starttime;
  long long endtime;
  int beam1_max = beam1_start_vector.size();
  int beam2_max = beam2_start_vector.size();
  if ((beam1_max > 0) && (beam2_max > 0)) {
    if (beam1_start_vector[0] <= beam2_start_vector[0]) {
      starttime = beam1_start_vector[0];
    } else {
      starttime = beam2_start_vector[0];
    }
    if (beam1_end_vector[beam1_max-1] <= beam2_end_vector[beam2_max-1]) {
      endtime = beam2_end_vector[beam2_max-1] + 1500;
    } else {
      endtime = beam1_end_vector[beam1_max-1] + 1500;
    }
  } else {
    if (beam1_max > 0) {
      starttime = beam1_start_vector[0];
      endtime = beam1_end_vector[beam1_max-1] + 1500;
    } else {
      if (beam2_max > 0) {
        starttime = beam2_start_vector[0];
        endtime = beam2_end_vector[beam2_max-1] + 1500;
      } else {
//        std::cout << "No beam 1 or beam 2 data" << std::endl;
        return;
      }
    }
  }
  long long currenttime = starttime;
  bool oldInBeam = false;
  int i = 0;
  while (currenttime < endtime) {
    bool inBeam = checkInBeam(currenttime);
    if ((inBeam == true) && (oldInBeam == false)) {
      beam1or2_start_vector.push_back(currenttime);
      oldInBeam = inBeam;
    } else {
      if ((inBeam == false) && (oldInBeam == true)) {
        beam1or2_end_vector.push_back(currenttime - 1000);
        oldInBeam = inBeam;
        i = i + 1;
      } else {
        oldInBeam = inBeam;
      }
    }
    // check every second
    currenttime = currenttime + 1000;
  }
}

void RunEffPlugin::set_beam1and2andStable_vectors() {
  beam1and2andStable_start_vector.clear();
  beam1and2andStable_end_vector.clear();
  // set vectors
  if (beams_start_vector.size() > 0) {
    for (unsigned int i = 0; i < beams_start_vector.size(); i++ ) {
      if (beam1and2_start_vector.size() > 0) {
        for (unsigned int j = 0; j < beam1and2_start_vector.size(); j++ ) {
          if ((beams_start_vector[i] <= beam1and2_start_vector[j]) &&
              (beams_end_vector[i] > beam1and2_start_vector[j]) &&
              (beams_end_vector[i] <= beam1and2_end_vector[j])) {
            beam1and2andStable_start_vector.push_back(beam1and2_start_vector[j]);
            beam1and2andStable_end_vector.push_back(beams_end_vector[i]);
          } else {
            if ((beams_start_vector[i] <= beam1and2_start_vector[j]) &&
                (beam1and2_end_vector[j] <= beams_end_vector[i])) {
              beam1and2andStable_start_vector.push_back(beam1and2_start_vector[j]);
              beam1and2andStable_end_vector.push_back(beam1and2_end_vector[j]);
            } else {
              if ((beam1and2_start_vector[j] <= beams_start_vector[i]) &&
                  (beams_start_vector[i] < beam1and2_end_vector[j]) &&
                  (beam1and2_end_vector[j] <= beams_end_vector[i])) {
                 beam1and2andStable_start_vector.push_back(beams_start_vector[i]);
                 beam1and2andStable_end_vector.push_back(beam1and2_end_vector[j]);
              } else {
                if ((beam1and2_start_vector[j] < beams_start_vector[i]) &&
                    (beams_end_vector[i] < beam1and2_end_vector[j])) {
                  beam1and2andStable_start_vector.push_back(beams_start_vector[i]);
                  beam1and2andStable_end_vector.push_back(beams_end_vector[i]);
                }
              }
            }
          }
        }
      }
    }
  }
}

void RunEffPlugin::set_beam1or2noRamp_vectors() {
  beam1or2noRamp_start_vector.clear();
  beam1or2noRamp_end_vector.clear();
  // set vectors
  if (beam1or2_start_vector.size() > 0) {
    for (unsigned int i = 0; i < beam1or2_start_vector.size(); i++ ) {
      if (ramp_start_vector.size() > 0) {
        for (unsigned int j = 0; j < ramp_start_vector.size(); j++ ) {
          if ((beam1or2_start_vector[i] <= ramp_start_vector[j]) &&
              (beam1or2_end_vector[i] > ramp_start_vector[j]) &&
              (beam1or2_end_vector[i] <= ramp_end_vector[j])) {
            beam1or2noRamp_start_vector.push_back(ramp_start_vector[j]);
            beam1or2noRamp_end_vector.push_back(beam1or2_end_vector[i]);
          } else {
            if ((beam1or2_start_vector[i] <= ramp_start_vector[j]) &&
                (ramp_end_vector[j] <= beam1or2_end_vector[i])) {
              beam1or2noRamp_start_vector.push_back(ramp_start_vector[j]);
              beam1or2noRamp_end_vector.push_back(ramp_end_vector[j]);
            } else {
              if ((ramp_start_vector[j] <= beam1or2_start_vector[i]) &&
                  (beam1or2_start_vector[i] < ramp_end_vector[j]) &&
                  (ramp_end_vector[j] <= beam1or2_end_vector[i])) {
                 beam1or2noRamp_start_vector.push_back(beam1or2_start_vector[i]);
                 beam1or2noRamp_end_vector.push_back(ramp_end_vector[j]);
              } else {
                if ((ramp_start_vector[j] < beam1or2_start_vector[i]) &&
                    (beam1or2_end_vector[i] < ramp_end_vector[j])) {
                  beam1or2noRamp_start_vector.push_back(beam1or2_start_vector[i]);
                  beam1or2noRamp_end_vector.push_back(beam1or2_end_vector[i]);
                }
              }
            }
          }
        }
      }
    }
  }
}

void RunEffPlugin::set_beam1and2noRamp_vectors() {
  beam1and2noRamp_start_vector.clear();
  beam1and2noRamp_end_vector.clear();
  // set vectors
  if (beam1and2_start_vector.size() > 0) {
    for (unsigned int i = 0; i < beam1and2_start_vector.size(); i++ ) {
      if (ramp_start_vector.size() > 0) {
        for (unsigned int j = 0; j < ramp_start_vector.size(); j++ ) {
          if ((beam1and2_start_vector[i] <= ramp_start_vector[j]) &&
              (beam1and2_end_vector[i] > ramp_start_vector[j]) &&
              (beam1and2_end_vector[i] <= ramp_end_vector[j])) {
            beam1and2noRamp_start_vector.push_back(ramp_start_vector[j]);
            beam1and2noRamp_end_vector.push_back(beam1and2_end_vector[i]);
          } else {
            if ((beam1and2_start_vector[i] <= ramp_start_vector[j]) &&
                (ramp_end_vector[j] <= beam1and2_end_vector[i])) {
              beam1and2noRamp_start_vector.push_back(ramp_start_vector[j]);
              beam1and2noRamp_end_vector.push_back(ramp_end_vector[j]);
            } else {
              if ((ramp_start_vector[j] <= beam1and2_start_vector[i]) &&
                  (beam1and2_start_vector[i] < ramp_end_vector[j]) &&
                  (ramp_end_vector[j] <= beam1and2_end_vector[i])) {
                 beam1and2noRamp_start_vector.push_back(beam1and2_start_vector[i]);
                 beam1and2noRamp_end_vector.push_back(ramp_end_vector[j]);
              } else {
                if ((ramp_start_vector[j] < beam1and2_start_vector[i]) &&
                    (beam1and2_end_vector[i] < ramp_end_vector[j])) {
                  beam1and2noRamp_start_vector.push_back(beam1and2_start_vector[i]);
                  beam1and2noRamp_end_vector.push_back(beam1and2_end_vector[i]);                }
              }
            }
          }
        }
      }
    }
  }
}

    void
    RunEffPlugin::buildTables() 
      throw()
    {

      int fileIndex = m_outStream->open();
 
      Link RunEffStableLink("../Run_Eff_wmi/", "Run efficiency for stable beams");
      Link RunEffAndLink("../Run_Eff_B1andB2_wmi/", "Run efficiency for both beams present");
      Link RunEffOrLink("../Run_Eff_B1orB2_wmi/", "Run efficiency for at least one beam present");
      if ((beamType == 0) || (beamType == 3)) {
        m_outStream->write(RunEffAndLink);
        m_outStream->write(this->br);
        m_outStream->write(RunEffOrLink);
        m_outStream->write(this->br);
      }
      if (beamType == 1) {
        m_outStream->write(RunEffStableLink, fileIndex);
        m_outStream->write(this->br);
        m_outStream->write(RunEffOrLink);
        m_outStream->write(this->br);
      }
      if (beamType == 2) {
        m_outStream->write(RunEffStableLink, fileIndex);
        m_outStream->write(this->br);
        m_outStream->write(RunEffAndLink);
        m_outStream->write(this->br);
      }
      m_outStream->write(this->hr, fileIndex);

      Text plotsTitle("Plots", 5, DARK_BLUE, TextOption("b"));
      m_outStream->write(plotsTitle);

      std::unique_ptr<Table> tb3(new Table(1, 1));
      tb3->setBGColor(LIGHT_GREY);
      setTableColors(*tb3, 1, 1, true, true);
      printEffGraph(*tb3);
      m_outStream->write(*(tb3.get()));
      this->buildBeamsPlot();
      this->buildRunsPlot();
      this->buildRatePlot();
      
      // Write table title
      Text runTableTitle("Run Table", 5, DARK_BLUE, TextOption("b"));
      m_outStream->write(runTableTitle, fileIndex);
     
   
      // Main table
      const std::unique_ptr<Table> tb(this->createMainTable());

      m_outStream->write(*(tb.get()), fileIndex);
      m_outStream->write(this->hr, fileIndex);

      static const std::string AvBeam("Beam Table");
      Text AvBeamTx(AvBeam, 5, DARK_BLUE, TextOption("b"));
      m_outStream->write(AvBeamTx, fileIndex);

      const std::unique_ptr<Table> tb2(this->createBeamTable());
      m_outStream->write(*(tb2.get()), fileIndex);
      m_outStream->write(this->hr, fileIndex);
      m_outStream->close(fileIndex);
    }

    void
    RunEffPlugin::buildRunsPlot() throw()
    {
      std::unique_ptr<Table> tb2(new Table(1, 1));
      tb2->setBGColor(LIGHT_GREY);
      setTableColors(*tb2, 1, 1, true, true);
      printRunsGraph(*tb2);
      m_outStream->write(*(tb2.get()));
    }
    
    void
    RunEffPlugin::buildBeamsPlot() throw()
    {
      std::unique_ptr<Table> tb(new Table(1, 1));
      tb->setBGColor(LIGHT_GREY);
      setTableColors(*tb, 1, 1, true, true);
      printBeamsGraph(*tb);
      m_outStream->write(*(tb.get()));
    }

    void
    RunEffPlugin::buildRatePlot() throw()
    {
      std::unique_ptr<Table> tb(new Table(1, 1));
      tb->setBGColor(LIGHT_GREY);
      setTableColors(*tb, 1, 1, true, true);
      printRateGraph(*tb);
      m_outStream->write(*(tb.get()));
    }

    Table*
    RunEffPlugin::createBeamTable() const
      throw()
    {
      Table* tb = new Table(beamTableColNum, beamTableRawNum);
      setTableColors(*tb, (int)beamTableColNum, beamTableRawNum, false, true);
      tb->setBorder(3);

      for (int i = 1; i < beamTableRawNum-1; i++)
         tb->setAlignCell(i, 1, 2);

    
      fillColorCell(*tb, 1, 1, RunEffPlugin::FillNumberLabel, "", 3, WHITE, "b");
      fillColorCell(*tb, 2, 1, RunEffPlugin::BeamStartLabel, "", 3, WHITE, "b");
      fillColorCell(*tb, 3, 1, RunEffPlugin::BeamDurationLabel, "", 3, WHITE, "b");
      fillColorCell(*tb, 4, 1, RunEffPlugin::RunLabel, "", 3, WHITE, "b");
      if ((beamType == 0) || (beamType == 3)) {
        fillColorCell(*tb, 5, 1, RunEffPlugin::EffLabel, "", 3, WHITE, "b");
        fillColorCell(*tb, 6, 1, RunEffPlugin::EffPhysLabel, "", 3, WHITE, "b");
        fillColorCell(*tb, 7, 1, RunEffPlugin::ReadyDelayLabel, "", 3, WHITE, "b");
        fillColorCell(*tb, 8, 1, RunEffPlugin::EndDelayLabel, "", 3, WHITE, "b");
        fillColorCell(*tb, 9, 1, RunEffPlugin::RunStoppedLabel, "", 3, WHITE, "b");
        fillColorCell(*tb, 10, 1, RunEffPlugin::StopReasonLabel, "", 3, WHITE, "b");
      } else {
        fillColorCell(*tb, 5, 1, RunEffPlugin::DelayLabel, "", 3, WHITE, "b");
      }
      for (int i = 0; i < beamTableRawNum-1; i++) {

        fillColorCell(*tb, 1, i+2, fill_number_vector[i], "", 3, BLACK, "b");
        fillColorCell(*tb, 2, i+2, beam_start_vector[i], "", 3, BLACK, "b");
        fillColorCell(*tb, 3, i+2, beam_duration_vector[i], "", 3, BLACK, "b");
        fillColorCell(*tb, 4, i+2, beam_run_vector[i], "", 3, BLACK, "b");
        if ((beamType == 0) || (beamType == 3)) {
          fillColorCell(*tb, 5, i+2, eff_vector[i], "", 3, BLACK, "b");
          fillColorCell(*tb, 6, i+2, eff_phys_vector[i], "", 3, BLACK, "b");
          fillColorCell(*tb, 7, i+2, ready_delay_vector[i], "", 3, BLACK, "b");
          fillColorCell(*tb, 8, i+2, end_delay_vector[i], "", 3, BLACK, "b");
          fillColorCell(*tb, 9, i+2, run_stopped_vector[i], "", 3, BLACK, "b");
          fillColorCell(*tb, 10, i+2, stop_reason_vector[i], "", 3, BLACK, "b");
        } else {
          fillColorCell(*tb, 5, i+2, beam_delay_vector[i], "", 3, BLACK, "b");
        }
      }
      return tb;
    }

    void
    RunEffPlugin::buildRunsPages() const
      throw()
    {
      std::map<std::string, RunData*>::const_iterator mapIt; 
      std::map<std::string, RunData*>::const_iterator mapItBegin(this->runsMap.begin());
      std::map<std::string, RunData*>::const_iterator mapItEnd(this->runsMap.end());
      for(mapIt = mapItBegin; mapIt != mapItEnd; ++mapIt) {
        const std::string runPageName(mapIt->first + ".html");

        // Open the run page
        int fileIndex = m_outStream->open(runPageName);
        
        // Write page title
        const std::string Run("Run " + mapIt->first);
        Text runTitle(Run, 5, DARK_BLUE, TextOption("b"));
        m_outStream->write(runTitle, fileIndex);

        // lvl1 rate plot
        std::unique_ptr<Table> tb(new Table(1, 1));
        tb->setBGColor(LIGHT_GREY);
        setTableColors(*tb, 1, 1, true, true);
        static std::string titleRate("LVL1 Rate (Hz)");

        const unsigned int rWidth = 800;
        const unsigned int rHeight = 200;

        tb->setWidth(rWidth);
        tb->setTDWidth(1, rWidth);

        tb->setAlignCell(1,1,2);
        tb->addElement(1, 1, new Text(titleRate, 3, WHITE, TextOption("b")));
        tb->addRow();
        tb->addElement(1, 2, new TimeGraph("", "", (mapIt->second)->l1Graph, rWidth, rHeight));

        m_outStream->write(*(tb.get()), fileIndex);

        if ((mapIt->second)->lb_vector.size() > 0) {
          int busyTableRawNum = (mapIt->second)->lb_vector.size() + 1;
          // busy table
          Table* tbb = new Table(RunEffPlugin::busyTableColNum, busyTableRawNum);
          setTableColors(*tbb, (int)RunEffPlugin::busyTableColNum, busyTableRawNum, false, true);
          tbb->setBorder(3);
          for (int i = 1; i < busyTableRawNum-1; i++) {
            tbb->setAlignCell(i, 1, 2);
          }
          fillColorCell(*tbb, 1, 1, RunEffPlugin::LumiBlockLabel, "", 3, WHITE, "b");
          fillColorCell(*tbb, 2, 1, RunEffPlugin::StartTimeLabel, "", 3, WHITE, "b");
          fillColorCell(*tbb, 3, 1, RunEffPlugin::BusyFractionLabel, "", 3, WHITE, "b");
          fillColorCell(*tbb, 4, 1, RunEffPlugin::BusySourceLabel, "", 3, WHITE, "b");
          for (int i = 0; i < busyTableRawNum-1; i++) {
            fillColorCell(*tbb, 1, i+2, (mapIt->second)->lb_vector[i], "", 3, BLACK, "b");
            fillColorCell(*tbb, 2, i+2, (mapIt->second)->solb_vector[i], "", 3, BLACK, "b");
            fillColorCell(*tbb, 3, i+2, (mapIt->second)->busy_fraction_vector[i], "", 3, BLACK, "b");
            fillColorCell(*tbb, 4, i+2, (mapIt->second)->busy_vector[i], "", 3, BLACK, "b");
          }
          Text busyTableTitle("Busy Table", 5, DARK_BLUE, TextOption("b"));
          m_outStream->write(busyTableTitle, fileIndex);
          const std::unique_ptr<Table> tbbp(tbb);
          m_outStream->write(*(tbbp.get()), fileIndex);
        }
        m_outStream->close(fileIndex);
      }
    } 

    Table*
    RunEffPlugin::createMainTable() const
      throw()
    {
        Table* tb = new Table(RunEffPlugin::generalTableColNum, generalTableRawNum);
        setTableColors(*tb, (int)RunEffPlugin::generalTableColNum, generalTableRawNum, false, true);
        tb->setBorder(3);

        for (int i = 1; i < generalTableRawNum-1; i++)
                tb->setAlignCell(i, 1, 2);

        fillColorCell(*tb, 1, 1, RunEffPlugin::RunNumberLabel, "", 3, WHITE, "b");
        fillColorCell(*tb, 2, 1, RunEffPlugin::NumberEventsLabel, "", 3, WHITE, "b");
        fillColorCell(*tb, 3, 1, RunEffPlugin::RunStartTimeLabel, "", 3, WHITE, "b");
        fillColorCell(*tb, 4, 1, RunEffPlugin::RunStopTimeLabel, "", 3, WHITE, "b");
        fillColorCell(*tb, 5, 1, RunEffPlugin::RunDurationLabel, "", 3, WHITE, "b");
        fillColorCell(*tb, 6, 1, RunEffPlugin::LumiBlocksLabel, "", 3, WHITE, "b");
        fillColorCell(*tb, 7, 1, RunEffPlugin::LowRateLumiBlocksLabel, "", 3, WHITE, "b");
        fillColorCell(*tb, 8, 1, RunEffPlugin::HighBusyLumiBlocksLabel, "", 3, WHITE, "b");
        for (int i = 0; i < generalTableRawNum-1; i++) {
           Link* lk = new Link((rn_vector[i] + ".html"), rn_vector[i]);
           tb->addElement(1, i+2, lk);
           fillColorCell(*tb, 2, i+2, ev_vector[i], "", 3, BLACK, "b");
           fillColorCell(*tb, 3, i+2, sor_vector[i], "", 3, BLACK, "b");
           fillColorCell(*tb, 4, i+2, eor_vector[i], "", 3, BLACK, "b");
           fillColorCell(*tb, 5, i+2, run_duration_vector[i], "", 3, BLACK, "b");
           fillColorCell(*tb, 6, i+2, lb_vector[i], "", 3, BLACK, "b");
           fillColorCell(*tb, 7, i+2, lrlb_vector[i], "", 3, BLACK, "b");
           fillColorCell(*tb, 8, i+2, hblb_vector[i], "", 3, BLACK, "b");
        }
        return tb;
    }

     /**
     * The background colors of the cells of the table are set according to the rules:
     *   1. if the table has a header (hasLabelHeader) the first line has the background color = CAPTION_BG (dark blue)
     *   2. the cells containing information have background color = WHITE
     *   3. the cells containing labels have background color = CELL_BG (light cyan)
     *   4. label vs information cells: hasLabelColumn = true means that it is expected that the odd columns of the table contain labels (painted in light cyan) and the even columns contain values (painted in white).
     *   5. Any other particular formats need to be treated locally in order to set the background color of the cells
     */

    void RunEffPlugin::setTableColors(Table& tb, int colNo, int lineNo, bool hasLabelColumn, bool hasLabelHeader)
       const throw()
   {
        int i, j, start_line;
        if (hasLabelHeader)
                start_line = 2;
        else
                start_line = 1;

        for (i = 2; i <= colNo; i += 2)
                for (j = start_line; j <= lineNo; j++)
                        tb.setBGColorCell(i, j, WHITE);

        for (i = 1; i <= colNo; i++)
                for (j = 1; j < start_line; j++)
                        tb.setBGColorCell(i, j, CAPTION_BG);


        if (hasLabelColumn)
                for (i = 1; i <= colNo; i += 2)
                        for (j = start_line; j <= lineNo; j++)
                                tb.setBGColorCell(i, j, CELL_BG);
        else
                for (i = 1; i <= colNo; i += 2)
                        for (j = start_line; j <= lineNo; j++)
                                tb.setBGColorCell(i, j, WHITE);
        return;
        }

     /**
     * Sets the background colors for a line that is added on the way to a table. The same protocol as for the setTableColors(...) method is used, except that the hasHeaderLine does no longer count, since it is supposed that the table is already colored except for the current line
     */
    void RunEffPlugin::setLineColors(Table &tb, int colNo, int row, bool hasLabelColumn = true) const throw()

  {
        int i;
        for (i = 2; i <= colNo; i += 2)
                tb.setBGColorCell(i, row, WHITE);
        if (hasLabelColumn)
                for (i = 1; i <= colNo; i +=2)
                        tb.setBGColorCell(i, row, CELL_BG);
        else
                for (i =1; i < colNo; i += 2)
                        tb.setBGColorCell(i, row, WHITE);

        }


    void RunEffPlugin::fillInfoCell(Table& tb, int colNo, int lineNo, std::string value, std::string defaultValue)
       const throw()
   {

        if (defaultValue.empty())
                defaultValue = "N/A";

        Text* textValue;
        if (!value.empty() && value != " ")
                textValue = new Text(value);
        else textValue = new Text(defaultValue);

        tb.addElement(colNo, lineNo, textValue);

   }

   void RunEffPlugin::fillColorCell(Table& tb, int colNo, int lineNo, std::string value, std::string defaultValue, int textSize, Color textColor, std::string textOptions)
       const throw()
   {

        if (defaultValue.empty())
                defaultValue = "N/A";

        std::string stringValue;
        if (value.empty())
                stringValue = defaultValue;
        else
                stringValue = value;

        if (textSize == 0)
                textSize = 2;

        if (textOptions.empty())
                tb.addElement(colNo, lineNo, new Text(stringValue, textSize, textColor));
        else
                tb.addElement(colNo, lineNo, new Text(stringValue, textSize, textColor, TextOption(textOptions)));

   }
  } // namespace rewmi
} // namespace daq

