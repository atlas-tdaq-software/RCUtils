#ifndef RUN_EFF_PLUGIN_INCLUDED
#define RUN_EFF_PLUGIN_INCLUDED

#include <wmi/pluginbase.h>
#include <wmi/table.h>
#include <wmi/br.h>
#include <wmi/hr.h>

#include <string>
#include <map>
#include <vector>

#include "ers/ers.h"
#include "owl/time.h"
#include <CoolKernel/IFolder.h>
#include <CoolKernel/IDatabase.h>
#include <CoolKernel/ValidityKey.h>

#include <boost/regex.hpp>

namespace daq {
  namespace rewmi {
    
    class RunEffPlugin : public daq::wmi::PluginBase {
      
    public:

      RunEffPlugin();
      virtual ~RunEffPlugin();

      void periodicAction();
      void stop();
      void configure(const ::wmi::InfoParameter& params);
    
      int valInBeam(long long startBlock, long long endBlock, long long startBeam, long long endBeam);
      long long checkRunInRNDB(std::string partitionName, int runNumber);
      double fraction(long long startTime, long long endtime, cool::IFolderPtr & the_busy_folder );
      double busy(long long startTime, long long endTime, std::string identifier, cool::IFolderPtr & the_busy_folder );
      void get_beam1_data(long long startTime, long long endTime, cool::IFolderPtr & folder);
      void get_beam2_data(long long startTime, long long endTime, cool::IFolderPtr & folder);
      void get_beams_data(long long startTime, long long endTime, cool::IFolderPtr & folder);
      void get_beam_data_from_file(long long starttime, long long endtime);
      void get_ramp_data(long long startTime, long long endTime, cool::IFolderPtr & folder);
      void print_vectors(std::vector<long long> start_vector, std::vector<long long> end_vector, std::string file_name);
      void set_main_vectors(std::vector<long long> start_vector, std::vector<long long> end_vector);
      int get_total_beam_time();
      void set_beam1and2_vectors();
      void set_beam1and2andStable_vectors();
      void set_beam1or2noRamp_vectors();
      void set_beam1and2noRamp_vectors();
      bool checkInBeam(long long time);
      double getEfficiency(long long starttime, long long endtime, int readyForPhysics);
      std::string getStopReason(long long starttime, long long endtime);
      std::string get_stop_reason(long long starttime, long long endtime);
      void set_beam_vectors(long long starttime, long long endtime);
      void set_beam1or2_vectors();
      void openDatabasesAndFolders();
      void closeDatabases();
      int ready_for_physics(cool::ValidityKey key, cool::IFolderPtr & datatakingmode_folder);
      int get_ctpid(cool::ValidityKey sKey, cool::ValidityKey uKey, std::string itemName, cool::IFolderPtr & lvl1menu_folder);
      std::string get_run_numbers(long long starttime, long long endtime);
      long long get_beam_start_time(long long starttime, int fillnumber, cool::IFolderPtr & folder);
      double veto_fraction(cool::ValidityKey key, int ctpid, cool::IFolderPtr & l1counters_folder);

    protected:

      class RunData;

      daq::wmi::Table* createMainTable() const throw();
      daq::wmi::Table* createBeamTable() const throw();
      void updateGlobalEfficiency() throw();
      void updateEfficiencyData() throw();
      void buildTables() throw();
      void buildRatePlot() throw();
      void buildEffPlot()  throw();
      void buildBeamsPlot()  throw();
      void buildRunsPlot()  throw();
      void buildRunsPages() const throw();
      void takeTime() throw();
      void cleanRunsMap() throw();
      void setTableColors(daq::wmi::Table& tb, int colNo, int lineNo, bool hasLabelColumn, bool hasHeaderColumn) const throw();

      void setLineColors(daq::wmi::Table& tb, int colNo, int row, bool hasLabelColumn) const throw();
      void fillColorCell(daq::wmi::Table& tb, int colNo, int lineNo, std::string value, std::string defaultValue, int textSize, daq::wmi::Color textColor, std::string textOptions) const throw();
      void fillInfoCell(daq::wmi::Table& tb, int colNo, int lineNo, std::string value, std::string defaultValue) const throw();
 
      void printRateGraph(daq::wmi::Table& tb) const throw();
      void printEffGraph(daq::wmi::Table& tb) const throw();
      void printRunsGraph(daq::wmi::Table& tb) const throw();
      void printBeamsGraph(daq::wmi::Table& tb) const throw();
        
      typedef std::vector< std::pair<unsigned long, float> > graph;
	
      graph totalGraph;
      graph beamGraph;
      graph rateGraph;
      graph effGraph;
      graph runsGraph;
      graph beamsGraph;

      class RunData {

        friend class RunEffPlugin;
  
      public:
  
        virtual ~RunData() throw();

      protected:
        explicit RunData(const std::string& runNumber) throw();
        RunData(const RunData& rhs);  // ?? is needed ??

      private:
        graph l1Graph;
        
        std::vector<std::string> lb_vector;
        std::vector<std::string> solb_vector;
        std::vector<std::string> busy_fraction_vector;
        std::vector<std::string> busy_vector;
 
        const std::string runNumber;

      }; // RunData

    private:
      static unsigned int hours;
      static unsigned int duration;
      static unsigned int seconds;
      static unsigned int writeBeamFile;
      static unsigned int noRamp;
      static unsigned int intervalMinutes;
      static unsigned int beamLimit;
      static double rateLimit;
      static double lumiBlockBusyLimit;
      static double detBusyLimit;
      static double busyLimit;
      static const unsigned int graphTableColNum;
      static std::string beamDataFile;
      static std::string outFile;
      static long long nowtime;
      static long long start_time;
      static long long end_time;
      static unsigned int runningOffline;
      static unsigned int beamType;
      static unsigned int readyForPhysics;
      static std::string triggerItem;

      static const unsigned int graphPoints;
      typedef std::list< std::pair<unsigned int, double> > ratePairList;
      ratePairList tRate; // temporary for rate calculation
      ratePairList bRate; // temporary for rate calculation
      void updateRateGraphs(unsigned int xValue, double yValue, graph& gr, ratePairList& rpl) const throw();

      std::map<std::string, RunData*> runsMap;      
  
      mutable daq::wmi::BR br;
      mutable daq::wmi::HR hr;

      static const unsigned int generalTableColNum;
//      static const unsigned int beamTableColNum;
      static const unsigned int busyTableColNum; 

      static const std::string RunNumberLabel;
      static const std::string NumberEventsLabel;
      static const std::string RunStartTimeLabel;
      static const std::string RunStopTimeLabel;
      static const std::string LumiBlocksLabel;
      static const std::string LowRateLumiBlocksLabel;
      static const std::string HighBusyLumiBlocksLabel;

      static const std::string BeamStartLabel;
      static const std::string FillNumberLabel;
      static const std::string BeamDurationLabel;
      static const std::string RunLabel;
      static const std::string RunDurationLabel;
      static const std::string EffLabel;
      static const std::string EffPhysLabel;
      static const std::string ReadyDelayLabel;
      static const std::string EndDelayLabel;
      static const std::string RunStoppedLabel;
      static const std::string StopReasonLabel;
      static const std::string DelayLabel;

      static const std::string LumiBlockLabel;
      static const std::string StartTimeLabel;
      static const std::string BusyFractionLabel;
      static const std::string BusySourceLabel;

      static const std::string EOR_Name;
      static const std::string SOR_Name;
      static const std::string EC_Name;
      static const std::string StopReason_Name;
      static const std::string CTP_Name;
      static const std::string LB_Name;
      static const std::string LBT_Name;
      static const std::string BusyRate_Name;
      static const std::string BusyConf_Name;
      static const std::string FillState_Name;
      static const std::string TotalInt_Name;
      static const std::string L1Counters_Name;
      static const std::string L1Menu_Name;
      static const std::string DataTakingMode_Name;

      cool::IDatabasePtr db;
      cool::IDatabasePtr dbt;
      cool::IDatabasePtr dbm;
      cool::IDatabasePtr dbd;

      cool::IFolderPtr EOR_Folder;
      cool::IFolderPtr SOR_Folder;
      cool::IFolderPtr EC_Folder;
      cool::IFolderPtr StopReason_Folder;
      cool::IFolderPtr CTP_Folder;
      cool::IFolderPtr BusyRate_Folder;
      cool::IFolderPtr BusyConf_Folder;
      cool::IFolderPtr FillState_Folder;
      cool::IFolderPtr TotalInt_Folder;
      cool::IFolderPtr LB_Folder;
      cool::IFolderPtr LBT_Folder;
      cool::IFolderPtr L1Counters_Folder;
      cool::IFolderPtr L1Menu_Folder;
      cool::IFolderPtr DataTakingMode_Folder;

      std::vector<std::string> rn_vector;
      std::vector<std::string> ev_vector;
      std::vector<std::string> sor_vector;
      std::vector<std::string> eor_vector;
      std::vector<std::string> lb_vector;
      std::vector<std::string> hblb_vector;
      std::vector<std::string> lrlb_vector;

      std::vector<std::string> beam_start_vector;
      std::vector<std::string> fill_number_vector;
      std::vector<std::string> beam_duration_vector;
      std::vector<std::string> beam_run_vector;
      std::vector<std::string> run_duration_vector;
      std::vector<std::string> beam_delay_vector;
      std::vector<std::string> eff_vector;
      std::vector<std::string> eff_phys_vector;
      std::vector<std::string> ready_delay_vector;
      std::vector<std::string> end_delay_vector;
      std::vector<std::string> run_stopped_vector;
      std::vector<std::string> stop_reason_vector;

      std::vector<long long> start_beam_vector;
      std::vector<long long> end_beam_vector;

      std::vector<long long> beam1_start_vector;
      std::vector<long long> beam2_start_vector;
      std::vector<long long> beam1_end_vector;
      std::vector<long long> beam2_end_vector;
      std::vector<long long> beams_start_vector;
      std::vector<long long> beams_end_vector;
      std::vector<long long> beam1and2_start_vector;
      std::vector<long long> beam1and2_end_vector;
      std::vector<long long> beam1or2_start_vector;
      std::vector<long long> beam1or2_end_vector;
      std::vector<long long> beam1and2andStable_start_vector;
      std::vector<long long> beam1and2andStable_end_vector;
      std::vector<long long> beam1or2noRamp_start_vector;
      std::vector<long long> beam1or2noRamp_end_vector;
      std::vector<long long> beam1and2noRamp_start_vector;
      std::vector<long long> beam1and2noRamp_end_vector;
      std::vector<long long> ramp_start_vector;
      std::vector<long long> ramp_end_vector;

      int runTime;
      int totalTime;
      int beamTime;
      int lumiblocks;
      int generalTableRawNum;
      int beamTableRawNum;
      int beamTableColNum;
      double beamEff;
      std::string meanBeamEff;
      double runStopped_minutes;
      double endDelay_minutes;
      double startDelay_minutes;
      bool no_busy_info;

    };

    extern "C" daq::wmi::PluginBase* create() {
      return new RunEffPlugin();
    }
    
    extern "C" void destroy(daq::wmi::PluginBase* p) {
      delete p;
    }
    
  } // end rewmi namespace
} // end daq namespace

#endif
