// Run Status Plugin
// Configurable parameters: database connection

#include <set>
#include <iostream>
#include <string>

#include <wmi/text.h>
#include <wmi/link.h>
#include <wmi/font.h>
#include <wmi/tree.h>
#include <wmi/br.h>
#include <ers/ers.h>

// IS structures
#include "is/infoiterator.h"
#include <is/infodynany.h>
#include <is/infodictionary.h>
#include <RCDLTPModule/LumiBlockNamed.h>

#include <CoralBase/Attribute.h>
#include <CoolKernel/Exception.h>
#include <CoolKernel/ValidityKey.h>
#include <CoolKernel/IDatabaseSvc.h>
#include <CoolKernel/IDatabase.h>
#include <CoolKernel/IFolder.h>
#include <CoolKernel/IObject.h>
#include <CoolKernel/IObjectIterator.h>
#include <CoolKernel/IRecordSpecification.h>
#include <CoolKernel/Record.h>
#include <CoolApplication/DatabaseSvcFactory.h>

#include "wmiresources.h"

const daq::wmi::Color RED(255, 0, 0);
const daq::wmi::Color GREEN(0, 180, 0);
const daq::wmi::Color LIGHT_GREEN(0, 255, 0);
const daq::wmi::Color DARK_RED(183, 60, 60);
const daq::wmi::Color DARK_BLUE(60, 60, 183);
const daq::wmi::Color WHITE(255, 255, 255);
const daq::wmi::Color GREY(198, 198, 198);
const daq::wmi::Color DARK_GREY(150, 151, 150);
const daq::wmi::Color CELL_BG(219, 239, 251);
const daq::wmi::Color CAPTION_BG(0, 64, 128);
const daq::wmi::Color LIGHT_GREY(240, 240, 240);
const daq::wmi::Color BLACK(0, 0, 0);

const int CHANNEL_COLUMNS=100;

using daq::wmi::Text;
using daq::wmi::Link;
using daq::wmi::Table;
using daq::wmi::TextOption;
using daq::wmi::Color;

int S_Runno=0;

std::string connectString = "oracle://ATLAS_COOLPROD;schema=ATLAS_COOLONL_TDAQ;dbname=COMP200";

std::string cherrypyBase = "http://atlas-service-tdaq-enabled.web.cern.ch/atlas-service-tdaq-enabled/php/getcool.php?";

ERS_DECLARE_ISSUE(	rc,
			BadCoolDB,
			"Failed to access db " << connection,
                        ((std::string) connection)
		)


  using namespace cool;

namespace daq {
  namespace ccwmi {
    /*********************/
    /** ResourcesPlugin **/
    /*********************/
    
    ResourcesPlugin::ResourcesPlugin() : 
      PluginBase()
    {
      
    }

    ResourcesPlugin::~ResourcesPlugin() {
      delete m_outStream;
    }

    void ResourcesPlugin::addColorCode(int fd) {
      Table colorTab(5, 1);
      Text *col = new Text("Color code:", 2, BLACK, TextOption("b"));
      Text *grey = new Text("Disabled by configuration", 2, DARK_GREY, TextOption("b"));
      Text *green = new Text("Enabled in Run", 2, GREEN, TextOption("b"));
      Text *lightgreen = new Text("Re-enabled during Run", 2, LIGHT_GREEN, TextOption("b"));
      Text *red = new Text("Disabled during Run", 2, RED, TextOption("b"));
      colorTab.addElement(1,1, col);
      colorTab.addElement(2,1, grey);
      colorTab.addElement(3,1, green);
      colorTab.addElement(5,1, lightgreen);
      colorTab.addElement(4,1, red);
      m_outStream->write(colorTab, fd);
    }

    Link* ResourcesPlugin::prepareSecondaryTable(const std::string &linkName, const std::string &folderName, const std::string &title, const std::set<std::string> & theSet, int& ena, int& dis, int& rem) {

      ena = 0;
      dis = 0;
      rem = 0;
      int fdp = m_outStream->open(linkName+".html");
      Text x(title.c_str(), 5, DARK_BLUE, TextOption("b"));
      m_outStream->write(x, fdp);
      Table *tab = new Table(CHANNEL_COLUMNS, (theSet.size()/CHANNEL_COLUMNS + 1));
      int row = 1;
      int index = 1;
      for(std::set<std::string>::const_iterator chit=theSet.begin(); chit != theSet.end(); ++chit) {
	std::map<std::string, uint32_t>::const_iterator it;
	
	std::ostringstream ln;
	ln << cherrypyBase << "start=" << S_Runno << ".0&end=" << S_Runno << ".65535&folder="
	   <<folderName << "&channel=" << (*chit);
	std::ostringstream lb;

	if((it = m_ISEnabled.find(*chit)) != m_ISEnabled.end()) {
	  ++ena;
	  // GREEN
	  if ( (*it).second > 0)
	    tab->setBGColorCell(index,row, LIGHT_GREEN);
	  else
	    tab->setBGColorCell(index,row, GREEN);

	  lb << (*it).second;
	}
	else if ((it = m_ISDisabled.find(*chit)) != m_ISDisabled.end()) {
	  ++rem;
	  // RED
	  tab->setBGColorCell(index,row, RED);
	  lb << (*it).second;
	}
	else {
	  ++dis;
	  //GREY
	  lb << "x";
	  tab->setBGColorCell(index,row, GREY);
	}

	Link *y = new Link(ln.str().c_str(), lb.str().c_str());
	tab->addElement(index,row,y);

	if (index == CHANNEL_COLUMNS) {
	  index=1;
	  ++row;
	}
	else {
	  ++index;
	}
      }
      m_outStream->write(*tab, fdp);
      delete tab;
      addColorCode(fdp);
      m_outStream->close(fdp);

      std::string lkn(linkName+".html");
      Link * lk = new Link(lkn.c_str(),linkName.c_str());
      return lk;
    }

    void ResourcesPlugin::prepareMainTable(int fd) {
      Table * wrapTable = new Table(2,14);
      wrapTable->setBorder(3);
      int tabRow=1;
      std::vector<T_subDet>::const_iterator mapit=m_detectors.begin();


      for(;mapit!=m_detectors.end(); ++mapit) {

	int rows = (*mapit).primaryChannels.size() / CHANNEL_COLUMNS + 1;
	Table * tab = new Table(CHANNEL_COLUMNS, rows);
	Text * nam = new Text((*mapit).name, TextOption("b"));

	std::set<std::string>::const_iterator chit=(*mapit).primaryChannels.begin();
	int row=1;
	int index=1;

	int rolEna=0, rolDis=0, rolRem=0;

	for(;chit != (*mapit).primaryChannels.end(); ++chit) {
	  int fdp = m_outStream->open(*chit+".html");
	  std::ostringstream chantxt;
	  chantxt << (*chit) << "\n";
	  Text x(chantxt.str().c_str(), 5, DARK_BLUE, TextOption("b"));
	  m_outStream->write(x, fdp);
	  m_outStream->write(this->br, fdp);
	  m_outStream->write(this->hr, fdp);
	  m_outStream->close(fdp);
	  //std::cout << "Checking status of channel " << (*chit) << std::endl;
	  std::map<std::string, uint32_t>::const_iterator it;

	  std::ostringstream ln;
	  ln << cherrypyBase << "start=" << S_Runno << ".0&end=" << S_Runno << ".65535&folder="
	     << (*mapit).primaryFolder << "&channel=" << (*chit);
	  std::ostringstream lb;

	  if((it = m_ISEnabled.find(*chit)) != m_ISEnabled.end()) {
	    // GREEN
	    ++rolEna;
	    if ((*it).second > 0)
	      tab->setBGColorCell(index,row, LIGHT_GREEN);
	    else
	      tab->setBGColorCell(index,row, GREEN);

	    lb << (*it).second;
	  }
	  else if ((it = m_ISDisabled.find(*chit)) != m_ISDisabled.end()) {
	    // RED
	    ++rolRem;
	    tab->setBGColorCell(index,row, RED);
	    lb << (*it).second;
	  }
	  else {
	    ++rolDis;
	    tab->setBGColorCell(index,row, GREY);
	    lb << "x";
	  }

	  Link *y = new Link(ln.str().c_str(), lb.str().c_str());
	  tab->addElement(index,row,y);

	  if (index == CHANNEL_COLUMNS) {
	    index=1;
	    ++row;
	  }
	  else {
	    ++index;
	  }
	}
	Table * leftHeadings = new Table(1,2);
	
	if((*mapit).secondaryChannels.size() > 0) {
	  int ena, dis, rem;
	  Link *subLk = prepareSecondaryTable((*mapit).name, (*mapit).secondaryFolder, (*mapit).secondaryName , (*mapit).secondaryChannels, ena, dis,rem);
	  leftHeadings->addElement(1, 1, subLk);
	  // here add color coded text for enabled/disabled sub-resources
	  std::stringstream enastr, disstr, remstr, secnamstr;
	  enastr << ena;
	  disstr << dis;
	  remstr << rem;
	  secnamstr << (*mapit).secondaryName << ":";
	  Text *secNam = new Text(secnamstr.str().c_str(), 3, BLACK);
	  Text *enat = new Text(enastr.str().c_str(), 3, GREEN, TextOption("b"));
	  Text *remt = new Text(remstr.str().c_str(), 3, RED, TextOption("b"));
	  Text *dist = new Text(disstr.str().c_str(), 3, DARK_GREY, TextOption("b"));
	  leftHeadings->addElement(1, 2, secNam);
	  leftHeadings->addElement(1, 2, enat);
	  leftHeadings->addElement(1, 2, dist);
	  leftHeadings->addElement(1, 2, remt);
	}
	else {
	  leftHeadings->addElement(1, 1, nam);
	}

	std::stringstream rolEnastr, rolDisstr, rolRemstr;
	rolEnastr << rolEna;
	rolDisstr << rolDis;
	rolRemstr << rolRem;
	Text *rolt = new Text(" ROLs:", 3, BLACK); 
	Text *rolEnat = new Text(rolEnastr.str().c_str(), 3, GREEN, TextOption("b")); 
	Text *rolDist = new Text(rolDisstr.str().c_str(), 3, DARK_GREY, TextOption("b")); 
	Text *rolRemt = new Text(rolRemstr.str().c_str(), 3, RED, TextOption("b")); 

	leftHeadings->addElement(1, 1, rolt);
	leftHeadings->addElement(1, 1, rolEnat);
	leftHeadings->addElement(1, 1, rolDist);
	leftHeadings->addElement(1, 1, rolRemt);


	wrapTable->addElement(1, tabRow, leftHeadings);
	wrapTable->addElement(2, tabRow, tab);
	++tabRow;	  
      }
      
      m_outStream->write(*wrapTable, fd);
      delete wrapTable;
      addColorCode(fd);
      return; 
    }

    void ResourcesPlugin::periodicAction() {

      int fd = m_outStream->open("ATLAS_Resources.html");

      // if not ATLAS RUNNING => show all channels grey
      IPCPartition p("ATLAS");
      LumiBlockNamed rp(p, "RunParams.LumiBlock");
      try {	
	rp.checkout();
	//rp.RunNumber = 188483;
	//rp.LBN = 8;
      }
      catch(ers::Issue & e) {
	// ATLAS is not running
	  Text plotTitle("ATLAS is not UP", 5, DARK_BLUE, TextOption("b"));
	  m_outStream->write(plotTitle, fd);
	  m_outStream->close(fd);      	
	return;
      }

      if (rp.RunNumber == 0) {
	Text plotTitle("ATLAS is not in the RUNNING state", 5, DARK_BLUE, TextOption("b"));
	m_outStream->write(plotTitle, fd);      
	m_outStream->write(this->br, fd);
	m_outStream->close(fd);
	return;
      }

      // get all robin channels in COOL (irrelevant if enabled/disabled)
      IDatabaseSvc & dbSvc = DatabaseSvcFactory::databaseService();
      IDatabasePtr db;
      try {
	S_Runno = rp.RunNumber;
	db = dbSvc.openDatabase(connectString);
	cool::UInt63 a = rp.RunNumber;
	uint64_t  since= (a << 32) | 1;
	uint64_t  until= (a << 32) | rp.LBN ;
	cool::ValidityKey skey(since);
	cool::ValidityKey tkey(until);
	cool::ChannelSelection all;

	//std::cout << "Checking IOV " << since << " - " << until << std::endl;
	m_ISEnabled.clear();
	m_ISDisabled.clear();

	// Loop over all folders and get the status of resources => fill m_ISEnabled and m_ISDisabled which is then used for representation
	for (size_t i = 0; i < m_folderNames.size() ; ++i) {
	  //std::cout << "Reading folder " << folderNames[i] << std::endl;
	  IFolderPtr folder = db->getFolder(m_folderNames[i]);
	  folder->setPrefetchAll(true);
	  IObjectIteratorPtr objiter = folder->browseObjects(skey, tkey, all);
	  std::map<ChannelId, std::string> channelMap = folder->listChannelsWithNames();

	  cool::ChannelId last_id(0);
	  std::string last_name("");
	  
	  while ( objiter->goToNext() ) {
	    const cool::IObject& obj(objiter->currentRef());
	    //if(last_id != obj.channelId()) {
	    last_id = obj.channelId();
	    last_name = channelMap.find(last_id)->second;
	    //std::cout << "Got channel " << last_id << " " << last_name << " with IOV " << obj.since() << " - " << obj.until() << std::endl; 
	    if (obj.until() >= tkey) {
	      uint32_t lb = 0;
	      if(obj.since() > skey) {
		lb = obj.since() - skey + 1;
	      }

	      if (m_ISEnabled.find(last_name) != m_ISEnabled.end())
		m_ISEnabled.erase(m_ISEnabled.find(last_name));

	      m_ISEnabled.insert(std::pair<std::string,uint32_t> (last_name,lb)); // Enabled
	      //std::cout << last_name <<  " => enabled since " << obj.since() << " sor " << skey << std::endl;
	    }
	    else if (obj.until() < tkey) {
	      uint32_t lb = obj.until() - skey;
	      if (m_ISDisabled.find(last_name) != m_ISDisabled.end())
		m_ISDisabled.erase(m_ISDisabled.find(last_name));
	      
	      m_ISDisabled.insert(std::pair<std::string,uint32_t> (last_name,lb)); // Disabled in run
	      //std::cout << last_name << " => diabled in run " << " at LB = " << lb << std::endl;
	    }
	  }
	}
	db->closeDatabase();
      }
      catch (std::exception &e) {
	ers::fatal(rc::BadCoolDB(ERS_HERE, connectString, e));
	sleep(3);
	exit(-1);
      }
    
      std::ostringstream txt;
      txt << "Status of Resources for run " << rp.RunNumber << " at LB " << rp.LBN ;

      Text plotTitle(txt.str().c_str(), 5, DARK_BLUE, TextOption("b"));
      m_outStream->write(plotTitle, fd);      

      m_outStream->write(this->br, fd);
      Text plotMessage("!!! Note that some detector channels might be disabled even if from a DAQ point of view the status of the ROLs appears as green. For some detectors you can browse the sub-ROD status of resources following the links on this page; for MUONs you shall rely on DCS information!!!", 3, DARK_GREY, TextOption("i"));
      m_outStream->write(plotMessage, fd);      
      
      m_outStream->write(this->br, fd);
      
      this->prepareMainTable(fd);
      
      // Close all the ouput streams
      m_outStream->close(fd);      
    }

    void 
    ResourcesPlugin::stop() 
    {
      // stop()
      m_outStream->closeAll();      
    }

    void 
    ResourcesPlugin::configure(const ::wmi::InfoParameter& params) 
    {
      // List of channels to ignore
      std::set<std::string> oldPix;
      oldPix.insert("ROL-PIX-B-00-112405");
      oldPix.insert("ROL-PIX-B-00-112407");
      oldPix.insert("ROL-PIX-B-00-112408");
      oldPix.insert("ROL-PIX-B-00-112409");
      oldPix.insert("ROL-PIX-B-00-112410");
      oldPix.insert("ROL-PIX-B-00-112411");
      oldPix.insert("ROL-PIX-B-00-112412");
      oldPix.insert("ROL-PIX-B-00-112414");
      oldPix.insert("ROL-PIX-B-00-112415");
      oldPix.insert("ROL-PIX-B-00-112416");
      oldPix.insert("ROL-PIX-B-00-112417");
      oldPix.insert("ROL-PIX-B-01-112418");
      oldPix.insert("ROL-PIX-B-01-112419");
      oldPix.insert("ROL-PIX-B-01-112420");
      oldPix.insert("ROL-PIX-B-01-112421");
      oldPix.insert("ROL-PIX-B-01-112505");
      oldPix.insert("ROL-PIX-B-01-112506");
      oldPix.insert("ROL-PIX-B-01-112507");
      oldPix.insert("ROL-PIX-B-01-112508");
      oldPix.insert("ROL-PIX-B-01-112509");
      oldPix.insert("ROL-PIX-B-01-112510");
      oldPix.insert("ROL-PIX-B-01-112511");
      oldPix.insert("ROL-PIX-B-01-112512");
      oldPix.insert("ROL-PIX-B-02-111705");
      oldPix.insert("ROL-PIX-B-02-111707");
      oldPix.insert("ROL-PIX-B-02-111708");
      oldPix.insert("ROL-PIX-B-02-112514");
      oldPix.insert("ROL-PIX-B-02-112515");
      oldPix.insert("ROL-PIX-B-02-112516");
      oldPix.insert("ROL-PIX-B-02-112517");
      oldPix.insert("ROL-PIX-B-02-112518");
      oldPix.insert("ROL-PIX-B-02-112519");
      oldPix.insert("ROL-PIX-B-02-112520");
      oldPix.insert("ROL-PIX-B-02-112521");
      oldPix.insert("ROL-PIX-B-03-111709");
      oldPix.insert("ROL-PIX-B-03-111710");
      oldPix.insert("ROL-PIX-B-03-111711");
      oldPix.insert("ROL-PIX-B-03-111712");
      oldPix.insert("ROL-PIX-B-03-111714");
      oldPix.insert("ROL-PIX-B-03-111715");
      oldPix.insert("ROL-PIX-B-03-111716");
      oldPix.insert("ROL-PIX-B-03-111717");
      oldPix.insert("ROL-PIX-B-03-111719");
      oldPix.insert("ROL-PIX-B-03-111721");
      oldPix.insert("ROL-PIX-B-04-111805");
      oldPix.insert("ROL-PIX-B-04-111806");
      oldPix.insert("ROL-PIX-B-04-111807");
      oldPix.insert("ROL-PIX-B-04-111808");
      oldPix.insert("ROL-PIX-B-04-111809");
      oldPix.insert("ROL-PIX-B-04-111810");
      oldPix.insert("ROL-PIX-B-04-111811");
      oldPix.insert("ROL-PIX-B-04-111812");
      oldPix.insert("ROL-PIX-B-04-111814");
      oldPix.insert("ROL-PIX-B-04-111816");
      oldPix.insert("ROL-PIX-B-05-111818");
      oldPix.insert("ROL-PIX-B-05-111819");
      oldPix.insert("ROL-PIX-B-05-111820");
      oldPix.insert("ROL-PIX-B-05-111821");
      oldPix.insert("ROL-PIX-BL-00-130005");
      oldPix.insert("ROL-PIX-BL-00-130006");
      oldPix.insert("ROL-PIX-BL-00-130008");
      oldPix.insert("ROL-PIX-BL-01-130018");
      oldPix.insert("ROL-PIX-BL-01-130019");
      oldPix.insert("ROL-PIX-BL-01-130020");
      oldPix.insert("ROL-PIX-BL-01-130021");
      oldPix.insert("ROL-PIX-BL-01-130105");
      oldPix.insert("ROL-PIX-BL-01-130106");
      oldPix.insert("ROL-PIX-BL-03-130312");
      oldPix.insert("ROL-PIX-BL-03-130314");
      oldPix.insert("ROL-PIX-BL-03-130315");
      oldPix.insert("ROL-PIX-BL-03-130316");
      oldPix.insert("ROL-PIX-BL-03-130317");
      oldPix.insert("ROL-PIX-DISK-00-120206");
      oldPix.insert("ROL-PIX-DISK-00-120208");
      oldPix.insert("ROL-PIX-DISK-00-120210");
      oldPix.insert("ROL-PIX-DISK-00-120212");
      oldPix.insert("ROL-PIX-DISK-00-120215");
      oldPix.insert("ROL-PIX-DISK-00-120217");
      oldPix.insert("ROL-PIX-DISK-01-120218");
      oldPix.insert("ROL-PIX-DISK-01-120220");
      oldPix.insert("ROL-PIX-DISK-01-121609");
      oldPix.insert("ROL-PIX-DISK-01-121611");
      oldPix.insert("ROL-PIX-DISK-01-121614");
      oldPix.insert("ROL-PIX-DISK-01-121616");
      oldPix.insert("ROL-TDQ-SPARE-03-000000");
      oldPix.insert("ROL-TDQ-SPARE-03-000001");
      oldPix.insert("ROL-TDQ-SPARE-03-000002");
      oldPix.insert("ROL-TDQ-SPARE-03-000003");
      oldPix.insert("ROL-TDQ-SPARE-03-000004");
      oldPix.insert("ROL-TDQ-SPARE-03-000005");
      oldPix.insert("ROL-TDQ-SPARE-03-000006");
      oldPix.insert("ROL-TDQ-SPARE-03-000007");
      oldPix.insert("ROL-TDQ-SPARE-03-000008");
      oldPix.insert("ROL-TDQ-SPARE-03-000009");
      oldPix.insert("ROL-TDQ-SPARE-03-00000a");
      oldPix.insert("ROL-TDQ-SPARE-03-00000b");

      std::set<std::string> badLAr;
      badLAr.insert("ROL-TDQ-SPARE-03-000000");
      badLAr.insert("ROL-TDQ-SPARE-03-000001");
      badLAr.insert("ROL-TDQ-SPARE-03-000002");
      badLAr.insert("ROL-TDQ-SPARE-03-000003");
      badLAr.insert("ROL-TDQ-SPARE-03-000004");
      badLAr.insert("ROL-TDQ-SPARE-03-000005");
      badLAr.insert("ROL-TDQ-SPARE-03-000006");
      badLAr.insert("ROL-TDQ-SPARE-03-000007");
      badLAr.insert("ROL-TDQ-SPARE-03-000008");
      badLAr.insert("ROL-TDQ-SPARE-03-000009");
      badLAr.insert("ROL-TDQ-SPARE-03-00000a");
      badLAr.insert("ROL-TDQ-SPARE-03-00000b");



      // Read settings from the config file
      unsigned int nums = params.length();
      for(unsigned int i = 0; i < nums; ++i) {
	if(strcmp(params[i].name, "dbconnection") == 0) {
	  std::cout << "DB connection to use is " << params[i].value << std::endl;
	  connectString = params[i].value;
	}
      }

      std::string det[]={"PIXEL", "SCT", "TRT", "LAR", "TILE", 
			 "MUON_MDT", "MUON_TGC", "MUON_RPC", "MUON_SCT", 
			 "FORWARD", "L1CALO", "TDAQ_MUON_CTP", "TDAQ_CTP", "FTK"};
      std::string detName[]={"PIXEL", "SCT", "TRT", "LAR", "TILE", 
			     "MDT", "TGC", "RPC", "CSC", 
			     "FORWARD", "L1CALO", "MUCTPI", "CTP", "FTK"};

      // get all robin channels in COOL (irrelevant if enabled/disabled)
      IDatabaseSvc & dbSvc = DatabaseSvcFactory::databaseService();
      IDatabasePtr db;
      try {
	db = dbSvc.openDatabase(connectString);


	// Put all folders in a list
	m_folderNames.clear();
	for (size_t i = 0; i < 14 ; ++i) {
	  m_folderNames.push_back("/TDAQ/EnabledResources/ATLAS/"+det[i]+"/Robins");
	}
	
	m_folderNames.push_back("/TDAQ/EnabledResources/ATLAS/PIXEL/Modules");
	m_folderNames.push_back("/TDAQ/EnabledResources/ATLAS/L1CALO/Connections");
	m_folderNames.push_back("/TDAQ/EnabledResources/ATLAS/TDAQ_MUON_CTP/MioctSectorInputs");
	m_folderNames.push_back("/TDAQ/EnabledResources/ATLAS/TILE/Drawers");
	m_folderNames.push_back("/TDAQ/EnabledResources/ATLAS/GLOBAL/CTP_Input");


	// Create channel lists for all detectors
	m_detectors.clear();      
	for (size_t i = 0; i < 14 ; ++i) {
	  T_subDet detS;
	  detS.name = detName[i];
	  detS.primaryFolder = det[i];

	  IFolderPtr folder = db->getFolder("/TDAQ/EnabledResources/ATLAS/"+det[i]+"/Robins");
	  std::map<ChannelId, std::string> channelMap = folder->listChannelsWithNames();
	  std::map<ChannelId, std::string>::const_iterator it=channelMap.begin();
	  //std::cout << "Adding channels for det " << det[i] <<":" << std::endl; 
	  for (; it != channelMap.end() ; ++it) {
	    // Mask old/bad Pixel channels
	    if((det[i] == "PIXEL") && (oldPix.find((*it).second) != oldPix.end()) ) {
	      continue;
	    }
	    // Mask old/bad LAr channels
	    if((det[i] == "LAR") && (badLAr.find((*it).second) != badLAr.end()) ) {
	      continue;
	    }

	    detS.primaryChannels.insert((*it).second);	      
	  }

	  if (detName[i] == "PIXEL") {
	    // Take Pixel Modules
	    detS.secondaryName = "Pixel Modules";
	    detS.secondaryFolder = "PIXEL/Modules";
	    IFolderPtr folder = db->getFolder("/TDAQ/EnabledResources/ATLAS/PIXEL/Modules");
	    std::map<ChannelId, std::string> channelMap = folder->listChannelsWithNames();
	    std::map<ChannelId, std::string>::const_iterator it=channelMap.begin();
	    for (; it != channelMap.end() ; ++it) {
	      detS.secondaryChannels.insert((*it).second);
	    }	    
	  }
	  
	// Take L1 Calo Connections
	if (detName[i] == "L1CALO") {
	  detS.secondaryName = "L1 Calo Connections";
	  detS.secondaryFolder = "L1CALO/Connections";	  
	  IFolderPtr folder = db->getFolder("/TDAQ/EnabledResources/ATLAS/L1CALO/Connections");
	  std::map<ChannelId, std::string> channelMap = folder->listChannelsWithNames();
	  std::map<ChannelId, std::string>::const_iterator it=channelMap.begin();
	  for (; it != channelMap.end() ; ++it) {
	    detS.secondaryChannels.insert((*it).second);
	  }	    
	}

	// Take MiOct Sector inputs
	if (detName[i] == "MUCTPI"){
	  detS.secondaryName = "MIOCT Sector Logic Inputs";
	  detS.secondaryFolder = "TDAQ_MUON_CTP/MioctSectorInputs";	  
	  IFolderPtr folder = db->getFolder("/TDAQ/EnabledResources/ATLAS/TDAQ_MUON_CTP/MioctSectorInputs");
	  std::map<ChannelId, std::string> channelMap = folder->listChannelsWithNames();
	  std::map<ChannelId, std::string>::const_iterator it=channelMap.begin();
	  for (; it != channelMap.end() ; ++it) {
	    detS.secondaryChannels.insert((*it).second);
	  }	    
	}

	// Take Tile Drawers
	if (detName[i] == "TILE"){
	  detS.secondaryName = "Tile Drawers";
	  detS.secondaryFolder = "TILE/Drawers";	  
	  IFolderPtr folder = db->getFolder("/TDAQ/EnabledResources/ATLAS/TILE/Drawers");
	  std::map<ChannelId, std::string> channelMap = folder->listChannelsWithNames();
	  std::map<ChannelId, std::string>::const_iterator it=channelMap.begin();
	  for (; it != channelMap.end() ; ++it) {
	    detS.secondaryChannels.insert((*it).second);
	  }	    
	}

	// Take CTP input
	if (detName[i] == "CTP"){
	  detS.secondaryName = "CTP Inputs";
	  detS.secondaryFolder = "GLOBAL/CTP_Input";	  
	  IFolderPtr folder = db->getFolder("/TDAQ/EnabledResources/ATLAS/GLOBAL/CTP_Input");
	  std::map<ChannelId, std::string> channelMap = folder->listChannelsWithNames();
	  std::map<ChannelId, std::string>::const_iterator it=channelMap.begin();
	  for (; it != channelMap.end() ; ++it) {
	    detS.secondaryChannels.insert((*it).second);
	  }	    
	}
	m_detectors.push_back(detS);
	}
	db->closeDatabase();
      }
      catch (std::exception &e) {
	ers::fatal(rc::BadCoolDB(ERS_HERE, connectString, e));
	sleep(3);
	exit(-1);
      }
    }
  }
}
