#ifndef ISCTPBUSYNAMED_H
#define ISCTPBUSYNAMED_H

#include <is/namedinfo.h>

#include <string>
#include <ostream>
#include <vector>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * CTP busy-monitoring values
 * 
 * @author  generated by the IS tool
 * @version 08/06/09
 */

class ISCTPBUSYNamed : public ISNamedInfo {
public:

    /**
     * Busy percentage of Event Counter Reset (CTPMI)
     */
    double                        ctpmi_ecr_rate;

    /**
     * Busy percentage of on-demand BUSY (CTPMI)
     */
    double                        ctpmi_vme_rate;

    /**
     * Busy percentage of VETO0 (CTPMI)
     */
    double                        ctpmi_vto0_rate;

    /**
     * Busy percentage of VETO1 (CTPMI)
     */
    double                        ctpmi_vto1_rate;

    /**
     * Busy percentage via backplane (CTPMI)
     */
    double                        ctpmi_bckp_rate;

    /**
     * Overflow flag (CTPMI)
     */
    bool                          ctpmi_overflow;

    /**
     * Busy percentage of readout block (CTPCORE)
     */
    double                        ctpcore_rdt_rate;

    /**
     * Busy percentage of monitoring (CTPCORE)
     */
    double                        ctpcore_mon_rate;

    /**
     * Busy percentage via backplane (CTPCORE)
     */
    double                        ctpcore_bckp_rate;

    /**
     * Busy percentage of result block (CTPCORE)
     */
    double                        ctpcore_rslt_rate;

    /**
     * Busy percentage of configurable counter 0
     */
    double                        ctpcore_moni0_rate;

    /**
     * Busy percentage of configurable counter 1
     */
    double                        ctpcore_moni1_rate;

    /**
     * Busy percentage of configurable counter 2
     */
    double                        ctpcore_moni2_rate;

    /**
     * Busy percentage of configurable counter 3
     */
    double                        ctpcore_moni3_rate;

    /**
     * Name configurable counter 0
     */
    std::string                   ctpcore_moni0_label;

    /**
     * Name configurable counter 1
     */
    std::string                   ctpcore_moni1_label;

    /**
     * Name configurable counter 2
     */
    std::string                   ctpcore_moni2_label;

    /**
     * Name configurable counter 3
     */
    std::string                   ctpcore_moni3_label;

    /**
     * Busy percentage of ROI SLINK FIFO
     */
    double                        ctpcore_roi_slink_rate;

    /**
     * Busy percentage of DAQ SLINK FIFO
     */
    double                        ctpcore_daq_slink_rate;

    /**
     * Overflow flag (CTPCORE)
     */
    bool                          ctpcore_overflow;

    /**
     * CTPOUT #12 busy percentages
     */
    std::vector<double>           ctpout_12;

    /**
     * Overflow flag (CTPOUT)
     */
    bool                          ctpout_12_overflow;

    /**
     * CTPOUT #13 busy percentages
     */
    std::vector<double>           ctpout_13;

    /**
     * Overflow flag (CTPOUT)
     */
    bool                          ctpout_13_overflow;

    /**
     * CTPOUT #14 busy percentages
     */
    std::vector<double>           ctpout_14;

    /**
     * Overflow flag (CTPOUT)
     */
    bool                          ctpout_14_overflow;

    /**
     * CTPOUT #15 busy percentages
     */
    std::vector<double>           ctpout_15;

    /**
     * Overflow flag (CTPOUT)
     */
    bool                          ctpout_15_overflow;

    /**
     * Total time of busy monitoring
     */
    double                        total_monitoring_time;


    static const ISType & type() {
	static const ISType type_ = ISCTPBUSYNamed( IPCPartition(), "" ).ISInfo::type();
	return type_;
    }

    virtual std::ostream & print( std::ostream & out ) const {
	ISNamedInfo::print( out );
	out << "ctpmi_ecr_rate: " << ctpmi_ecr_rate << "	//Busy percentage of Event Counter Reset (CTPMI)" << std::endl;
	out << "ctpmi_vme_rate: " << ctpmi_vme_rate << "	//Busy percentage of on-demand BUSY (CTPMI)" << std::endl;
	out << "ctpmi_vto0_rate: " << ctpmi_vto0_rate << "	//Busy percentage of VETO0 (CTPMI)" << std::endl;
	out << "ctpmi_vto1_rate: " << ctpmi_vto1_rate << "	//Busy percentage of VETO1 (CTPMI)" << std::endl;
	out << "ctpmi_bckp_rate: " << ctpmi_bckp_rate << "	//Busy percentage via backplane (CTPMI)" << std::endl;
	out << "ctpmi_overflow: " << ctpmi_overflow << "	//Overflow flag (CTPMI)" << std::endl;
	out << "ctpcore_rdt_rate: " << ctpcore_rdt_rate << "	//Busy percentage of readout block (CTPCORE)" << std::endl;
	out << "ctpcore_mon_rate: " << ctpcore_mon_rate << "	//Busy percentage of monitoring (CTPCORE)" << std::endl;
	out << "ctpcore_bckp_rate: " << ctpcore_bckp_rate << "	//Busy percentage via backplane (CTPCORE)" << std::endl;
	out << "ctpcore_rslt_rate: " << ctpcore_rslt_rate << "	//Busy percentage of result block (CTPCORE)" << std::endl;
	out << "ctpcore_moni0_rate: " << ctpcore_moni0_rate << "	//Busy percentage of configurable counter 0" << std::endl;
	out << "ctpcore_moni1_rate: " << ctpcore_moni1_rate << "	//Busy percentage of configurable counter 1" << std::endl;
	out << "ctpcore_moni2_rate: " << ctpcore_moni2_rate << "	//Busy percentage of configurable counter 2" << std::endl;
	out << "ctpcore_moni3_rate: " << ctpcore_moni3_rate << "	//Busy percentage of configurable counter 3" << std::endl;
	out << "ctpcore_moni0_label: " << ctpcore_moni0_label << "	//Name configurable counter 0" << std::endl;
	out << "ctpcore_moni1_label: " << ctpcore_moni1_label << "	//Name configurable counter 1" << std::endl;
	out << "ctpcore_moni2_label: " << ctpcore_moni2_label << "	//Name configurable counter 2" << std::endl;
	out << "ctpcore_moni3_label: " << ctpcore_moni3_label << "	//Name configurable counter 3" << std::endl;
	out << "ctpcore_roi_slink_rate: " << ctpcore_roi_slink_rate << "	//Busy percentage of ROI SLINK FIFO" << std::endl;
	out << "ctpcore_daq_slink_rate: " << ctpcore_daq_slink_rate << "	//Busy percentage of DAQ SLINK FIFO" << std::endl;
	out << "ctpcore_overflow: " << ctpcore_overflow << "	//Overflow flag (CTPCORE)" << std::endl;
	out << "ctpout_12[" << ctpout_12.size() << "]:	//CTPOUT #12 busy percentages" << std::endl;
	for ( size_t i = 0; i < ctpout_12.size(); ++i )
	    out << i << ": " << ctpout_12[i] << std::endl;
	out << "ctpout_12_overflow: " << ctpout_12_overflow << "	//Overflow flag (CTPOUT)" << std::endl;
	out << "ctpout_13[" << ctpout_13.size() << "]:	//CTPOUT #13 busy percentages" << std::endl;
	for ( size_t i = 0; i < ctpout_13.size(); ++i )
	    out << i << ": " << ctpout_13[i] << std::endl;
	out << "ctpout_13_overflow: " << ctpout_13_overflow << "	//Overflow flag (CTPOUT)" << std::endl;
	out << "ctpout_14[" << ctpout_14.size() << "]:	//CTPOUT #14 busy percentages" << std::endl;
	for ( size_t i = 0; i < ctpout_14.size(); ++i )
	    out << i << ": " << ctpout_14[i] << std::endl;
	out << "ctpout_14_overflow: " << ctpout_14_overflow << "	//Overflow flag (CTPOUT)" << std::endl;
	out << "ctpout_15[" << ctpout_15.size() << "]:	//CTPOUT #15 busy percentages" << std::endl;
	for ( size_t i = 0; i < ctpout_15.size(); ++i )
	    out << i << ": " << ctpout_15[i] << std::endl;
	out << "ctpout_15_overflow: " << ctpout_15_overflow << "	//Overflow flag (CTPOUT)" << std::endl;
	out << "total_monitoring_time: " << total_monitoring_time << "	//Total time of busy monitoring" << std::endl;
	return out;
    }

    ISCTPBUSYNamed( const IPCPartition & partition, const std::string & name )
      : ISNamedInfo( partition, name, "ISCTPBUSY" )
    {
	initialize();
    }

    ~ISCTPBUSYNamed(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    ISCTPBUSYNamed( const IPCPartition & partition, const std::string & name, const std::string & type )
      : ISNamedInfo( partition, name, type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << ctpmi_ecr_rate << ctpmi_vme_rate << ctpmi_vto0_rate << ctpmi_vto1_rate << ctpmi_bckp_rate;
	out << ctpmi_overflow << ctpcore_rdt_rate << ctpcore_mon_rate << ctpcore_bckp_rate;
	out << ctpcore_rslt_rate << ctpcore_moni0_rate << ctpcore_moni1_rate << ctpcore_moni2_rate;
	out << ctpcore_moni3_rate << ctpcore_moni0_label << ctpcore_moni1_label << ctpcore_moni2_label;
	out << ctpcore_moni3_label << ctpcore_roi_slink_rate << ctpcore_daq_slink_rate << ctpcore_overflow;
	out << ctpout_12 << ctpout_12_overflow << ctpout_13 << ctpout_13_overflow << ctpout_14;
	out << ctpout_14_overflow << ctpout_15 << ctpout_15_overflow << total_monitoring_time;
    }

    void refreshGuts( ISistream & in ){
	in >> ctpmi_ecr_rate >> ctpmi_vme_rate >> ctpmi_vto0_rate >> ctpmi_vto1_rate >> ctpmi_bckp_rate;
	in >> ctpmi_overflow >> ctpcore_rdt_rate >> ctpcore_mon_rate >> ctpcore_bckp_rate;
	in >> ctpcore_rslt_rate >> ctpcore_moni0_rate >> ctpcore_moni1_rate >> ctpcore_moni2_rate;
	in >> ctpcore_moni3_rate >> ctpcore_moni0_label >> ctpcore_moni1_label >> ctpcore_moni2_label;
	in >> ctpcore_moni3_label >> ctpcore_roi_slink_rate >> ctpcore_daq_slink_rate >> ctpcore_overflow;
	in >> ctpout_12 >> ctpout_12_overflow >> ctpout_13 >> ctpout_13_overflow >> ctpout_14;
	in >> ctpout_14_overflow >> ctpout_15 >> ctpout_15_overflow >> total_monitoring_time;
    }

private:
    void initialize()
    {
	ctpmi_ecr_rate = 0.0;
	ctpmi_vme_rate = 0.0;
	ctpmi_vto0_rate = 0.0;
	ctpmi_vto1_rate = 0.0;
	ctpmi_bckp_rate = 0.0;
	ctpmi_overflow = 0;
	ctpcore_rdt_rate = 0.0;
	ctpcore_mon_rate = 0.0;
	ctpcore_bckp_rate = 0.0;
	ctpcore_rslt_rate = 0.0;
	ctpcore_moni0_rate = 0.0;
	ctpcore_moni1_rate = 0.0;
	ctpcore_moni2_rate = 0.0;
	ctpcore_moni3_rate = 0.0;
	ctpcore_roi_slink_rate = 0.0;
	ctpcore_daq_slink_rate = 0.0;
	ctpcore_overflow = 0;
	ctpout_12.resize( 5 );
	ctpout_12[0] = 0;
	ctpout_12[1] = 0;
	ctpout_12[2] = 0;
	ctpout_12[3] = 0;
	ctpout_12[4] = 0;
	ctpout_12_overflow = 0;
	ctpout_13.resize( 5 );
	ctpout_13[0] = 0;
	ctpout_13[1] = 0;
	ctpout_13[2] = 0;
	ctpout_13[3] = 0;
	ctpout_13[4] = 0;
	ctpout_13_overflow = 0;
	ctpout_14.resize( 5 );
	ctpout_14[0] = 0;
	ctpout_14[1] = 0;
	ctpout_14[2] = 0;
	ctpout_14[3] = 0;
	ctpout_14[4] = 0;
	ctpout_14_overflow = 0;
	ctpout_15.resize( 5 );
	ctpout_15[0] = 0;
	ctpout_15[1] = 0;
	ctpout_15[2] = 0;
	ctpout_15[3] = 0;
	ctpout_15[4] = 0;
	ctpout_15_overflow = 0;
	total_monitoring_time = 0.0;

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const ISCTPBUSYNamed & info ) {
    info.print( out );
    return out;
}

#endif // ISCTPBUSYNAMED_H
