//
// dal_print_partition_env.cc
//
// This program returns the environment for the partition as needed
// by play_daq and setup_daq
//
#include <unistd.h>
#include <pwd.h>

#include <string>
#include <map>
#include <vector>
#include <iostream>

#include <boost/program_options.hpp>

#include "system/Host.h"

#include "config/ConfigObject.h"
#include "config/Configuration.h"

#include "dal/Partition.h"
#include "dal/Segment.h"
#include "dal/OnlineSegment.h"
#include "dal/Application.h"
#include "dal/Binary.h"
#include "dal/Computer.h"
#include "dal/SW_Object.h"
#include "dal/Variable.h"
#include "dal/RunControlApplicationBase.h"
#include "dal/util.h"

#include "RunControl/Common/Exceptions.h"

const std::map<std::string, std::string> * conv_map;
std::string command_word;
std::string separator;
std::string finaliser;
bool var_not_set;

namespace po = boost::program_options;

void add_to_map(const std::string& var_name, std::map<std::string, std::string>* env_map) {
    std::string var_string = "${" + var_name + "}";
    std::string var_value = "";
    try {
        var_value = daq::core::substitute_variables(var_string, conv_map, "${", "}");
    }
    catch(ers::Issue &e) {
        ers::error(daq::rc::ConfigurationIssue(ERS_HERE, "Parameter substitution failed", e));
    }

    if(var_value != var_string) {
        // Substitution has occured so enter it into the map
        (*env_map)[var_name] = var_value;
    } else {
        // No Substitution so print an error, set the error flag and continue
        std::cerr << "Error: Variable '" << var_name
                << "' not defined in database or not included in the Partition, Parameters field." << std::endl;
        var_not_set = true;
    }
}

void print_env_map(std::map<std::string, std::string>* env_map) {
    std::map<std::string, std::string>::iterator map_iter;
    for(map_iter = env_map->begin(); map_iter != env_map->end(); map_iter++) {
        std::cout << command_word << map_iter->first << separator << map_iter->second << finaliser << std::endl;
    }
}

int main(int argc, char ** argv) {
    std::string partition(""), tdaqDb("");
    bool csh;
    if(getenv("TDAQ_PARTITION"))
        partition = getenv("TDAQ_PARTITION");
    if(getenv("TDAQ_DB"))
        tdaqDb = getenv("TDAQ_DB");

    try {
        po::options_description desc("This program gets the environment for the partition as needed by setup_daq.");

        desc.add_options()
             ("partition,p", po::value<std::string>(&partition)->default_value(partition), "Name of the partition(default is $TDAQ_PARTITION)")
             ("tdaqDb,d", po::value<std::string>(&tdaqDb)->default_value(tdaqDb), "Name of the db (default is $TDAQ_DB)")
             ("csh_syntax,c", po::value<bool>(&csh)->default_value(false), "Use csh syntax (default is sh syntax)")
             ("help,h", "Print help message");

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        if(vm.count("help")) {
            std::cout << desc << std::endl;
            return EXIT_SUCCESS;
        }

        if(partition.empty()) {
            ers::error(daq::rc::CmdLineError(ERS_HERE,"Missing partition name!"));
            std::cout << desc << std::endl;
            return EXIT_FAILURE;
        }
        if(tdaqDb.empty()) {
            ers::error(daq::rc::CmdLineError(ERS_HERE,"Missing database name!"));
            std::cout << desc << std::endl;
            return EXIT_FAILURE;
        }
    }
    catch(std::exception& ex) {
        ers::error(daq::rc::CmdLineError(ERS_HERE, "", ex));
        return EXIT_FAILURE;
    }

    // Are all variables defined and linked?
    var_not_set = false;

    // Find out what syntax to use
    bool use_sh_syntax = !csh;

    // Define the separators
    if(use_sh_syntax) {
        command_word = "export ";
        separator = "=\"";
        finaliser = "\"";
    } else {
        command_word = "setenv ";
        separator = " ";
        finaliser = "";
    }

    try {
        // Load the configuration
        Configuration conf(tdaqDb);

        if(!conf.loaded()) {
            std::cerr << "Can not load database: " << tdaqDb << std::endl;
            throw daq::rc::ConfigurationIssue(ERS_HERE, "Configuration object could not be loaded");
        }

        // Get the partition

        const daq::core::Partition* db_part = daq::core::get_partition(conf, partition);
        if(db_part == NULL) {
            throw daq::rc::ConfigurationIssue(ERS_HERE, "Partition not found");
        }

        // Register substitution function
        daq::core::SubstituteVariables* subst_var_p = new daq::core::SubstituteVariables(*db_part);
        conf.register_converter(subst_var_p);

        std::map<std::string, std::string> p_params_map;

        // Get the conversion map pointer
        conv_map = subst_var_p->get_conversion_map();

        add_to_map("TDAQ_LOGS_ROOT", &p_params_map);
        add_to_map("TDAQ_LOGS_PATH", &p_params_map);
        add_to_map("TDAQ_BACKUP_PATH", &p_params_map);
        add_to_map("TDAQ_RESULTS_LOGS_PATH", &p_params_map);
        add_to_map("TDAQ_RESULTS_FILE_NAME", &p_params_map);
        add_to_map("TDAQ_LOGS_ARCHIVE_PATH", &p_params_map);

        const daq::core::Segment* seg = db_part->get_segment(db_part->get_OnlineInfrastructure()->UID());

        p_params_map["ROOT_CONTROLLER"] = seg->get_controller()->UID();
        p_params_map["ONLINE_SEGMENT"] = seg->UID();

        // Extract environment for root controller in a single variable

        try {

            std::vector<std::string> file_names;
            std::map<std::string, std::string> environment;
            std::string x, y;

            // This is the only variable that can be missing but still allows things to work (TDAQ_DB can be used)
            if(!getenv("TDAQ_DB_DATA"))
                setenv("TDAQ_DB_DATA", "UNKNOWN", false);

            seg->get_controller()->get_info(environment,
                                            file_names,
                                            x,
                                            y);

            // Add RC host to environment
            p_params_map["RC_HOST"] = seg->get_controller()->get_host()->UID();

            std::ostringstream concatEnv;
            for(std::map<std::string, std::string>::iterator j = environment.begin(); j != environment.end(); ++j) {
                concatEnv << " -e " << j->first << "=\'" << j->second << "\'";
            }
            p_params_map["ENV_FOR_RC"] = concatEnv.str();
        }
        catch(ers::Issue& e) {
            ers::error(e);
        }

        // Set environment which is ok for starting PMG
        try {
            const daq::core::Binary * pmg =
                    conf.cast<daq::core::Binary>(db_part->get_OnlineInfrastructure()->get_PmgAgent());
            if(pmg != NULL) {
                std::vector<const daq::core::Parameter*> pmgenv = pmg->get_ProcessEnvironment();
                for(unsigned int i = 0; i < pmgenv.size(); i++) {
                    const daq::core::Variable * var = conf.cast<daq::core::Variable>(pmgenv[i]);
                    if(var)
                        p_params_map[var->get_Name()] = var->get_Value();
                }
            } else {
                throw daq::rc::ConfigurationIssue(ERS_HERE, "The mandatory Binary for the PMG agent is not defined.");
            }
        }
        catch (ers::Issue& e ) {
            ers::fatal(e);
            return EXIT_FAILURE;
        }

        // One method is to print all entries in the map
        // Print all the variables in the map (ie those which have non null value)
        print_env_map(&p_params_map);

    }
    catch(ers::Issue &e) {
        ers::fatal(e);
        return EXIT_FAILURE;
    }

    if(var_not_set) {
        // Write the general message/suggestion about definition in setup segment
        std::cerr << std::endl
                << "Error: The log path Variables which have not been found are defined with default values in the OnlineSegment Segment."
                << std::endl
                << "They are grouped into a VariableSet named 'CommonParameters' and this should be linked into the Partition Parameters field if you do not wish to change the default values."
                << std::endl << "You might have forgotten to do this in your Partition." << std::endl;
        return 4;
    }

    return 0;
}

