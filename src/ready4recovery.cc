/*
 * error_generator.cc
 *
 * This utility reproduce different kinds of application error on user demand.
 * It runs interactively, the supported errors are prompted in a menu and the chosen
 * one is reproduced and propagated.
 *
 *  Author: lmagnoni
 */

#include <iostream>
#include <unistd.h>
#include "ers/ers.h"
#include "ipc/core.h"
#include "RunControl/Common/UserExceptions.h"

int main(int argc, char **argv)
{
  std::string applicationID;
  std::string channelId;
  int waitTimeInSecond = 1; //1 second

  IPCCore::init(argc, argv);

  if ( argc < 3 ) {
	std::cout << "Usage: rc_ready4recovery ChannelID ApplicationID "<< std::endl;
	exit(1);
  }
  else {
    channelId=argv[1];
    applicationID = argv[2];
  }

  //Defining User Error Type
  daq::rc::ReadyForHardwareRecovery issueRFHR(ERS_HERE,channelId.c_str(),applicationID.c_str());
  ers::info(issueRFHR);

  //sleep to allow error propagation
  sleep(waitTimeInSecond);
  exit(0);
}
