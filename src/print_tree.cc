// $Id$
//
// ATLAS Run Control
//
// Current: G. Lehmann Miotto, September 2002 

// Print the run controller tree

#include <iostream>
#include <sstream>
#include <boost/program_options.hpp>

#include "ipc/core.h"
#include "ers/ers.h"

#include "config/Configuration.h"

#include "RunControl/Common/Exceptions.h"

#include "dal/Partition.h"
#include "dal/Segment.h"
#include "dal/BaseApplication.h"
#include "dal/OnlineSegment.h"
#include "dal/RunControlApplicationBase.h"

static bool s_showApps = false;
class PrintChildController {
    public:
        inline PrintChildController(int level) :
                level_(level)
        {
        }

        inline ~PrintChildController() {
        }

        inline void operator()(const daq::core::Segment* segment) const {
            if(segment->is_disabled() == false) {
                const daq::core::BaseApplication* app = segment->get_controller();
                std::cout << std::string(level_, '\t') << app->UID() << std::endl;
                if(s_showApps) {
                    for(const auto a : segment->get_infrastructure()) {
                        std::cout << std::string(level_ + 1, '\t') << a->UID() << std::endl;
                    }
                    for(const auto a : segment->get_applications()) {
                        std::cout << std::string(level_ + 1, '\t') << a->UID() << std::endl;
                    }
                }

            } else {
                std::cout << std::string(level_, '\t') << segment->UID() << " (DISABLED)" << std::endl;
            }

            std::for_each(segment->get_nested_segments().begin(),
                          segment->get_nested_segments().end(),
                          PrintChildController(level_ + 1));
        }

    private:
        int level_;
};

int main(int argc, char ** argv) {

    try {
        IPCCore::init(argc, argv);
    }
    catch(daq::ipc::CannotInitialize& e) {
        ers::fatal(e);
        abort();
    }
    catch(daq::ipc::AlreadyInitialized& e) {
        ers::warning(e);
    }

    // take defaults from environment
    std::string tdaq_partition(""), tdaq_db("");
    //  std::string def_p, def_d, def_a, def_l;
    if(getenv("TDAQ_PARTITION"))
        tdaq_partition = getenv("TDAQ_PARTITION");
    if(getenv("TDAQ_DB"))
        tdaq_db = getenv("TDAQ_DB");

    // parse commandline parameters

    boost::program_options::options_description desc("Allowed options", 128);
    desc.add_options()
         ("help,h", "help message")
         ("partition,p", boost::program_options::value<std::string>(&tdaq_partition)->default_value(tdaq_partition), "partition name (TDAQ_PARTITION)")
         ("database,d", boost::program_options::value<std::string>(&tdaq_db)->default_value(tdaq_db), "database (TDAQ_DB)")
         ("showapps,a", boost::program_options::value<bool>(&s_showApps)->default_value(s_showApps), "show leaf applications");

    boost::program_options::variables_map vm;
    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);

    if(vm.count("help")) {
        desc.print(std::cout);
        std::cout << std::endl;
        return EXIT_SUCCESS;
    }

    if(tdaq_partition.empty() || tdaq_db.empty()) {
        daq::rc::CmdLineError issue(ERS_HERE,"");
        ers::error(issue);
        return EXIT_FAILURE;
    }

    Configuration config(tdaq_db);

    if(!config.loaded()) {
        daq::rc::ConfigurationIssue issue(ERS_HERE,"Failed to load database");
        ers::fatal(issue);
        return EXIT_FAILURE;
    }

    // find root controller in the root partition

    const daq::core::Partition * partition = config.get<daq::core::Partition>(tdaq_partition);
    if(!partition) {
        daq::rc::ConfigurationIssue issue(ERS_HERE,"partition not found ", tdaq_partition.c_str());
        ers::fatal(issue);
        return EXIT_FAILURE;
    }

    const daq::core::Segment * online = partition->get_OnlineInfrastructure();
    if(!online) {
        daq::rc::ConfigurationIssue issue(ERS_HERE,"segment not found ", "OnlineInfrastructure");
        ers::fatal(issue);
        return EXIT_FAILURE;
    }

    try {
        const daq::core::Segment* seg = partition->get_segment(online->UID());

        std::cout << "Root:\t" << seg->get_controller()->UID() << std::endl;

        std::for_each(seg->get_nested_segments().begin(),
                      seg->get_nested_segments().end(),
                      PrintChildController(2));
    }
    catch(ers::Issue& ex) {
        ers::fatal(ex);
        return EXIT_SUCCESS;
    }

    return EXIT_SUCCESS;
}
