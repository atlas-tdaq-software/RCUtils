// $Id$
//
// ATLAS Run Control
//
// Author: Bob Jones, Sarah Wheeler, Dietmar Schweiger
// Current: G. Lehmann Miotto 2005 

#include <string>
#include <cstddef>
#include <cstring>

#include "cmdl/cmdargs.h"
#include "ers/ers.h"
#include "ipc/core.h"
#include "TimeTest.h"
#include "RunControl/Common/Exceptions.h"
#include "is/infodictionary.h"

inline std::string str(const char * word)
{
   return word ? word : "";
} 

int main(int argc, char ** argv)
{ 
   try{
   	IPCCore::init(argc,argv);
   }catch(daq::ipc::CannotInitialize& e){
   	ers::fatal(e);
	abort();
   }catch(daq::ipc::AlreadyInitialized& e){
   	ers::warning(e);
   }
   CmdArgStr	partition  ('p',"partition",  "partition",  "partition name (default $TDAQ_PARTITION)");
   CmdArgStr	controller ('c',"controller", "controller", "RC controller name", CmdArg::isREQ);
   CmdArgStr	server	  ('s',"server",     "IS-server",  "optional IS server name (default RunCtrl)");
   CmdArgStr	initial    ('I',"initial",    "state",      "initial state (none, booted, initial, configured, connected, running)", CmdArg::isREQ);
   CmdArgStr	final      ('F',"final",      "state",      "final state", CmdArg::isREQ);
   CmdArgFloat timeout    ('T',"timeout",    "seconds",    "timeout for execution of one command (default 60).");
   CmdArgInt	cycles	  ('N',"cycles",     "number",     "number of measured cycles (default 1)");			
   CmdArgInt	verbosity  ('v',"verbosity",  "level",      "trace output (default 0)");

   partition=std::getenv("TDAQ_PARTITION");
   server="RunCtrl";
   timeout=60;
   cycles=1;
   verbosity=0;

   CmdLine  cmd(*argv,&partition,&controller,&server,&initial,&final,&timeout,&cycles,&verbosity,0);
	
   cmd.description("This test program sends the appropriate commands to a RC-controller to cycle it between two states and measures the time taken.");

   CmdArgvIter	argvIter (--argc,++argv);
   
   if (cmd.parse(argvIter))
   {
      daq::rc::CmdLineError issue(ERS_HERE,"");
      ers::error(issue);
      return EXIT_FAILURE;
   }

   IPCPartition p (partition);
   daq::RCUtils::TimeTest timeTest(p,str(controller),str(server));
   
   timeTest.verbosity(verbosity);
   timeTest.timeout(timeout);
   
   int initialState = daq::RCUtils::TimeTest::state2Index(str(initial));
   if ( initialState < 0 )
   {
      ERS_INFO("Initial state " << str(initial).c_str() << " does not exist.");
      return 101;
   }
   int finalState = daq::RCUtils::TimeTest::state2Index(str(final));
   if ( finalState < 0 )
   {
      ERS_INFO("Final state " << str(final).c_str() << " does not exist.");
      return 102;
   }
   
   int rc = timeTest.cycle(initialState,finalState,cycles);
   
   std::cout << timeTest.averageTime() << std::endl;
   
   return rc;

}
