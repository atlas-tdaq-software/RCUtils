//************************
//
// ers_sound_player.cc
// 
// Subscribes to some ERS issues and plays a corresponding sound, if defined
// by an audio file with the same name.
// Author: G. Lehmann Miotto
//************************

#include <boost/program_options.hpp>
#include <signal.h>

#include "ers/ers.h"
#include "ipc/core.h"
#include "system/File.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/UserExceptions.h"

bool running = false;

class ERSReceiver : public ers::IssueReceiver {
public:
    ERSReceiver(int volume, std::string & files_path):m_volume(volume),m_filepath(files_path+"/"){} ///< ctor

    void receive(const ers::Issue& issue) {
        ERS_DEBUG(2,"Received an issue of class = " << issue.get_class_name());

	// Check if file is there (should also check the permissions...)
	System::File file(m_filepath +  issue.get_class_name() + ".au");
	try {
	  file.permissions();
	}
	catch(ers::Issue &e) {
	  ers::warning(e);
	  return;
	}
	std::ostringstream cmd;
	cmd << "/usr/bin/play -v " << m_volume << " " << file.full_name();
	system(cmd.str().c_str());
    }
private:
  int m_volume;
  std::string m_filepath;
};
	
void signal_handler(int) {
  running = false;
}

int main(int argc, char ** argv)
{
  try{
    IPCCore::init(argc,argv);
  }catch(daq::ipc::CannotInitialize& e){
    ers::fatal(e);
    abort();
  }catch(daq::ipc::AlreadyInitialized& e){
    ers::warning(e);
  }

  // take defaults from environment
  std::string tdaq_partition(""), tdaq_audio("");
  if (getenv("TDAQ_PARTITION"))
     tdaq_partition = getenv("TDAQ_PARTITION");
  if (getenv("TDAQ_INST_PATH")) {
    tdaq_audio = getenv("TDAQ_INST_PATH");
    tdaq_audio.append("/share/data/mrs/audio");
  }
  int vol = 1;


  boost::program_options::options_description desc("This program listens on the MRS stream and issues sounds for specific messages", 128);
  desc.add_options()
    ("help,h", "help message")
    ("partition,p",    boost::program_options::value<std::string>(&tdaq_partition)->default_value(tdaq_partition), " TDAQ_PARTITION ")
    ("audiofiles,a",   boost::program_options::value<std::string>(&tdaq_audio)->default_value(tdaq_audio), " Directory containing audio files ")
    ("volume,v",       boost::program_options::value<int>(&vol)->default_value(vol), " 1 to 10 ");

  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::parse_command_line(argc,argv,desc), vm);
  boost::program_options::notify(vm);

  if(vm.count("help")) {
    desc.print(std::cout);
    std::cout << std::endl;
    return EXIT_SUCCESS;
  }

  if(tdaq_partition.empty() || tdaq_audio.empty()) {
    daq::rc::CmdLineError issue(ERS_HERE,"the partition and the audio file cannot be empty");
    ers::error(issue);
    return EXIT_FAILURE;
   }

  signal(SIGINT,signal_handler);
  signal(SIGTERM, signal_handler);
  
  ERSReceiver receiver(vol, tdaq_audio);
  ers::StreamManager::instance().add_receiver("mrs", "rc::StartOfRun", &receiver);
  ers::StreamManager::instance().add_receiver("mrs", "rc::EndOfRun", &receiver);
  ers::StreamManager::instance().add_receiver("mrs", "rc::HardwareError", &receiver);
  ers::StreamManager::instance().add_receiver("mrs", "rc::HardwareRecovered", &receiver);
  ers::StreamManager::instance().add_receiver("mrs", "rc::ReadyForHardwareRecovery", &receiver);
  ers::StreamManager::instance().add_receiver("mrs", "rc::HardwareSynchronization", &receiver);	
  ers::StreamManager::instance().add_receiver("mrs", "rc::PixelUp", &receiver);	
  ers::StreamManager::instance().add_receiver("mrs", "rc::PixelDown", &receiver);	
  
  running = true;  
  while (running) {
    sleep(1);
  }
  
  return 0;
}






















