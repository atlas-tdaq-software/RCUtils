// $Id$
//
// ATLAS Run Control
//
// Author: Dietrich Liko, Giovanna Lehmann

// Log the Run Control status to stdout

#include <unistd.h>
#include <sstream>

#include "ipc/core.h"
#include "is/infodictionary.h"
#include "ers/ers.h"
#include "rc/RCStateInfo.h"
#include "cmdl/cmdargs.h"
#include "RunControl/Common/Exceptions.h"
#include "Exceptions.h"

inline std::string str(const char * word)
{
   return word ? word : "";
} 


int main(int argc, char ** argv) 
{

  try{
   	IPCCore::init(argc,argv);
   }catch(daq::ipc::CannotInitialize& e){
   	ers::fatal(e);
	abort();
   }catch(daq::ipc::AlreadyInitialized& e){
   	ers::warning(e);
   } 
   CmdArgStr	partition	('p',"partition", "partition", "IPC partition name (default $TDAQ_PARTITION)");
   CmdArgStr	name 	      ('n',"name","controller","RC controller",CmdArg::isREQ);
   CmdArgStr	server   	('s',"server", "server", "IS server name (default RunCtrl)");
   CmdArgInt	waitTime   	   ('W',"wait","time","to wait for the requested state (default 0)");
   CmdArgStr   state       ("state","to wait for",CmdArg::isREQ);

   partition = std::getenv("TDAQ_PARTITION");
   waitTime = 0;
   server = "RunCtrl";
   
   CmdLine	cmd(*argv,&partition,&name,&server,&waitTime,&state,NULL);

   CmdArgvIter	argvIter(--argc,++argv);

   cmd.description("This program waits for the controller to go into the requested state.");

   if (cmd.parse(argvIter)) 
   {
    daq::rc::CmdLineError issue(ERS_HERE,"");
     ers::error(issue);
     return EXIT_FAILURE;
   }

   IPCPartition p(partition);
   ISInfoDictionary isInfoDict(p);

   RCStateInfo stateInfo;
   std::string isname = str(server) + "." + str(name); 

   int32_t waiting = waitTime;
   while ( (waitTime == 0) || (waiting > 0) ) {
     try 	{
       isInfoDict.findValue(isname.c_str(),stateInfo) ;
       if ( stateInfo.fault ) {
	 daq::rc::BadControllerState issue(ERS_HERE,"");
	 ers::fatal(issue);
	 return 108;
       }	
       if ( ! stateInfo.busy && strcasecmp(stateInfo.state.c_str(),state) == 0 ) {
	 ERS_LOG("State " << str(state).c_str() << " reached.");
	 return EXIT_SUCCESS;
       }
     }
     catch ( daq::is::Exception & ex ) {// FIXME or only NotFound?
       ers::warning (ex);
     }
     sleep(1);
     --waiting;
     
   }
   
   std::string reason = "Timed out waiting for state " + str(state);
   daq::rc::Timeout issue(ERS_HERE,reason.c_str());
   ers::fatal(issue);
   return 107;

 }
