// $Id$
//
// ATLAS Run Control
//
// Author: Dietrich Liko, Giovanna Lehmann 

#include "TimeTest.h"
#include "ers/ers.h"
#include "ipc/partition.h"
#include "ipc/alarm.h"
#include "rc/RCStateInfo.h"
#include "Exceptions.h"
#include "owl/semaphore.h"

#include <RunControl/Common/RunControlCommands.h>
#include <RunControl/FSM/FSMCommands.h>
#include <RunControl/FSM/FSMStates.h>

OWLSemaphore sem;

daq::RCUtils::TimeTest::TimeTest(IPCPartition & partition,const std::string & controller,const std::string & server) :
   isInfoReceiver_    (partition),
   isInfoDictionary_  (partition),
   commander_         (partition.name(), "TimeTest"),
   timeoutAlarm_      (0),
   controller_        (controller),
   stateInfoName_     (server+"."+controller),
   timeout_           (0),
   verbosity_(0)
{
}

daq::RCUtils::TimeTest::~TimeTest() 
{
   delete timeoutAlarm_;
}


const std::string daq::RCUtils::TimeTest::s_states [] =
{
    daq::rc::FSMStates::NONE_STATE,
	daq::rc::FSMStates::INITIAL_STATE,
	daq::rc::FSMStates::CONFIGURED_STATE,
	daq::rc::FSMStates::CONNECTED_STATE,
	daq::rc::FSMStates::RUNNING_STATE,
	"Unknown"
};

int daq::RCUtils::TimeTest::state2Index(const std::string & state)
{
	
	for (int i = 0; i<5; ++i)
	{
		if ( strcasecmp(state.c_str(),s_states[i].c_str()) == 0 ) return i;
	}
	return -1;
}

const std::string & daq::RCUtils::TimeTest::index2State(int state)
{
	if ( state<0 || state>4 ) return s_states[5];
	return s_states[state];
}


void daq::RCUtils::TimeTest::timeout(double timeout) 
{
   timeout_ = timeout;
}

void daq::RCUtils::TimeTest::verbosity(int verbosity) 
{
   verbosity_ = verbosity;
}

void daq::RCUtils::TimeTest::startTimeout()
{
   if ( timeoutAlarm_ )
   {
      ERS_DEBUG(0,"Timeout already set.");
      return;
   }
   timeoutAlarm_ = new IPCAlarm(timeout_,timeoutCallback,this);
}

void daq::RCUtils::TimeTest::clearTimeout()
{
   if ( ! timeoutAlarm_ ) return;
   delete timeoutAlarm_;
   timeoutAlarm_ = 0;
}


int daq::RCUtils::TimeTest::cycle(int initial,int final,int cycles) 
{
   
   RCStateInfo stateInfo;
   try { isInfoDictionary_.findValue(stateInfoName_.c_str(),stateInfo); }
   catch ( daq::is::Exception & ex )   {
     daq::rc::ISReadFailure issue(ERS_HERE, " ", ex);
     ers::fatal(issue);
     return 104;
   }
	
   if ( strcasecmp(stateInfo.state.c_str(),index2State(initial).c_str()) != 0 || stateInfo.busy || stateInfo.fault )
   {
     ERS_DEBUG(0,"Controller is not in right initial state " << stateInfo.state.c_str() << ".");
     return 105;      
   }

   initial_ = initial;
   final_   = final;
   cycles_  = cycles;
   
   current_ = 0;
   totalTime_ = 0;  
   returnCode_ = 0;
   
   try { isInfoReceiver_.subscribe(stateInfoName_.c_str(),stateChangeCallback,this); }
   catch ( daq::is::Exception & ex )   {
     daq::rc::ISReadFailure issue(ERS_HERE, "Failed to subscribe to IS: ", ex);
     ers::fatal(issue);
     return 110;
   }
   
   if ( ! nextStep(stateInfo) )
   {
      ERS_DEBUG(0,"Failing to start cycle");
      return 111;
   }
   
   sem.wait();

   isInfoReceiver_.unsubscribe(stateInfoName_.c_str());
   
   ERS_DEBUG(0,"Average time per cycle : " << averageTime() << ".");
   
   return returnCode_;
}


void daq::RCUtils::TimeTest::stateChangeCallback(ISCallbackInfo * isc) 
{
   TimeTest * that = static_cast<TimeTest*>(isc->parameter());
   
   RCStateInfo stateInfo;
   try { isc->value(stateInfo) ; }
   catch ( daq::is::Exception & ex )
   {
     daq::rc::ISReadFailure issue(ERS_HERE, "", ex);
     ers::fatal(issue);
      that->returnCode_ = 106;
      sem.post();
      return;
   }
	
   if (stateInfo.lastCmdName == "PUBLISH") return;
      
   if (!that->nextStep(stateInfo)) 
   {   
     sem.post();
   }
}

bool daq::RCUtils::TimeTest::timeoutCallback(void * parameter) 
{
   TimeTest * that = static_cast<TimeTest*>(parameter);
   
   ERS_DEBUG(0,"Timeout!!!");
   that->returnCode_ = 107;
   sem.post();
   return false;         
}   

const std::string & daq::RCUtils::TimeTest::nextCommand(int state) 
{
	
	static const std::string empty = "";
	
   if ( state == initial_ )
   {
      tp_ = std::chrono::steady_clock::now();
      reverse_ = false;
      ++current_;
   } 

   if ( state == final_ )
   {
      auto now = std::chrono::steady_clock::now();
      std::chrono::duration<double> diff = now - tp_;
      totalTime_ += diff.count();
      ERS_DEBUG(1,"Cycle " << current_ << " took " << diff.count() << " seconds");
      if ( current_ >= cycles_) 
      {
      	 return empty;
      }
      else
      {
      	 reverse_ = true;
      }
   } 
     
	static const std::string commandForward [] =
	{
	 daq::rc::FSMCommands::INITIALIZE_CMD,
	 daq::rc::FSMCommands::CONFIGURE_CMD,
	 daq::rc::FSMCommands::CONNECT_CMD,
	 daq::rc::FSMCommands::START_CMD,
	  ""
	};
	
	static const std::string commandBackward [] =
	{
		"",
		daq::rc::FSMCommands::SHUTDOWN_CMD,
		daq::rc::FSMCommands::UNCONFIGURE_CMD,
 		daq::rc::FSMCommands::DISCONNECT_CMD,
 		daq::rc::FSMCommands::STOP_CMD
	};
	
   if ( (initial_ < final_ && !reverse_) || (initial_ > final_ && reverse_ ))
   {
		if ( state < 0 || state > 4 ) return empty;
		return commandForward[state];
   }
   else
   {
		if ( state < 1 || state > 4 ) return empty;
		return commandBackward[state];
   }
}

bool daq::RCUtils::TimeTest::nextStep(RCStateInfo & stateInfo)
{

   if ( stateInfo.fault)
   {
      daq::rc::BadControllerState issue(ERS_HERE,"");
      ers::error(issue);
      returnCode_ = 108;
      return false;
   }
   
   if ( stateInfo.busy ) return true;

	int state = state2Index(stateInfo.state);
	if ( state < 0 )
	{
      return true;  
	}
		
   clearTimeout();
      
   std::string command = nextCommand(state);
   if ( command.empty() ) return false;
   
   ERS_DEBUG(1,"Command " << command.c_str());
   commander_.makeTransition(controller_, daq::rc::TransitionCmd(daq::rc::FSMCommands::stringToCommand(command)));
   startTimeout();

   return true;
    
}

double daq::RCUtils::TimeTest::averageTime() const
{
   return current_ > 0 ? totalTime_ / current_ : -1.;
}
