// Id: get_detectormask.cc
//
// ATLAS Run Control
//
// Author: Giovanna Lehmann Miotto, August 2007

// interprete the detector mask

#include <iostream>
#include <unistd.h>
#include <boost/program_options.hpp>
#include "eformat/DetectorMask.h"
#include "eformat/SourceIdentifier.h"

typedef struct {
  short dId;
  std::string dName;
} T_DetectorInfo;


// MAIN
int main(int ac, char ** av) 
{
  std::string detm;

  // parse commandline parameters

  boost::program_options::options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "help message")
    ("detector_mask,d",  boost::program_options::value<std::string>(), "detector mask")
    ;

  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::parse_command_line(ac,av,desc), vm);
  boost::program_options::notify(vm);

  if(vm.count("help")) {
    std::cout << desc << std::endl;
    exit (0);
  }
  if(vm.count("detector_mask")) {
    detm = vm["detector_mask"].as<std::string>();
    std::cout << "Detector mask is " << detm << std::endl;
  }
  else {
    std::cout << "Detector mask was not set \n" << desc << std::endl;
    exit (EXIT_FAILURE);
  }

  eformat::helper::DetectorMask detMask(detm);
  std::vector<eformat::SubDetector> ids;
  detMask.sub_detectors(ids);
  for(size_t n=0; n < ids.size() ; n++) {
    eformat::helper::SourceIdentifier sid(ids[n],0,0);
    std::cout << "Detector 0x" <<std::hex <<  ids[n] << std::dec
	      << " = " << sid.human_detector() << " is active."<<std::endl;
  }
   exit (EXIT_SUCCESS);
}
