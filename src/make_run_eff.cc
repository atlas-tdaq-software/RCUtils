#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <vector>
#include <signal.h>

#include <boost/program_options.hpp>

#include <CoralBase/Attribute.h>
#include <CoolKernel/Exception.h>
#include <CoolKernel/ValidityKey.h>
#include <CoolKernel/IDatabaseSvc.h>
#include <CoolKernel/IDatabase.h>
#include <CoolKernel/IFolder.h>
#include <CoolKernel/IRecordSpecification.h>
#include <CoolKernel/Record.h>
#include <CoolApplication/DatabaseSvcFactory.h>
#include <CoolKernel/IObjectIterator.h>
#include <CoolKernel/IObject.h>
#include <CoolKernel/ChannelSelection.h>
#include <CoolApplication/Application.h>

#include "owl/time.h"

#include "RunControl/Common/Exceptions.h"
#include "Exceptions.h"

namespace daq {
	ERS_DECLARE_ISSUE(rc,
		BadCoolDb,
		"Retrieving data from COOL failed because " << explanation,
		((const char*)explanation)
	)
}

extern "C" void signalhandler(int)
{
	signal(SIGINT, SIG_DFL);
	signal(SIGTERM, SIG_DFL);
	std::cout << "Terminating run_eff_new..." << std::endl;
}

static     cool::IDatabasePtr db;
static     cool::IDatabasePtr dbd;
static     cool::IDatabasePtr dbt;
static     cool::IDatabasePtr dbm;
static     cool::Application cool_app;

static     std::string dbNameTDAQ = "COOLONL_TDAQ/CONDBR2";
static     std::string dbNameDCS = "COOLOFL_DCS/CONDBR2";
static     std::string dbNameTRIGGER = "COOLONL_TRIGGER/CONDBR2";
static     std::string dbNameMONITOR = "COOLONL_TRIGGER/MONP200";
static     std::string FillState_Name = "/LHC/DCS/FILLSTATE";
static     std::string L1Counters_Name = "/TRIGGER/LUMI/LVL1COUNTERS";
static     std::string L1Menu_Name = "/TRIGGER/LVL1/Menu";
static     std::string LB_Name = "/TRIGGER/LUMI/LBLB";
static     std::string LBT_Name = "/TRIGGER/LUMI/LBTIME";
static     std::string DataTakingMode_Name = "/TDAQ/RunCtrl/DataTakingMode";
static     std::string BusyConf_Name = "/TRIGGER/LVL1/BUSYCONF";
static     std::string BusyRate_Name = "/TRIGGER/LVL1/BUSYRATES_RUN2";
static     std::string StopReason_Name = "/TDAQ/RunCtrl/StopReason";
static     std::string HoldTrigger_Name = "/TDAQ/RunCtrl/HoldTriggerReason";
static	   std::string t0Project_Name = "/TDAQ/RunCtrl/SOR";
static     std::string EOR_Name = "/TDAQ/RunCtrl/EOR";
static     std::string SOR_Name = "/TDAQ/RunCtrl/SOR";
static     std::string TotalInt_Name = "/LHC/DCS/BPTX/TOTALINT";
static     cool::IFolderPtr FillState_Folder;
static     cool::IFolderPtr L1Counters_Folder;
static     cool::IFolderPtr L1Menu_Folder;
static     cool::IFolderPtr LB_Folder;
static     cool::IFolderPtr LBT_Folder;
static     cool::IFolderPtr DataTakingMode_Folder;
static     cool::IFolderPtr BusyConf_Folder;
static     cool::IFolderPtr BusyRate_Folder;
static     cool::IFolderPtr StopReason_Folder;
static     cool::IFolderPtr HoldTrigger_Folder;
static	   cool::IFolderPtr t0Project_Folder;
static	   cool::IFolderPtr EOR_Folder;
static	   cool::IFolderPtr SOR_Folder;
static     cool::IFolderPtr TotalInt_Folder;

void getRunInfo(long long starttime_ns, long long endtime_ns);
void getStopInfo(long long starttime_ns, long long endtime_ns);
std::string get_date_OWL(long long nseconds, std::string mode = "output");
std::string takeCharOut(std::string text, char takeout = ' ');

int main(int argc, char** argv) {

	std::string startTime, endTime, timeStampPath;
	
	boost::program_options::options_description desc("This program retrieves info from COOL conditions database, evaluates the data taking efficiency and the inneficiency sources for selected time intervals and outputs the results in the standard output stream. The granularity of the information is per lumi block.");
	desc.add_options()
		("help,h", "help message")
		("startTime,s", boost::program_options::value<std::string>(), "Start time (epoch time in ns)")
		("endTime,e", boost::program_options::value<std::string>(), "End time (epoch time in ns)")
		("timeStampPath,p", boost::program_options::value<std::string>(&timeStampPath)->default_value(timeStampPath), "Timestamp file path (default: not writing the timestamp file)")
		("busyFolder,f", boost::program_options::value<std::string>(&BusyRate_Name)->default_value(BusyRate_Name), "Busy folder name (default /TRIGGER/LVL1/BUSYRATES_RUN2")
		;

	boost::program_options::variables_map vm;
	boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
	boost::program_options::notify(vm);

	if (vm.count("help")) {
		std::cout << desc << std::endl;
		return EXIT_SUCCESS;
	}
	if ((vm.count("startTime")) && (vm.count("endTime"))) {
		startTime = vm["startTime"].as<std::string>();
		endTime = vm["endTime"].as<std::string>();
	}
	else {
		std::cout << desc << std::endl;
		daq::rc::CmdLineError issue(ERS_HERE, "");
		ers::fatal(issue);
		return EXIT_FAILURE;
	}
	timeStampPath = vm["timeStampPath"].as<std::string>();
	unsigned long long StartTimeNS = atoll(startTime.c_str());
	unsigned long long EndTimeNS = atoll(endTime.c_str());
	StartTimeNS += 60'000'000'000;

	getRunInfo(StartTimeNS, EndTimeNS);
	getStopInfo(StartTimeNS, EndTimeNS);

	return EXIT_SUCCESS;
}

void getRunInfo(long long starttime_ns, long long endtime_ns) {
	try {
		db = cool_app.databaseService().openDatabase(dbNameTDAQ);
		dbd = cool_app.databaseService().openDatabase(dbNameDCS);
		dbt = cool_app.databaseService().openDatabase(dbNameTRIGGER);
		dbm = cool_app.databaseService().openDatabase(dbNameMONITOR);
		LBT_Folder = dbt->getFolder(LBT_Name);
		LB_Folder = dbt->getFolder(LB_Name);
		FillState_Folder = dbd->getFolder(FillState_Name);
		BusyConf_Folder = dbm->getFolder(BusyConf_Name);
		BusyRate_Folder = dbm->getFolder(BusyRate_Name);
		HoldTrigger_Folder = db->getFolder(HoldTrigger_Name);
		TotalInt_Folder = dbd->getFolder(TotalInt_Name);
		DataTakingMode_Folder = db->getFolder(DataTakingMode_Name);
		t0Project_Folder = db->getFolder(t0Project_Name);
	} catch (const std::exception& ex) {
		ers::error(daq::rc::CmdLineError(ERS_HERE, "DATABASE ACCESS PROBLEM", ex));
		return;
	}

	int buffer_rn = 0;
	int runNumber = 0;
	int lb = 0;
	unsigned long long sT = 0;
	unsigned long long eT = 0;
	int fillNumber = 0;
	static std::vector<std::pair<std::string, std::string>> Detector_List;
	try {
		cool::IObjectIteratorPtr rl_objects = LBT_Folder->browseObjects(cool::ValidityKey(starttime_ns), cool::ValidityKey(endtime_ns), 0);
		//per LB loop
		while (rl_objects->goToNext()) {
			const cool::IObject& obj = rl_objects->currentRef();
			runNumber = atoi(obj.payloadValue("Run").c_str());
			lb = atoi(obj.payloadValue("LumiBlock").c_str());
			cool::UInt63 runlumi_key = runNumber;
			runlumi_key = (runlumi_key << 32);
			runlumi_key += static_cast<cool::UInt63>(lb);
			cool::IObjectPtr obj_lb = LB_Folder->findObject(static_cast<cool::ValidityKey>(runlumi_key), 0);
			sT = atoll(obj_lb->payloadValue("StartTime").c_str());
			eT = atoll(obj_lb->payloadValue("EndTime").c_str());
			
			//FILL stuff
			cool::IObjectIteratorPtr objsFill = FillState_Folder->browseObjects(static_cast<cool::ValidityKey>(sT), static_cast<cool::ValidityKey>(eT), cool::ChannelSelection::all());
			std::string sb_flag = "";
			std::string beam_mode = "";
			if (objsFill->goToNext()) {
				const cool::IObject& objFill = objsFill->currentRef();
				fillNumber = atoi(objFill.payloadValue("FillNumber").c_str());
				sb_flag = objFill.payloadValue("StableBeams");
				beam_mode = objFill.payloadValue("BeamMode");
			}
			cool::IObjectIteratorPtr objsBP = TotalInt_Folder->browseObjects(cool::ValidityKey(sT), static_cast<cool::ValidityKey>(eT), cool::ChannelSelection::all());
			std::string beamPresence1 = "";
			std::string beamPresence2 = "";
			std::string r4p = "";
			std::string t0p = "";
			if (objsBP->goToNext()) {
				const cool::IObject& objBP = objsBP->currentRef();
				beamPresence1 = objBP.payloadValue("Beam1_BeamPresent");
				beamPresence2 = objBP.payloadValue("Beam2_BeamPresent");
				cool::IObjectPtr objR4P = DataTakingMode_Folder->findObject(static_cast<cool::ValidityKey>(runlumi_key), 0);
				r4p = objR4P->payloadValue("ReadyForPhysics");
				cool::IObjectPtr objT0P = t0Project_Folder->findObject(static_cast<cool::ValidityKey>(runlumi_key), 0);
				t0p = objT0P->payloadValue("T0ProjectTag");
			}

			//DETECTOR LIST info
			if (runNumber != buffer_rn) {
				buffer_rn = runNumber;
				Detector_List.clear();
				cool::UInt63 runlumiDetList = runNumber;
				runlumiDetList = (runlumiDetList << 32) + 1;
				Detector_List.push_back(std::make_pair("ctpmi_vme_rate", "VME"));
				Detector_List.push_back(std::make_pair("ctpcore_moni3_rate", "SMPL"));
				Detector_List.push_back(std::make_pair("ctpcore_moni1_rate", "CPX0"));
				Detector_List.push_back(std::make_pair("ctpcore_rate", "CTP"));
				Detector_List.push_back(std::make_pair("ctpmi_ecr_rate", "ECR"));
				Detector_List.push_back(std::make_pair("ctpcore_roi_fifo_rate", "DAQ"));
				Detector_List.push_back(std::make_pair("ctpcore_roi_slink_rate", "DAQslink"));
				Detector_List.push_back(std::make_pair("ctpcore_moni0_rate", "CTPbusy"));
				cool::IObjectIteratorPtr objsDL = BusyConf_Folder->browseObjects(static_cast<cool::ValidityKey>(runlumiDetList), static_cast<cool::ValidityKey>(static_cast<cool::UInt63>(runNumber + 1) << 32), cool::ChannelSelection::all());
				while (objsDL->goToNext()) {
					const cool::IObject& objDL = objsDL->currentRef();
					std::string identifier = objDL.payloadValue("identifier");
					std::string description = objDL.payloadValue("description");
					std::string busyEnabled = objDL.payloadValue("busyEnabled");
					if (busyEnabled.compare("TRUE") == 0) {
						Detector_List.push_back(std::make_pair(identifier, description));
					}
				}
			}
			int SB = (sb_flag.compare("TRUE") == 0) ? 1 : 0;
			std::cout << std::fixed 
				<< "Fill=" << fillNumber << ",RunNumber=" << runNumber << ",LumiBlock=" << lb
				<< ",StableBeams=" << SB 
				<< ",Beam1Present=" << beamPresence1 << ",Beam2Present=" << beamPresence2 
				<< ",ReadyForPhysics=" << r4p << ",T0Project=" << t0p
				<< ",StartDate=" << get_date_OWL(sT)
				<< ",EndDate=" << get_date_OWL(eT)
				<< ",Duration=" << ((eT - sT) / 1000000.);

			//BUSY info
			float busy_frac = -1.;
			float eff = 0.;
			cool::IObjectPtr objsBR;
			try {
				objsBR = BusyRate_Folder->findObject(static_cast<cool::ValidityKey>(runlumi_key), 0);
			} catch (const std::exception& e) {
				busy_frac = 0.;
			}
			for (auto& detector : Detector_List) {
				if (busy_frac < 0.) {
					if (detector.first.compare("ht_det") == 0)
						busy_frac = 0.;
					else
						busy_frac = atof(objsBR->payloadValue(detector.first.c_str()).c_str());
					if (detector.second.compare("CTPbusy") == 0)
						eff = 1. - busy_frac;
					else if (busy_frac > 0.)
						std::cout << "," << takeCharOut(detector.second) << "=" << busy_frac * static_cast<float>(eT - sT) / 1000000000.;
					busy_frac = -1.;
				}
				else {
					busy_frac = 0.;
					if (detector.second.compare("CTPbusy") == 0)
						eff = 1.;				
				}				
			}

			//HT info
			cool::IObjectIteratorPtr objsHT = HoldTrigger_Folder->browseObjects(static_cast<cool::ValidityKey>(sT), static_cast<cool::ValidityKey>(eT), 0);
			while (objsHT->goToNext()) {
				const cool::IObject& objHT = objsHT->currentRef();
				std::string subSystems = objHT.payloadValue("SubSystems");
				std::string reason = objHT.payloadValue("Reason");
				if (subSystems.compare("None") == 0 || reason.compare("None") == 0)
					continue;
				long long ht_start = 0;
				long long ht_stop = 0;
				if (objHT.since() < sT)
					ht_start = sT;
				else
					ht_start = objHT.since();
				if (objHT.until() >= eT)
					ht_stop = eT;
				else
					ht_stop = objHT.until();
				long long ht_dur = ht_stop - ht_start;
				std::cout << "," << takeCharOut(subSystems) << "_HT=" << static_cast<float>(ht_dur) / 1000000000 << "," << reason << "_HT=" << static_cast<float>(ht_dur) / 1000000000;
			}
			std::cout << ",Eff=" << eff << std::endl;
		}
	} catch (const std::exception& ex) {
                ers::error(daq::rc::CmdLineError(ERS_HERE, "DATA ACCESS PROBLEM", ex));
                return;
	}

	db->closeDatabase();
	dbm->closeDatabase();
	dbd->closeDatabase();
	dbt->closeDatabase();
}

void getStopInfo(long long starttime_ns, long long endtime_ns) {
	try {
		db = cool_app.databaseService().openDatabase(dbNameTDAQ);
		dbd = cool_app.databaseService().openDatabase(dbNameDCS);
		dbt = cool_app.databaseService().openDatabase(dbNameTRIGGER);
		FillState_Folder = dbd->getFolder(FillState_Name);
		StopReason_Folder = db->getFolder(StopReason_Name);
		LBT_Folder = dbt->getFolder(LBT_Name);
		t0Project_Folder = db->getFolder(t0Project_Name);
	} catch (const std::exception& ex) {
                ers::error(daq::rc::CmdLineError(ERS_HERE, "", ex));
                return;
	}
	try {
		std::string buff_sbflag = "FALSE";
		unsigned long long sT = 0;
		unsigned long long eT = 0;
		cool::IObjectIteratorPtr objsFill = FillState_Folder->browseObjects(static_cast<cool::ValidityKey>(starttime_ns), static_cast<cool::ValidityKey>(endtime_ns), cool::ChannelSelection::all());
		while (objsFill->goToNext()) {
			const cool::IObject& objFill = objsFill->currentRef();
			std::string fillNo = objFill.payloadValue("FillNumber");
			std::string sb_flag = objFill.payloadValue("StableBeams");
			std::string beam_mode = objFill.payloadValue("BeamMode");
			if (sb_flag.compare("FALSE") == 0 && buff_sbflag.compare("FALSE") == 0)
				continue;
			else if (sb_flag.compare("TRUE") == 0 && buff_sbflag.compare("FALSE") == 0) {
				buff_sbflag = "TRUE";
				sT = objFill.since();
			}
			else if (sb_flag.compare("FALSE") == 0 && buff_sbflag.compare("TRUE") == 0) {
				buff_sbflag = "FALSE";
				eT = objFill.until();

				cool::IObjectIteratorPtr objsSR = StopReason_Folder->browseObjects(static_cast<cool::ValidityKey>(sT), static_cast<cool::ValidityKey>(eT), cool::ChannelSelection::all());
				std::string subSystems = "";
				std::string reason = "";
				while (objsSR->goToNext()) {
					const cool::IObject& objSR = objsSR->currentRef();
					subSystems = objSR.payloadValue("SubSystems");
					reason = objSR.payloadValue("Reason");
					if (subSystems.compare("None") == 0)
						continue;
					
					//get run number that just stopped
					cool::IObjectIteratorPtr objsRN = LBT_Folder->browseObjects(static_cast<cool::ValidityKey>(objSR.since() - 60'000'000'000), static_cast<cool::ValidityKey>(objSR.since()), 0);
					std::string runNumber = "";
					std::string t0p = "";
					if (objsRN->goToNext()) {
						const cool::IObject& objRN = objsRN->currentRef();
						runNumber = objRN.payloadValue("Run");
						cool::UInt63 runlumi_key = atoll(runNumber.c_str());
						runlumi_key = (runlumi_key << 32);
						runlumi_key += static_cast<cool::UInt63>(1);

						//get project of run that was stopped
						cool::IObjectPtr objT0P = t0Project_Folder->findObject(static_cast<cool::ValidityKey>(runlumi_key), 0);
						t0p = objT0P->payloadValue("T0ProjectTag");
					}

					unsigned long long normalizedST = (sT < objSR.since()) ? objSR.since() : sT;
					unsigned long long normalizedET = (eT > objSR.until()) ? objSR.until() : eT;

					std::cout << std::fixed
						<< "Fill=" << fillNo << ",RunNumber=" << runNumber
						<< ",LumiBlock=0,StableBeams=1,Beam1Present=TRUE,Beam2Present=TRUE,"
						<< "ReadyForPhysics=0,T0Project=" << t0p
						<< ",StartDate=" << get_date_OWL(normalizedST)
						<< ",EndDate=" << get_date_OWL(normalizedET)
						<< ",Duration=" << (normalizedET - normalizedST) / 1000000.
						<< "," << takeCharOut(takeCharOut(subSystems, ',')) << "_S=" << (normalizedET - normalizedST) / 1000000.
						<< "," << takeCharOut(takeCharOut(reason, ',')) << "_S=" << (normalizedET - normalizedST) / 1000000.
						<< ",Eff=0" << std::endl;
				}
			}
		}
	} catch (const std::exception& ex) {
                ers::error(daq::rc::CmdLineError(ERS_HERE, "", ex));
                return;
	}
	db->closeDatabase();
	dbd->closeDatabase();
	dbt->closeDatabase();
}

std::string get_date_OWL(long long nseconds, std::string mode) {
	int seconds = nseconds / 1000000000;
	OWLTime owl_date = OWLTime(seconds);
	long year = owl_date.year();
	unsigned short month = owl_date.month();
	unsigned short day = owl_date.day();
	std::string date_string = std::string(owl_date.c_str());
	size_t lastIndex = date_string.size();
	std::string time_string = date_string.substr(lastIndex - 8, lastIndex);
	std::ostringstream stm;

	if (mode.compare("OWL") == 0) {
		if (day < 10)
			stm << "0";
		stm << day << "/";
		if (month < 10)
			stm << "0";
		stm << month << "/";
		stm << year;
		stm << " ";
		stm << time_string;
	}
	else {
		stm << year;
		stm << "-";
		if (month < 10)
			stm << "0";
		stm << month << "-";
		if (day < 10)
			stm << "0";
		stm << day;
		stm << " ";
		stm << time_string;
	}

	return stm.str();
}

std::string takeCharOut(std::string text, char takeout) {
	size_t position = 0;
	std::string output = text;
	while (output.find(takeout, position) != std::string::npos) {
		position = output.find(takeout, position);
		output.erase(output.begin() + position);
	}
	return output;
}
