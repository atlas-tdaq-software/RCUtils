// $Id$
//
// ATLAS Run Control
//
// Author: Bob Jones, Sarah Wheeler, Dietmar Schweiger
// Current: Dietrich Liko, September 2002 

// Log the Run Control status to stdout

#include <iostream>
#include <unistd.h>
#include <boost/program_options.hpp>

#include "ers/ers.h"

#include "ipc/core.h"
#include "ipc/partition.h"
#include "is/infodictionary.h"
#include "rc/RunParams.h"

#include "RunControl/Common/Exceptions.h"
#include "Exceptions.h"

namespace po = boost::program_options;

int main(int argc, char ** argv) 
{

   try{
   	IPCCore::init(argc,argv);
   }catch(daq::ipc::CannotInitialize& e){
   	ers::fatal(e);
	abort();
   }catch(daq::ipc::AlreadyInitialized& e){
   	ers::warning(e);
   }

   std::string partition(""),info("RunParams.RunParams");
   if (getenv("TDAQ_PARTITION"))
     partition = getenv("TDAQ_PARTITION");
   int timeout=0;

   try {
     po::options_description desc("This program gets the environment for the partition as needed by setup_daq.");
     
     desc.add_options()
       ("partition,p"  , po::value<std::string>(&partition)->default_value(partition), "Name of the partition(default is $TDAQ_PARTITION)")
       ("info,I"     , po::value<std::string>(&info)->default_value(info)      , "Name of the IS info")
       ("timeout,T" , po::value<int>(&timeout)->default_value(timeout)                 , "Timeout in seconds")
       ("help,h"                                                                     , "Print help message")
       ;
     
     po::variables_map vm;
     po::store(po::parse_command_line(argc, argv, desc), vm);
     po::notify(vm);
     
     if(vm.count("help")) {
       std::cout << desc << std::endl;
       return EXIT_SUCCESS;
     }
     
     if(partition.empty()) {
       ers::error(daq::rc::CmdLineError(ERS_HERE,"Missing partition name!"));
       std::cout << desc << std::endl;
       return EXIT_FAILURE;
     }
   }
   catch(std::exception& ex) {
     ers::error(daq::rc::CmdLineError(ERS_HERE, "", ex));
     return EXIT_FAILURE;
   }
   
   IPCPartition p(partition);
   ISInfoDictionary isInfoDict(p);

   RunParams runParameter;

   int time = 0;	
   while ( true ) 
   {
   try 	{
      	isInfoDict.findValue(info,runParameter);
	}
   catch ( daq::is::Exception & ex )
	{
	sleep(1);
      	time+=1;
	if ( timeout < time )
		{
       		std::ostringstream reason;
       		reason << "Failure reading Run Number in " << info ;
       		ers::fatal(daq::rc::ISReadFailure(ERS_HERE,reason.str().c_str(),ex));
      		return EXIT_FAILURE;
     		}		
	continue ;
	}
   break ; // info found
   } 
 
   std::cout << runParameter.run_number << std::endl;
   exit (EXIT_SUCCESS);
}
