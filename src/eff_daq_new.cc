#include <signal.h>
#include <fstream>
#include <vector>
#include <iostream>
#include <sstream>
#include <thread>
#include <list>
#include <algorithm>

#include <boost/program_options.hpp>

#include <CoralBase/Attribute.h>
#include <CoolKernel/Exception.h>
#include <CoolKernel/ValidityKey.h>
#include <CoolKernel/IDatabaseSvc.h>
#include <CoolKernel/IDatabase.h>
#include <CoolKernel/IFolder.h>
#include <CoolKernel/IRecordSpecification.h>
#include <CoolKernel/Record.h>
#include <CoolApplication/DatabaseSvcFactory.h>
#include <CoolKernel/IObjectIterator.h>
#include <CoolKernel/IObject.h>
#include <CoolKernel/ChannelSelection.h>
#include <CoolApplication/Application.h>
//cool location /cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq-common/tdaq-common-10-00-00/installed/external/x86_64-centos7-gcc11-opt/

#include "owl/time.h"

#include "ers/ers.h"
#include "RunControl/Common/Exceptions.h"

namespace daq {
	ERS_DECLARE_ISSUE(rc,
		BadCoolDb,
		"Retrieving data from COOL failed because " << explanation,
		((const char*)explanation)
	)
}

extern "C" void signalhandler(int)
{
	signal(SIGINT, SIG_DFL);
	signal(SIGTERM, SIG_DFL);
	std::cout << "Terminating eff_daq_new ..." << std::endl;
}

static     cool::IDatabasePtr db;
static     cool::IDatabasePtr dbd;
static     cool::IDatabasePtr dbt;
static     cool::IDatabasePtr dbm;
static     cool::Application cool_app;

static     std::string dbNameTDAQ = "COOLONL_TDAQ/CONDBR2";
static     std::string dbNameDCS = "COOLOFL_DCS/CONDBR2";
static     std::string dbNameTRIGGER = "COOLONL_TRIGGER/CONDBR2";
static     std::string dbNameMONITOR = "COOLONL_TRIGGER/MONP200";
static     std::string FillState_Name = "/LHC/DCS/FILLSTATE";
static     std::string L1Counters_Name = "/TRIGGER/LUMI/LVL1COUNTERS";
static     std::string L1Menu_Name = "/TRIGGER/LVL1/Menu";
static     std::string LB_Name = "/TRIGGER/LUMI/LBLB";
static     std::string LBT_Name = "/TRIGGER/LUMI/LBTIME";
static     std::string DataTakingMode_Name = "/TDAQ/RunCtrl/DataTakingMode";
static     std::string BusyConf_Name = "/TRIGGER/LVL1/BUSYCONF";
static     std::string BusyRate_Name = "/TRIGGER/LVL1/BUSYRATES_RUN2";
static     std::string StopReason_Name = "/TDAQ/RunCtrl/StopReason";
static     std::string HoldTrigger_Name = "/TDAQ/RunCtrl/HoldTriggerReason";
static	   std::string t0Project_Name = "/TDAQ/RunCtrl/SOR";
static     std::string EOR_Name = "/TDAQ/RunCtrl/EOR";
static     std::string SOR_Name = "/TDAQ/RunCtrl/SOR";
static     std::string TotalInt_Name = "/LHC/DCS/BPTX/TOTALINT";
static     cool::IFolderPtr FillState_Folder;
static     cool::IFolderPtr L1Counters_Folder;
static     cool::IFolderPtr L1Menu_Folder;
static     cool::IFolderPtr LB_Folder;
static     cool::IFolderPtr LBT_Folder;
static     cool::IFolderPtr DataTakingMode_Folder;
static     cool::IFolderPtr BusyConf_Folder;
static     cool::IFolderPtr BusyRate_Folder;
static     cool::IFolderPtr StopReason_Folder;
static     cool::IFolderPtr HoldTrigger_Folder;
static	   cool::IFolderPtr t0Project_Folder;
static	   cool::IFolderPtr EOR_Folder;
static	   cool::IFolderPtr SOR_Folder;
static     cool::IFolderPtr TotalInt_Folder;

static std::vector<std::vector<std::pair<std::string, std::string>>>* Detector_List = new std::vector<std::vector<std::pair<std::string, std::string>>>;
static std::vector<std::pair<long long, long long>>* Run_Times = new std::vector<std::pair<long long, long long>>;
static std::vector<int>* Runs = new std::vector<int>;
static std::vector<std::pair<long long, long long>>* Beam_Times = new std::vector<std::pair<long long, long long>>;
static std::map<int, std::vector<int>>* LB_Names = new std::map<int, std::vector<int>>;
static std::vector<std::pair<unsigned long long, unsigned long long>>* LB_Times = new std::vector<std::pair<unsigned long long, unsigned long long>>;
static std::vector<std::vector<float>>* Busy_Fractions = new std::vector<std::vector<float>>;
static std::vector<std::pair<long long, long long>>* Stop_Times = new std::vector<std::pair<long long, long long>>;
static std::vector<int>* Fills = new std::vector<int>;
static std::vector<bool>* StableBeam_Flags = new std::vector<bool>;
static std::vector<bool>* R4P_Flags = new std::vector<bool>;
static std::vector<std::string>* T0_Projects = new std::vector<std::string>;
static std::vector<std::pair<bool, bool>>* BeamPresence_Flags = new std::vector<std::pair<bool, bool>>;
static std::vector<int>* Stop_Runs = new std::vector<int>;
static std::vector<int>* Stop_Fills = new std::vector<int>;
static std::vector<std::string>* Stop_T0Projects = new std::vector<std::string>;
static std::list<std::vector<std::pair<std::string, std::string>>>* Stop_Machines = new std::list<std::vector<std::pair<std::string, std::string>>>;
static std::vector<std::vector<float>>* HoldTrigger_D = new std::vector<std::vector<float>>;
static std::vector<std::vector<std::string>>* HoldTrigger_S = new std::vector<std::vector<std::string>>;
static std::vector<std::vector<std::string>>* HoldTrigger_R = new std::vector<std::vector<std::string>>;


//methods
void setRunTimes(long long starttime_ns, long long endtime_ns, bool* found);
void setBeamTimes(unsigned long long starttime_ns, unsigned long long endtime_ns, bool* found);
bool setDetectorList();
bool setLumiBlockTimes();
bool setBusyFractions();
bool setHT();
void setStopTimes(long long starttime_ns, long long endtime_ns, bool* found);
std::string get_date_OWL(long long nseconds, std::string mode = "output");
bool setStaticParameters(); //LB_Times (setLumiBlockTimes) and LB_Names (setRunTimes) must be set before calling this
void getAllActive();
bool setStopInfo();
bool partitionVia(std::string list, std::vector<std::string>& partitioned, std::string breaker);

int main(int argc, char** argv) {

	std::string startTime, endTime, timeStampPath;

	boost::program_options::options_description desc("This program retrieves info from COOL conditions database, evaluates the data taking efficiency and the inneficiency sources for selected time intervals and outputs the results in the standard output stream. The granularity of the information is per lumi block.");
	desc.add_options()
		("help,h", "help message")
		("startTime,s", boost::program_options::value<std::string>(), "Start time (epoch time in ns)")
		("endTime,e", boost::program_options::value<std::string>(), "End time (epoch time in ns)")
		("timeStampPath,p", boost::program_options::value<std::string>(&timeStampPath)->default_value(timeStampPath), "Timestamp file path (default: not writing the timestamp file)")
		("busyFolder,f", boost::program_options::value<std::string>(&BusyRate_Name)->default_value(BusyRate_Name), "Busy folder name (default /TRIGGER/LVL1/BUSYRATES_RUN2")
		;

	boost::program_options::variables_map vm;
	boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
	boost::program_options::notify(vm);

	if (vm.count("help")) {
		std::cout << desc << std::endl;
		return EXIT_SUCCESS;
	}
	if ((vm.count("startTime")) && (vm.count("endTime"))) {
		startTime = vm["startTime"].as<std::string>();
		endTime = vm["endTime"].as<std::string>();
	}
	else {
		std::cout << desc << std::endl;
		daq::rc::CmdLineError issue(ERS_HERE, "");
		ers::fatal(issue);
		return EXIT_FAILURE;
	}

	timeStampPath = vm["timeStampPath"].as<std::string>();

	unsigned long long StartTimeNS = atoll(startTime.c_str());
	unsigned long long EndTimeNS = atoll(endTime.c_str());
	StartTimeNS += 60000000000;

	bool runsExist = false;//new bool(false);
	bool beamsExist = false;//new bool(false);
	bool stopsExist = false;
	try {
		//std::thread thRuns(setRunTimes, StartTimeNS, EndTimeNS, &runsExist);
		//std::thread thBeams(setBeamTimes, StartTimeNS, EndTimeNS, &beamsExist);
		//thRuns.join();
		//thBeams.join();
		setRunTimes(StartTimeNS, EndTimeNS, &runsExist);
		setBeamTimes(StartTimeNS, EndTimeNS, &beamsExist);
		if (runsExist || beamsExist)
			setStopTimes(StartTimeNS, EndTimeNS, &stopsExist);
		if (runsExist) {
			std::thread thDetList(setDetectorList);
			std::thread thLBTimes(setLumiBlockTimes);
			thDetList.join();
			thLBTimes.join();
			setStaticParameters();
			setBusyFractions();
			setHT();
			if (stopsExist)
				setStopInfo();
			getAllActive();
			if (timeStampPath.size() > 0) {
				timeStampPath += "lastTime.data";
				std::ofstream lastPeriod(timeStampPath.c_str());
				lastPeriod << endTime;
				lastPeriod.close();
			}
		}
	}
	catch (const std::runtime_error& e) {
		std::cerr << e.what() << std::endl;
		delete Run_Times;
		delete Beam_Times;
		delete Runs;
		delete Detector_List;
		delete LB_Names;
		delete LB_Times;
		delete Busy_Fractions;
		delete HoldTrigger_S;
		delete HoldTrigger_R;
		delete HoldTrigger_D;
		delete Stop_Times;
		delete Fills;
		delete StableBeam_Flags;
		delete R4P_Flags;
		delete T0_Projects;
		delete BeamPresence_Flags;
		delete Stop_Machines;
		delete Stop_Runs;
		delete Stop_Fills;
		delete Stop_T0Projects;
		return EXIT_FAILURE;
	}

	delete Run_Times;
	delete Beam_Times;
	delete Runs;
	delete Detector_List;
	delete LB_Names;
	delete LB_Times;
	delete Busy_Fractions;
	delete HoldTrigger_S;
	delete HoldTrigger_R;
	delete HoldTrigger_D;
	delete Stop_Times;
	delete Fills;
	delete StableBeam_Flags;
	delete R4P_Flags;
	delete T0_Projects;
	delete BeamPresence_Flags;
	delete Stop_Machines;
	delete Stop_Runs;
	delete Stop_Fills;
	delete Stop_T0Projects;

	return EXIT_SUCCESS;
}

void setRunTimes(long long starttime_ns, long long endtime_ns, bool* found) {
	Run_Times->clear();
	
	try {
		db = cool_app.databaseService().openDatabase(dbNameTDAQ);
		dbt = cool_app.databaseService().openDatabase(dbNameTRIGGER);
		LBT_Folder = dbt->getFolder(LBT_Name);
		EOR_Folder = db->getFolder(EOR_Name);
		SOR_Folder = db->getFolder(SOR_Name);
	}
	catch (const std::exception& ex) {
		std::string error = "setRunTimes(..) says: DATABASE ACCESS PROBLEM";
		throw(std::runtime_error(error));
	}

	int buffer_rn = 0;
	int runNumber = 0;
	int lb = 0;
	std::vector<int> lbs;
	try {
		cool::IObjectIteratorPtr rl_objects = LBT_Folder->browseObjects(cool::ValidityKey(starttime_ns), cool::ValidityKey(endtime_ns), 0);
		while (rl_objects->goToNext()) {
			const cool::IObject& obj = rl_objects->currentRef();
			runNumber = atoi(obj.payloadValue("Run").c_str());
			lb = atoi(obj.payloadValue("LumiBlock").c_str());
			if (runNumber > buffer_rn) {
				if (buffer_rn != 0) {
					Runs->push_back(buffer_rn);
					LB_Names->insert(std::pair<int, std::vector<int> >(buffer_rn, lbs));
				}
				buffer_rn = runNumber;
				lbs.clear();
				cool::UInt63 runlumi_key = runNumber;
				runlumi_key = (runlumi_key << 32);
				cool::IObjectPtr obj_lb;
				long long sor = 0;
				long long eor = 0;
				try {
					obj_lb = EOR_Folder->findObject(runlumi_key, 0);
					sor = atoll(obj_lb->payloadValue("SORTime").c_str());
					eor = atoll(obj_lb->payloadValue("EORTime").c_str());
				}
				catch (const std::exception& e) {
					std::cout << "Cought EOR exception for RN " << runNumber << std::endl;
					obj_lb = SOR_Folder->findObject(runlumi_key, 0);
					sor = atoll(obj_lb->payloadValue("SORTime").c_str());
					eor = sor + 1'000'000'000;
				}
				if (sor < starttime_ns && eor > endtime_ns)
					Run_Times->push_back(std::make_pair(starttime_ns, endtime_ns));
				else if (sor < starttime_ns && eor < endtime_ns)
					Run_Times->push_back(std::make_pair(starttime_ns, eor));
				else if (sor > starttime_ns && eor > endtime_ns)
					Run_Times->push_back(std::make_pair(sor, endtime_ns));
				else
					Run_Times->push_back(std::make_pair(sor, eor));
			}
			lbs.push_back(lb);
		}
		if (runNumber) {
			Runs->push_back(runNumber);
			LB_Names->insert(std::pair<int, std::vector<int> >(runNumber, lbs));
		}
	}
	catch (const std::exception& e) {
		std::string error = "setRunTimes(..) says: DATA ACCESS PROBLEM";
		throw(std::runtime_error(error));
	}

	db->closeDatabase();
	dbt->closeDatabase();

	if (Run_Times->size())
		*found = true;
	else
		*found = false;
}

void setBeamTimes(unsigned long long starttime_ns, unsigned long long endtime_ns, bool* found) {
	Beam_Times->clear();
	
	try {
		dbd = cool_app.databaseService().openDatabase(dbNameDCS);
		FillState_Folder = dbd->getFolder(FillState_Name);
	}
	catch (const std::exception& ex) {
		std::string error = "setBeamTimes(..) says: DATABASE ACCESS PROBLEM";
		throw(std::runtime_error(error));
	}

	std::pair<long long, long long> buffer_fill;
	std::string buffer_sbflag = "FALSE";
	try {
		cool::IObjectIteratorPtr objects = FillState_Folder->browseObjects(cool::ValidityKey(starttime_ns), cool::ValidityKey(endtime_ns), cool::ChannelSelection::all());
		while (objects->goToNext()) {
			const cool::IObject& obj = objects->currentRef();
			std::string sb_flag = obj.payloadValue("StableBeams");
			std::string beam_mode = obj.payloadValue("BeamMode");
			if (sb_flag.compare("TRUE") == 0 && buffer_sbflag.compare("FALSE") == 0) {
				buffer_fill.first = (long long)obj.since();
				buffer_sbflag = "TRUE";
			}
			else if (sb_flag.compare("FALSE") == 0 && buffer_sbflag.compare("TRUE") == 0) {
				buffer_fill.second = (long long)obj.since();
				buffer_sbflag = "FALSE";
				Beam_Times->push_back(buffer_fill);
			}
			else if (obj.until() >= endtime_ns && sb_flag.compare("TRUE") == 0 && buffer_sbflag.compare("TRUE") == 0) {
				buffer_fill.second = endtime_ns;
				Beam_Times->push_back(buffer_fill);
				break;
			}
		}
	}
	catch (const std::exception& e) {
		std::string error = "setBeamTimes(..) says: DATA ACCESS PROBLEM";
		throw(std::runtime_error(error));
	}

	dbd->closeDatabase();

	if (Beam_Times->size())
		*found = true;
	else
		*found = false;
}

bool setDetectorList() {
	Detector_List->clear();
	std::vector<std::pair<std::string, std::string>> dummy;

	try {
		dbm = cool_app.databaseService().openDatabase(dbNameMONITOR);
		BusyConf_Folder = dbm->getFolder(BusyConf_Name);
	}
	catch (const std::exception& ex) {
		std::string error = "setDetectorList(..) says: DATABASE ACCESS PROBLEM";
		throw(std::runtime_error(error));
	}

	int i = 0;
	std::vector<int>::iterator iterRuns = Runs->begin();
	try {
		for (; iterRuns != Runs->end(); ++iterRuns) {
			cool::UInt63 runlumi_keyA = *iterRuns;
			cool::UInt63 runlumi_keyZ = *iterRuns + 1;
			runlumi_keyA = (runlumi_keyA << 32) + 1;
			runlumi_keyZ = (runlumi_keyZ << 32);
			Detector_List->push_back(dummy);
			cool::IObjectIteratorPtr objs = BusyConf_Folder->browseObjects((cool::ValidityKey)runlumi_keyA, (cool::ValidityKey)runlumi_keyZ, cool::ChannelSelection::all());
			while (objs->goToNext()) {
				const cool::IObject& obj = objs->currentRef();
				std::string identifier = obj.payloadValue("identifier");
				std::string description = obj.payloadValue("description");
				std::string busyEnabled = obj.payloadValue("busyEnabled");
				//std::cout << identifier << "\t" << description << std::endl;
				if (busyEnabled.compare("TRUE") == 0) {
					Detector_List->at(i).push_back(std::make_pair(identifier, description));
				}
			}
			Detector_List->at(i).push_back(std::make_pair("ctpmi_vme_rate", "VME"));
			Detector_List->at(i).push_back(std::make_pair("ctpcore_moni3_rate", "SMPL"));
			Detector_List->at(i).push_back(std::make_pair("ctpcore_moni1_rate", "CPX0"));
			Detector_List->at(i).push_back(std::make_pair("ctpcore_rate", "CTP"));
			Detector_List->at(i).push_back(std::make_pair("ctpmi_ecr_rate", "ECR"));
			Detector_List->at(i).push_back(std::make_pair("ctpcore_roi_fifo_rate", "DAQ"));
			Detector_List->at(i).push_back(std::make_pair("ctpcore_roi_slink_rate", "DAQslink"));
			Detector_List->at(i).push_back(std::make_pair("ctpcore_moni0_rate", "CTPbusy"));

			//add HT detectors
			std::vector<std::string> testHT = { "Other","PIX","SCT","TRT","LAR","TIL","RPC","MDT","TGC","CSC","BCM","LUCID","ZDC","ALFA","L1CALO","L1CT","HLT","DAQ","IBL","DBM","MMEGA","L1TOPO","FTK","AFP","STGC" };
			for (unsigned int j = 0; j < testHT.size(); j++)
				Detector_List->at(i).push_back(std::make_pair("ht_det", testHT.at(j)));
			i++;
		}
	}
	catch (const std::exception& e) {
		std::string error = "setDetectorList(..) says: DATA PROCESSING PROBLEM";
		throw(std::runtime_error(error));
	}
	dbm->closeDatabase();

	if (Detector_List->size())
		return true;
	else
		return false;
}

bool setLumiBlockTimes() {
	LB_Times->clear();

	try {
		dbt = cool_app.databaseService().openDatabase(dbNameTRIGGER);
		LB_Folder = dbt->getFolder(LB_Name);
	}
	catch (const std::exception& e) {
		std::string error = "setLumiBlockTimes(..) says: DATABASE ACCESS PROBLEM";
		throw(std::runtime_error(error));
	}

	try {
		std::map<int, std::vector<int> >::iterator iterLB_Names = LB_Names->begin();
		for (; iterLB_Names != LB_Names->end(); ++iterLB_Names) {
			for (unsigned int i = 0; i < iterLB_Names->second.size(); i++) {
				cool::UInt63 runlumi_key = iterLB_Names->first;
				runlumi_key = (runlumi_key << 32);
				runlumi_key += (cool::UInt63)iterLB_Names->second.at(i);
				cool::IObjectPtr obj_lb = LB_Folder->findObject((cool::ValidityKey)runlumi_key, 0);
				LB_Times->push_back(std::make_pair(atoll(obj_lb->payloadValue("StartTime").c_str()), atoll(obj_lb->payloadValue("EndTime").c_str())));
			}
		}
	}
	catch (const std::exception& e) {
		std::string error = "setLumiBlockTimes(..) says: DATA ACCESS PROBLEM";
		throw(std::runtime_error(error));
	}

	dbt->closeDatabase();

	if (LB_Times->size())
		return true;
	else
		return false;
}

bool setBusyFractions() {
	Busy_Fractions->clear();

	try {
		dbm = cool_app.databaseService().openDatabase(dbNameMONITOR);
		BusyRate_Folder = dbm->getFolder(BusyRate_Name);
	}
	catch (const std::exception& e) {
		std::string error = "setBusyFractions(..) says: DATABASE ACCESS PROBLEM";
		throw(std::runtime_error(error));
	}

	std::vector<float> dummy;
	std::map<int, std::vector<int> >::iterator iterLBNames = LB_Names->begin();
	std::vector<std::vector<std::pair<std::string, std::string> > >::iterator iterDetectorList = Detector_List->begin();
	unsigned int k = 0;
	cool::IObjectPtr objects;
	for (; iterLBNames != LB_Names->end(); ++iterLBNames) {
		for (unsigned int i = 0; i < iterLBNames->second.size(); i++) {
			Busy_Fractions->push_back(dummy);
			cool::UInt63 runlumi_key = iterLBNames->first;
			runlumi_key = (runlumi_key << 32);
			runlumi_key += (cool::UInt63)iterLBNames->second.at(i);

			float busy_frac = -1.;
			try {
				objects = BusyRate_Folder->findObject(cool::ValidityKey(runlumi_key), 0);
			}
			catch (const std::exception& e) {
				std::cerr << e.what() << std::endl;
				busy_frac = 0.;
			}

			for (unsigned int j = 0; j < iterDetectorList->size(); j++) {
				if (busy_frac == -1.) {
					if (iterDetectorList->at(j).first.compare("ht_det") == 0)
						busy_frac = 0;
					else
						busy_frac = atof(objects->payloadValue(iterDetectorList->at(j).first.c_str()).c_str());
					Busy_Fractions->at(k).push_back(busy_frac);
					busy_frac = -1.;
				}
				else {
					busy_frac = 0.;
					Busy_Fractions->at(k).push_back(busy_frac);
				}
			}
			k++;
		}
		++iterDetectorList;
	}

	dbm->closeDatabase();

	if (Busy_Fractions->size())
		return true;
	else
		return false;
}

bool setHT() {
	HoldTrigger_S->clear();
	HoldTrigger_R->clear();
	HoldTrigger_D->clear();

	try {
		db = cool_app.databaseService().openDatabase(dbNameTDAQ);
		HoldTrigger_Folder = db->getFolder(HoldTrigger_Name);
	}
	catch (const std::exception& e) {
		std::string error = "setHT(..) says: DATABASE ACCESS PROBLEM";
		throw(std::runtime_error(error));
	}

	auto iterLBTimes = LB_Times->begin();
	std::vector<float> buffD;
	std::vector<std::string> buffS;
	std::vector<std::string> buffR;
	unsigned int k = 0;

	try {
		for (; iterLBTimes != LB_Times->end(); ++iterLBTimes) {
			buffD.clear();
			buffS.clear();
			buffR.clear();
			cool::IObjectIteratorPtr ht_objects = HoldTrigger_Folder->browseObjects(cool::ValidityKey(iterLBTimes->first), cool::ValidityKey(iterLBTimes->second), 0);
			while (ht_objects->goToNext()) {
				const cool::IObject& ht_obj = ht_objects->currentRef();
				long long ht_start = 0;
				long long ht_stop = 0;
				if (ht_obj.since() < iterLBTimes->first)
					ht_start = iterLBTimes->first;
				else
					ht_start = ht_obj.since();

				if (ht_obj.until() >= iterLBTimes->second)
					ht_stop = iterLBTimes->second;
				else
					ht_stop = ht_obj.until();

				long long ht_dur = ht_stop - ht_start;
				std::string subSystems = ht_obj.payloadValue("SubSystems");
				std::string reason = ht_obj.payloadValue("Reason");
				buffD.push_back(ht_dur);
				buffS.push_back(subSystems);
				buffR.push_back(reason);
			}
			HoldTrigger_D->push_back(buffD);
			HoldTrigger_S->push_back(buffS);
			HoldTrigger_R->push_back(buffR);

			k++;
		}
	}
	catch (const std::exception& e) {
		std::string error = "setHT(..) says: DATA ACCESS PROBLEM";
		throw(std::runtime_error(error));
	}
	db->closeDatabase();

	return true;
}

void setStopTimes(long long starttime_ns, long long endtime_ns, bool* found) { //reversed-intersection between run and beam times
	std::vector<std::pair<long long, long long> >* blackOutTimes = new std::vector<std::pair<long long, long long> >;
	std::pair<long long, long long> buff_pair;
	try {
		Stop_Times->clear();
		auto iterRunTimes = Run_Times->begin();
		auto iterBeamTimes = Beam_Times->begin();

		//get black-out periods - inverse the run times
		if (Run_Times->size() && iterRunTimes->first > starttime_ns)
			blackOutTimes->push_back(std::make_pair(starttime_ns, iterRunTimes->first));
		while (iterRunTimes != Run_Times->end()) {
			buff_pair.first = iterRunTimes->second;
			++iterRunTimes;
			if (iterRunTimes == Run_Times->end())
				break;
			buff_pair.second = iterRunTimes->first;
			blackOutTimes->push_back(buff_pair);
		}
		if (Run_Times->size() && (--iterRunTimes)->second < endtime_ns) {
			blackOutTimes->push_back(std::make_pair(iterRunTimes->second, endtime_ns));
		}
		if (Run_Times->size() == 0)
			blackOutTimes->push_back(std::make_pair(starttime_ns, endtime_ns));

		//get the intersection between beam times and blackouts
		auto iterBlackoutTimes = blackOutTimes->begin();
		for (; iterBeamTimes != Beam_Times->end(); ++iterBeamTimes) {
			for (; iterBlackoutTimes != blackOutTimes->end(); ++iterBlackoutTimes) {
				if (iterBlackoutTimes->first >= iterBeamTimes->first && iterBlackoutTimes->second <= iterBeamTimes->second)
					Stop_Times->push_back(*iterBlackoutTimes);
				else if (iterBlackoutTimes->first < iterBeamTimes->first && iterBlackoutTimes->second > iterBeamTimes->first && iterBlackoutTimes->second <= iterBeamTimes->second)
					Stop_Times->push_back(std::make_pair(iterBeamTimes->first, iterBlackoutTimes->second));
				else if (iterBlackoutTimes->first > iterBeamTimes->first && iterBlackoutTimes->first < iterBeamTimes->second && iterBlackoutTimes->second > iterBeamTimes->second)
					Stop_Times->push_back(std::make_pair(iterBlackoutTimes->first, iterBeamTimes->second));
				else if (iterBlackoutTimes->first < iterBeamTimes->first && iterBlackoutTimes->second > iterBeamTimes->second)
					Stop_Times->push_back(*iterBeamTimes);
			}
			iterBlackoutTimes = blackOutTimes->begin();
		}
	}
	catch (const std::exception& e) {
		std::string error = "setStopTimes(..) says: DATA PROCESSING PROBLEM";
		throw(std::runtime_error(error));
	}

	delete blackOutTimes;

	if (Stop_Times->size())
		*found = true;
	else
		*found = false;
}

std::string get_date_OWL(long long nseconds, std::string mode) {
	int seconds = nseconds / 1000000000;
	OWLTime owl_date = OWLTime(seconds);
	long year = owl_date.year();
	unsigned short month = owl_date.month();
	unsigned short day = owl_date.day();
	std::string date_string = std::string(owl_date.c_str());
	size_t lastIndex = date_string.size();
	std::string time_string = date_string.substr(lastIndex - 8, lastIndex);
	std::ostringstream stm;

	if (mode.compare("OWL") == 0) {
		if (day < 10)
			stm << "0";
		stm << day << "/";
		if (month < 10)
			stm << "0";
		stm << month << "/";
		stm << year;
		stm << " ";
		stm << time_string;
	}
	else {
		stm << year;
		stm << "-";
		if (month < 10)
			stm << "0";
		stm << month << "-";
		if (day < 10)
			stm << "0";
		stm << day;
		stm << " ";
		stm << time_string;
	}

	return stm.str();
}

bool setStaticParameters() {
	Fills->clear();
	StableBeam_Flags->clear();
	R4P_Flags->clear();
	T0_Projects->clear();
	BeamPresence_Flags->clear();
	std::pair<bool, bool> buff_pair;

	try {
		dbd = cool_app.databaseService().openDatabase(dbNameDCS);
		FillState_Folder = dbd->getFolder(FillState_Name);
		TotalInt_Folder = dbd->getFolder(TotalInt_Name);
	}
	catch (const std::exception& e) {
		std::string error = "setStaticParameters(..) says: DATABASE ACCESS PROBLEM";
		throw(std::runtime_error(error));
	}

	auto iterLBTimes = LB_Times->begin();
	for (; iterLBTimes != LB_Times->end(); ++iterLBTimes) {
		std::string fillNumber = "";
		std::string stableBeams = "";
		try {
			cool::IObjectIteratorPtr objectFills = FillState_Folder->browseObjects(cool::ValidityKey(iterLBTimes->first), cool::ValidityKey(iterLBTimes->second), cool::ChannelSelection::all());
			objectFills->goToNext();
			const cool::IObject& objFill = objectFills->currentRef();

			fillNumber = objFill.payloadValue("FillNumber");
			stableBeams = objFill.payloadValue("StableBeams");
		}
		catch (const std::exception& e) {
			std::cerr << "In setStaticParams: no fill number or stable beams flag (" << e.what() << ")" << std::endl;
			fillNumber = "0";
			stableBeams = "0";
		}
		std::string beamPresence1 = "";
		std::string beamPresence2 = "";
		try {
			cool::IObjectIteratorPtr objectBeamPresence = TotalInt_Folder->browseObjects(cool::ValidityKey(iterLBTimes->first), cool::ValidityKey(iterLBTimes->second), cool::ChannelSelection::all());
			while (objectBeamPresence->goToNext()) {
				const cool::IObject& objBeamPresence = objectBeamPresence->currentRef();
				if (objectBeamPresence) {
					beamPresence1 = objBeamPresence.payloadValue("Beam1_BeamPresent");
					beamPresence2 = objBeamPresence.payloadValue("Beam2_BeamPresent");
				}
				else {
					beamPresence1 = "TRUE";
					beamPresence2 = "TRUE";
				}
			}
		}
		catch (const std::exception& e) {
			std::cerr << "In setStaticParams: no beam presence info (" << e.what() << ")" << std::endl;
			beamPresence1 = "TRUE";
			beamPresence2 = "TRUE";
		}

		Fills->push_back(atoi(fillNumber.c_str()));
		StableBeam_Flags->push_back(((stableBeams.compare("TRUE") == 0) ? true : false));

		buff_pair.first = ((beamPresence1.compare("TRUE") == 0) ? true : false);
		buff_pair.second = ((beamPresence2.compare("TRUE") == 0) ? true : false);
		BeamPresence_Flags->push_back(buff_pair);
	}
	dbd->closeDatabase();

	try {
		db = cool_app.databaseService().openDatabase(dbNameTDAQ);
		DataTakingMode_Folder = db->getFolder(DataTakingMode_Name);
		t0Project_Folder = db->getFolder(t0Project_Name);
	}
	catch (const std::exception& e) {
		std::string error = "setStaticParameters(..) says: DATABASE ACCESS PROBLEM";
		throw(std::runtime_error(error));
	}

	try {
		auto iterLBNames = LB_Names->begin();
		for (; iterLBNames != LB_Names->end(); ++iterLBNames) {
			for (unsigned int i = 0; i < iterLBNames->second.size(); i++) {
				cool::UInt63 key = iterLBNames->first;
				key = key << 32;
				key += iterLBNames->second.at(i);

				cool::IObjectPtr objectR4P = DataTakingMode_Folder->findObject(cool::ValidityKey(key), 0);
				cool::IObjectPtr objectT0P = t0Project_Folder->findObject(cool::ValidityKey(key), 0);

				std::string r4p = objectR4P->payloadValue("ReadyForPhysics");
				std::string t0p = "";
				if (objectT0P)
					t0p = objectT0P->payloadValue("T0ProjectTag");
				else
					t0p = "None";
				R4P_Flags->push_back(((r4p.compare("1") == 0) ? true : false));
				T0_Projects->push_back((t0p.compare("") == 0) ? "None" : t0p);
			}
		}
	}
	catch (const std::exception& e) {
		std::string error = "setStaticParameters(..) says: DATA ACCESS PROBLEM";
		throw(std::runtime_error(error));
	}
	db->closeDatabase();

	if (LB_Times->size())
		return true;
	else
		return false;
}

void getAllActive() {

	auto iterLBTimes = LB_Times->begin();
	auto iterLBNames = LB_Names->begin();
	auto iterDetList = Detector_List->begin();
	auto iterBusyFractions = Busy_Fractions->begin();
	auto iterHTD = HoldTrigger_D->begin();
	auto iterHTS = HoldTrigger_S->begin();
	auto iterHTR = HoldTrigger_R->begin();
	auto iterFills = Fills->begin();
	auto iterStableBeamFlags = StableBeam_Flags->begin();
	auto iterR4PFlags = R4P_Flags->begin();
	auto iterT0Projects = T0_Projects->begin();
	auto iterBeamPresenceFlags = BeamPresence_Flags->begin();
	auto iterStopTimes = Stop_Times->begin();
	auto iterStopRuns = Stop_Runs->begin();
	auto iterStopFills = Stop_Fills->begin();
	auto iterStopT0 = Stop_T0Projects->begin();
	auto iterStopMachines = Stop_Machines->begin();
	std::string detector_buff = "";
	float busy_time = 0.;
	float eff = 0;

	try {
		while (iterLBNames != LB_Names->end()) {
			for (unsigned int i = 0; i < iterLBNames->second.size(); i++) {
				std::cout << "Fill=" << *iterFills << ",RunNumber=" << iterLBNames->first << ",LumiBlock=" << iterLBNames->second.at(i) << ",StableBeams=" << *iterStableBeamFlags << ",Beam1Present=" << (iterBeamPresenceFlags->first == true ? "TRUE" : "FALSE") << ",Beam2Present=" << (iterBeamPresenceFlags->second == true ? "TRUE" : "FALSE") << ",ReadyForPhysics=" << *iterR4PFlags << ",T0Project=" << *iterT0Projects;
				std::cout << ",StartDate=" << get_date_OWL(iterLBTimes->first) << ",EndDate=" << get_date_OWL(iterLBTimes->second) << ",Duration=" << ((iterLBTimes->second - iterLBTimes->first) / 1000000.);
				for (unsigned int j = 0; j < iterDetList->size(); j++) {
					size_t slashIndex = iterDetList->at(j).second.find("/", 0); //only one replacement of the slash gets done as that is usually the case with some detector names, e.g. LArH/F, IBL/DBM
					if (slashIndex != std::string::npos)
						iterDetList->at(j).second.replace(slashIndex, 1, "");
					size_t blankIndex = iterDetList->at(j).second.find(" ", 0);
					detector_buff = iterDetList->at(j).second;
					while (blankIndex != std::string::npos) {
						detector_buff.replace(blankIndex, 1, "");
						blankIndex = iterDetList->at(j).second.find(" ", blankIndex + 1);
					}
					if (detector_buff.compare("CTPbusy") == 0)
						eff = 1 - iterBusyFractions->at(j);
					else
						std::cout << "," << detector_buff << "=" << (iterBusyFractions->at(j) * ((iterLBTimes->second - iterLBTimes->first) / 1000000000.));
					busy_time += (iterBusyFractions->at(j) * ((iterLBTimes->second - iterLBTimes->first) / 1000000000.));
				}
				for (unsigned int k = 0; k < iterHTD->size(); k++) {
					if (iterHTD->at(k) > 0 && iterHTS->at(k).compare("None") != 0)
						std::cout << "," << iterHTS->at(k) << "_HT=" << iterHTD->at(k) / 1000000000. << "," << iterHTR->at(k) << "_HT=" << iterHTD->at(k) / 1000000000.;
				}
				std::cout << ",Eff=" << eff << std::endl;
				busy_time = 0.;
				++iterLBTimes;
				++iterFills;
				++iterStableBeamFlags;
				++iterR4PFlags;
				++iterT0Projects;
				++iterBeamPresenceFlags;
				++iterBusyFractions;
				++iterHTD;
				++iterHTS;
				++iterHTR;
			}
			++iterLBNames;
			++iterDetList;
		}

		while (iterStopTimes != Stop_Times->end()) {
			std::cout << "Fill=" << *iterStopFills << ",RunNumber=" << *iterStopRuns << ",LumiBlock=0,StableBeams=1,Beam1Present=TRUE,Beam2Present=TRUE,ReadyForPhysics=0,T0Project=" << *iterStopT0 << ",StartDate=" << get_date_OWL(iterStopTimes->first) << ",EndDate=" << get_date_OWL(iterStopTimes->second) << ",Duration=" << (iterStopTimes->second - iterStopTimes->first) / 1000000 << ",";
			for (unsigned int i = 0; i < iterStopMachines->size(); i++) {
				std::cout << iterStopMachines->at(i).first << "_S=" << (iterStopTimes->second - iterStopTimes->first) / 1000000 << "," << iterStopMachines->at(i).second << "_S=" << (iterStopTimes->second - iterStopTimes->first) / 1000000 << ",";
			}
			std::cout << "Eff=0" << std::endl;
			++iterStopTimes;
			++iterStopMachines;
			++iterStopRuns;
			++iterStopFills;
			++iterStopT0;
		}
	}
	catch (const std::exception& e) {
		std::string error = "getAllActive(..) says: DATA PROCESSING PROBLEM";
		throw(std::runtime_error(error));
	}
}

bool setStopInfo() {
	try {
		db = cool_app.databaseService().openDatabase(dbNameTDAQ);
		dbd = cool_app.databaseService().openDatabase(dbNameDCS);
		dbt = cool_app.databaseService().openDatabase(dbNameTRIGGER);
		StopReason_Folder = db->getFolder(StopReason_Name);
		t0Project_Folder = db->getFolder(t0Project_Name);
		FillState_Folder = dbd->getFolder(FillState_Name);
		LBT_Folder = dbt->getFolder(LBT_Name);
	}
	catch (const std::exception& ex) {
		std::string error = "setStopInfo(..) says: DATABASE ACCESS PROBLEM";
		throw(std::runtime_error(error));
	}

	auto iterStopTimes = Stop_Times->begin();
	auto iterRunTimes = Run_Times->begin();
	auto iterRuns = Runs->begin();
	std::vector<std::string> systems;
	std::vector<std::string> reasons;

	try {
		//set stop runs
		std::vector<std::pair<std::string, std::string>> buffStop;
		for (; iterStopTimes != Stop_Times->end(); ++iterStopTimes) {
			for (; iterRunTimes != Run_Times->end(); ++iterRunTimes) {
				if ((iterStopTimes->first - iterRunTimes->second) <= 0 && (iterStopTimes->first - iterRunTimes->second) <= 1800000000000) {
					Stop_Runs->push_back(*iterRuns);

					//get the fill
					cool::IObjectIteratorPtr sf_objects = FillState_Folder->browseObjects(cool::ValidityKey(iterStopTimes->first - 180000000000), cool::ValidityKey(iterStopTimes->first), cool::ChannelSelection::all());
					sf_objects->goToNext();
					const cool::IObject& obj_sf = sf_objects->currentRef();
					Stop_Fills->push_back(atoi(obj_sf.payloadValue("FillNumber").c_str()));

					//get the tier 0 project
					cool::UInt63 runlumi_key = *iterRuns;
					runlumi_key = (runlumi_key << 32) + 1;
					cool::IObjectPtr st_object = t0Project_Folder->findObject(cool::ValidityKey(runlumi_key), 0);
					Stop_T0Projects->push_back(st_object->payloadValue("T0ProjectTag"));

					iterRuns = Runs->begin();
					break;
				}
				else
					++iterRuns;
			}

			//if there is no run in the searched period then a back-track loop will get the stop static parameters
			if (Runs->size() == 0) {
				long long past = 60000000000;
				long long mins = 1;
				while (true) {
					cool::IObjectIteratorPtr run_objects = LBT_Folder->browseObjects(cool::ValidityKey(iterStopTimes->first - past * mins), cool::ValidityKey(iterStopTimes->second), cool::ChannelSelection::all());
					if (run_objects->goToNext()) {
						const cool::IObject& run_obj = run_objects->currentRef();
						Stop_Runs->push_back(atoi(run_obj.payloadValue("Run").c_str()));

						cool::IObjectIteratorPtr sf_objects = FillState_Folder->browseObjects(cool::ValidityKey(iterStopTimes->first - past * mins), cool::ValidityKey(iterStopTimes->first), cool::ChannelSelection::all());
						sf_objects->goToNext();
						const cool::IObject& obj_sf = sf_objects->currentRef();
						Stop_Fills->push_back(atoi(obj_sf.payloadValue("FillNumber").c_str()));

						cool::UInt63 runlumi_key = atoi(run_obj.payloadValue("Run").c_str());
						runlumi_key = (runlumi_key << 32) + 1;
						cool::IObjectPtr st_object = t0Project_Folder->findObject(cool::ValidityKey(runlumi_key), 0);
						Stop_T0Projects->push_back(st_object->payloadValue("T0ProjectTag"));

						break;
					}
					mins++;
				}
			}
			cool::IObjectIteratorPtr sr_objects = StopReason_Folder->browseObjects(cool::ValidityKey(iterStopTimes->first), cool::ValidityKey(iterStopTimes->second), cool::ChannelSelection::all());
			std::string subSystems = "";
			std::string reason = "";
			if (sr_objects->goToNext()) {
				const cool::IObject& obj = sr_objects->currentRef();
				subSystems = obj.payloadValue("SubSystems");
				reason = obj.payloadValue("Reason");
			}
			else {
				reason = "Unknown, ";
				subSystems = "Unknown, ";
			}

			//check and sepparate systems and reasons...
			partitionVia(subSystems, systems, ", ");
			partitionVia(reason, reasons, ", ");

			int diffStoR = systems.size() - reasons.size();
			if (diffStoR > 0) {
				for (int i = 0; i < diffStoR; i++)
					reasons.push_back(reasons.at(reasons.size() - 1));
			}
			else if (diffStoR < 0) {
				for (int i = 0; i < abs(diffStoR); i++)
					systems.push_back(systems.at(systems.size() - 1));
			}

			for (unsigned int i = 0; i < systems.size(); i++) {
				buffStop.push_back(std::make_pair(systems.at(i), reasons.at(i)));
			}
			Stop_Machines->push_back(buffStop);
			buffStop.clear();
		}
	}
	catch (const std::exception& e) {
		std::string error = "setRunTimes(..) says: DATA ACCESS PROBLEM";
		throw(std::runtime_error(error));
	}

	if (Stop_Machines->size())
		return true;
	else
		return false;
}

bool partitionVia(std::string list, std::vector<std::string>& partitioned, std::string breaker) {
	std::vector<size_t> idxBreaker;
	size_t buffIdx = 0;
	std::string buffStr = "";
	while (list.find(breaker, buffIdx) != std::string::npos) {
		buffIdx = list.find(breaker, buffIdx);
		idxBreaker.push_back(buffIdx);
		buffIdx++;
	}
	if (idxBreaker.size() == 0)
		return false;
	buffIdx = 0;
	for (unsigned int i = 0; i < idxBreaker.size(); i++) {
		buffStr = list.substr(buffIdx, idxBreaker.at(i) - buffIdx);
		buffIdx = idxBreaker.at(i) + breaker.size();
		partitioned.push_back(buffStr);
	}

	return true;
}
