// $Id$
//
// ATLAS Run Control
//
// Author: Dietrich Liko, Giovanna Lehmann  

// Print the application name of the root controller or of the Online Segment

#include <iostream>

#include <boost/program_options.hpp>

#include "ipc/core.h"
#include "ers/ers.h"

#include "config/Configuration.h"

#include "RunControl/Common/Exceptions.h"

#include "dal/Partition.h"
#include "dal/OnlineSegment.h"
#include "dal/RunControlApplication.h"


int main(int argc, char ** argv) {

   try{
   	IPCCore::init(argc,argv);
   }catch(daq::ipc::CannotInitialize& e){
   	ers::fatal(e);
	abort();
   }catch(daq::ipc::AlreadyInitialized& e){
   	ers::warning(e);
   }            

  // take defaults from environment
  std::string tdaq_partition(""), tdaq_db("");
  //  std::string def_p, def_d, def_a, def_l;
  if (getenv("TDAQ_PARTITION"))
     tdaq_partition = getenv("TDAQ_PARTITION");
  if (getenv("TDAQ_DB"))
    tdaq_db = getenv("TDAQ_DB");

  // parse commandline parameters

  boost::program_options::options_description desc("Allowed options", 128);
  desc.add_options()
    ("help,h", "help message")
    ("partition,p",   boost::program_options::value<std::string>(&tdaq_partition)->default_value(tdaq_partition), "partition name (TDAQ_PARTITION)")
    ("database,d",    boost::program_options::value<std::string>(&tdaq_db)->default_value(tdaq_db), "database (TDAQ_DB)")
    ("segment,s", "print online segment instead of root controller")
;
  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::parse_command_line(argc,argv,desc), vm);
  boost::program_options::notify(vm);

  if(vm.count("help")) {
    desc.print(std::cout);
    std::cout << std::endl;
    return EXIT_SUCCESS;
  }

   if(tdaq_partition.empty() || tdaq_db.empty()){
    daq::rc::CmdLineError issue(ERS_HERE,"");
     ers::error(issue);
     return EXIT_FAILURE;
   }

   Configuration config(tdaq_db);
   
   if (!config.loaded()) 
   {
      daq::rc::ConfigurationIssue issue(ERS_HERE,"Failed to load database");
      ers::fatal(issue);
      return EXIT_FAILURE;
   }
 
   // find root controller in the root partition

   const daq::core::Partition * root = config.get<daq::core::Partition>(tdaq_partition);
   if (!root)
   {
      daq::rc::ConfigurationIssue issue(ERS_HERE,"Partition not found: ", tdaq_partition.c_str());
      ers::fatal(issue);
      return EXIT_FAILURE;
   }

   const daq::core::Segment * online = root->get_OnlineInfrastructure();
   if (!online)
   {
       daq::rc::ConfigurationIssue issue(ERS_HERE,"Segment not found: ", "OnlineInfrastructure");
      ers::fatal(issue);

      return EXIT_FAILURE;      
   }

   if(vm.count("segment")) {
	std::cout << online->UID() << std::endl;
        return EXIT_SUCCESS;
   }

   const daq::core::RunControlApplicationBase * rootController = online->get_IsControlledBy();
   if (!rootController)
   {
      daq::rc::ConfigurationIssue issue(ERS_HERE, "RootController for OnlineInfraStructure not found", "");
      ers::fatal(issue);
      return EXIT_FAILURE;
   }

   std::cout << rootController->UID() << std::endl;
   
   return EXIT_SUCCESS;
}
