// $Id$
//
// ATLAS Run Control
//
// Author: Bob Jones, Sarah Wheeler, Dietmar Schweiger
// Current: G. Lehmann Miotto, September 2002 

// Log the Run Control status to stdout

#include <unistd.h>
#include <functional>

#include <boost/program_options.hpp>

#include "ers/ers.h"
#include "ipc/core.h"
#include "is/inforeceiver.h"
#include "is/infodictionary.h"
#include "rc/RCStateInfo.h"
#include "RunControl/Common/Exceptions.h"
#include "Exceptions.h"


int main(int argc, char ** argv) 
{
   try{
   	IPCCore::init(argc,argv);
   }catch(daq::ipc::CannotInitialize& e){
   	ers::fatal(e);
	abort();
   }catch(daq::ipc::AlreadyInitialized& e){
   	ers::warning(e);
   }

   std::string partition="" , name="RootController", server="RunCtrl";
   int timeout=10;

   if (getenv("TDAQ_PARTITION"))
     partition = getenv("TDAQ_PARTITION");

  // parse commandline parameters

  boost::program_options::options_description desc("Allowed options", 128);
  desc.add_options()
    ("help,h", "help message")
    ("partition,p",   boost::program_options::value<std::string>(&partition)->default_value(partition), "partition name (TDAQ_PARTITION)")
    ("name,n",    boost::program_options::value<std::string>(&name)->default_value(name), "controller name (RootController)")
    ("server,s",   boost::program_options::value<std::string>(&server)->default_value(server), "IS server (RunCtrl)")
    ("timeout,T",   boost::program_options::value<int>(&timeout)->default_value(timeout), "timeout before giving up")
    ;

  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::parse_command_line(argc,argv,desc), vm);
  boost::program_options::notify(vm);


  if(vm.count("help")) {
    desc.print(std::cout);
    std::cout << std::endl;
    return EXIT_SUCCESS;
  }

  if(partition.empty()) {
     daq::rc::CmdLineError issue(ERS_HERE,"");
     ers::error(issue);
     return EXIT_FAILURE;
   }

   IPCPartition p(partition);
   ISInfoDictionary isInfoDict(p);

   RCStateInfo stateInfo;
   std::string isname = server + "." + name; 

   int time = 0;	
   while ( true ) 
   {
   try 	{
      	isInfoDict.findValue(isname.c_str(),stateInfo);
	}
   catch ( daq::is::Exception & ex )
	{
	sleep(1);
      	time+=1;
	if ( timeout < time )
		{
       		std::ostringstream reason;
       		reason << "Failure reading state info for " << name << ":";
       		ers::fatal(daq::rc::ISReadFailure(ERS_HERE,reason.str().c_str(),ex));
      		return EXIT_FAILURE;
     		}		
	continue ;
	}
   break ; // info found
   } 

   auto printError = [&stateInfo]() -> std::string
                     {
                         std::string errStr;

                         if(stateInfo.errorReasons.empty() == true) {
                             errStr = "NONE";
                         } else {
                             for(const auto& e : stateInfo.errorReasons) {
                                 errStr += e + "; ";
                             }
                         }

                         return errStr;
                     };

   ERS_INFO("State: " << stateInfo.state.c_str() <<
   	       ", Fault: " << (stateInfo.fault ? "Error" : "Ok") <<
	       ", Busy: " <<  (stateInfo.busy  ? "Busy" : "Free") <<
           ", Last Transition: " << (stateInfo.lastTransitionName.empty() ? "NONE": stateInfo.lastTransitionName) <<
           ", Current Transition: " << (stateInfo.currentTransitionName.empty() ? "NONE": stateInfo.currentTransitionName) <<
	       ", Last Command: " << (stateInfo.lastCmdName.empty() ? "NONE": stateInfo.lastCmdName) <<
	       ", Error: " << printError() << "."
	     );

   return EXIT_SUCCESS;
}
