//      Controller dispatching LB and prescale changes to the HLTSVs (for emulation and testing)
//
//      G. Lehmann Miotto, January 2010

#include <iostream>
#include <memory>
#include <atomic>

#include <boost/program_options.hpp>

#include <ers/ers.h>

#include <config/Configuration.h>

#include <dal/util.h>
#include <dal/Segment.h>
#include <dal/Partition.h>
#include <dal/BaseApplication.h>

#include <RunControl/ItemCtrl/ItemCtrl.h>
#include <RunControl/ItemCtrl/ControllableDispatcher.h>
#include <RunControl/Common/Controllable.h>
#include <RunControl/Common/Exceptions.h>
#include <RunControl/Common/CommandSender.h>
#include <RunControl/Common/OnlineServices.h>
#include <RunControl/Common/RunControlCommands.h>
#include <RunControl/Common/CmdLineParser.h>
#include <RunControl/FSM/FSMStates.h>

#include <TriggerCommander/TriggerCommander.h>
#include <TriggerCommander/CommandedTrigger.h>
#include <TriggerCommander/MasterTrigger.h>
#include <TriggerCommander/HoldTriggerInfo.h>

#include <TTCInfo/LumiBlockNamed.h>

#include <rc/RunParamsNamed.h>

#include "Exceptions.h"

namespace po = boost::program_options;

namespace daq {
namespace rc {

class MyMasterTrigger: public daq::trigger::MasterTrigger {
    public:
        /** Trigger extension example **/

        MyMasterTrigger(std::vector<std::string> & l2svs, IPCPartition ipcPart) :
                daq::trigger::MasterTrigger(), m_ipcPart(ipcPart), m_lb(ATOMIC_VAR_INIT(0))
        {
            for(size_t i = 0; i < l2svs.size(); ++i) {
                daq::trigger::TriggerCommander * tc = new daq::trigger::TriggerCommander(ipcPart, l2svs[i]);
                m_l2svs.push_back(tc);
            }
        }

        ~MyMasterTrigger() {
            for(size_t i = 0; i < m_l2svs.size(); ++i) {
                delete m_l2svs[i];
            }

            m_l2svs.clear();
        }

        virtual daq::trigger::HoldTriggerInfo hold(const std::string &s) override {
            ERS_LOG("Putting trigger on hold and retrieving last valid ECR");
            daq::trigger::HoldTriggerInfo trgInfo {};
            for(size_t i = 0; i < m_l2svs.size(); ++i) {
                try {
                    trgInfo = m_l2svs[i]->hold(s);
                }
                catch(ers::Issue &e) {
                    ers::warning(e);
                }
            }
            return trgInfo;
        }

        virtual void resume(const std::string &s) override {
            ERS_LOG("Resuming trigger");
            for(size_t i = 0; i < m_l2svs.size(); ++i) {
                try {
                    m_l2svs[i]->resume(s);
                }
                catch(ers::Issue &e) {
                    ers::warning(e);
                }
            }
        }

        virtual void setPrescales(uint32_t l1p, uint32_t hltp) override {
            ERS_LOG("Setting L1 and HLT prescale keys to " << l1p << " and " << hltp);
            for(size_t i = 0; i < m_l2svs.size(); ++i) {
                try {
                    m_l2svs[i]->setPrescales(l1p, hltp);
                }
                catch(ers::Issue &e) {
                    ers::warning(e);
                }

            }
        }

        virtual void setL1Prescales(uint32_t l1p) override {
            ERS_LOG("Setting L1 prescales key to " << l1p);
            for(size_t i = 0; i < m_l2svs.size(); ++i) {
                try {
                    m_l2svs[i]->setL1Prescales(l1p);
                }
                catch(ers::Issue &e) {
                    ers::warning(e);
                }
            }
        }

        virtual void setHLTPrescales(uint32_t hltp) override {
            ERS_LOG("Switching to HLT prescales key " << hltp);
            for(size_t i = 0; i < m_l2svs.size(); ++i) {
                try {
                    m_l2svs[i]->setHLTPrescales(hltp);
                }
                catch(ers::Issue &e) {
                    ers::warning(e);
                }
            }
        }

        virtual void setLumiBlockInterval(uint32_t seconds) override {
            ERS_LOG("Setting the lumi block interval to " << seconds << " seconds");
            for(size_t i = 0; i < m_l2svs.size(); ++i) {
                try {
                    m_l2svs[i]->setLumiBlockInterval(seconds);
                }
                catch(ers::Issue &e) {
                    ers::warning(e);
                }
            }
        }

        virtual void setMinLumiBlockLength(uint32_t seconds) override {
            ERS_LOG("Setting the minimum lumi block length to " << seconds << " seconds");
            for(size_t i = 0; i < m_l2svs.size(); ++i) {
                try {
                    m_l2svs[i]->setMinLumiBlockLength(seconds);
                }
                catch(ers::Issue &e) {
                    ers::warning(e);
                }
            }
        }

        virtual void increaseLumiBlock(uint32_t runno) override {
            ERS_LOG("Increasing luminosity block number");
            for(size_t i = 0; i < m_l2svs.size(); ++i) {
                try {
                    m_l2svs[i]->increaseLumiBlock(runno);
                }
                catch(ers::Issue &e) {
                    ers::warning(e);
                }
            }

            const uint32_t lb = ++m_lb;

            // tag time
            OWLTime now(OWLTime::Microseconds);
            u_int nowns = now.mksec() * 1000;
            // Get Run number
            RunParamsNamed run_params(m_ipcPart, "RunParams.RunParams");
            run_params.checkout();
            // publish LumiBlockInfo
            LumiBlockNamed lumiblock_is_entry(m_ipcPart, "RunParams.LumiBlock");
            // fill LB start value
            lumiblock_is_entry.RunNumber = run_params.run_number;
            lumiblock_is_entry.LumiBlockNumber = lb;
            lumiblock_is_entry.Time = nowns;

            lumiblock_is_entry.checkin();
        }

        virtual void setBunchGroup(uint32_t bg) override {
            ERS_LOG("Setting bunch group key to " << bg);
            for(size_t i = 0; i < m_l2svs.size(); ++i) {
                try {
                    m_l2svs[i]->setBunchGroup(bg);
                }
                catch(ers::Issue &e) {
                    ers::warning(e);
                }
            }
        }

        virtual void setConditionsUpdate(uint32_t folderIndex, uint32_t lb) override {
            ERS_LOG("Updating conditions at folder index  " << folderIndex << " from luminosity block " << lb);
            for(size_t i = 0; i < m_l2svs.size(); ++i) {
                try {
                    m_l2svs[i]->setConditionsUpdate(folderIndex, lb);
                }
                catch(ers::Issue &e) {
                    ers::warning(e);
                }
            }
        }

        virtual void setPrescalesAndBunchgroup(uint32_t l1p, uint32_t hltp, uint32_t bg) override {
            ERS_LOG("Setting L1 prescale key to  " << l1p << ", HLT prescale key to " << hltp << " and bunch group key to " << bg);
            for(size_t i = 0; i < m_l2svs.size(); ++i) {
                try {
                    m_l2svs[i]->setPrescalesAndBunchgroup(l1p, hltp, bg);
                }
                catch(ers::Issue &e) {
                    ers::warning(e);
                }
            }
        }

    private:
        std::vector<daq::trigger::TriggerCommander*> m_l2svs;
        IPCPartition m_ipcPart;
        std::atomic<unsigned int> m_lb;
};

/**
 * Example of how to inherit from the Controllable interface.
 */
class MyControllable: public daq::rc::Controllable {
    public:

        ~MyControllable() noexcept {
        }

        virtual void configure(const daq::rc::TransitionCmd&) override {

            m_commander = new daq::rc::CommandSender(daq::rc::OnlineServices::instance().getIPCPartition(),
                                                     daq::rc::OnlineServices::instance().applicationName());
            m_l2svs.clear();

            // Get L2SVs from DB
            std::set<std::string> app_types;
            app_types.insert("HLTSVApplication");
            std::vector<const daq::core::BaseApplication*> appConfigs = daq::rc::OnlineServices::instance().getPartition().get_all_applications(&app_types);
            /*
             if(appConfigs.size() == 0) {
             throw(daq::rc::BadConfiguration(ERS_HERE, "no L2SV in partition."));
             }
             */
            for(unsigned int i = 0; i < appConfigs.size(); ++i) {
                m_l2svs.push_back(appConfigs[i]->UID());
            }

            m_master = new daq::rc::MyMasterTrigger(m_l2svs, daq::rc::OnlineServices::instance().getIPCPartition());
            m_commandedTrigger =
                    new daq::trigger::CommandedTrigger(daq::rc::OnlineServices::instance().getIPCPartition(),
                                                       daq::rc::OnlineServices::instance().applicationName(),
                                                       m_master);
        }

        virtual void unconfigure(const daq::rc::TransitionCmd &) override {
            m_commandedTrigger->_destroy();
            delete m_master;
            delete m_commander;
        }

        virtual void user(const daq::rc::UserCmd& uc) override {
            const std::string& cmd = uc.commandName();
            if(cmd == "UPDATELVL1PRESCALES" || cmd == "SETHLTCOUNTER" || cmd == "SETBUSY" || cmd == "UNSETBUSY"
                    || cmd == "LUMIBLOCK") {

                std::string cmdToSend(cmd);
                for(unsigned int i = 0; i << m_l2svs.size(); ++i) {
                    try {
                        m_commander->userCommand(m_l2svs[i], uc);
                    }
                    catch(ers::Issue &e) {
                        ers::fatal(e);
                    }
                }
            }
        }

        virtual void onExit(FSM_STATE state) noexcept override {
            ERS_LOG("Exiting application from state " << FSMStates::stateToString(state));
        }

    private:
        std::vector<std::string> m_l2svs;
        daq::rc::CommandSender* m_commander;
        daq::trigger::CommandedTrigger *m_commandedTrigger;
        daq::rc::MyMasterTrigger *m_master;
};

} // rc
} // daq

int main(int argc, char **argv) {
    std::string parentName, segmentName, name;

    try {
        daq::rc::CmdLineParser cmdParser(argc, argv, true);

        const char *partName = std::getenv("TDAQ_PARTITION");
        if(partName == NULL) {
            std::cerr << "${TDAQ_PARTITION} is not set." << std::endl;
            return EXIT_FAILURE;
        }

        daq::rc::ItemCtrl * myItem =
                new daq::rc::ItemCtrl(cmdParser, std::shared_ptr<daq::rc::Controllable>(new daq::rc::MyControllable()));

        myItem->init();
        myItem->run();

        delete myItem;
    }
    catch(daq::rc::CmdLineHelp& ex) {
        std::cout << "This program is a simple data taking application" << std::endl;
        std::cout << ex.message() << std::endl;
    }
    catch(ers::Issue& ex) {
        ers::fatal(ex);
        return EXIT_FAILURE;
    }

    std::exit(EXIT_SUCCESS);
}
