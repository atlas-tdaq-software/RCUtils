#!/usr/bin/env tdaq_python

from __future__ import print_function
from builtins import input
import sys
import traceback
import argparse
import textwrap

import config

def printRacks(templateSegments):
    for ts in templateSegments:
        print("\n### " + ts.id)
        print(ts.Racks)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description = textwrap.dedent('''\
                                                      This program allows to change the allocation of TPU racks to segments.
                                                      Racks can be specified in a comma-separated way using the rack UID only:
                                                      tpu-rack-01,tpu-rack-02,...
                                                      '''))
    parser.add_argument('-d', '--database', dest = 'database', help = 'OKS database file', required = True, action = 'store')
    parser.add_argument('-l', '--list', dest = 'list', help = 'List the current rack allocation only, without applying any change', 
                       action = 'store_true')
    options = parser.parse_args()

    try:
        mydb = 'oksconfig:' + options.database

        db = config.Configuration(mydb)

        templateSegments = db.get_dals('TemplateSegment')

        if options.list == False:
            for ts in templateSegments:
                print("\n### Segment \"" + ts.id + "\" has the following racks: ")
                print(ts.Racks)
                print("\nPlease input a new rack list or just press enter to keep the current list: ")

                readInput = True

                while readInput:
                    newRackList = input("# ")
                    if len(newRackList) != 0:
                        newRackArray = newRackList.strip().split(',')
                        rackDals = []

                        badRack = False
                        for r in newRackArray:
                            try:
                                rackObject = db.get_dal("Rack", r.strip())
                                rackDals.append(rackObject)
                            except:
                                badRack = True
                                print("Rack \"" + r + "\" seems to not be valid")                            

                        readInput = badRack

                        if badRack == True:
                            print("Please, insert again the rack list for segment \"" + ts.id + "\"")
                        else:
                            ts.Racks = rackDals
                            db.update_dal(ts)
                    else:
                        readInput = False

            print("\nHere is the new rack configuration:")
            printRacks(templateSegments)

            commit = input("\nPlease, type \'yes\' to commit changes: ")
            if commit == "yes":
                db.commit()
                print("Configuration has been committed, exiting")
            else:
                print("Configuration has not been committed, exiting")

        else:
            print("\nHere is the current rack configuration:")
            printRacks(templateSegments)
            
    except KeyboardInterrupt:              
        sys.exit(1)
    except:
        print("ERROR!")
        traceback.print_exc()
        sys.exit(183)

    sys.exit(0)
