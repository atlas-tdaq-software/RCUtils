#!/usr/bin/python
# -*- coding: utf-8 -*-


from __future__ import division
from __future__ import print_function
from builtins import map
from builtins import range
from past.utils import old_div
import os
import sys
import pprint
import osirispy
import json
import struct
import datetime
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

"""
ATLAS Run Efficiency. Get data from P-BEAST.
Alejandro Santos - alejandro.santos@cern.ch

IMPORTANT: Python installation is broken with TDAQ, use native Python from the machine.

It also takes two timestamps as arguments, dont forget the quotes. Example:

  /usr/bin/python calc_run_eff.py "2015-5-29 14:38:42" "2015-5-29 15:36:42"

- Use the machine's Python, tdaq_python has a broken PYTHONPATH:

  /usr/bin/python

- Use a machine with numpy available, ie, pc-tdq-bst-04.

- Requires SAReplay OSIRIS Python bindings. Already on nightly, soon in the TDAQ release.

In the meantime from P1:

export LD_LIBRARY_PATH=/atlas-home/1/asantos/bin:$LD_LIBRARY_PATH
export PYTHONPATH=/atlas-home/1/asantos/bin:$PYTHONPATH
"""

# CABLES TO PLOT, by cable index in a list: [1, 3, 4]
ctpout_cables_to_plot = None

# Enable debug messages from inside the API
osirispy.settings.set_verbose(0)

def description_nice(d):
    return {'part_name': d.part_name, 
        'class_name': d.class_name, 
        'attr_name': list(d.attr_name),
        'type': (d.type_str, d.data_type, d.is_array)}

def playback_iter(P):
    P.begin()
    while P.hasnext():
        e = P.getnext()
        yield e
        P.advance()

CONV = {
    osirispy.BOOL: int,
    osirispy.S8: int,
    osirispy.U8: int,
    osirispy.S16: int,
    osirispy.U16: int,
    osirispy.S32: int,
    osirispy.U32: int,
    osirispy.S64: int,
    osirispy.U64: int,
    osirispy.FLOAT: float,
    osirispy.DOUBLE: float,
    osirispy.ENUM: int,
    osirispy.DATE: int,
    osirispy.TIME: int,
    osirispy.STRING: str,
    osirispy.OBJECT: str,
    osirispy.VOID: str,
    osirispy.OSIRIS_DOUBLE_AS_LONG: (lambda x: struct.unpack("d", struct.pack("q", int(V)))[0]),
    osirispy.OSIRIS_FLOAT_AS_INT: (lambda x: struct.unpack("f", struct.pack("i", int(x)))[0]),
}

def parse_osiris_value(raw_value, data_type):
    if data_type[1]: # array
        assert raw_value.startswith("array:")
        v = raw_value.split(":", 3)
        t = int(v[1])
        l = int(v[2])
        d = json.loads(v[3])
    else:
        assert raw_value.startswith("single:")
        v = raw_value.split(":", 2)
        t = int(v[1])
        l = 1
        d = json.loads(v[2])

    assert len(d) == l
    w = list(map(CONV[t], d))

    if data_type[1]:
        return w
    else:
        return w[0]

def fmtts(ts):
    return datetime.datetime.utcfromtimestamp(old_div(ts,1e6)).isoformat()

def calc_dead_time(s, since, till):
    s = list(s)

    if len(s):
        assert s[-1][0] < till

    # take the last value on the sequence until the till ts
    s.append([till, None])

    start = s[0][0]
    end = s[-1][0]
    tlen = end - start 

    # print start, end, tlen, since, till

    #assert start < since and end >= till 

    # print since - start 

    a = 0
    for i in range(len(s) - 1):
        t = (s[i + 1][0] - s[i][0]) * s[i][1]
        a += t 

    if tlen > 0:
        return (a, float(tlen))
    return (0.0, 0.0)

def overlaps(x, y):
    #      (c, d)
    #   (a, b)(a, b)
    a, b = x
    c, d = y
    return c <= b and d >= a 

def filter_enabled(frac, enabled, since, till):
    #       [a, b]  [c, d] (enabled)
    #  1 2 3 4 5 6 7 8 9 0 1
    # 
    #      (c, d)
    #   (a, b)(e, f)

    p = 0

    if len(frac) == 0:
        return []

    # FIXME: If the point begins before start, move it to start.

    if since > frac[0][0]:
        L = []
    else:
        L = [(since, 0.0)]

    if len(enabled) == 0:
        return L

    # FIXME: If the point begins before the enabled interval, cut it to start from 
    # the beginning of the interval.

    for i in range(len(frac)):
        start = frac[i][0]
        if i < (len(frac) - 1):
            end = frac[i+1][0]
        else:
            end = till
        if overlaps((start, end), enabled[p]):
            L.append(frac[i])
        elif enabled[p][1] < start:
            L.append((enabled[p][1], 0.0))
            p += 1
            if p == len(enabled):
                break
    return L

def main():
    since, till = None, None 
    if len(sys.argv) < 3:
        # RN: 256050, 18:02:54
        #since = osirispy.str2ts("2015-03-14 16:36:10")
        #till = osirispy.str2ts("2015-03-14 17:36:09")
        #till = osirispy.str2ts("2015-03-15 10:39:10")
        #till = osirispy.str2ts("2015-03-15 11:08:00")
        print("Please run with time intervals in GMT time as arguments. Example: ", sys.argv[0] + ' "2015-05-29 9:00:00" "2015-05-30 3:00:00"')
        sys.exit(1)
    else:
        since = osirispy.str2ts(sys.argv[1])
        till = osirispy.str2ts(sys.argv[2])
        assert since < till

    if ('TDAQ_SETUP_POINT1' in os.environ):
        A = osirispy.api_corba("initial", "pbeast-server")
    else:
        A = osirispy.api_eos(os.environ['HOME'] + "/eos/atlas/atlascerngroupdisk/tdaq-opmon/pbeast")

    print("since:", since, "till:", till)

    if False:
        D1 = A.describe_class("ATLAS", "CtpBusyInfo", since, till, True)
        pprint.pprint(list(map(description_nice, D1)))

    S = osirispy.series_fetcher_by_part_class_attr(A, None)
    # S.add_part_and_class("ATLAS", "RunInfo", since, till)
    # S.add_part_and_class("ATLAS", "RCStateInfo", since, till)
    S.add_part_and_class("ATLAS", "CtpBusyInfo", since, till)

    ts = 0

    P = osirispy.playback(A, since, till)
    P.inject_fetcher(S)

    ctpout_cables_names = {}
    ctpout_cables_enabled = dict([(i, []) for i in range(25)])
    ctpout_cables_fraction = dict([(i, []) for i in range(25)])

    ctpcore_objects_names = {}
    ctpcore_objects_enabled = dict([(i, []) for i in range(15)])
    ctpcore_objects_fraction = dict([(i, []) for i in range(15)])

    for e in playback_iter(P):
        # Because a P-BEAST object name of a nested type object follows the format of 
        # the obj_name + nested_attribute_names, we need to parse it.
        VPII = osirispy.PairStringIntVector()
        osirispy.parse_nested_obj_name(e.attr_name, e.obj_name, VPII)
        parsed_obj_name = [(x[0], x[1]) for x in VPII]

        part_name = e.part_name
        class_name = e.class_name
        # The attribute name of a nested type consists of the path of the attribute.
        attr_name = list(e.attr_name)
        raw_obj_name = e.obj_name
        # parsed_obj_name
        orig_created = e.series.get_current_created()
        orig_last_updated = e.series.get_current_last_updated()
        stripped_created = P.get_current_created()
        stripped_last_updated = P.get_current_last_updated()
        data_type = (e.series.get_pbeast_type(), e.series.get_is_array())
        # This is the string representation of the value, becase from OSIRIS we are doing 
        # type erasure of P-BEAST data types.
        raw_value = e.series.get_current_value_str()
        # And this is the value with a real Python data type.
        parsed_value = parse_osiris_value(raw_value, data_type)

        if parsed_obj_name[0][0] != 'L1CT.CTP.Instantaneous.BusyFractions':
            continue

        if orig_created < ts:
            pass
            #print "ignoring: "
            #print fmtts(orig_created), fmtts(orig_last_updated), (orig_last_updated - orig_created), parsed_obj_name, attr_name, data_type, raw_value, parsed_value
        else:
            ts = orig_created
            if attr_name == ['ctpout_cables', 'CtpoutBusyInfoCable', 'name']:
                idx = parsed_obj_name[1][1]
                if idx in ctpout_cables_names:
                    if not ctpout_cables_names[idx] == parsed_value:
                        print("ctpout cable changing name from ", ctpout_cables_names[idx], " to ", parsed_value)
                ctpout_cables_names[idx] = parsed_value
            elif attr_name == ['ctpcore_objects', 'CtpcoreBusyInfoObject', 'name']:
                idx = parsed_obj_name[1][1]
                if idx in ctpcore_objects_names:
                    if not ctpcore_objects_names[idx] == parsed_value:
                        print("ctpcore object changing name from", ctpcore_objects_names[idx], " to ", parsed_value)
                ctpcore_objects_names[idx] = parsed_value

            if attr_name == ['ctpcore_objects', 'CtpcoreBusyInfoObject', 'enabled']:
                pass
                idx = parsed_obj_name[1][1]
                if parsed_value:
                    ctpcore_objects_enabled[idx].append([orig_created, orig_last_updated])

            if attr_name == ['ctpout_cables', 'CtpoutBusyInfoCable', 'enabled']:
                pass 
                #print fmtts(orig_created), fmtts(orig_last_updated), (orig_last_updated - orig_created), parsed_obj_name, attr_name, data_type, raw_value, parsed_value
                idx = parsed_obj_name[1][1]
                if parsed_value:
                    ctpout_cables_enabled[idx].append([orig_created, orig_last_updated])

            if attr_name == ['ctpcore_objects', 'CtpcoreBusyInfoObject', 'fraction']:
                pass
                #print fmtts(orig_created), fmtts(orig_last_updated), (orig_last_updated - orig_created), parsed_obj_name, attr_name, data_type, raw_value, parsed_value
                idx = parsed_obj_name[1][1]
                ctpcore_objects_fraction[idx].append([orig_created, parsed_value])

            if attr_name == ['ctpout_cables', 'CtpoutBusyInfoCable', 'fraction']:
                pass
                #print fmtts(orig_created), fmtts(orig_last_updated), (orig_last_updated - orig_created), parsed_obj_name, attr_name, data_type, raw_value, parsed_value
                idx = parsed_obj_name[1][1]
                ctpout_cables_fraction[idx].append([orig_created, parsed_value])

    pprint.pprint(("ctpout_cables_names", ctpout_cables_names))
    pprint.pprint(("ctpcore_objects_names", ctpcore_objects_names))

    for idx in ctpout_cables_enabled:
        ctpout_cables_enabled[idx].sort()

    for idx in ctpcore_objects_enabled:
        ctpcore_objects_enabled[idx].sort()

    pprint.pprint(("ctpout_cables_enabled", ctpout_cables_enabled))
    pprint.pprint(("ctpcore_objects_enabled", ctpcore_objects_enabled))

    ctpout_cables_dead = {}
    wa = 0.0
    wt = 0.0

    for x in ctpout_cables_fraction:
        ctpout_cables_fraction[x].sort()
        ff = filter_enabled(ctpout_cables_fraction[x], ctpout_cables_enabled[x], since, till)
        # print "cable", x, ctpout_cables_fraction[x], ff
        ctpout_cables_fraction[x] = ff

        raw = calc_dead_time(ff, since, till)
        wa += raw[0]
        wt += raw[1]
        if raw[1] > 0.0:
            dead = old_div(raw[0], raw[1])
        else:
            dead = 0.0
        ctpout_cables_dead[x] = {'raw': raw, 'dead': dead, 'eff': 1.0 - dead, 'name': ctpout_cables_names[x]}

    ctpcore_objects_dead = {}
    wa = 0.0
    wt = 0.0

    for x in ctpcore_objects_fraction:
        ctpcore_objects_fraction[x].sort()
        ff = filter_enabled(ctpcore_objects_fraction[x], ctpcore_objects_enabled[x], since, till)
        # print "objects", x, ctpcore_objects_fraction[x], ff
        ctpcore_objects_fraction[x] = ff

        raw = calc_dead_time(ff, since, till)
        wa += raw[0]
        wt += raw[1]
        if raw[1] > 0.0:
            dead = old_div(raw[0], raw[1])
        else:
            dead = 0.0
        ctpcore_objects_dead[x] = {'raw': raw, 'dead': dead, 'eff': 1.0 - dead, 'name': ctpcore_objects_names[x]}

    # pprint.pprint(("ctpout_cables_fraction", ctpout_cables_fraction))
    pprint.pprint(("ctpout_cables_dead", ctpout_cables_dead))
    pprint.pprint(("ctpcore_objects_dead", ctpcore_objects_dead))

    plt_total0_x = np.array([datetime.datetime.utcfromtimestamp(old_div(e[0], 1e6)) for e in ctpcore_objects_fraction[8]])
    plt_total0_y = np.array([(1.0 - e[1]) for e in ctpcore_objects_fraction[8]])

    plt.plot(plt_total0_x, plt_total0_y, '-', label="Total0")
    plt.legend(loc='upper left')

    plt.show()

    plt.cla()
    plt.clf()

    plt.plot(plt_total0_x, plt_total0_y, '-', label="Total0")

    # Plot all the cables.
    # FIXME: Fine tune which cables you want to plot?
    
    global ctpout_cables_to_plot

    if ctpout_cables_to_plot is None:
        ctpout_cables_to_plot = sorted(ctpout_cables_fraction)

    for idx in ctpout_cables_to_plot:
        plt_x = np.array([datetime.datetime.utcfromtimestamp(old_div(e[0], 1e6)) for e in ctpout_cables_fraction[idx]])
        plt_y = np.array([(1.0 - e[1]) for e in ctpout_cables_fraction[idx]])

        if len([e for e in plt_y if e > 0.0]) > 1: # Plot cables with at least two values, prevent broken plots.
            plt.plot(plt_x, plt_y, '-', label=ctpout_cables_names[idx])

    plt.legend(loc='upper left')
    plt.show()

if __name__ == '__main__':
    main()
