package RCUtils

author  Giovanna.Lehmann@cern.ch, Giuseppe.Avolio@cern.ch
manager Giuseppe.Avolio@cern.ch

use CORAL * TDAQExternal
use COOL * TDAQExternal
use Boost * TDAQCExternal
use tbb
use ers
use eformat
use system
use owl
use cmdl
use ProcessManager
use RCInfo
use ddcInfo
use tmgr
use dal
use DFConfiguration
use is
use ipc
use config
use wmi
use RunControl
use TriggerCommander
use TTCInfo
private
use Qt4    * TDAQExternal



#-------------------------------------------------------------------------------
# Linking options
#-------------------------------------------------------------------------------
macro my_linkopts           "-lboost_program_options-$(boost_libsuffix) -lers"
macro my_is_linkopts        "-lis "
macro my_config_linkopts    "-ldaq-core-dal -lconfig "

#-------------------------------------------------------------------------------
# QT (momentary disabled - it needs to be ported to the new RC).
#-------------------------------------------------------------------------------
#make_fragment moc_header
#make_fragment moc -header=moc_header -suffix=moc.cpp

#make_fragment ui_header
#make_fragment ui -header=ui_header -suffix=ui.h 
#pattern qt_uigen \
# document ui <name> -s=<dir> <files>

#qt_uigen  name=uigen   dir=../src/qt files="rc_commander.ui"
 
#pattern qt_mocgen \
# document moc <name> -s=<dir> <files>
                    
#qt_mocgen name=mocs dir=../src/qt files="rc_commander.h"

#application rc_commander $(lib_opts) "./qt/rc_commander.cpp $(bin)rc_commander.moc.cpp ./qt/main.cpp"
#macro rc_commander_dependencies "uigen mocs "
                                  
#macro  rc_commanderlinkopts   "-lQtGui -lcommander $(my_linkopts)"


#-------------------------------------------------------------------------------
# Other utilities
#-------------------------------------------------------------------------------

#application rc_tst                 -no_prototypes      tst.cc
#macro rc_tstlinkopts               "$(my_is_linkopts) $(my_linkopts)"
application rc_decode_detectormask -no_prototypes      get_detectormask.cc
application rc_encode_detectormask -no_prototypes      encode_detectormask.cc
application rc_getrunnumber        -no_prototypes      get_runnumber.cc
application rc_isread              -no_prototypes      isread.cc
application rc_waitstate           -no_prototypes      waitstate.cc
application rc_timetest            -no_prototypes      "time_test.cc TimeTest.cc"
application rc_print_root          -no_prototypes       print_root.cc
application rc_print_tree          -no_prototypes       print_tree.cc
application rc_checkapps           -no_prototypes       checkapps.cc
application rc_print_partition_env -no_prototypes      print_partition_env.cc
application rc_up2initial          -no_prototypes      up2initial.cc
application rc_am_requester          -no_prototypes      am_requester.cc
application rc_error_generator 		-no_prototypes 		error_generator.cc
application rc_ready4recovery 		-no_prototypes 		ready4recovery.cc
#application rc_beam_unstable_reactor    -no_prototypes 		beam_unstable_reactor.cc
application rc_pixel_ready_reactor      -no_prototypes 		pixel_ready_reactor.cc
application rc_clocksetting             -no_prototypes 		clocksetting.cc
application rc_sound_player     	-no_prototypes		ers_sound_player.cc
application rc_ctp_emulator             -no_prototypes   ctp_emulator.cc

macro rc_decode_detectormasklinkopts  "$(my_linkopts) -leformat"
macro rc_encode_detectormasklinkopts  "$(my_linkopts) -ldaq-df-dal -leformat"
macro rc_getrunnumberlinkopts         "$(my_is_linkopts) $(my_linkopts)"
macro rc_isreadlinkopts               "$(my_is_linkopts) $(my_linkopts)"
macro rc_waitstatelinkopts            "-lcmdline $(my_is_linkopts) $(my_linkopts)"
macro rc_print_rootlinkopts           "$(my_config_linkopts) $(my_linkopts) -lipc"
macro rc_print_treelinkopts           "$(my_config_linkopts) $(my_linkopts) -lipc"
macro rc_checkappslinkopts            "$(my_config_linkopts) $(my_linkopts) -lipc"
macro rc_timetestlinkopts             "-lcmdline -lrc_Commander -lrc_FSMStates -lrc_FSMCommands -lrc_RCCommands $(my_is_linkopts) $(my_linkopts)"
macro rc_print_partition_envlinkopts "$(my_config_linkopts) -losw $(my_linkopts)"
macro rc_up2initiallinkopts            "-lrc_Commander -lrc_FSMStates -lrc_FSMCommands -lrc_RCCommands $(my_linkopts)"
macro rc_am_requesterlinkopts            "-lAccessManager -losw"
macro rc_error_generatorlinkopts	  "-lDFExceptions $(my_linkopts) $(my_is_linkopts) -lipc"
macro rc_ready4recoverylinkopts	        "$(my_linkopts) $(my_is_linkopts) -lipc"
#macro rc_beam_unstable_reactorlinkopts	  "-lESServer $(my_linkopts) $(my_is_linkopts) -lipc"
macro rc_pixel_ready_reactorlinkopts	  " -leformat $(my_linkopts) $(my_is_linkopts) -lipc"
macro rc_clocksettinglinkopts           "-lrc_Commander -ltrgCommander $(my_is_linkopts) $(my_config_linkopts) $(my_linkopts)"
macro rc_sound_playerlinkopts           	"-losw -lipc $(my_linkopts)"
macro rc_ctp_emulatorlinkopts           "$(my_linkopts) -ldaq-df-dal -lrc_ItemCtrl -ltrgCommander"

#--------------------------------------
# Cool archiving application
#--------------------------------------
macro           LcgLibs  "-llcg_CoolApplication -lnsl -lcrypt -ldl"

application rc_is2cool_archive -no_prototypes is2cool_archive.cc
macro rc_is2cool_archivelinkopts " $(LcgLibs) $(my_is_linkopts) $(my_config_linkopts) -lpmgsync $(my_linkopts) -l tbb"

application rc_dq_flag_writer -no_prototypes dq_flag_writer.cc
macro rc_dq_flag_writerlinkopts " $(LcgLibs) $(my_is_linkopts) -lpmgsync $(my_linkopts)"

#--------------------------------------
# Run efficiency applications
#--------------------------------------
macro           RootLibs  "-lHist -lCore -lMatrix -lRIO -lGraf"

application rc_get_running_time -no_prototypes get_running_time.cc
macro rc_get_running_timelinkopts " $(LcgLibs) $(my_is_linkopts) $(my_config_linkopts) -lpmgsync $(my_linkopts)"

application rc_get_efficiency -no_prototypes get_efficiency.cc
macro rc_get_efficiencylinkopts " $(LcgLibs) $(my_is_linkopts) $(my_config_linkopts) -lpmgsync $(my_linkopts)

application eff_run_efficiency -no_prototypes eff_run_efficiency.cc
macro eff_run_efficiencylinkopts " $(LcgLibs) $(my_is_linkopts) $(my_config_linkopts) -lpmgsync $(my_linkopts)

#application eff_csv_splunk -no_prototypes eff_csv_splunk.cc
#macro eff_csv_splunklinkopts " $(LcgLibs) $(my_is_linkopts) $(my_config_linkopts) -lpmgsync $(my_linkopts)

application eff_daq_efficiency -no_prototypes eff_daq_efficiency.cc
macro eff_daq_efficiencylinkopts " $(LcgLibs) $(my_is_linkopts) $(my_config_linkopts) -lpmgsync $(my_linkopts)

application eff_daq_splunk -no_prototypes eff_daq_splunk.cc
macro eff_daq_splunklinkopts " $(LcgLibs) $(my_is_linkopts) $(my_config_linkopts) -lpmgsync $(my_linkopts)

application eff_csv_splunk -no_prototypes eff_csv_splunk.cc
macro eff_csv_splunklinkopts " $(LcgLibs) $(my_is_linkopts) $(my_config_linkopts) -lpmgsync $(my_linkopts)

application eff_make_csv_file -no_prototypes eff_make_csv_file.cc
macro eff_make_csv_filelinkopts " $(LcgLibs) $(my_is_linkopts) $(my_config_linkopts) -lpmgsync $(my_linkopts) -lcmdline

application eff_make_combined_graphs -no_prototypes eff_make_combined_graphs.cc
macro eff_make_combined_graphslinkopts " $(LcgLibs) $(RootLibs) $(my_is_linkopts)  $(my_config_linkopts) -lpmgsync $(my_linkopts) -lcmdline

#--------------------------------------
# WMI plugin
#--------------------------------------
#library         wmiccplugin                    "WMIPlugin/wmiccplugin.cxx"
#macro           wmiccplugin_shlibflags         "-lboost_regex-$(boost_libsuffix) -lboost_date_time-$(boost_libsuffix) -lers -lis \
#                                                 -lipc -lwmi -lconfig -ldaq-core-dal"

library         wmireplugin                    "WMIPlugin/wmireplugin.cxx"
macro           wmireplugin_shlibflags         "-lboost_regex-$(boost_libsuffix) -lboost_date_time-$(boost_libsuffix) -llcg_CoolApplication -lnsl -lcrypt -ldl -lers -lis -lipc -lwmi -lconfig -ldaq-core-dal"

#library         wmiresources                    "WMIPlugin/wmiresources.cxx"
#macro           wmiresources_shlibflags         "-lboost_regex-$(boost_libsuffix) -lboost_date_time-$(boost_libsuffix) -lers \
#                                                $(LcgLibs) $(my_is_linkopts) -lipc -lwmi $(my_config_linkopts)"

#-------------------------------------------------------------------------------
# Go to sw repository database
#-------------------------------------------------------------------------------


macro sw.repository.binary.rc_is2cool_archive:description   "Application to archive run control information into COOL"
macro sw.repository.binary.rc_is2cool_archive:help.url   "https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltControlAndConfiguration"
macro sw.repository.binary.rc_is2cool_archive:needs.environment  "CORAL_AUTH_PATH"


macro sw.repository.binary.rc_dq_flag_writer:description   "Application to archive DQ flag into COOL"
macro sw.repository.binary.rc_dq_flag_writer:help.url   "https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltControlAndConfiguration"
macro sw.repository.binary.rc_dq_flag_writer:needs.environment  "CORAL_AUTH_PATH"

macro sw.repository.binary.rc_get_running_time:description   "Application to read run control information about the running time from COOL"
macro sw.repository.binary.rc_get_running_time:help.url   "https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltControlAndConfiguration"
macro sw.repository.binary.rc_get_running_time:needs.environment  "CORAL_AUTH_PATH"

macro sw.repository.binary.rc_get_efficiency:description   "Application to evaluate data taking efficency"
macro sw.repository.binary.rc_get_efficiency:help.url   "https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltControlAndConfiguration"
macro sw.repository.binary.rc_get_efficiency:needs.environment  "CORAL_AUTH_PATH"

macro sw.repository.binary.eff_run_efficiency:description   "Application to evaluate run efficency"
macro sw.repository.binary.eff_run_efficiency:help.url   "https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltControlAndConfiguration"
macro sw.repository.binary.eff_run_efficiency:needs.environment  "CORAL_AUTH_PATH"

#macro sw.repository.binary.eff_csv_splunk:description   "Application to evaluate run efficency"
#macro sw.repository.binary.eff_csv_splunk:help.url   "https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltControlAndConfiguration"
#macro sw.repository.binary.eff_csv_splunk:needs.environment  "CORAL_AUTH_PATH"

macro sw.repository.binary.eff_daq_efficiency:description   "Application to evaluate lumi block efficiency"
macro sw.repository.binary.eff_daq_efficiency:help.url   "https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltControlAndConfiguration"
macro sw.repository.binary.eff_daq_efficiency:needs.environment  "CORAL_AUTH_PATH"

macro sw.repository.binary.eff_daq_splunk:description   "Application to evaluate lumi block efficiency"
macro sw.repository.binary.eff_daq_splunk:help.url   "https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltControlAndConfiguration"
macro sw.repository.binary.eff_daq_splunk:needs.environment  "CORAL_AUTH_PATH"

macro sw.repository.binary.eff_csv_splunk:description   "Application to evaluate lumi block efficiency"
macro sw.repository.binary.eff_csv_splunk:help.url   "https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltControlAndConfiguration"
macro sw.repository.binary.eff_csv_splunk:needs.environment  "CORAL_AUTH_PATH"

macro sw.repository.binary.eff_make_csv_file:description   "Application to produce a csv file with run efficiency data"
macro sw.repository.binary.eff_make_csv_file:help.url   "https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltControlAndConfiguration"
macro sw.repository.binary.eff_make_csv_file:needs.environment  "CORAL_AUTH_PATH"

macro sw.repository.binary.eff_make_combined_graphs:description   "Application to produce a root file from efficiency data files"
macro sw.repository.binary.eff_make_combined_graphs:help.url   "https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltControlAndConfiguration"

#macro sw.repository.binary.rc_beam_unstable_reactor:name              "Unstable Beam Reactor"
#macro sw.repository.binary.rc_beam_unstable_reactor:description       "Utility to do warm STOP if beam is unstable"

macro sw.repository.binary.rc_pixel_ready_reactor:name              "Pixel Ready Reactor"
macro sw.repository.binary.rc_pixel_ready_reactor:description       "Utility to launch PixelUp if HV ready"

macro sw.repository.binary.rc_sound_player:name              "Sound Player for some RC Issues"
macro sw.repository.binary.rc_sound_player:description       "Plays sounds if some RC ERS issues are sent."

macro sw.repository.binary.rc_ctp_emulator:name              "CTP commands dispatcher"
macro sw.repository.binary.rc_ctp_emulator:description       "Dispatches the commands received from RC to the CTP to all L2SVs, for emulation"


#-------------------------------------------------------------------------------
# Java
#-------------------------------------------------------------------------------
apply_pattern   build_jar       name=RCUtils \
                                src_dir="../jsrc" \
                                sources="RCUtils/*.java" \
                                others=""
                                
# IS Utils

apply_pattern 	build_jar \
	           					name="ISUtils" \
	  				            src_dir="../jsrc/ISUtils/isutils" \
	  				            sources=" *.java ./cmd/*.java ./ist/*.java ./ist/generator/*.java ./clo/*.java ./conf/*.java "       

#--------------------------------------------------------------------------------
# install
#--------------------------------------------------------------------------------
macro rc_apps   "rc_ctp_emulator rc_sound_player rc_pixel_ready_reactor rc_clocksetting \
		 rc_get_efficiency eff_run_efficiency eff_daq_efficiency rc_get_running_time \
		eff_make_csv_file eff_csv_splunk eff_make_combined_graphs rc_dq_flag_writer rc_is2cool_archive \
		rc_decode_detectormask rc_encode_detectormask rc_getrunnumber \
		rc_isread rc_waitstate rc_print_root rc_checkapps rc_print_tree \
		rc_timetest rc_print_partition_env rc_up2initial rc_am_requester rc_error_generator \
		rc_ready4recovery eff_daq_splunk" 

apply_pattern install_apps     files="$(rc_apps) "
apply_pattern install_scripts  src_dir="../bin" files="setup_daq setup_functions old2newRC.sh play* run_eff_display eff_display infogen infogen_typefinder"
#apply_pattern install_libs     files="libwmiccplugin.so libwmireplugin.so libwmiresources.so"
apply_pattern install_libs     files="libwmireplugin.so"
 
public

apply_pattern   install_jar     files=RCUtils.jar
apply_pattern   javadoc         name=RCUtils        src_dir=../jsrc

apply_pattern   install_jar     files=ISUtils.jar
apply_pattern   javadoc         name=ISUtils        src_dir=../jsrc/ISUtils
