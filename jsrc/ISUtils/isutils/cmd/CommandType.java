package isutils.cmd;

public enum CommandType {
	INFO("info"),
	CONFIG("config"),
	ANALYSIS("analysis");
	
	private String type;
	private CommandType(String t) {
		type=t;
	}
	
	public String getName() {
		return type;
	}
}
