package isutils.cmd;

import is.AnyInfo;
import is.Info;
import is.InfoDocument;
import isutils.clo.Multiplicity;
import isutils.clo.OptionSet;
import isutils.clo.Options;
import isutils.clo.Separator;
import isutils.conf.ConfManager;
import isutils.ist.ISEventManager;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Vector;

import config.ConfigException;

//import pmg.PMGPublishedProcessDataNamed;

import dal.Application;

public class AnalysisCommand extends Command {
	
	public AnalysisCommand( OptionSet o) {
		super(o);
	}
	
	@Override
	public void execute() throws ConfigException {
/////////////// INIT PART TO BE REMOVED
		
		String database = null;
		String segment = "MySegment";
		String defaultPartition = "MyPartition";
		String defaultServer = "MyServer";
		String application_type = null;
		String partition = null;
		String server = null;
		String hostname = null;
		String parameter = null;
		
		if (oset.isSet("d")){
			database = oset.getOption("d").getResultValue(0);
		} else {
			System.out.println("Error: database is needed.");
			System.exit(1);
		}
		
		if (oset.isSet("g")){
			segment = oset.getOption("g").getResultValue(0);
		}
		
		if (oset.isSet("p")){
			partition = oset.getOption("p").getResultValue(0);
		} else
			partition = defaultPartition;
		
		if (oset.isSet("s")) {
			server = oset.getOption("s").getResultValue(0);
		} else
			server = defaultServer;
		
		if (oset.isSet("t")) 
			application_type = oset.getOption("t").getResultValue(0);
		
		if (oset.isSet("o")) 
			hostname = oset.getOption("o").getResultValue(0);
		
		if (oset.isSet("m")) 
			parameter = oset.getOption("m").getResultValue(0);
		
		//////////////////////////////
		StringBuffer sb = new StringBuffer();
		sb.append("\n**** Running analysis on: \n");
		sb.append("** database file:"+database+"\n");
		sb.append("** for partition:"+partition+"\n");
		sb.append("** looking up for application in segment:"+segment+"\n");
		sb.append("** published in IS server:"+server+"\n");
		sb.append("** with runningn hostname:"+hostname+"\n");
		sb.append("** looking for paramter:"+parameter+"\n");
		
		System.out.println(sb.toString());
		
		//////////////////////////////
		
		ConfManager cf = new ConfManager(database);
		ISEventManager manager = new ISEventManager(partition);
		
		//ListAllApplicationBySegment
		//cf.listApplicationBySegment(segment);
		//List<String> appList = new Vector<String>();
		//for(Application a: cf.getApplicationBySegment(segment)){
		//	appList.add(a.UID());
		//	
		//}
		
		System.out.println("Applications found in segment(and children) "+segment+" are: \n");
		cf.listApplicationBySegmentAndChildren(segment);
		
		System.out.println("\n Starting the analysis job...\n");
		
		//// Leggere il DB per capire quali applicazionoi un segment ho associato
		
		//cf.listApplicationBySegment(segment);
		
		
		for(dal.BaseApplication a: cf.getApplicationBySegment(segment)){

			//Filtrare per tipo di applicazione specificata, se esiste
			if ( (application_type==null) || ((application_type != null)&&(a.class_name().equals(application_type))) ){
					
				// per ogni applicazione 
				// elaborare nome con qui dovrebbe essere presente in IS
				
				String is_app_name=null;
				if(hostname!=null)
					is_app_name = server +"."+hostname+"|"+a.UID();
				//else
					//get the computer to bild the IS identifier from the database
					//is_app_name = server +"."+a.get_RunsOn().UID()+"|"+a.UID();
				
				// Interrogare IS per prendere informazioni
				System.out.println("Looking for information in IS with name: "+is_app_name);
				
				Info res = manager.getInfo(is_app_name); 
				
				System.out.println("Infor retrieved from IS:"+res.getType());
				
				///SAPENDO CHE E' PMG
				
				//PMGPublishedProcessDataNamed pmgdata = (PMGPublishedProcessDataNamed) res;
				//System.out.println("Infon as PMG:"+pmgdata.toString());
				
//				PMGPublishedProcessDataNamed pmgdata = manager.getPMGDataNamedInfo(is_app_name);
//				
//				
//				
//				
//				
//				
//				
//				if(parameter==null) {
//					//looking for cpu usage
//					System.out.println("CPU Usage of "+is_app_name+" application :"+pmgdata.cpu_usage);
//					System.out.println("Library path of "+is_app_name+" application :"+pmgdata.library_paths);
//				} else {
//					//Looking for pmg parameter specified
//					try {
//						Field f =  pmgdata.getClass().getField(parameter);
//						System.out.println("Parameter "+parameter+" of "+is_app_name+" application :"+f.get(pmgdata));
//				
//						
//						
//					} catch (SecurityException e) {
//						System.out.println("Field not found!");
//						e.printStackTrace();
//					} catch (NoSuchFieldException e) {
//						System.out.println("Field not found!");
//						e.printStackTrace();
//					} catch (IllegalArgumentException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					} catch (IllegalAccessException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
					
					
					
					
					/////////////////// INFODCUMENT
					
					AnyInfo ai = manager.getAnyInfo(is_app_name);
					InfoDocument id = manager.getInfoDoc(is_app_name);
					
					for( int i = 0; i < ai.getAttributeCount(); i++ )
					{
						//byte type = ai.getAttributeType(i);
						java.lang.Object attr = ai.getAttribute(i);
						InfoDocument.Attribute adoc = id.getAttribute(i);
						if(adoc.getName().equals(parameter) ){
							System.out.println("Application "+is_app_name+":\n Parameter: "+parameter+"\n type:"
									+adoc.getType()+"\n descrition: "+adoc.getDescription()+"\n value:"+attr);
							
							
						}
						
//						out.print( offset + "Attribute \"" + adoc.getName() + "\" of type \""
//								+ adoc.getType() + "\" - "
//								+ adoc.getDescription() + " : " );
					
					}
					
				}
				
				
				
			//} else
			//	;//skip element
			
		}

		

	}

	@Override
	protected void check() {
		// TODO Auto-generated method stub
		
	}

	static public void init(Options opt) {
		String set_analyze=CommandType.ANALYSIS.getName();
		
		//Config mode set
		opt.addSet(set_analyze);
		opt.getSet(set_analyze).addOption("A", Multiplicity.ONCE);
		opt.getSet(set_analyze).addOption("d", Separator.BLANK, Multiplicity.ZERO_OR_ONE); // database
		opt.getSet(set_analyze).addOption("g", Separator.BLANK, Multiplicity.ZERO_OR_ONE); //segment
		opt.getSet(set_analyze).addOption("t", Separator.BLANK, Multiplicity.ZERO_OR_ONE); //application type	
		opt.getSet(set_analyze).addOption("p", Separator.BLANK, Multiplicity.ZERO_OR_ONE); //is partition
		opt.getSet(set_analyze).addOption("s", Separator.BLANK, Multiplicity.ZERO_OR_ONE); //is server
		opt.getSet(set_analyze).addOption("k", Separator.BLANK, Multiplicity.ZERO_OR_ONE); //parameter name
		opt.getSet(set_analyze).addOption("o", Separator.BLANK, Multiplicity.ZERO_OR_ONE);
		opt.getSet(set_analyze).addOption("m", Separator.BLANK, Multiplicity.ZERO_OR_ONE);
	}
	

}
