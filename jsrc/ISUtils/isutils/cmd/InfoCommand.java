package isutils.cmd;

import isutils.clo.Multiplicity;
import isutils.clo.OptionSet;
import isutils.clo.Options;
import isutils.clo.Separator;
import isutils.ist.ISEventManager;
import isutils.ist.OperationType;

import java.util.ArrayList;
import java.util.List;


public class InfoCommand extends Command {
	
	
	private static final String defaultPartition = "MyPartition";
	private static final String defaultserver = "MyServer";
	
	
	public InfoCommand( OptionSet o) {
		super(o);
	}
	
	@Override
	public void execute() {
		String partition = null;
		String server = null;
		String operation = null;
		String infoname = null;
		String infotype = null;
		
		
		if (oset.isSet("p")){
			partition = oset.getOption("p").getResultValue(0);
		} else
			partition = defaultPartition;
		
		if (oset.isSet("o")) {
			operation = oset.getOption("o").getResultValue(0);
		} else
			//System.err.println("operation not set");
			operation = "smart";
		
		if (oset.isSet("s")) {
			server = oset.getOption("s").getResultValue(0);
		} else
			server = defaultserver;
		
		if (oset.isSet("t")){
			infotype = oset.getOption("t").getResultValue(0);
		} else
			System.err.println("type not set");	
		
		if (oset.isSet("n")) {
			infoname = oset.getOption("n").getResultValue(0);
		} else
			System.err.println("name not set");

		
		OperationType op = OperationType.fromString(operation);
		ISEventManager manager = new ISEventManager(partition);
		String param = server + "." + infoname;
		
		//Get the list of parameter as a simple List<String>
		List<String> parameters = null;
		if(oset.isSet("D")) {
			parameters = new ArrayList<String>();
			for(int index=0;index < oset.getOption("D").getResultCount(); index++) {
				parameters.add(oset.getOption("D").getResultValue(index));
			}
		}
		//Get the op type and do work
		switch (op) {
			//case INSERT: test.set(param, args[4]);break;
			case INSERT: manager.insert(infotype, param, parameters);
			case UPDATE: manager.update(infotype, param, parameters);break;
			case REMOVE: manager.remove(param);break;
			case SMART: manager.smart(infotype, param, parameters); break;
			case LISTANY: manager.listAny(param); break;
			case LISTCOUPLE: manager.listCouple(param); break;
			
		} 
		
		
		

	}

	@Override
	protected void check() {
		// TODO Auto-generated method stub
		
	}

	static public void init(Options opt) {
		
		String set_info=CommandType.INFO.getName();
		//Info mode set
		opt.addSet(set_info);
		opt.getSet(set_info).addOption("I", Multiplicity.ONCE);
		opt.getSet(set_info).addOption("p", Separator.BLANK, Multiplicity.ZERO_OR_ONE);
		opt.getSet(set_info).addOption("o", Separator.BLANK, Multiplicity.ZERO_OR_ONE);
		opt.getSet(set_info).addOption("s", Separator.BLANK, Multiplicity.ZERO_OR_ONE);
		opt.getSet(set_info).addOption("t", Separator.BLANK, Multiplicity.ZERO_OR_ONE);
		opt.getSet(set_info).addOption("n", Separator.BLANK, Multiplicity.ONCE);
		opt.getSet(set_info).addOption("D", Separator.BLANK, Multiplicity.ZERO_OR_MORE);
	}

}
