package isutils.cmd;

import isutils.clo.OptionSet;
import isutils.clo.Options;

public abstract class Command {
	
	OptionSet oset = null;
	
	public Command() {
		
		oset = null;
	}
	
	public Command(OptionSet o){
		oset = o;
		
	}
	
	
	public abstract void execute() throws Exception;
	
	protected abstract void check() throws Exception;
	
	//Initialize command line option
	//protected abstract  void init(Options opt);

}
