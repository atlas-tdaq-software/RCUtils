package isutils.cmd;

import isutils.clo.OptionSet;
import isutils.cmd.CommandType;
public class CommandFactory {
	
	public static Command getCommand(OptionSet oset) {
		
		if(oset.getSetName().equals(CommandType.INFO.getName()))
			return new InfoCommand(oset);
		else if(oset.getSetName().equals(CommandType.CONFIG.getName()))
			return new ConfigCommand(oset);
		else if(oset.getSetName().equals(CommandType.ANALYSIS.getName()))
			return new AnalysisCommand(oset);
		else
			return null;
	}

}
