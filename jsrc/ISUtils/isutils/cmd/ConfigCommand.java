package isutils.cmd;

import isutils.clo.Multiplicity;
import isutils.clo.OptionSet;
import isutils.clo.Options;
import isutils.clo.Separator;
import isutils.conf.ConfManager;

import java.util.List;
import java.util.Vector;

import config.ConfigException;
import dal.Application;
import dal.Segment;

public class ConfigCommand extends Command {
	
	public ConfigCommand( OptionSet o) {
		super(o);
	}
	
	@Override
	public void execute() throws ConfigException {
		
		String database = null;
		String segment = "MySegment";

		if (oset.isSet("d")){
			database = oset.getOption("d").getResultValue(0);
		} else {
			System.out.println("Error: database is needed.");
			System.exit(1);
		}
		
		if (oset.isSet("s")){
			segment = oset.getOption("s").getResultValue(0);
		}
		
		
		ConfManager cf = new ConfManager(database);

		//Read Partition
		cf.readPartition();

		//PrintAll
		cf.printAll();
		
		//Check if segment specified Exists

		Segment seg = cf.getSegment(segment);
		if(seg == null) {
			System.err.println( "ERROR can not find Segment "+segment);
			System.exit(1);
		}
		else {
			System.out.println( "Seg Name = \'" + seg.UID() + "\'" );
		}

		//ListAllApplicationBySegment
		cf.listApplicationBySegmentAndChildren(segment);
		List<String> aList = new Vector<String>();
		for(dal.BaseApplication a: cf.getApplicationBySegment(segment))
			aList.add(a.UID());
		
		System.out.println("Print details of Application in segment "+ segment);
		
		for(String aID:aList)
			cf.listApplicationDetails(aID);
		
		//// Cerca nell'IS informazioni legate agli oggetti che ha trovato nel DB
		
		
		
		

	}

	@Override
	protected void check() {
		// TODO Auto-generated method stub
		
	}

	static public void init(Options opt) {
		String set_config=CommandType.CONFIG.getName();
		
		//Config mode set
		opt.addSet(set_config);
		opt.getSet(set_config).addOption("C", Multiplicity.ONCE);
		opt.getSet(set_config).addOption("d", Separator.BLANK, Multiplicity.ZERO_OR_ONE);
		opt.getSet(set_config).addOption("s", Separator.BLANK, Multiplicity.ZERO_OR_ONE);
		opt.getSet(set_config).addOption("g", Separator.BLANK, Multiplicity.ZERO_OR_ONE);	

		
	}

}
