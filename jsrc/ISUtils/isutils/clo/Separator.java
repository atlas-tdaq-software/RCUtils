package isutils.clo;

public enum Separator {
	  
	/**
	 * Separate option and value by ":"
	 */

	    COLON(':'),

	/**
	 * Separate option and value by "="
	 */

	    EQUALS('='),

	/**
	 * Separate option and value by blank space
	 */

	    BLANK(' '),      // Or, more precisely, whitespace (as allowed by the CLI)

	/**
	 * This is just a placeholder in case no separator is required (i. e. for non-value options)
	 */

	    NONE('D');       // NONE is a placeholder in case no separator is required, 'D' is just an arbitrary dummy value
	  
	    private char c;
	  
	    private Separator(char c) {
	      this.c = c;
	    }
	    
	    /**
	     * Return the actual separator character 
	     * <p>
	     * @return The actual separator character
	     */

	        char getName() {
	          return c;
	        }
}
