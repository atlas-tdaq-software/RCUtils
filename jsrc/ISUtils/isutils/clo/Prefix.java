package isutils.clo;


/**
 * An enum encapsulating the possible prefixes identifying options (and separating them from command line data items)
 */

  public enum Prefix {

/**
 * Options start with a "-" (typically on Unix platforms)
 */

    DASH('-'),

/**
 * Options start with a "/" (typically on Windows platforms)
 */

    SLASH('/');
  
    private char c;
  
    private Prefix(char c) {
      this.c = c;
    }
  
/**
 * Return the actual prefix character 
 * <p>
 * @return The actual prefix character
 */

    char getName() {
      return c;
    }
  
}

