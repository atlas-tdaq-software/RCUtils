package isutils.clo;

public enum Multiplicity {

	/**
	 * Option needs to occur exactly once
	 */

	    ONCE,

	/**
	 * Option needs to occur at least once
	 */

	    ONCE_OR_MORE,

	/**
	 * Option needs to occur either once or not at all
	 */

	    ZERO_OR_ONE, 

	/**
	 * Option can occur any number of times
	 */

	    ZERO_OR_MORE; 

	  }

