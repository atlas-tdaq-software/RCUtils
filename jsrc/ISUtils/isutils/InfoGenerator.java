package isutils;

import isutils.clo.Multiplicity;
import isutils.clo.OptionSet;
import isutils.clo.Options;
import isutils.clo.Prefix;
import isutils.cmd.AnalysisCommand;
import isutils.cmd.CommandFactory;
import isutils.cmd.CommandType;
import isutils.cmd.ConfigCommand;
import isutils.cmd.InfoCommand;


/**
 * This is an IS and Configuration utility to interacts with IS servers and with configuration database performing different kind of
 * operations for test purposes.
 * <p>
 * The utility operates in 3 mode: <b>Info</b> (for test on Information System), <b>Configuration</b> (for test on Configuration Database)
 * and <b>Analysis</b> (for test on both).
 * <p>
 * In Info mode, the utility support different operations (adding, removing, modifying) on ISInfo published into an IS server. It relies on
 * Java reflection and introspection capabilities, so it supports each specialization of ISInfo if a proper JAR is provided and added to the
 * CLASSPATH. New information can be created specifying values for all desired parameters, otherwise they will remain empty.
 * <p>
 * Configuration and Analysis mode are still prototype, but they already allow respectively to parse a database file and to look for parsed
 * information into a certain IS server.
 * <p>
 * Usage Examples:
 * <p>
 * Adding new Information into an IS Server:
 * <p>
 * <tt> java -classpath $TDAQ_CLASSPATH isutils.InfoGenerator -I -p MyPartition -s MyServer -n my_new_info -t rc.RCStateInfo -o insert -D
 * sender:S:test_user_name
 * <p>
 * configuration example:
 * <p>
 * <tt>java -classpath $TDAQ_CLASSPATH isutils.InfoGenerator -C -d oksconfig:./databases/part_ef.data.xml
 * <p>
 * analysis example:
 * <p>
 * <tt>java -classpath $TDAQ_CLASSPATH isutils.InfoGenerator -A -d oksconfig:./databases/part_ef.data.xml -g EBEF-Segment-1 -p part_ef -s
 * PMG
 * <p>
 * HELP:
 * <p>
 * <tt>java -classpath $TDAQ_CLASSPATH isutils.InfoGenerator
 * 
 * @author lmagnoni
 */
public class InfoGenerator {

    private static String set_help = "help";

    private static Options opt;

    /**
     * @param args
     */
    public static void main(String[] args) {
        try {
            // Print invocation args
            // System.err.println(Arrays.toString(args));

            opt = new Options(args, Prefix.DASH);

            // Define the option sets for the different operation mode
            // initOptionSet();

            // FOREACH COMMAND TYPE
            // COMMAND.init(ops)
            InfoCommand.init(opt);
            ConfigCommand.init(opt);
            AnalysisCommand.init(opt);
            // Evaluate the matching set
            OptionSet oset = opt.getMatchingSet(false, false);

            if(oset != null) {

                // Command Factory based on the oset name to build the right Command
                CommandFactory.getCommand(oset).execute();

            } else {

                System.out.println("Error: Wrong parameter specified:");
                opt = new Options(args, Prefix.DASH);
                initOptionSet();
                InfoCommand.init(opt);
                ConfigCommand.init(opt);
                AnalysisCommand.init(opt);
                // checkOptions(opt, CommandType.CONFIG.getName());
                // checkOptions(opt, CommandType.INFO.getName());
                // checkOptions(opt, CommandType.ANALYSIS.getName());
                // printHelp();
                printInfoHelp();
                System.exit(1);
            }
        }
        catch(final Exception ex) {
            ex.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * 
     */

    private static void initOptionSet() {
        opt.addSet(set_help);
        opt.getSet(set_help).addOption("h", Multiplicity.ONCE);
    }

    private static void printHelp() {

        StringBuilder sb = new StringBuilder();
        sb.append("\nUsage: infogen -MODE [options] [-D parameter_name1:TYPE:value, -D parameter_name2:TYPE:value ...]  \n");
        sb.append("where: \n");
        sb.append("\tMODE:  [ I (info)| C (configuration)| A (Analyisis) ]\n");
        sb.append("\tinfo options: \n");
        sb.append("\t\t-p partition \n");
        sb.append("\t\t-s server \n");
        // sb.append("\t\t-o operation [insert, update, remove, listAny, listCouple ] \n");
        sb.append("\t\t-o operation [insert, update, remove] \n");
        sb.append("\t\t-t infoType [complete package.class name. Please note the JAR containing the type have to be included in the CLASSPATH]\n");
        sb.append("\t\t-n infoName \n");
        sb.append("\t\t-D parameterName:TYPE:value [TYPE: I=Integer, L=Long, D=Double, F=Float, S=String, B=Boolean, x=Byte, A=String[], T=Date ]\n\n");
        sb.append("\tconfiguration options: \n");
        sb.append("\t\t-d database filename \n");
        sb.append("\t\t-g segment name \n");
        sb.append("\tanalysis options: \n");
        sb.append("\t\t-d database filename \n");
        sb.append("\t\t-g segment \n");
        sb.append("\t\t-t application type \n");
        sb.append("\t\t-p partition \n");
        sb.append("\t\t-s is server \n");
        sb.append("\t\t-o hostname \n");

        sb.append("\nExamples:\n");
        sb.append("\nAdding new Information into an IS Server:\n");
        sb.append("\ninfogen " + "-I -p MyPartition -s MyServer -n my_new_info -t rc.RCStateInfo -o insert -D sender:S:user_name\n");
        sb.append("\nconfig:\n");
        sb.append("\ninfogen " + "-C -d oksconfig:/home/lmagnoni/afs/databases/be_test.data.xml\n");
        sb.append("\nanalysis:\n");
        sb.append("\ninfogen -A -d oksconfig:/home/lmagnoni/afs/databases/part_ef.data.xml  -g EBEF-Segment-1 -p part_ef -s PMG\n");
        // sb.append("\nAdding new Information into an IS Server:\n");
        // sb.append("\njava -classpath $TDAQ_CLASSPATH isutils.InfoGenerator " +
        // "-I -p MyPartition -s MyServer -n my_new_info -t rc.RCStateInfo -o insert -D sender:S:test_user_name\n");
        // sb.append("\nconfig:\n");
        // sb.append("\njava -classpath $TDAQ_CLASSPATH isutils.InfoGenerator " +
        // "-C -d oksconfig:/home/lmagnoni/afs/databases/be_test.data.xml\n");
        // sb.append("\nanalysis:\n");
        // sb.append("\njava -classpath $TDAQ_CLASSPATH isutils.InfoGenerator -A -d oksconfig:/home/lmagnoni/afs/databases/part_ef.data.xml
        // -g EBEF-Segment-1 -p part_ef -s PMG\n");
        //
        System.err.println(sb.toString());

    }

    private static void printInfoHelp() {

        StringBuilder sb = new StringBuilder();
        sb.append("\nUsage: infogen -I [options] [-D parameter_name1:TYPE:value, -D parameter_name2:TYPE:value ...]  \n");
        sb.append("where: \n");
        sb.append("\tinfo options: \n");
        sb.append("\t\t-p partition_name \n");
        sb.append("\t\t-s IS_server_name \n");
        // sb.append("\t\t-o operation [insert, update, remove, listAny, listCouple ] \n");
        sb.append("\t\t-o operation [insert, update, remove, smart (insert if new, update if existing)] \n");
        sb.append("\t\t-t infoType [complete package.class name. Please be sure infoType's JAR is included in the TDAQ_CLASSPATH]\n");
        sb.append("\t\t-n infoName \n");
        sb.append("\t\t-D parameterName:TYPE:value [TYPE: I=Integer, L=Long, D=Double, F=Float, S=String, B=Boolean, x=Byte, A=String[], T=Date ]\n\n");

        sb.append("\nExamples:\n");
        sb.append("\nAdding new Information into an IS Server:\n");
        sb.append("\ninfogen " + "-I -p MyPartition -s MyServer -n my_new_info -t rc.RCStateInfo -o insert -D sender:S:user_name\n");
        // sb.append("\nAdding new Information into an IS Server:\n");
        // sb.append("\njava -classpath $TDAQ_CLASSPATH isutils.InfoGenerator " +
        // "-I -p MyPartition -s MyServer -n my_new_info -t rc.RCStateInfo -o insert -D sender:S:test_user_name\n");
        // sb.append("\nconfig:\n");
        // sb.append("\njava -classpath $TDAQ_CLASSPATH isutils.InfoGenerator " +
        // "-C -d oksconfig:/home/lmagnoni/afs/databases/be_test.data.xml\n");
        // sb.append("\nanalysis:\n");
        // sb.append("\njava -classpath $TDAQ_CLASSPATH isutils.InfoGenerator -A -d oksconfig:/home/lmagnoni/afs/databases/part_ef.data.xml
        // -g EBEF-Segment-1 -p part_ef -s PMG\n");
        //
        System.err.println(sb.toString());

    }

    public static boolean checkOptions(Options opt, String setName) {

        // System.out.println("*** Set " + setName);

        boolean result = opt.check(setName, false, false);

        // System.out.println("Check result for setName "+setName+": " + result);
        // System.out.println("Check result:");
        // System.out.println(opt.getCheckErrors());

        for(String s : opt.getSet(setName).getData()) {
            System.out.println("Data: " + s);
        }

        for(String s : opt.getSet(setName).getUnmatched()) {
            System.out.println("Unmatched: " + s);
        }

        return result;

    }

}
