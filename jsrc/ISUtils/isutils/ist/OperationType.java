package isutils.ist;

public enum OperationType {
	INSERT, 
	UPDATE, 
	REMOVE,
	SMART, 
	LISTANY,
	LISTCOUPLE;
	

	
	public static OperationType fromString(String value) {
		if(value.toLowerCase().equals("insert"))
			return INSERT;
		else if (value.toLowerCase().equals("update"))
			return UPDATE;
		else if (value.toLowerCase().equals("remove"))
			return REMOVE;
		else if (value.toLowerCase().equals("listany"))
			return LISTANY;
		else if (value.toLowerCase().equals("listcouple"))
			return LISTCOUPLE;
		else if (value.toLowerCase().equals("smart"))
			return SMART;
		else
			return null; 
	}

}
