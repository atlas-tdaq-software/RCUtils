package isutils.ist;

import is.Info;

public class CoupleInfo extends Info {
	public int val1;
    private int val2=20; //without the true in put the int has inserter ad signed, so as a Float
    String name = "test";

	public CoupleInfo() {
		super("CoupleInfo");
	}
	
	protected CoupleInfo(String type) {
		super(type);
	}
	
	public void publishGuts( is.Ostream out ){
		super.publishGuts( out );
		out.put(val1,true).put( val2,true ).put(name);
	
	}

}
