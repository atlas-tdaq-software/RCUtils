package isutils.ist;


import isutils.clo.Multiplicity;
import isutils.clo.Options;
import isutils.clo.Prefix;
import isutils.clo.Separator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;






public class TestInfo {
	
	private static final String defaultPartition = "MyPartition";
	private static final String defaultserver = "MyServer";
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String partition = null;
		String server = null;
		String operation = null;
		String infoname = null;
		String infotype = null;

		if ( args.length < 3 )
		{
			StringBuilder sb = new StringBuilder();
		    sb.append("Usage:\n\n");
		    sb.append("TestInfo options [-Dparameter_name1=value, -Dparameter_name2=value ...]  \n");
		    sb.append("options: \n");
		    sb.append("-p partition \n");
		    sb.append("-s server \n");
		    sb.append("-o operation [insert, update, remove, listAny ] \n");
		    sb.append("-t infoType \n");
		    sb.append("-n infoName \n");
		    sb.append("-D parameterName:TYPE:value");
		    
		    System.err.println(sb.toString());
		    System.exit( 1 );
		}
		
		Options opt = new Options(args, Prefix.DASH);
		
		opt.getSet().addOption("p", Separator.BLANK, Multiplicity.ZERO_OR_ONE);
		opt.getSet().addOption("o", Separator.BLANK, Multiplicity.ONCE);
		opt.getSet().addOption("s", Separator.BLANK, Multiplicity.ZERO_OR_ONE);
		opt.getSet().addOption("t", Separator.BLANK, Multiplicity.ZERO_OR_ONE);
		opt.getSet().addOption("n", Separator.BLANK, Multiplicity.ONCE);
		opt.getSet().addOption("D", Separator.BLANK, Multiplicity.ZERO_OR_MORE);
		
		if(!opt.check()){
			//System.err.println( "You must provide the partition..."+opt);
			//System.exit( 1 );
     	}

		if (opt.getSet().isSet("p")){
			partition = opt.getSet().getOption("p").getResultValue(0);
		} else
			partition = defaultPartition;
		if (opt.getSet().isSet("o")) {
			operation = opt.getSet().getOption("o").getResultValue(0);
		} else
			System.err.println("operation not set");
		if (opt.getSet().isSet("s")) {
			server = opt.getSet().getOption("s").getResultValue(0);
		} else
			server = defaultserver;
		if (opt.getSet().isSet("t")){
			infotype = opt.getSet().getOption("t").getResultValue(0);
		} else
			System.err.println("type not set");	
		if (opt.getSet().isSet("n")) {
			infoname = opt.getSet().getOption("n").getResultValue(0);
		} else
			System.err.println("name not set");
		//String param = args[2] + "." + args[3];
		
		System.out.println(Arrays.toString(args));
		
		OperationType op = OperationType.fromString(operation);
		
		//p = ( args.length < 4 ) ? new Partition() : new Partition( args[0] );
		
		ISEventManager manager = new ISEventManager(partition);
		String param = server + "." + infoname;
		
		List<String> parameters = null;
		if(opt.getSet().isSet("D")) {
			parameters = new ArrayList<String>();
			for(int index=0;index < opt.getSet().getOption("D").getResultCount(); index++) {
				parameters.add(opt.getSet().getOption("D").getResultValue(index));
			}
		}
		//Get the op type and do work
		switch (op) {
			//case INSERT: test.set(param, args[4]);break;
			case INSERT: manager.insert(infotype, param, parameters);
			case UPDATE: manager.update(infotype, param, parameters);break;
			case REMOVE: manager.remove(param);break;
			case LISTANY: manager.listAny(param); break;
			case LISTCOUPLE: manager.listCouple(param); break;
			
		} 
		
		
		//manager.listCouple(param);
		//manager.listAny("MyServer.employee1");
		
		
		}
	
	
	

    }
	
	
	
