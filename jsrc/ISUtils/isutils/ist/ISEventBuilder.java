package isutils.ist;

import java.util.List;

import is.Info;
import isutils.ist.generator.Generator;
import isutils.ist.generator.GeneratorFactory;

public class ISEventBuilder {
	
	public Info createInfoWithValue(String infoClassType, List<String> opt) {
		//There should be added the logic to understand which generator to call
		// Random., method,etc.
		
		//This can be configurable
		Generator g = GeneratorFactory.getGenerator("parameter");
		return g.generate(infoClassType, opt );
	}

}
