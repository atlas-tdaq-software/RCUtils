package isutils.ist;

import is.Info;

public class StringInfo extends Info {
	
    String value = "string_value";

	public StringInfo() {
		super("string");
	}
	
	protected StringInfo(String type) {
		super(type);
	}
	
	public void setValue(String v){
		value = v;
	}
	
	public void publishGuts( is.Ostream out ){
		super.publishGuts( out );
		out.put(value);
	
	}

}
