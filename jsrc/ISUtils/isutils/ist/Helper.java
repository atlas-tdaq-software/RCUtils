package isutils.ist;

import is.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class Helper {
	
	 
	public static String getParameterName(String option) {
		return option.split(":")[0];
	}
	

	public static Object getParameterValue(String option) {
		/*
		 ** we specify Locale.US since months are in english
		 */
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US);

		// TODO Auto-generated method stub
		String type = option.split(":")[1];
		//String value = option.split(":")[2];
		String value = option.substring(option.indexOf(":", option.indexOf(":")+1)+1);

		switch(type.charAt(0)){
		case ('I'): return new Integer(value);
		case ('L'): return new Long(value);
		case ('D'): return new Double(value);
		case ('F'): return new Float(value);
		case ('S'): return new String(value);
		case ('B'): return new Boolean(value);
		case ('x'): return Byte.decode(value);
		case ('A'): return new String[] {value};
		case ('T'): {
			try {
						//return sdf.parse(value);
				//System.out.println("Value = "+value+" "+(new Long(sdf.parse(value).getTime()/1000)).toString());
				// IS uses sql.Date... no comment
				return new java.sql.Time(sdf.parse(value).getTime());
			} catch (ParseException e) {
			    System.out.println("ERROR: Invalid date specified. Please use format: dd-MM-yyyy HH:mm:ss (remember to escape the space with \\)");
				return null;
			} catch (IllegalArgumentException e) {
				System.out.println("ERROR: Invalid date specified. Please use format: dd-MM-yyyy HH:mm:ss (remember to escape the space with \\)");
				return null;
			}

		}
		}

		return null;
	}

}
