package isutils.ist;

import java.lang.reflect.Array;
import java.util.List;



import ipc.Partition;
import is.AnyInfo;
import is.Info;
import is.InfoDocument;
import is.InfoNotFoundException;
import is.Repository;
import is.Type;

public class ISEventManager {
	
	private static Repository repository;
	private static Partition partition;
	private ISEventBuilder builder;
	
	public ISEventManager(String p) {
		partition = new Partition(p);
		repository = new Repository(partition);
		builder = new ISEventBuilder();
	}
	
	public void insert(String infoType, String infoName, List<String> opt) {
		try {
			Info isinfo = builder.createInfoWithValue(infoType, opt);
			if(isinfo!=null)
				repository.insert(infoName,isinfo);
		}
		catch(final is.RepositoryNotFoundException | is.InfoAlreadyExistException ex) {
			throw new java.lang.RuntimeException(ex);
		}
	}

	public  void update(String infoType,String infoName, List<String> opt ){
		try {
			Info isinfo = builder.createInfoWithValue(infoType, opt);
			if(isinfo!=null)
				repository.update(infoName, isinfo);
		}
		catch(final is.RepositoryNotFoundException | is.InfoNotFoundException | is.InfoNotCompatibleException ex) {
			throw new java.lang.RuntimeException(ex);
		}
	}
	
	public  void smart(String infoType,String infoName, List<String> opt ){
		Info isinfo = builder.createInfoWithValue(infoType, opt);
		try {
			if(isinfo!=null)
				repository.update(infoName, isinfo);
		} catch (InfoNotFoundException e) {
			try {
				repository.insert(infoName,isinfo);
			}
			catch(final is.RepositoryNotFoundException | is.InfoAlreadyExistException ex) {
				throw new java.lang.RuntimeException(ex);
			}
		}
                catch(final is.RepositoryNotFoundException | is.InfoNotCompatibleException ex) {
			throw new java.lang.RuntimeException(ex);
		}
	}
	
	
	public  void remove(String infoName) {
		try {
			repository.remove(infoName);
		}
		catch(final is.RepositoryNotFoundException | is.InfoNotFoundException ex) {
			throw new java.lang.RuntimeException(ex);
		}
	}
	
	public  Info getInfo(String infoName) {
		try {
			Info info = new AnyInfo();
			repository.getValue(infoName, info);
			return info;
		}
		catch(final is.RepositoryNotFoundException | is.InfoNotFoundException | is.InfoNotCompatibleException ex) {
			throw new java.lang.RuntimeException(ex);
		}
	}
	
	public  AnyInfo getAnyInfo(String infoName) {
		try {
			AnyInfo info = new AnyInfo();
			repository.getValue(infoName, info);
			return info;
		}
		catch(final is.RepositoryNotFoundException | is.InfoNotFoundException ex) {
                        throw new java.lang.RuntimeException(ex);
                }
	}
	
	
	public void listCouple(String infoName) {
		try {
			AnyInfo ai = new AnyInfo();
			repository.getValue(infoName, ai);
			System.out.println("General entity type: "+ ai.getType().getName());
			System.out.println("General entity name: "+ai.getName());
			System.out.println("General entity attribute number : "+ai.getAttributeCount());
			System.out.println("General entity attribute1 type: "+ ( (Byte) ai.getAttributeType(0)).equals(Type.FLOAT) );
			System.out.println("General entity attribute1 name: "+ai.getAttributeTypeName(0));
			System.out.println("General entity attribute1 value : "+(Integer)ai.getAttribute(0));
		}
                catch(final is.RepositoryNotFoundException | is.InfoNotFoundException ex) {
                        throw new java.lang.RuntimeException(ex);
                }
	}
	
	public void listAny(String infoName) {
		try {
			AnyInfo ai = new AnyInfo();
			repository.getValue(infoName, ai);
			System.out.println( "Info has type \"" + ai.getType().getName() + "\":" );
			printAnyInfo( System.out, ai, partition, "    " );
		}
                catch(final is.RepositoryNotFoundException | is.InfoNotFoundException ex) {
                        throw new java.lang.RuntimeException(ex);
                }
	}
	
	public InfoDocument getInfoDoc(String infoName) {
		try {
			Info info = new AnyInfo();
			repository.getValue(infoName, info);
			return new InfoDocument(partition,info);
		}
                catch(final is.UnknownTypeException | is.RepositoryNotFoundException | is.InfoNotFoundException | is.InfoNotCompatibleException ex) {
                        throw new java.lang.RuntimeException(ex);
                }	
	}
	
	
	protected static void printAnyInfo( java.io.PrintStream out, is.AnyInfo ai, Partition p, String offset ) {
		try {
			InfoDocument id = new InfoDocument( p, ai );
			for( int i = 0; i < ai.getAttributeCount(); i++ )
			{
				byte type = ai.getAttributeType(i);
				java.lang.Object attr = ai.getAttribute(i);
				InfoDocument.Attribute adoc = id.getAttribute(i);
				out.print( offset + "Attribute \"" + adoc.getName() + "\" of type \""
						+ adoc.getType() + "\" - "
						+ adoc.getDescription() + " : " );

				if ( attr.getClass().isArray() )
				{
					out.println( "(" + Array.getLength( attr ) + " elements) : " );
					for ( int j = 0; j < Array.getLength( attr ); j++ )
					{
						if ( type != is.Type.INFO )
							out.print( Array.get( attr, j ) + " " );
						else
							printAnyInfo( out, (is.AnyInfo)Array.get( attr, j ), p, offset + offset );
					}
					if ( type != is.Type.INFO )
						out.println();
				}
				else
				{
					if ( type != is.Type.INFO )
						out.println( attr );
					else
					{
						out.println();
						printAnyInfo( out, (is.AnyInfo)attr, p, offset + offset );
					}	
				}
			}
		}
		catch(final is.UnknownTypeException ex) {
			throw new java.lang.RuntimeException(ex);	
		}
	}

}
