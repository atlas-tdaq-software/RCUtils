package isutils.ist.generator;

public enum PolicyType { 
	RANDOM, 
	PARAMETER,
	STATIC,
	EMPTY;
	
	public static PolicyType fromString(String policy) {
		if(policy.toLowerCase().equals("random"))
			return RANDOM;
		else if(policy.toLowerCase().equals("parameter"))
			return PARAMETER;
		else if(policy.toLowerCase().equals("static"))
			return STATIC;
		else if(policy.toLowerCase().equals("empty"))
			return EMPTY;
		else
			//DEFAULT CASE
			return EMPTY;
	}

}
