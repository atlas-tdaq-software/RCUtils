package isutils.ist.generator;

import java.lang.reflect.Field;
import java.util.List;

import is.Info;
import isutils.ist.Helper;
import isutils.ist.StringInfo;

public class ParamGenerator extends StaticGenerator {
	
	@Override
	public Info generate(String classType, List<String> opt) {
		
		if (opt!=null) {
			
			System.out.println("Creating object..."+classType+"\n");
			
			if(classType.equals("StringInfo")) {
				StringInfo s =  new StringInfo();
				if(opt.size()>0)
					s.setValue((String)Helper.getParameterValue(opt.get(0)));
				return s;
			}
			
			
			
			System.out.println("Creating object..."+classType+"\n");
			Class c;
			try {
				
				c = Class.forName(classType);
				Object cist = c.newInstance();
				Field fields[] = c.getFields();

				//Reflection generator with paramter
				for (int index = 0; index < opt.size();index++) {

					String option = opt.get(index);
					String name = Helper.getParameterName(option);
					//Type type =  Helper.getParamterType(option);
					Object value = Helper.getParameterValue(option);
					
					for(Field f:fields) {
						if(f.getName().equals(name)) {
							f.set(cist,value);
						}
					}

				}
				
				return (Info) cist;

			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else {
				//Call EmptyGenerator
				return super.generate(classType);
		}
		//Unreachble code??!!
		return null;
	
	}
	

}
