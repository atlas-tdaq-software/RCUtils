package isutils.ist.generator;

public class GeneratorFactory {
	
	public static Generator getGenerator(String policy) {
		PolicyType pt = PolicyType.fromString(policy);
		switch(pt) {
			case EMPTY : return new EmptyGenerator();
			case STATIC: return new StaticGenerator();
			case PARAMETER: return new ParamGenerator();
			default: 
				return new EmptyGenerator();
		}
		
	}

}
