package isutils.ist.generator;

import java.util.List;

import is.Info;

public interface Generator {
	
	public Info generate(String classType, List<String> opt);
	public Info generate(String classType);
	

}
