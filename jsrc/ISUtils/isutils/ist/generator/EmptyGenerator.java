package isutils.ist.generator;

import java.util.List;

import is.Info;

public class EmptyGenerator implements Generator {
	
	@Override
	public Info generate(String classType, List<String> opt) {
		return generate(classType);
	}
	
	@Override
	public Info generate(String classType) {
		Class infoClass;
		try {
			
			infoClass = Class.forName(classType);
		    return (Info) infoClass.newInstance();
			
		} catch (ClassNotFoundException e) {
			System.out.println("Error:class not found: "+e);
			return null;
		} catch (InstantiationException e) {
			System.out.println("Error: instantiation error: "+e);
			return null;
		} catch (IllegalAccessException e) {
			System.out.println("Error: illegal access: "+e);
			return null;
		}
	}
	

}
