package isutils.conf;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

import config.ConfigException;
import config.Configuration;
import config.Query;
import dal.Application;
import dal.Application_Helper;
import dal.BaseApplication;
import dal.Partition;
import dal.Partition_Helper;
import dal.Segment;
import dal.Segment_Helper;

public class ConfManager {
	
	Configuration db ;
	
	//initString name-of-plugin:plugin-parameters, 
	//such as oksconfig:/home/lmagnoni/ciccio.xml
	
	public ConfManager(String initString){
		try {
			db = new Configuration(initString);
		}catch(config.SystemException ex) {
			System.err.println( "Caught \'config.System\' exception \"" + ex.toString() + "\", exiting ...");
			System.exit( 1 );
		}
	}
	
	public void readPartition() throws ConfigException {
		
		//Generic query to get all objects
		Partition ps[] = Partition_Helper.get(db, new Query());
		
		for(Partition p:ps) {
			System.out.println("\npartition:"+p.UID());
			if(p.get_Segments().length!=0)
				for(Segment s:p.get_Segments()) {
					System.out.println( " * test segment \'" + s.UID() + "\' from partition \'" + p.UID() + "\'");
					//Store segment in map add_enabled_segments(s, p, segments);
				}
			else
				System.out.println( "No segment in this partition");
		}
	
	}
	
	public Segment getSegment(String ID) throws ConfigException {
		return Segment_Helper.get(db, ID);
	}

	public void printAll() throws ConfigException {
		Partition part_objs[] = Partition_Helper.get(db, new config.Query());
		for(int j = 0; j < part_objs.length; ++j) { part_objs[j].print(""); }
		

		if(part_objs.length == 0) {
			System.err.println( "ERROR [Test.main()]: can not find a partition object" );
			System.exit(1);
		}
		
	}
	
	
	
	
	
	public void listApplicationBySegmentAndChildren(String segmentName) throws ConfigException {
		List<BaseApplication> as = this.getApplicationBySegment(segmentName);		
		for(BaseApplication a:as)
			System.out.println("Application: "+a.UID());
	}
	
	
	
	
	public List<BaseApplication> getApplicationBySegment(String segmentName) throws ConfigException {
		
		List<BaseApplication> app_list = new Vector<BaseApplication>();
		
		Segment ciccio = Segment_Helper.get(db,segmentName);
		
		this.getAppRecursive(ciccio, app_list);
		
		return app_list;
		
	}
	
	
	private void getAppRecursive(Segment s, List<BaseApplication> destination) throws ConfigException {
		
		//app di I livello
		List<BaseApplication> app_list = Arrays.asList(s.get_Applications());
		System.out.println("In Segment:"+s.UID());
		boolean found=false;
		for(BaseApplication a:app_list) {
			System.out.println("\tApplication::"+a.UID());
			destination.add(a);
			found=true;
		}
		if(!found){
			System.out.println("\t No Application found!");
		}
		
		
		for(Segment seg:s.get_Segments()) {
			this.getAppRecursive(seg, destination);
		}
		
	}
	
	public Application getApplication(String appID) throws ConfigException {
		return Application_Helper.get(db,appID);
	}
	
	public void listApplicationDetails(String appID) throws ConfigException {
		Application a = Application_Helper.get(db,appID);
		System.out.println("Application details: "+"ID:"+a.UID()+" param:"+a.get_Parameters()+" className:"+a.class_name());
	}
	
}
