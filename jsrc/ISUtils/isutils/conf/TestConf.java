package isutils.conf;

import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import dal.Segment;


public class TestConf {
    public static void main(String args[]) {
        try {
            if(args.length == 0) {
                System.err.println("ERROR [Test.main()]: no arguments given (server-name [subscribe])");
            }

            // create configuration object
            System.err.println(Arrays.toString(args));

            ConfManager cf = new ConfManager(args[0]);

            // Read Partition
            cf.readPartition();

            // PrintAll
            cf.printAll();

            // Check if segment specified Exists

            Segment seg = cf.getSegment(args[1]);
            if(seg == null) {
                System.err.println("ERROR can not find Segment " + args[1]);
                System.exit(-1);
            } else {
                System.out.println("Seg Name = \'" + seg.UID() + "\'");
            }

            // ListAllApplicationBySegment
            cf.listApplicationBySegmentAndChildren(args[1]);
            List<String> aList = new Vector<String>();
            for(dal.BaseApplication a : cf.getApplicationBySegment(args[1]))
                aList.add(a.UID());

            System.out.println("Print details of Application in segment " + args[1]);

            for(String aID : aList)
                cf.listApplicationDetails(aID);

        }
        catch(final Exception ex) {
            ex.printStackTrace();
            System.exit(-1);
        }
    }

}
