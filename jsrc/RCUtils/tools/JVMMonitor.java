package RCUtils.tools;

import java.lang.management.CompilationMXBean;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.lang.management.ThreadMXBean;
import java.util.List;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;

import is.InfoNotCompatibleException;
import is.InvalidCriteriaException;
import is.RepositoryNotFoundException;
import rc.JVMCompilationNamed;
import rc.JVMGarbageCollectorNamed;
import rc.JVMMemoryUsageNamed;
import rc.JVMThreadInfoNamed;


/**
 * Utility class to be used to publish JVM statistics to the Information Service (IS).
 * <p>
 * JVM statistics are grouped into different categories, and corresponding IS classes are defined in the <em>RCInfo</em> package:
 * <ul>
 * <li>JVMCompilation: statistics for the compilation system of the Java virtual machine;
 * <li>JVMGarbageCollector: statistics for the JVM Garbage Collector;
 * <li>JVMMemoryUsage: statistics from the memory system of the Java virtual machine;
 * <li>JVMThreadInfo: statistics for the thread system of the Java virtual machine;
 * </ul>
 * <p>
 * The application name is used to build the name of the IS information. Information is collected and published periodically (the period is
 * selected by the user via the {@link #start(long, TimeUnit)} and {@link #restart(long, TimeUnit)} methods).
 */
public class JVMMonitor {
    private final static String TDAQ_APPLICATION_NAME = "TDAQ_APPLICATION_NAME";
    private final static String TDAQ_PARTITION = "TDAQ_PARTITION";

    private final String applicationName;
    private final String serverName;
    private final ipc.Partition partition;
    private final ScheduledThreadPoolExecutor scheduler = new ScheduledThreadPoolExecutor(1);
    private ScheduledFuture<?> scheduledTask = null;

    @SuppressWarnings("serial")
    private static class CannotPublish extends ers.Issue {
        CannotPublish(final String message) {
            super(message);
        }

        CannotPublish(final String message, final Exception cause) {
            super(message, cause);
        }
    }

    private static class MonitorTask implements Runnable {
        private final String app;
        private final ipc.Partition part;
        private final String server;
        private final AtomicBoolean publishCompilerInfo = new AtomicBoolean(true);

        MonitorTask(final String app, final ipc.Partition part, final String server) {
            this.app = app;
            this.part = part;
            this.server = server;
        }

        @Override
        public void run() {
            final String baseName = this.server + "." + this.app;

            if(this.publishCompilerInfo.get() == true) {
                // Check interruption state before any collection and publication
                if(Thread.currentThread().isInterrupted() == true) {
                    return;
                }

                try {
                    final CompilationMXBean compBean = ManagementFactory.getCompilationMXBean();

                    final JVMCompilationNamed info = new JVMCompilationNamed(this.part, baseName + ".JVMCompiler");
                    info.name = compBean.getName();
                    info.totalCompilationTime = compBean.getTotalCompilationTime();
                    info.checkin();
                }
                catch(final NullPointerException ex) {
                    // This means the compiler monitoring is not available
                    // In that case the "compBean" variable is null
                    ers.Logger.error(new CannotPublish("Cannot publish JVM compiler info: not supported by the current JVM"));

                    this.publishCompilerInfo.set(false);
                }
                catch(final InfoNotCompatibleException | RepositoryNotFoundException ex) {
                    ers.Logger.warning(new CannotPublish("Failed to publish JVM compiler info: " + ex.getMessage(), ex));
                }
            }

            // Check interruption state before any collection and publication
            if(Thread.currentThread().isInterrupted() == true) {
                return;
            }

            try {
                final List<GarbageCollectorMXBean> gcBeans = ManagementFactory.getGarbageCollectorMXBeans();
                for(final GarbageCollectorMXBean b : gcBeans) {
                    final String gcName = b.getName();
                    final JVMGarbageCollectorNamed info =
                                                        new JVMGarbageCollectorNamed(this.part, baseName + "." + gcName + ".JVMCollector");
                    info.name = gcName;
                    info.collectionCount = b.getCollectionCount();
                    info.collectionTime = b.getCollectionTime();
                    info.checkin();
                }
            }
            catch(final InfoNotCompatibleException | RepositoryNotFoundException ex) {
                ers.Logger.warning(new CannotPublish("Failed to publish JVM garbage collector info: " + ex.getMessage(), ex));
            }

            // Check interruption state before any collection and publication
            if(Thread.currentThread().isInterrupted() == true) {
                return;
            }

            final MemoryMXBean memBean = ManagementFactory.getMemoryMXBean();

            try {
                final MemoryUsage heap = memBean.getHeapMemoryUsage();
                final JVMMemoryUsageNamed heapInfo = new JVMMemoryUsageNamed(this.part, baseName + ".JVMMemory_HEAP");
                heapInfo.committed = heap.getCommitted();
                heapInfo.init = heap.getInit();
                heapInfo.max = heap.getMax();
                heapInfo.used = heap.getUsed();
                heapInfo.checkin();
            }
            catch(final InfoNotCompatibleException | RepositoryNotFoundException ex) {
                ers.Logger.warning(new CannotPublish("Failed to publish JVM heap memory info: " + ex.getMessage(), ex));
            }

            // Check interruption state before any collection and publication
            if(Thread.currentThread().isInterrupted() == true) {
                return;
            }

            try {
                final MemoryUsage nonHeap = memBean.getNonHeapMemoryUsage();
                final JVMMemoryUsageNamed non_heapInfo = new JVMMemoryUsageNamed(this.part, baseName + ".JVMMemory_NON_HEAP");
                non_heapInfo.committed = nonHeap.getCommitted();
                non_heapInfo.init = nonHeap.getInit();
                non_heapInfo.max = nonHeap.getMax();
                non_heapInfo.used = nonHeap.getUsed();
                non_heapInfo.checkin();
            }
            catch(final InfoNotCompatibleException | RepositoryNotFoundException ex) {
                ers.Logger.warning(new CannotPublish("Failed to publish JVM non-heap memory info: " + ex.getMessage(), ex));
            }

            // Check interruption state before any collection and publication
            if(Thread.currentThread().isInterrupted() == true) {
                return;
            }

            try {
                final ThreadMXBean threadBean = ManagementFactory.getThreadMXBean();
                final JVMThreadInfoNamed threadInfo = new JVMThreadInfoNamed(this.part, baseName + ".JVMThreads");
                threadInfo.daemonThreadCount = threadBean.getDaemonThreadCount();
                threadInfo.peakThreadCount = threadBean.getPeakThreadCount();
                threadInfo.threadCount = threadBean.getThreadCount();
                threadInfo.totalStartedThreadCount = threadBean.getTotalStartedThreadCount();
                threadInfo.checkin();
            }
            catch(final InfoNotCompatibleException | RepositoryNotFoundException ex) {
                ers.Logger.warning(new CannotPublish("Failed to publish JVM threads info: " + ex.getMessage(), ex));
            }
        }
    }

    {
        this.scheduler.setRemoveOnCancelPolicy(true);
    }

    /**
     * Constructor.
     * <p>
     * Use this constructor only if the environment variables TDAQ_APPLICATION_NAME and TDAQ_PARTITION are defined.
     * 
     * @param serverName The name of the IS server the information should be published to
     * @throws IllegalStateException Thrown if the environment variables TDAQ_APPLICATION_NAME or TDAQ_PARTITION are not defined or empty
     * @throws IllegalArgumentException Thrown if <em>serverName</em> is empty or <em>null</em>
     */
    public JVMMonitor(final String serverName) throws IllegalStateException, IllegalArgumentException {
        this.applicationName = System.getenv(JVMMonitor.TDAQ_APPLICATION_NAME);
        if((this.applicationName == null) || this.applicationName.isEmpty()) {
            throw new IllegalStateException("The environment variable " + JVMMonitor.TDAQ_APPLICATION_NAME + " is not set or empty");
        }

        final String partitionName = System.getenv(JVMMonitor.TDAQ_PARTITION);
        if((partitionName == null) || partitionName.isEmpty()) {
            throw new IllegalStateException("The environment variable " + JVMMonitor.TDAQ_PARTITION + " is not set or empty");
        }

        this.partition = new ipc.Partition(partitionName);

        if((serverName == null) || serverName.isEmpty()) {
            throw new IllegalArgumentException("The server name cannot be null or empty");
        }

        this.serverName = serverName;
    }

    /**
     * Constructor.
     * 
     * @param appName The name of the application being monitored. The application name is used to build the name of the published IS
     *            information
     * @param partitionName The name of the partition the IS server belongs to
     * @param serverName The name of the IS server the information should be published to
     * @throws IllegalArgumentException Thrown if any of the provided arguments is empty or <em>null</em>
     */
    public JVMMonitor(final String appName, final String partitionName, final String serverName) throws IllegalArgumentException {
        if((appName == null) || appName.isEmpty()) {
            throw new IllegalArgumentException("The application name cannot be null or empty");
        }

        if((partitionName == null) || partitionName.isEmpty()) {
            throw new IllegalArgumentException("The partition name cannot be null or empty");
        }

        if((serverName == null) || serverName.isEmpty()) {
            throw new IllegalArgumentException("The server name cannot be null or empty");
        }

        this.applicationName = appName;
        this.partition = new ipc.Partition(partitionName);
        this.serverName = serverName;
    }

    /**
     * It starts the periodic publishing of IS information.
     * 
     * @param delay The delay between two different IS publications
     * @param unit The time unit of the delay
     * @throws IllegalStateException Thrown if the IS publishing has already been started
     * @throws IllegalArgumentException Thrown if <em>delay</em> is less than zero
     */
    synchronized public void start(final long delay, final TimeUnit unit) throws IllegalStateException, IllegalArgumentException {
        if(this.scheduledTask != null) {
            throw new IllegalStateException("JVM monitor already started, stop it before");
        }

        this.scheduledTask = this.scheduler.scheduleWithFixedDelay(new MonitorTask(this.applicationName, this.partition, this.serverName),
                                                                   0,
                                                                   delay,
                                                                   unit);
    }

    /**
     * It stops the periodic publishing of the IS information.
     * <p>
     * If an information publishing is in progress, it cannot be guaranteed to be be stopped.
     */
    synchronized public void stop() {
        if(this.scheduledTask != null) {
            this.scheduledTask.cancel(true);
            this.scheduledTask = null;
        }
    }

    /**
     * It stops the periodic publishing of the IS information and removes all the information in the IS server.
     * @see #stop()
     */
    synchronized public void stopAndClean() {
        this.stop();
        
        // Remove information from IS
        final is.Repository isRepo = new is.Repository(this.partition);
        try {
            isRepo.removeAll(this.serverName, new is.Criteria(Pattern.compile(this.applicationName + ".*")));
        }
        catch(final RepositoryNotFoundException | InvalidCriteriaException ex) {
            ers.Logger.warning(ex);
        }
    }
    
    /**
     * It restarts the periodic publishing of the IS information with a new delay.
     * 
     * @param newDelay The delay between two different IS publications
     * @param unit The time unit of the delay
     * @throws IllegalArgumentException Thrown if <em>newDelay</em> is less than zero
     */
    synchronized public void restart(final long newDelay, final TimeUnit unit) throws IllegalArgumentException {
        if(this.scheduledTask != null) {
            this.scheduledTask.cancel(true);
        }

        this.scheduledTask = this.scheduler.scheduleWithFixedDelay(new MonitorTask(this.applicationName, this.partition, this.serverName),
                                                                   0,
                                                                   newDelay,
                                                                   unit);
    }
}
