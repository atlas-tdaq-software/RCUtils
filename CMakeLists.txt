tdaq_package()

set(my_linkopts Boost::program_options tdaq-common::ers)
set(my_config_linkopts daq-core-dal config)

tdaq_add_executable(rc_decode_detectormask       
  src/get_detectormask.cc
  LINK_LIBRARIES ${my_linkopts} tdaq-common::eformat)

tdaq_add_executable(rc_encode_detectormask  
  src/encode_detectormask.cc
  LINK_LIBRARIES ${my_linkopts} daq-df-dal tdaq-common::eformat)

tdaq_add_executable(rc_getrunnumber 
  src/get_runnumber.cc
  INCLUDE_DIRECTORIES RunControl
  LINK_LIBRARIES RCInfo_ISINFO is ${my_linkopts})

tdaq_add_executable(rc_isread    
  src/isread.cc
  INCLUDE_DIRECTORIES RunControl
  LINK_LIBRARIES RCInfo_ISINFO is ${my_linkopts} )

tdaq_add_executable(rc_waitstate 
  src/waitstate.cc
  INCLUDE_DIRECTORIES RunControl
  LINK_LIBRARIES RCInfo_ISINFO cmdline is ${my_linkopts})

tdaq_add_executable(rc_timetest  
  src/time_test.cc 
  src/TimeTest.cc
  LINK_LIBRARIES RCInfo_ISINFO cmdline rc_Commander rc_FSMStates rc_FSMCommands rc_RCCommands is ${my_linkopts} )

tdaq_add_executable(rc_print_root  
  src/print_root.cc
  INCLUDE_DIRECTORIES RunControl
  LINK_LIBRARIES ${my_config_linkopts} ${my_linkopts} ipc )

tdaq_add_executable(rc_print_tree  
  src/print_tree.cc
  INCLUDE_DIRECTORIES RunControl
  LINK_LIBRARIES ${my_config_linkopts} ${my_linkopts} ipc )

tdaq_add_executable(rc_checkapps  
  src/checkapps.cc
  INCLUDE_DIRECTORIES RunControl
  LINK_LIBRARIES ${my_config_linkopts} ${my_linkopts} ipc )

tdaq_add_executable(rc_print_partition_env  
  src/print_partition_env.cc
  INCLUDE_DIRECTORIES RunControl
  LINK_LIBRARIES ${my_config_linkopts} osw ${my_linkopts})

tdaq_add_executable(rc_up2initial  
  src/up2initial.cc
  INCLUDE_DIRECTORIES tmgr
  LINK_LIBRARIES RCInfo_ISINFO rc_Commander rc_FSMStates rc_FSMCommands rc_RCCommands is ${my_linkopts} )

tdaq_add_executable(rc_am_requester  
  src/am_requester.cc
  LINK_LIBRARIES AccessManager osw)

tdaq_add_executable(rc_error_generator 	
  src/error_generator.cc
  INCLUDE_DIRECTORIES RunControl
  LINK_LIBRARIES DFExceptions ${my_linkopts} is ipc)

tdaq_add_executable(rc_ready4recovery 	
  src/ready4recovery.cc
  INCLUDE_DIRECTORIES RunControl
  LINK_LIBRARIES ${my_linkopts} is ipc)

tdaq_add_executable(rc_pixel_ready_reactor  
  src/pixel_ready_reactor.cc
  INCLUDE_DIRECTORIES RunControl
  LINK_LIBRARIES ddcInfo_ISINFO RCInfo_ISINFO tdaq-common::eformat ${my_linkopts} is ipc)

tdaq_add_executable(rc_clocksetting  
  src/clocksetting.cc
  LINK_LIBRARIES rc_Commander trgCommander is ${my_config_linkopts} ${my_linkopts})

tdaq_add_executable(rc_sound_player  
  src/ers_sound_player.cc
  INCLUDE_DIRECTORIES RunControl
  LINK_LIBRARIES osw ipc ${my_linkopts})

tdaq_add_executable(rc_ctp_emulator  
  src/ctp_emulator.cc
  LINK_LIBRARIES ${my_linkopts} RCInfo_ISINFO TTCInfo_ISINFO daq-df-dal rc_ItemCtrl trgCommander)

set(LcgLibs  COOL::CoolApplication COOL::RelationalCool COOL::CoolKernel CORAL::RelationalAccess CORAL::CoralBase CORAL::CoralKernel nsl crypt dl ROOT)

tdaq_add_executable(rc_is2cool_archive  
  src/is2cool_archive.cc
  INCLUDE_DIRECTORIES RunControl  
  LINK_LIBRARIES RCInfo_ISINFO TTCInfo_ISINFO ${LcgLibs} is ${my_config_linkopts} pmgsync ${my_linkopts})

tdaq_add_executable(rc_dq_flag_writer  
  src/dq_flag_writer.cc
  INCLUDE_DIRECTORIES RunControl
  LINK_LIBRARIES RCInfo_ISINFO ${LcgLibs} is pmgsync ${my_linkopts} ROOT)

set(RootLibs ROOT::Gui ROOT::Gpad ROOT::Hist ROOT::Core ROOT::Matrix ROOT::RIO ROOT::Graf ROOT::Thread ROOT::MathCore)

tdaq_add_executable(run_eff_new
  src/make_run_eff.cc
  INCLUDE_DIRECTORIES RunControl
  LINK_LIBRARIES ${LcgLibs} is ${my_config_linkopts} pmgsync ${my_linkopts})

tdaq_add_executable(eff_daq_new 
  src/eff_daq_new.cc
  INCLUDE_DIRECTORIES RunControl
  LINK_LIBRARIES ${LcgLibs} is ${my_config_linkopts} pmgsync ${my_linkopts})

tdaq_add_executable(daq_eff_page 
  src/eff_daq.cc
  INCLUDE_DIRECTORIES RunControl
  LINK_LIBRARIES ${LcgLibs} ${RootLibs} is ${my_config_linkopts} pmgsync ${my_linkopts})

tdaq_add_library(wmireplugin   
  src/WMIPlugin/wmireplugin.cxx
  LINK_LIBRARIES Boost::date_time COOL::CoolApplication CORAL::CoralBase nsl crypt dl tdaq-common::ers is ipc wmi config daq-core-dal ROOT)

tdaq_add_jar(RCUtils  
  jsrc/RCUtils/tools/*.java
  INCLUDE_JARS TDAQExtJars/external.jar RCInfo/RCInfo.jar is/is.jar ipc/ipc.jar Jers/ers.jar)
                                
tdaq_add_jar(ISUtils
   jsrc/ISUtils/isutils/*.java
   jsrc/ISUtils/isutils/cmd/*.java
   jsrc/ISUtils/isutils/ist/*.java
   jsrc/ISUtils/isutils/ist/generator/*.java
   jsrc/ISUtils/isutils/clo/*.java
   jsrc/ISUtils/isutils/conf/*.java
   INCLUDE_JARS rdb/rdb.jar is/is.jar ipc/ipc.jar config/config.jar dal/dal.jar TDAQExtJars/external.jar Jers/ers.jar)

tdaq_add_scripts(bin/setup_daq bin/setup_functions bin/old2newRC.sh bin/play* bin/infogen bin/infogen_typefinder)
 
tdaq_add_scripts(scripts/*.py)

tdaq_add_jar_to_repo(RCUtils.jar
    DESCRIPTION "jar with common utilities for the Run Control system")

