#!/bin/sh

perl -pi.bak -e 's:setup/setup.data.xml:daq/segments/setup.data.xml:g' *.data.xml
perl -pi.bak -e 's:<attr name="ControlledByOnline" type="bool">0</attr>::g' *.data.xml
perl -pi.bak -e 's:<attr name="ControlledByOnline" type="bool">1</attr>::g' *.data.xml
perl -pi.bak -e 's/rc_empty_controller/run_controller/g' *.data.xml
