# RCUtils

## tdaq-10-00-00

-   If the `setup_daq` script detects the `tbed` environment, then it does not attempt to start the `initial` partition and the `PMG` servers.

## tdaq-09-03-00

-   The `initial` partition is now started as all the other partitions using `rc_bootstrap` ([ADTCC-259](https://its.cern.ch/jira/browse/ADTCC-259));
-   *setup_functions*: better error reporting when `OKS-GIT` is used;
-   Some java-based tools updated to proper handle new exceptions from the `config` layer;
-   Removed the deprecated usage of `tbb:atomic`.


## tdaq-09-02-01

-   Adapting to the new `OKS GIT` back-end ([ADTCC-227](https://its.cern.ch/jira/browse/ADTCC-227)):    
    The proper `OKS` version to use is set by the `setup_daq` script;   
    The `RunParams` IS server is started early by `setup_daq` in order to allow storing information about the `OKS` version to use;   
-   Fixed several issues in `setup_daq` building the RootController environment in the case of complex environment variables;
-   Several fixes to the DAQ efficiency tools in the area of the `Hold Trigger` accounting.

## tdaq-09-00-00

-   *rc_error_generator*: added the generation of `ERS` issues related to the stop-less removal/recovery for `SWROD`;
-   Added utility class to monitor several metrics of the Java Virtual Machine: the new class (`JVMMonitor`) is defined in the `RCUtils.tools` package. The `RCUtils.jar` jar should be added to the classpath;
-   Several improvements to the DAQ efficiency tools.
